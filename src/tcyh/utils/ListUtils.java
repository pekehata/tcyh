/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.utils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import jfx.messagebox.MessageBox;
import tcyh.BedOccupation;
import static tcyh.Constants.fdormFixedString;
import static tcyh.Constants.fdormString;
import static tcyh.Constants.mdormFixedString;
import static tcyh.Constants.mdormString;
import static tcyh.Constants.spareString;
import tcyh.Tcyh;

/**
 *
 * @author masanori
 */
public final class ListUtils {
    public static List<String> remove(List<String> list, String rsvNum) {
        //Orderの項目が大きなものから削除されるよう、降順にsortする。
        list.sort(BedOccupation.comparatorDesc());
        boolean removed = false;
        for(String orgStr : list){
            USVMap um = new USVMap(orgStr);
            //渡されたrsvNumがorgStrと同じ形式(USV)の場合と、予約番号のみを含む場合
            if((orgStr.equals(rsvNum) || um.getRsvNum().equals(rsvNum))
                    && list.remove(orgStr)){
                list.add("");
                removed = true;
                break;
            }
        }
        if( ! removed)
            System.out.println("remove(): No data removed, data would be corrupted... / rsvNum = " + rsvNum);
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    public static List<String> removeAll(List<String> list, String rsvNum) {
        list = list.stream()
                .map(str -> {
                    if(new USVMap(str).getRsvNum().equals(rsvNum)){
                        return "";
                    }
                    return str;
                })
                .sorted(BedOccupation.comparatorAsc())
                .collect(Collectors.toList());
        return list;
    }
    public static List<String> replace(List<String> list, String oldNum, String newNum) {
        for(String str : list) {
            if (str.equals("")) {
                continue;
            }
            USVMap um = new USVMap(str);
            if (um.getRsvNum().equals(oldNum)) {
                list.remove(str);
                um.putRsvNum(newNum);
                list.add(um.toString());
                break;
            }
        }
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    public static List<String> replaceAll(List<String> list, String oldNum, String newNum) {
        list = (List<String>) list.stream().map(str -> {
            if (str.equals("")) {
                return str;
            }
            USVMap um = new USVMap(str);
            if (um.getRsvNum().equals(oldNum)) {
                um.putRsvNum(newNum);
            }
            return um.toString();
        }).collect(Collectors.toList());
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    private static String rsvNum;
    private static Integer counter = 1;
    public static BedOccupation replaceUM(BedOccupation bo, USVMap um) {
        List<String> list = bo.getList(um.getRoomType());
        list = replaceUM(list, um);
        bo.setList(um.getRoomType(), list);
        return bo;
    }
    public static List<String> replaceUM(List<String> list, USVMap um) {
        return list.stream()
                .map(str -> str.equals(um.getOrgStr()) ? um.toString() : str )
                .collect(Collectors.toList());
    }
    public static List<String> addBlank(List<String> list, Integer blankNum) {
        while(blankNum > 0){
            list.add("");
            blankNum--;
        }
        return list;
    }
    public static List<String> removeBlank(List<String> list, Integer blankNum) {
        while(blankNum > 0) {
            Boolean isBlankRemoved = list.remove("");
            if(isBlankRemoved) {
            } else if( ! isBlankRemoved && list.contains(spareString)) {
                MessageBox.show(Tcyh.stage, "ドミの予備を一つ削除して、処理を続行します。", "警告", MessageBox.OK);
                list.remove(spareString);
            } else {
                MessageBox.show(Tcyh.stage, "", "", MessageBox.OK);
            }
            blankNum--;
        }
        return list;
    }
    public static List<String> replaceFirstBlank(List<String> list, String str) {
        USVMap um = new USVMap(str);
        for(String orgStr : list) {
            if(Objects.isNull(orgStr) || orgStr.isEmpty()) continue;
            USVMap refUM = new USVMap(orgStr);
            if(refUM.getRsvNum().equals(um.getRsvNum()))
                um.putOrder(Integer.toString(Integer.decode(refUM.getOrder()) + 1));
        }
        if(list.isEmpty()) list.add(um.toString());
        else list.set(list.indexOf(""), um.toString());
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    public static List<String> replaceSingleBlank(List<String> list, USVMap um) {
        if (isFirstEmpty(list)) {
            um.putOrder("1");
            list = replaceFirstBlank(list, um.toString());
        }
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    public static List<String> replaceMultipleBlank(List<String> list, USVMap um, Integer required) {
        //すでに予約がある場合、orderを1以外から始めなければならない可能性があるので、
        //対象の予約番号の中で最大のorderを見つける。
        //List内に同じ予約番号の既存予約がなければ1を返す。
        Integer maxOrder = list.stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(um.getRsvNum()))
                .map(str -> Integer.decode(new USVMap(str).getOrder()))
                .max((a,b) -> a.compareTo(b))
                .orElse(0) + 1;
        while (required > 0) {
            um.putOrder(maxOrder.toString());
            list = replaceFirstBlank(list, um.toString());
            required--;
            maxOrder++;
        }
        list.sort(BedOccupation.comparatorAsc());
        return list;
    }
    public static Integer getBlankNum(List<String> list) {
        return Double.valueOf(list.stream().filter(str -> Objects.isNull(str) || str.isEmpty()).count()).intValue();
    }
    public static Boolean isFirstEmpty(List<String> list) {
        if(list.isEmpty()) return true;
        return Objects.isNull(list.get(0)) || list.get(0).isEmpty() || list.get(0).equals("");
    }
    public static Boolean isDormRoom(List<String> list) {
        if(isFirstEmpty(list)) return false;
        switch (list.get(0)) {
            case mdormString:
            case mdormFixedString:
            case fdormString:
            case fdormFixedString:
                return true;
            default: return false;
        }
    }
    public static Boolean contains(List<String> list, String rsvNum) {
        for(String orgStr : list)
            if(new USVMap(orgStr).getRsvNum().equals(rsvNum))
                return Boolean.TRUE;
        return Boolean.FALSE;
    }
}