/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.utils;

import java.util.Objects;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

/**
 *
 * @author masanori
 */
public class ReplaceFullWithHalf implements ChangeListener<Boolean> {
    Node node;
    public ReplaceFullWithHalf(Node node) {
        this.node = node;
    }
    @Override
    public void changed(ObservableValue observable, Boolean oldValue, Boolean newValue) {
        if(node instanceof TextField) {
            TextField tf = (TextField)node;
            if(tf.getText().length() == 0) {
                return;
            }
            if( ! newValue) {
                tf.setText(replaceFullWithHalf(tf.getText()));
                if( ! Objects.isNull(tf.getParent()) &&
                        tf.getParent() instanceof ComboBox<?> &&
                        tf.getText().length() != 0) {
                    ComboBox<?> cb = (ComboBox<?>)tf.getParent();
                    cb.getEditor().setText(Integer.decode(tf.getText()).toString());
                } else if (tf.getText().length() == 0) {
                    tf.setText("0");
                }
            }
            tf.commitValue();
        } else if (node instanceof Spinner) {
            Spinner<Integer> spinner = (Spinner) node;
            spinner.getEditor().setText(
                    replaceFullWithHalf(spinner.getEditor().getText()));
            if( ! Objects.isNull(spinner.getEditor().getText()) &&
                    ! spinner.getEditor().getText().isEmpty())
                spinner.getValueFactory().valueProperty().setValue(Integer.decode(spinner.getEditor().getText()));
            else spinner.getValueFactory().valueProperty().setValue(0);
        }
    }
    private String replaceFullWithHalf(String includeFull) {
        return includeFull
                .replaceAll("１", "1")
                .replaceAll("２", "2")
                .replaceAll("３", "3")
                .replaceAll("４", "4")
                .replaceAll("５", "5")
                .replaceAll("６", "6")
                .replaceAll("７", "7")
                .replaceAll("８", "8")
                .replaceAll("９", "9")
                .replaceAll("０", "0")
                .replaceAll("\\D", "");
    }
}
