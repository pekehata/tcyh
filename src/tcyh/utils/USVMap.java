/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author masanori
 */
public class USVMap extends HashMap<String, String>{
    public USVMap() {
        //toStringした時に、USVとして出力しないValue
        this.putOrgStr("");
        this.putRsvSrc("");
        this.putRsvName("");
        this.putDays("0/0");

        //toStringした時に、USVとして出力するValue
        this.putRsvNum("");
        this.putOrder("1");
        this.putExBed("0");
        this.putMark("");
        this.putBrkDwn("");
        this.putAssign("");
        this.putRoomType("");
        this.putRoomChange("");
        this.putBreakFast("");
        this.putAgeNationality("");
        this.putMembership("");
        this.putKeyStatus("");
    }
    public USVMap(String str) {
        this();
        this.putOrgStr(str);
        List<String> list = Arrays.asList(str.split("_",-1));
        if(list.isEmpty())return;
        if(list.size()==1){
            this.put("rsvNum",list.get(0));
            return;
        }
        //rsvNumに値を代入
        this.putRsvNum(list.get(0));
        this.putOrder(list.get(1).equals("") ? "1" : list.get(1));
        this.putExBed(list.get(2).equals("") ? "0" : list.get(2));
        this.putMark(list.get(3));
        this.putBrkDwn(list.get(4));
        this.putAssign(list.get(5));
        this.putRoomType(list.size() <= 7 ? "" : list.get(6));
        this.putRoomChange(list.size() <= 8 ? "" : list.get(7));
        this.putBreakFast(list.size() <= 9 ? "" : list.get(8));
        this.putKeyStatus(list.size() <= 10 ? "" : list.get(9));
        this.putMembership(list.size() <= 11 ? "" : list.get(10));
        this.putAgeNationality(list.size() <= 12 ? "" : list.get(11));
    }
    public USVMap(Integer rsvNum) {
        this(rsvNum.toString());
    }

    @Override
    public String toString() {
        if(this.getRsvNum().equals(""))
            return "";
        StringBuilder sb = new StringBuilder();
        sb.append(Objects.isNull(this.getRsvNum()) ? "" : this.getRsvNum());
        sb.append("_");
        sb.append(Objects.isNull(this.getOrder()) ? "" : this.getOrder());
        sb.append("_");
        sb.append(Objects.isNull(this.getExBed()) ? "" : this.getExBed());
        sb.append("_");
        sb.append(Objects.isNull(this.getMark()) ? "" : this.getMark());
        sb.append("_");
        sb.append(Objects.isNull(this.getBrkDwn()) ? "" : this.getBrkDwn());
        sb.append("_");
        sb.append(Objects.isNull(this.getAssign()) ? "" : this.getAssign());
        sb.append("_");
        sb.append(Objects.isNull(this.getRoomType()) ? "" : this.getRoomType());
        sb.append("_");
        sb.append(Objects.isNull(this.getRoomChange()) ? "" : this.getRoomChange());
        sb.append("_");
        sb.append(Objects.isNull(this.getBreakFast()) ? "" : this.getBreakFast());
        sb.append("_");
        sb.append(Objects.isNull(this.getKeyStatus()) ? "" : this.getKeyStatus());
        sb.append("_");
        sb.append(Objects.isNull(this.getMembership()) ? "" : this.getMembership());
        sb.append("_");
        sb.append(Objects.isNull(this.getAgeNationality()) ? "" : this.getAgeNationality());
        return sb.toString();
    }
    public String getOrgStr() {
        return this.get("orgStr");
    }
    public final String putOrgStr(String value) {
        return this.put("orgStr", value);
    }
    public String getRsvSrc() {
        return this.get("rsvSrc");
    }
    public final String putRsvSrc(String value) {
        return this.put("rsvSrc", value);
    }
    public String getRsvName() {
        return this.get("rsvName");
    }
    public final String putRsvName(String value) {
        return this.put("rsvName", value);
    }
    public String getDays() {
        return this.get("days");
    }
    public final String putDays(String value) {
        return this.put("days", value);
    }
    public String getRsvNum() {
        return this.get("rsvNum");
    }
    public final String putRsvNum(String value) {
        return this.put("rsvNum", value);
    }
    public String getOrder() {
        return this.get("order");
    }
    public final String putOrder(String value) {
        return this.put("order", value);
    }
    public String getExBed() {
        return this.get("exBed");
    }
    public final String putExBed(String value) {
        return this.put("exBed", value);
    }
    public String getBrkDwn() {
        return this.get("brkDwn");
    }
    public String getBrkDwnDecoded() {
        return this.get("brkDwn")
                .replaceAll("%5F", "_")
                .replaceAll("%2C", ",");
    }
    public final String putBrkDwn(String value) {
        this.put("brkDwn",value
                        .replaceAll("_", "%5F")
                        .replaceAll(",", "%2C"));
        return this.getBrkDwn();
    }
    public String getMark() {
        return this.get("mark");
    }
    public final String putMark(String value) {
        return this.put("mark", value);
    }
    public String getAssign() {
        return this.get("assign");
    }
    public final String putAssign(String value) {
        return this.put("assign", value);
    }
    public String getRoomType() {
        return this.get("roomType");
    }
    public final String putRoomType(String value) {
        return this.put("roomType", value);
    }
    public String getRoomChange() {
        return this.get("roomChange");
    }
    public final String putRoomChange(String value) {
        return this.put("roomChange", value);
    }
    public String getBreakFast() {
        return this.get("breakFast");
    }
    public final String putBreakFast(String value) {
        return this.put("breakFast", value);
    }
    public String getAgeNationality() {
        return this.get("ageNationality");
    }
    public final String putAgeNationality(String value) {
        return this.put("ageNationality", value);
    }
    public String getMembership() {
        return this.get("membership");
    }
    public final String putMembership(String value) {
        return this.put("membership", value);
    }
    public String getKeyStatus() {
        return this.get("keyStatus");
    }
    public final String putKeyStatus(String value) {
        return this.put("keyStatus", value);
    }
}
