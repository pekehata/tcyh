/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.utils;

import com.sun.javafx.scene.control.skin.DatePickerContent;
import com.sun.javafx.scene.control.skin.DatePickerSkin;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author masanori
 */
public class CustomDatePickerSkin extends DatePickerSkin {
    public CustomDatePickerSkin(final DatePicker datePicker){
        super(datePicker);
    }
    @Override public Node getPopupContent() {
        DatePickerContent dpc = (DatePickerContent)super.getPopupContent();
        BorderPane bp = (BorderPane)dpc.getChildren().get(0);
        HBox hb = (HBox)bp.getChildren().get(1);
        hb.getChildren().removeIf(node -> node instanceof Button);
        return (Node)dpc;
    }
}