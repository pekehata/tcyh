/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import jfx.messagebox.MessageBox;
import tcyh.customcontrols.ColoredTableRow;
import tcyh.customcontrols.FormattedDateTableCell;
import tcyh.customcontrols.FormattedTimestampTableCell;

/**
 * FXML Controller class
 *
 * @author masanori
 */
public class ChangeHistory implements Initializable {
    private static ChangeHistory instance;
    static ChangeHistory getInstance() {
        return instance;
    }
    
    @FXML private TableView<Reservations> rsvBySpecificRevisionFX;
    @FXML private TableColumn<Reservations, Date> dateOfStayCol;
    @FXML private TableColumn<Reservations, Date> reservationDateCol;
    @FXML private TableColumn<Reservations, Timestamp> updateTimestampCol;
    
    @FXML private TextField rsvNumTextFieldFX;
    @FXML private Spinner<Integer> toggleRevSpinnerFX;
    @FXML private Label maxRevNumLabelFX;
    private final IntegerSpinnerValueFactory svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,1);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        rsvBySpecificRevisionFX.setRowFactory(tv -> new ColoredTableRow());
        dateOfStayCol.setCellFactory(column -> new FormattedDateTableCell());
        reservationDateCol.setCellFactory(column -> new FormattedDateTableCell());
        updateTimestampCol.setCellFactory(column -> new FormattedTimestampTableCell());
        toggleRevSpinnerFX.setValueFactory(svf);
        toggleRevSpinnerFX.getEditor().textProperty().addListener(action -> {
            toggleByRevision();
        });
    }
    
    private List<Reservations> rsvList;
    @FXML private void showReservationsByEnter(KeyEvent ke) {
        if(ke.getCode() == KeyCode.ENTER)
            showReservations();
        ke.consume();
    }
    @FXML private void showReservations(){
        if(Objects.isNull(rsvNumTextFieldFX.getText()) || rsvNumTextFieldFX.getText().isEmpty()) {
            MessageBox.show(Tcyh.stage, "予約番号が入力されていません。", "警告", MessageBox.OK);
            return;
        } else if(rsvNumTextFieldFX.getText().replaceAll("\\d", "").length() > 0) {
            MessageBox.show(Tcyh.stage, "予約番号に「半角数字」以外が入力されています。", "警告", MessageBox.OK);
            return;
        }
        showReservations(Integer.decode(rsvNumTextFieldFX.getText()));
    }
    protected void showReservations(Integer rsvNum) {
        rsvNumTextFieldFX.setText(rsvNum.toString());
        rsvList = ReservationsModel.fetchReservationsByRsvNum(rsvNum);
        if(rsvList.isEmpty()) {
            MessageBox.show(Tcyh.stage, "該当の予約番号は存在しません。", "警告", MessageBox.OK);
            return;
        }
        
        Short maxRev = rsvList.stream()
                .map(rsv -> rsv.getRevisionnumber())
                .max((Short rev1, Short rev2) -> rev1.compareTo(rev2))
                .get();
        rsvBySpecificRevisionFX.setItems(FXCollections.observableArrayList(rsvList.stream()
                        .filter(rsv -> rsv.getRevisionnumber().equals(maxRev))
                        .sorted((a,b) -> a.getSequencenumber().compareTo(b.getSequencenumber()))
                        .collect(Collectors.toList())
        ));
        svf.setMax(maxRev.intValue());
        svf.setValue(maxRev.intValue());
        toggleRevSpinnerFX.setValueFactory(svf);
        maxRevNumLabelFX.setText(maxRev.toString());
    }
    private void toggleByRevision() {
        Short revision = toggleRevSpinnerFX.getValue().shortValue();
        rsvBySpecificRevisionFX.setItems(FXCollections.observableArrayList(
                rsvList.stream()
                        .filter(rsv -> rsv.getRevisionnumber().equals(revision))
                        .sorted((a,b) -> a.getSequencenumber().compareTo(b.getSequencenumber()))
                        .collect(Collectors.toList())
        ));
        toggleRevSpinnerFX.setValueFactory(svf);
    }
}
