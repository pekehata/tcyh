/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import jfx.messagebox.MessageBox;
import static tcyh.Constants.*;
import static tcyh.utils.ListUtils.*;
import tcyh.utils.USVMap;

/**
 * FXML Controller class
 *
 * @author masanori
 */
public class RoomingController implements Initializable {
    @FXML private GridPane roomingGridPaneFX;
            
    private List<Node> defaultChildren;
    private LoadRoomingService loadRoomingService;

    private static RoomingController instance;
    public static RoomingController getInstance() {
        return instance;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        defaultChildren = new ArrayList(roomingGridPaneFX.getChildren());
        loadRoomingService = new LoadRoomingService();
        
        initClickedBorderPanePopUp();
    }
    
    private final Popup pup = new Popup();
    private ChoiceBox<String> roomChangeCB;
    private ChoiceBox<String> keyStatusCB;
    private final Spinner<Integer> adultSpinner = new Spinner(0, 100, 0);
    private final Spinner<Integer> youthSpinner = new Spinner(0, 100, 0);
    private final Spinner<Integer> familySpinner = new Spinner(0, 100, 0);
    private final Spinner<Integer> lifeSpinner = new Spinner(0, 100, 0);
    private void initClickedBorderPanePopUp() {
        Label roomChangeLabel = new Label("翌日部屋替え");
        //clickedBorderPane()内でChoiceBoxのindexで値を取得しているので、
        //List内の順番を変える際はそちらも変更が必要
        roomChangeCB = new ChoiceBox(FXCollections.observableArrayList(
                "不要","必要(未伝)","必要(伝済)","部屋替え先Keep","部屋替え完了"));
        Button roomChangeButton = new Button("変更");
        roomChangeButton.setOnMouseClicked(me -> {
            me.consume();
            if(Objects.isNull(roomChangeCB.getValue())) {
                MessageBox.show(Tcyh.stage, "部屋替えのステータスが正しく入力されていません。", "警告", MessageBox.OK);
                return;
            }
            if(Objects.isNull(pup.getUserData())) {
                return;
            }
            USVMap um = new USVMap((String)pup.getUserData());
            if(Objects.isNull(um.getRoomType()) || um.getRoomType().isEmpty())
                return;
            switch(roomChangeCB.getValue()) {
                case "不要" :
                    um.putRoomChange("");
                    break;
                case "必要(未伝)" :
                    //NUI = Required but Not Informed
                    um.putRoomChange("RNI");
                    break;
                case "必要(伝済)" :
                    //NI = Required and Informed
                    um.putRoomChange("RI");
                    break;
                case "部屋替え先Keep" :
                    //RCD = Room Change Destination
                    um.putRoomChange("RCD");
                    break;
                case "部屋替え完了" :
                    //RCC = Room Change Completed
                    um.putRoomChange("RCC");
                    break;
            }
            BedOccupation bo = BedOccupationModel.getBOToday();
            if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
                pup.hide();
                loadRooming();
            }
        });
        HBox roomChangeHB = new HBox(roomChangeLabel, roomChangeCB, roomChangeButton);
        roomChangeHB.setAlignment(Pos.CENTER_LEFT);
        roomChangeHB.setSpacing(5);

        Label releaseRoomAssignLabel = new Label("部屋割り解除");
        Button releaseRoomAssignButton = new Button("解除");
        releaseRoomAssignButton.setOnMouseClicked(me -> {
            me.consume();
            if(Objects.isNull(pup.getUserData())) {
                return;
            }
            USVMap um = new USVMap((String)pup.getUserData());
            if(um.getAssign().isEmpty())
                return;
            if(MessageBox.show(Tcyh.stage,
                    "この部屋(ベッド)を部屋割りから削除しますか？", "確認",
                    MessageBox.YES | MessageBox.NO) == MessageBox.YES) {
                um.putAssign("");
                BedOccupation bo = BedOccupationModel.getBOToday();
                if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
                    pup.hide();
                    loadRooming();
                }
            }
        });
        HBox releaseRoomAssignHB = new HBox(releaseRoomAssignLabel, releaseRoomAssignButton);
        releaseRoomAssignHB.setAlignment(Pos.CENTER_LEFT);
        releaseRoomAssignHB.setSpacing(5);
        
        Label keyStatusLabel = new Label("鍵の返却状況");
        //clickedBorderPane()内でChoiceBoxのindexで値を取得しているので、
        //List内の順番を変える際はそちらも変更が必要
        keyStatusCB = new ChoiceBox(FXCollections.observableArrayList(
                "チェックアウト済み", "鍵返却済み(連泊)", "鍵未返却"));
        Button keyStatusButton = new Button("変更");
        keyStatusButton.setOnMouseClicked(me -> {
            me.consume();
            if(Objects.isNull(keyStatusCB.getValue())) {
                MessageBox.show(Tcyh.stage, "鍵返却のステータス異常です。", "警告", MessageBox.OK);
                return;
            }
            if(Objects.isNull(pup.getUserData())) {
                return;
            }
            USVMap um = new USVMap((String)pup.getUserData());
            switch(keyStatusCB.getValue()) {
                case "チェックアウト済み" :
                    //CO = Checked-Out
                    um.putKeyStatus("CO");
                    break;
                case "鍵返却済み(連泊)" :
                    //ST = Stay Tomorrow
                    um.putKeyStatus("ST");
                    break;
                case "鍵未返却" :
                    um.putKeyStatus("");
                    break;
            }
            BedOccupation bo = BedOccupationModel.getBOToday();
            if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
                pup.hide();
                loadRooming();
            }
        });
        HBox keyStatusHB = new HBox(keyStatusLabel, keyStatusCB, keyStatusButton);
        keyStatusHB.setAlignment(Pos.CENTER_LEFT);
        keyStatusHB.setSpacing(5);
        
        Label membershipLabel = new Label("会員情報　　");
        membershipLabel.setPadding(new Insets(5,0,0,0));
        Label adultLabel = new Label("成人 x ");
        Label youthLabel = new Label("青年 x ");
        Label familyLabel = new Label("家族 x ");
        Label lifeLabel = new Label("終身 x ");
        adultSpinner.setMaxWidth(60);
        youthSpinner.setMaxWidth(60);
        familySpinner.setMaxWidth(60);
        lifeSpinner.setMaxWidth(60);
        GridPane membershipGP = new GridPane();
        membershipGP.setPadding(new Insets(5,5,5,5));
        membershipGP.setStyle("-fx-border-color: red");
        membershipGP.add(adultLabel, 0, 0);
        membershipGP.add(adultSpinner, 1, 0);
        membershipGP.add(youthLabel, 0, 1);
        membershipGP.add(youthSpinner, 1, 1);
        membershipGP.add(familyLabel, 0, 2);
        membershipGP.add(familySpinner, 1, 2);
        membershipGP.add(lifeLabel, 0, 3);
        membershipGP.add(lifeSpinner, 1, 3);
        membershipGP.setHgap(5);
        membershipGP.setVgap(5);
        Button membershipButton = new Button("変更");
        membershipButton.setOnMouseClicked(me -> {
            me.consume();
            if(Objects.isNull(pup.getUserData())) {
                return;
            }
            USVMap um = new USVMap((String)pup.getUserData());
            um.putMembership("");
            if( ! Objects.isNull(adultSpinner.getValue()) && ! adultSpinner.getValue().equals(0))
                um.putMembership(um.getMembership()
                        + (um.getMembership().isEmpty() ? "" : "/")
                        + "A" + adultSpinner.getValue().toString());
            if( ! Objects.isNull(youthSpinner.getValue()) && ! youthSpinner.getValue().equals(0))
                um.putMembership(um.getMembership()
                        + (um.getMembership().isEmpty() ? "" : "/")
                        + "Y" + youthSpinner.getValue().toString());
            if( ! Objects.isNull(familySpinner.getValue()) && ! familySpinner.getValue().equals(0))
                um.putMembership(um.getMembership()
                        + (um.getMembership().isEmpty() ? "" : "/")
                        + "F" + familySpinner.getValue().toString());
            if( ! Objects.isNull(lifeSpinner.getValue()) && ! lifeSpinner.getValue().equals(0))
                um.putMembership(um.getMembership()
                        + (um.getMembership().isEmpty() ? "" : "/")
                        + "L" + lifeSpinner.getValue().toString());
            BedOccupation bo = BedOccupationModel.getBOToday();
            if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
                pup.hide();
                loadRooming();
            } else
                MessageBox.show(Tcyh.stage, "変更に失敗しました。", "警告", MessageBox.OK);
        });
        HBox membershipHB = new HBox(membershipLabel, membershipGP, membershipButton);
        membershipHB.setAlignment(Pos.TOP_LEFT);
        membershipHB.setSpacing(5);
        
        VBox vb = new VBox(roomChangeHB, releaseRoomAssignHB, keyStatusHB, membershipHB);
        vb.setSpacing(5);
        vb.setPadding(new Insets(10,10,10,10));
        vb.setStyle("-fx-background-color: darkorange");
        pup.setAutoHide(true);
        pup.getContent().add(vb);
    }
    
    private final HashMap<Label, String> labelToReset = new HashMap();
    private void resetLabel() {
        labelToReset.forEach((label, str) -> {
            label.setText(str);
            label.setStyle("");
        });
    }
    List<Node> nodeRemoveList = new ArrayList();
    private void resetNodes() {
        nodeRemoveList.clear();
        roomingGridPaneFX.getChildren().forEach((Node node) -> {
            if(node instanceof Pane) {
                if(node.getId().equals("line"))
                    ((Pane) node).getChildren().clear();
                node.setStyle("");
                node.getStyleClass().removeAll(
                        "roomChangeRequiredNotInformed",
                        "roomChangeRequiredInformed",
                        "roomChangeDestination",
                        "roomChangeCompleted");
            }
            
            if(node instanceof Label)
                node.setUserData(null);
            
            if(node instanceof BorderPane) {
                BorderPane bp = (BorderPane)node;
                bp.getStyleClass().removeAll(
                        "roomChangeRequiredNotInformed",
                        "roomChangeRequiredInformed",
                        "roomChangeDestination",
                        "roomChangeCompleted");
                HBox topHB = (HBox)bp.getTop();
                if( ! Objects.isNull(topHB)) {
                    topHB.getChildren().forEach(subNode -> {
                        if(Objects.isNull(subNode.getId()))
                            nodeRemoveList.add(subNode);
                    });
                    topHB.getChildren().forEach(subNode -> {
                        if(Objects.isNull(subNode.getId())) {
                            nodeRemoveList.add(subNode);
                            return;
                        }
                        if(subNode.getId().equals("breakfastBox")) {
                            Pane pane = (Pane)subNode;
                            pane.getChildren().clear();
                        }
                    });
                    nodeRemoveList.forEach(removedNode -> {
                        topHB.getChildren().remove(removedNode);
                    });
                }
                HBox centerHB = (HBox)bp.getCenter();
                if( ! Objects.isNull(centerHB)) {
                    centerHB.getChildren().forEach(subNode -> {
                        if(Objects.isNull(subNode.getId()))
                            nodeRemoveList.add(subNode);
                    });
                    centerHB.getChildren().forEach(subNode -> {
                        if(Objects.isNull(subNode.getId())) {
                            nodeRemoveList.add(subNode);
                            return;
                        }
                        if(subNode.getId().equals("breakfastBox")) {
                            Pane pane = (Pane)subNode;
                            pane.getChildren().clear();
                        }
                    });
                    nodeRemoveList.forEach(removedNode -> {
                        centerHB.getChildren().remove(removedNode);
                    });
                }
                HBox bottomHB = (HBox)bp.getBottom();
                if( ! Objects.isNull(bottomHB)) {
                    bottomHB.getChildren().forEach(subNode -> {
                        if(Objects.isNull(subNode.getId())) {
                            nodeRemoveList.add(subNode);
                            return;
                        }
                        if(subNode.getId().equals("breakfastBox")) {
                            Pane pane = (Pane)subNode;
                            pane.getChildren().clear();
                        }
                    });
                    nodeRemoveList.forEach(removedNode -> {
                        bottomHB.getChildren().remove(removedNode);
                    });
                }
            }
        });
    }
    
    public void loadRooming() {
        loadRooming(BedOccupationModel.getBOToday());
    }
    public void loadRooming(BedOccupation bo) {
        loadRoomingService.createTask(bo);
        loadRoomingService.restart();
    }
    private void drawQuadAndBarrierFree(BedOccupation bo) {
        List<String> quadAndBarrierFree = new ArrayList();
        quadAndBarrierFree.addAll(bo.getQuadroomList());
        quadAndBarrierFree.addAll(bo.getB1801List());
        quadAndBarrierFree.addAll(bo.getB1811List());
        for(String orgStr : quadAndBarrierFree) {
            if(Objects.isNull(orgStr) || orgStr.isEmpty())
                continue;
            USVMap um = new USVMap(orgStr);
            String roomNum = null;
            int length = 0;
            if(Objects.isNull(um.getRoomType())
                    || um.getRoomType().isEmpty()
                    || Objects.isNull(um.getAssign())
                    || um.getAssign().isEmpty())
                continue;
            if(um.getRoomType().equals("quad")) {
                roomNum = um.getAssign().replaceFirst("-\\w", "");
                length = 4;
            } else if (um.getRoomType().matches("1801" + "|" + "1811")) {
                roomNum = um.getRoomType();
                length = 2;
            }
            BorderPane bedA = (BorderPane)roomingGridPaneFX.lookup("#" + roomNum + "-A");
            bedA.setUserData(orgStr);
            if(um.getRsvNum().equals(spareString))
                drawSpare(1,length,bedA);
            else if(um.getRsvNum().matches(
                    mdormString + "|" + mdormFixedString + "|" +
                    fdormString + "|" + fdormFixedString))
                setupRoomLabel(um.getRsvNum(), roomNum);
            else if( ! um.getRsvNum().isEmpty()) {
                setupRoomLabel(null, roomNum);
                Reservations rsv = findRsv(um);
                Pane background = (Pane)roomingGridPaneFX.lookup("#" + roomNum + "Background");
                drawCheckedIn(rsv, background);
                drawGuestName(rsv, bedA);
                drawDaysAndMembership(rsv, bedA, um);
                drawRoomChange(background, um);
                drawExBed(um);
                drawKeyStatus(1,4,background,um);
                //朝食ステータスの描画
                if( ! Objects.isNull(um.getBreakFast()) || ! um.getBreakFast().isEmpty()) {
                    List<String> keyNums = um.getRoomType().equals("quad") ?
                            Arrays.asList("-A", "-B", "-C", "-D", "-E")
                            : Arrays.asList("-A", "-B", "-C");
                    int i = 0;
                    for(String breakFast : um.getBreakFast().split("/")) {
                        if((um.getRoomType().equals("quad") && i > 5)
                                || (um.getRoomType().matches("1801|1811") && i > 3)) {
                            System.out.println(roomNum + "の朝食の数が定員より多く設定されています。");
                            break;
                        }
                        BorderPane bp = (BorderPane)roomingGridPaneFX.lookup(
                                "#" + roomNum + keyNums.get(i++));
                        drawBreakfast((Pane)bp.lookup("#breakfastBox"), breakFast);
                    }
                }
            }
        }
    }
    private void drawTatami(BedOccupation bo) {
        List<String> tatami = new ArrayList();
        tatami.addAll(bo.getT1807List());
        tatami.addAll(bo.getT1819List());
        for(String orgStr : tatami) {
            if(Objects.isNull(orgStr) || orgStr.isEmpty())
                continue;
            USVMap um = new USVMap(orgStr);
            if(Objects.isNull(um.getRoomType()) || um.getRoomType().isEmpty())
                continue;
            String roomNum = um.getRoomType();
            BorderPane bedA = (BorderPane)roomingGridPaneFX.lookup("#" + roomNum + "-A");
            bedA.setUserData(orgStr);
            if(um.getRsvNum().equals(spareString))
                drawSpare(1,4,bedA);
            else if( ! um.getRsvNum().isEmpty()) {
                setupRoomLabel(null, roomNum);
                Reservations rsv = findRsv(um);
                Pane background = (Pane)roomingGridPaneFX.lookup("#" + roomNum + "Background");
                drawCheckedIn(rsv, background);
                drawGuestName(rsv, bedA);
                drawDaysAndMembership(rsv, bedA, um);
                drawRoomChange(bedA, um);
                drawKeyStatus(1,4,background,um);
                //朝食ステータスの描画
                Set<Node> bfBoxList;
                BorderPane bp = (BorderPane)roomingGridPaneFX.lookup(
                        "#" + roomNum + "-E");
                bfBoxList = bp.lookupAll("#breakfastBox");
                int i = 0;
                for(String breakFast : um.getBreakFast().split("/")) {
                    if(i > 5) {
                        System.out.println(roomNum + "の朝食の数が、定員より多く設定されています。");
                        break;
                    }
                    drawBreakfast((Pane)bfBoxList.toArray()[i++], breakFast);
                    
                }
            }
        }
    }
    private void drawLarge(BedOccupation bo) {
        for(String roomNum : Arrays.asList("1808", "1820", "1908", "1917")) {
            List<String> large = bo.getList(roomNum);
            if(isFirstEmpty(large))
                continue;
            USVMap um = new USVMap(large.get(0));
            String orgStr = um.getOrgStr();
            if(Objects.isNull(orgStr) || orgStr.isEmpty())
                continue;
            BorderPane bedA = (BorderPane)roomingGridPaneFX.lookup("#" + roomNum + "-A");
            bedA.setUserData(orgStr);
            if(um.getRsvNum().equals(spareString)) {
                int height = roomNum.equals("1808") || roomNum.equals("1820") ? 5 : 4;
                drawSpare(2,height,bedA);
            } else if(um.getRsvNum().matches(
                    mdormString + "|" + mdormFixedString + "|" +
                    fdormString + "|" + fdormFixedString))
                setupRoomLabel(um.getRsvNum(), roomNum);
            else if( ! um.getRsvNum().isEmpty()) {
                Reservations rsv = findRsv(um);
                Pane background = (Pane)roomingGridPaneFX.lookup("#" + roomNum + "Background");
                int height = roomNum.equals("1808") || roomNum.equals("1820") ? 5 : 4;
                drawCheckedIn(rsv, background);
                drawGuestName(rsv, bedA);
                drawDaysAndMembership(rsv, bedA, um);
                drawKeyStatus(2,height,background,um);
                drawRoomChange(background, um);
                setupRoomLabel(null, roomNum);
            }
        }
    }
    private void drawDorm(BedOccupation bo) {
        List<String> dorm = new ArrayList();
        dorm.addAll(bo.getMDormList());
        dorm.addAll(bo.getFDormList());
        for(String orgStr : dorm) {
            if(Objects.isNull(orgStr) || orgStr.isEmpty())
                continue;
            USVMap um = new USVMap(orgStr);
            if(Objects.isNull(um.getAssign()) || um.getAssign().isEmpty())
                continue;
            String assign = um.getAssign();
            BorderPane bed = (BorderPane)roomingGridPaneFX.lookup("#" + assign);
            bed.setUserData(orgStr);
            if(um.getRsvNum().equals(spareString))
                drawSpare(1,1,bed);
            else if( ! um.getRsvNum().isEmpty()) {
                Reservations rsv = findRsv(um);                
                drawCheckedIn(rsv, bed);
                drawGuestName(rsv, bed);
                drawDaysAndMembership(rsv, bed, um);
                drawRoomChange(bed, um);
                drawKeyStatus(1,1,bed,um);
                //朝食ステータスの描画
                if( ! Objects.isNull(um.getBreakFast()) || ! um.getBreakFast().isEmpty())
                    drawBreakfast((Pane)bed.lookup("#breakfastBox"), um.getBreakFast());
            }
        }
    }

    private void drawCheckedIn(Reservations rsv, Node node) {
        if(rsv.getResultnum() > 0)
            node.setStyle("-fx-background-color: MistyRose");
        else
            node.setStyle("-fx-background-color: white");
    }
    private void drawGuestName(Reservations rsv, BorderPane bp) {
        if( ! Objects.isNull(rsv.getGuestname())) {
            Label guestName = new Label(rsv.getGuestname().replaceFirst(" |　", "\n"));
            guestName.setPadding(new Insets(1,0,0,2));
            guestName.setLineSpacing(-4);
            ((HBox)bp.getTop()).getChildren().add(guestName);
        }
    }
    private final String styleForMarks =
                "-fx-font: 8pt \"Meiryo \";" +
                "-fx-border-width: 1;" +
                "-fx-border-radius: 3;" +
                "-fx-border-color: ";
    private void drawDaysAndMembership(Reservations rsv, BorderPane bp, USVMap um) {
        Label days = new Label(
                rsv.getNightspassed().toString() + "/" + rsv.getTotalnights().toString());
        if( ! Objects.isNull(um.getMembership()) && ! um.getMembership().isEmpty()) {
            VBox vb = new VBox();
            HBox marksHB = new HBox();
            Label mark;
            for(String membership : um.getMembership().split("/")) {
                switch(membership.replaceAll("\\d", "")) {
                    case "A" :
                        mark = new Label("成");
                        mark.setStyle(styleForMarks + "springgreen");
                        break;
                    case "Y" :
                        mark = new Label("青");
                        mark.setStyle(styleForMarks + "blue");
                        break;
                    case "F" :
                        mark = new Label("家");
                        mark.setStyle(styleForMarks + "red");
                        break;
                    case "L" :
                        mark = new Label("終");
                        mark.setStyle(styleForMarks + "springgreen");
                        break;
                    default :
                        mark = new Label();
                        break;
                }
                marksHB.getChildren().addAll(
                mark, new Label("" + membership.replaceAll("\\D", "")));
            }
            vb.getChildren().addAll(marksHB, days);
            ((HBox)bp.getBottom()).getChildren().add(vb);
        } else
            ((HBox)bp.getBottom()).getChildren().add(days);
    }
    private void drawBreakfast(Pane bfBox, String breakfast) {
        Circle circle;
        Line line1;
        Line line2;
        switch(breakfast) {
            case "PAID" :
                circle = new Circle(12.5,12.5,10,Color.WHITE);
                circle.setStrokeWidth(2);
                circle.setStroke(Color.BLACK);
                bfBox.getChildren().addAll(circle);
                break;
            case "HANDED" :
                line1 = new Line(0,0,25,25);
                line2 = new Line(25,0,0,25);
                line1.setStrokeWidth(2);
                line2.setStrokeWidth(2);
                bfBox.getChildren().addAll(line1, line2);
                break;
            case "PAIDANDHANDED" : 
                circle = new Circle(12.5,12.5,10,Color.WHITE);
                circle.setStrokeWidth(2);
                circle.setStroke(Color.BLACK);
                line1 = new Line(0,0,25,25);
                line2 = new Line(25,0,0,25);
                line1.setStrokeWidth(2);
                line2.setStrokeWidth(2);
                bfBox.getChildren().addAll(circle, line1, line2);
                break;
        }
    }
    private void drawSpare(int width, int height, BorderPane bed) {
        Line line1 = new Line(0, 0, sideLength*width-3, sideLength*height-3);
        Line line2 = new Line(sideLength*width-3, 0, 0, sideLength*height-3);
        Pane lineBackGround = new Pane(line1, line2);
        lineBackGround.setId("line");
        roomingGridPaneFX.add(lineBackGround,
                GridPane.getColumnIndex(bed),
                GridPane.getRowIndex(bed));
    }
    private void drawExBed(USVMap um) {
        if( ! Objects.isNull(um.getExBed()) && ! um.getExBed().isEmpty()) {
            Integer e = Integer.parseInt(um.getExBed());
            BorderPane exBed = (BorderPane)roomingGridPaneFX
                    .lookup("#" + um.getAssign().replaceAll("-\\w", "")
                            + (um.getRoomType().equals("quad") ? "-E" : "-C"));
            for (int i = e; i > 0; i--) {
                double widthAdjuster = 25;
                exBed.setStyle("-fx-background-color: MediumSpringGreen");
                Line line = new Line(
                        sideLength * i / (e + 1), 0,
                        sideLength * i / (e + 1), sideLength / 3);
                Circle circle = new Circle(
                        sideLength * i / (e + 1), sideLength / 3, circleRadius);
                AnchorPane ap = new AnchorPane(line, circle);
                ap.setPadding(new Insets(0,0,0,-widthAdjuster * e));
                ap.setMaxSize(sideLength, sideLength*3/5);
                ((HBox)exBed.getTop()).getChildren().add(ap);
            }
            //eが0以下の場合
            for (int i = e; i < 0; i++) {
                BorderPane emptyBed = (BorderPane)roomingGridPaneFX.getChildren().get(
                        roomingGridPaneFX.getChildren().indexOf(exBed) + i);
                SVGPath star = new SVGPath();
                star.setContent("M 20,2 L 14.8,16 2,16 14,24 8,38 20,30 32,38 28,24 38,16 25.2,16 Z");
                star.setStyle("-fx-fill: lightgray");
                ((HBox)emptyBed.getCenter()).getChildren().add(star);
                ((HBox)emptyBed.getCenter()).setAlignment(Pos.CENTER);
            }
        }
    }
    private void drawRoomChange(Node node, USVMap um) {
        if(um.getRoomChange().matches("RNI"))
            node.getStyleClass().add("roomChangeRequiredNotInformed");
        else if(um.getRoomChange().matches("RI"))
            node.getStyleClass().add("roomChangeRequiredInformed");
        else if(um.getRoomChange().matches("RCD")) 
            node.getStyleClass().add("roomChangeDestination");
        else if(um.getRoomChange().matches("RCC")) 
            node.getStyleClass().add("roomChangeCompleted");
    }
    private void drawKeyStatus(int width, int height, Node node, USVMap um) {
        if(Objects.isNull(um.getKeyStatus()) || um.getKeyStatus().isEmpty())
            return;
        if(um.getKeyStatus().equals("CO")){
            Line line1 = new Line(0, 0, sideLength*width-3, sideLength*height-3);
            Line line2 = new Line(sideLength*width-3, 0, 0, sideLength*height-3);
            line1.setStyle("-fx-stroke: red; -fx-stroke-width: 2");
            line2.setStyle("-fx-stroke: red; -fx-stroke-width: 2");
            Pane lineBackGround = new Pane(line1, line2);
            lineBackGround.setId("line");
            roomingGridPaneFX.add(lineBackGround,
                    GridPane.getColumnIndex(node),
                    GridPane.getRowIndex(node));
            lineBackGround.setMouseTransparent(true);
        } else if(um.getKeyStatus().equals("ST")){
            Line line1 = new Line(sideLength*width-3, 0, 0, sideLength*height-3);
            line1.setStyle("-fx-stroke: blue; -fx-stroke-width: 2");
            Pane lineBackGround = new Pane(line1);
            lineBackGround.setId("line");
            roomingGridPaneFX.add(lineBackGround,
                    GridPane.getColumnIndex(node),
                    GridPane.getRowIndex(node));
            lineBackGround.setMouseTransparent(true);
        }
    }
    private void setupRoomLabel(String dormType, String roomNum) {
        Label roomLabel = (Label)roomingGridPaneFX.lookup("#" + roomNum + "Label");
        labelToReset.put(roomLabel, roomLabel.getText());
        roomLabel.setStyle("-fx-background-color: " + (
                Objects.isNull(dormType) ? "yellow" :
                dormType.equals(mdormString) ? "lightskyblue" :
                dormType.equals(mdormFixedString) ? "deepskyblue" :
                dormType.equals(fdormString) ? "lightpink" :
                dormType.equals(fdormFixedString) ? "hotpink" : ""));
        roomLabel.setText((
                Objects.isNull(dormType) ? "個室\n" :
                dormType.equals(mdormString) ? "M\n" :
                dormType.equals(mdormFixedString) ? "M(固定)\n" :
                dormType.equals(fdormString) ? "F\n" :
                dormType.equals(fdormFixedString) ? "F(固定)\n" : "")
                + roomNum);
        roomLabel.setUserData(dormType);
    }
    private Reservations findRsv(USVMap um) {
        for (Reservations rsv : ReservationsModel.getRsvListToday()) {
            if( ! rsv.getReservationstatus().equals(reservationStatusValidString))
                continue;
            if (String.valueOf(rsv.getReservationnumber()).equals(um.getRsvNum())) {
                return rsv;
            }
        }
        return new Reservations();
    }
    
    @FXML private void droppedOnLabel(MouseDragEvent me) {
        if( ! (me.getGestureSource() instanceof Label)) return;
        Label sourceLabel = (Label)me.getGestureSource();
        Label targetLabel = (Label)me.getSource();
        if(Objects.isNull(sourceLabel.getUserData())
                || ((String)sourceLabel.getUserData()).isEmpty())
            return;
        String orgStr = (String)sourceLabel.getUserData();
        USVMap um = new USVMap(orgStr);
        if(um.getRsvNum()
                .matches(mdormString + "|" + mdormFixedString+ "|" +
                        fdormString + "|" + fdormFixedString)
                && ( ! Objects.isNull(targetLabel.getId())
                || ! ((String)targetLabel.getId()).isEmpty())) {
            System.out.println(um);
            um.putAssign(targetLabel.getId().replaceAll("Label", ""));
            BedOccupation bo = BedOccupationModel.getBOToday();
            if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
                MainWindowController.getInstance().selectTab();
                sourceLabel.setUserData(orgStr);
            } else
                MessageBox.show(Tcyh.stage, "変更に失敗しました。", "警告", MessageBox.OK);
            System.out.println(bo.getList("quad"));
            System.out.println(um);
        }
    }
    @FXML private void droppedOnBorderPane(MouseDragEvent me) {
        me.consume();
        if( ! (me.getSource() instanceof BorderPane))
            return;
        BorderPane bp = (BorderPane)me.getSource();
        VBox vb = (VBox)me.getGestureSource();        
        if(Objects.isNull(vb.lookup("#orgStr"))) return;
        
        //ドミのアサインを行う
        //部屋割り台帳内での移動を可能にする(me.getSource() instanceof BorderPaneで分岐)
        //一つの予約の中で、一部の部屋だけ泊数が異なる場合にどう対応するか
        //4人部屋の場合は、drop先がAベッド以外でも-Aで保存するようにする？(しなくていいかも)
        String orgStr = ((Text)vb.lookup("#orgStr")).getText();
        USVMap um = new USVMap(orgStr);
        Label label;
        switch(um.getRoomType()) {
            case "mdorm" :
                label = (Label)roomingGridPaneFX.lookup("#" + bp.getId().replaceAll("-\\w", "Label"));
                if(Objects.isNull(label)
                        || Objects.isNull(label.getUserData())
                        || ! ((String)label.getUserData()).contains(mdormString)) {
                    MessageBox.show(Tcyh.stage, "割り振ろうとした部屋はMドミではありません。", "警告", MessageBox.OK);
                    return;
                }
                break;
            case "fdorm" :
                label = (Label)roomingGridPaneFX.lookup("#" + bp.getId().replaceAll("-\\w", "Label"));
                if(Objects.isNull(label)
                        || Objects.isNull(label.getUserData())
                        || ! ((String)label.getUserData()).contains(fdormString)) {
                    MessageBox.show(Tcyh.stage, "割り振ろうとした部屋はFドミではありません。", "警告", MessageBox.OK);
                    return;
                }
                break;
            case "quad" :
                label = (Label)roomingGridPaneFX.lookup("#" + bp.getId().replaceAll("-\\w", "Label"));
                if(Objects.isNull(label)
                        || label.getText().matches("(1801|1811|1807|1819|1808|1820|1908|1917|staff[lr]).*")) {
                    MessageBox.show(Tcyh.stage, "割り振ろうとした部屋は4人部屋ではありません。", "警告", MessageBox.OK);
                    return;
                }
                break;
            default :
                if(Objects.isNull(um.getRoomType()) || um.getRoomType().isEmpty()) {
                    MessageBox.show(Tcyh.stage, "部屋タイプが不明です。", "警告", MessageBox.OK);
                    return;
                }
                MessageBox.show(Tcyh.stage, "該当の部屋は、ドラッグ&ドロップで部屋割り出来ません。\n"
                        + "空室台帳上で部屋を入れ替えて下さい。", "警告", MessageBox.OK);
                return;
        }
        BedOccupation bo = BedOccupationModel.getBOToday();

        //予約が複数ある場合
        int count = 0;
        if(um.getRoomType().matches("mdorm|fdorm")
                && ! um.getRsvNum().matches(
                        mdormString + "|" + mdormFixedString + "|" +
                        fdormString + "|" + fdormFixedString + "|" +
                        spareString)) {
            System.out.println("called");
            Boolean assigned = Boolean.FALSE;
            for(String refOrgStr : bo.getList(um.getRoomType())){
                USVMap umRef = new USVMap(refOrgStr);
                if(umRef.getRsvNum().equals(um.getRsvNum())) {
                    count++;
                    if(Objects.isNull(umRef.getAssign())
                        || umRef.getAssign().isEmpty()) {
                        umRef.putAssign(bp.getId());
                        um = umRef;
                        assigned = Boolean.TRUE;
                        break;
                    }
                }
            }
            if( ! assigned && count == 1 ) {
                if(MessageBox.show(Tcyh.stage,
                        "該当の部屋は既に割り振り済みですが\n"
                        + "変更しますか？", "警告", MessageBox.YES|MessageBox.NO) == MessageBox.YES)
                    um.putAssign(bp.getId());
                else
                    return;
            } else if( ! assigned && count > 1 ) {
                MessageBox.show(Tcyh.stage,
                        "ドミに複数人数の予約がある場合は、自動で部屋の入れ替えができません。\n"
                        + "移動したい部屋を一旦削除してから、再度試して下さい。", "警告", MessageBox.OK);
                return;
            }
        } else {
            if( ! Objects.isNull(um.getAssign()) && ! um.getAssign().isEmpty()
                    && MessageBox.show(Tcyh.stage,
                            "該当の部屋は既に割り振り済みですが\n"
                            + "変更しますか？", "警告", MessageBox.YES|MessageBox.NO) != MessageBox.YES)
                return;
            else
                um.putAssign(bp.getId());
        }
        
        for(String otherOrgStr : bo.getList(um.getRoomType())) {
            if(otherOrgStr.equals(orgStr))
                continue;
            USVMap otherUM = new USVMap(otherOrgStr);
            if(Objects.isNull(otherUM.getAssign()) || otherUM.getAssign().isEmpty())
                continue;
            if(otherUM.getAssign().equals(um.getAssign())) {
                MessageBox.show(Tcyh.stage, "該当の部屋には、既に予約が割り振られています。", "警告", MessageBox.OK);
                return;
            }
        }
        
        if(BedOccupationModel.commitBO(replaceUM(bo, um))) {
            MainWindowController.getInstance().selectTab();
            ((Text)vb.lookup("#orgStr")).setText(um.toString());
        }
    }
    @FXML private void clickedBorderPane(MouseEvent me) {
        me.consume();
        if(pup.isShowing())
            pup.hide();
        BorderPane bp = (BorderPane)me.getSource();
        if(Objects.isNull(bp.getUserData()))
            return;
        String orgStr = (String)bp.getUserData();
        USVMap um = new USVMap(orgStr);
        if(Objects.isNull(um.getRoomType()))
            return;
        roomChangeCB.getSelectionModel().clearSelection();
        int roomChangeSelection;
        switch(um.getRoomChange()) {
            case "RNI":
                roomChangeSelection = 1;
                break;
            case "RI":
                roomChangeSelection = 2;
                break;
            case "RCD":
                roomChangeSelection = 3;
                break;
            case "RCC":
                roomChangeSelection = 4;
                break;
            default :
                roomChangeSelection = 0;
        }
        roomChangeCB.getSelectionModel().select(roomChangeSelection);
        keyStatusCB.getSelectionModel().clearSelection();
        switch(um.getRoomChange()) {
            case "CO":
                keyStatusCB.getSelectionModel().select(0);
                break;
            case "ST":
                keyStatusCB.getSelectionModel().select(1);
                break;
            default :
                keyStatusCB.getSelectionModel().select(2);
                break;
        }
        adultSpinner.getValueFactory().setValue(0);
        youthSpinner.getValueFactory().setValue(0);
        familySpinner.getValueFactory().setValue(0);
        lifeSpinner.getValueFactory().setValue(0);
        for(String membership : um.getMembership().split("/")) {
            switch(membership.replaceAll("\\d", "")) {
                case "A" :
                    adultSpinner.getValueFactory().setValue(
                            Integer.valueOf(membership.replaceAll("\\D", "")));
                    break;
                case "Y" :
                    youthSpinner.getValueFactory().setValue(
                            Integer.valueOf(membership.replaceAll("\\D", "")));
                    break;
                case "F" :
                    familySpinner.getValueFactory().setValue(
                            Integer.valueOf(membership.replaceAll("\\D", "")));
                    break;
                case "L" :
                    lifeSpinner.getValueFactory().setValue(
                            Integer.valueOf(membership.replaceAll("\\D", "")));
                    break;
                default :
                    break;
            }
        }
        pup.show(Tcyh.stage, me.getSceneX() + 10, me.getSceneY() + 5);
        pup.setUserData(orgStr);
    }
    
    private class LoadRoomingService extends Service {
        @Override
        protected Task createTask() {
            return createTask(BedOccupationModel.getBOToday());
        }
        protected Task createTask(BedOccupation bo) {
            return new Task<Void>(){
                @Override
                protected Void call() {
                    Platform.runLater(() -> {
                        roomingGridPaneFX.getChildren().clear();
                        roomingGridPaneFX.getChildren().addAll(defaultChildren);
                        resetLabel();
                        resetNodes();
                        drawQuadAndBarrierFree(bo);
                        drawLarge(bo);
                        drawTatami(bo);
                        drawDorm(bo);
                    });
                    return null;
                }
            };
        }    
    }
}
