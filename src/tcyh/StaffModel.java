/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 *
 * @author masanori
 */
public class StaffModel {
    private static final StaffModel instance = new StaffModel();
    protected static StaffModel getInstance() {
        return instance;
    }
    private StaffModel() {}
    protected static ObservableList<Staff> fetchStaff() {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Staff.findAll", String.class);
        long start = System.currentTimeMillis();
        List<Staff> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        ObservableList<Staff> data = FXCollections.observableArrayList(list);
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static void addNewStaff(Staff newStaff) {
        if(Objects.isNull(newStaff.getId())) {
            MessageBox.show(Tcyh.stage, "「No.」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }
        for(Staff staff : fetchStaff()) {
            if(staff.getId().equals(newStaff.getId())) {
                MessageBox.show(Tcyh.stage, "入力したNo.は既に使用されています。", "警告", MessageBox.OK);
                return;
            }
        }
        if(Objects.isNull(newStaff.getLastname()) || newStaff.getLastname().isEmpty()) {
            MessageBox.show(Tcyh.stage, "「姓」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }
        if(Objects.isNull(newStaff.getFirstname()) || newStaff.getFirstname().isEmpty()) {
            MessageBox.show(Tcyh.stage, "「名」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        try {
            em.persist(newStaff);
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            tx.rollback();
            em.clear();
            MessageBox.show(Tcyh.stage, "問題が発生しました。\n処理を中止します。", "警告", MessageBox.OK);
            return;
        }
        MessageBox.show(Tcyh.stage, "新しいスタッフを追加しました。", "完了", MessageBox.OK);
    }
    protected static void deleteStaff(Short staffId) {
        List<Staff> list = fetchStaff().stream()
                .filter(staff -> staff.getId().equals(staffId))
                .collect(Collectors.toList());
        if(Objects.isNull(list) || list.isEmpty()) {
            MessageBox.show(Tcyh.stage, "対応するNo.のスタッフが、データベースに存在しません。", "警告", MessageBox.OK);
            return;
        }
        if(MessageBox.show(Tcyh.stage, "選択したスタッフ情報を削除します。\nよろしいですか？",
                "確認", MessageBox.YES|MessageBox.NO) != MessageBox.YES)
            return;

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            em.persist(list.get(0));
            em.remove(list.get(0));
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(StaffModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            MessageBox.show(Tcyh.stage, "問題が発生しました。\n処理を中止します。", "警告", MessageBox.OK);
            return;
        }
        MessageBox.show(Tcyh.stage, "該当のスタッフ情報を削除しました。", "完了", MessageBox.OK);
    }
    protected static void modifyStaff(Staff modifiedStaff) {
        if(Objects.isNull(modifiedStaff.getId())) {
            MessageBox.show(Tcyh.stage, "No.が入力されていません。", "警告", MessageBox.OK);
            return;
        }
        if(Objects.isNull(modifiedStaff.getLastname()) || modifiedStaff.getLastname().isEmpty()) {
            MessageBox.show(Tcyh.stage, "「姓」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }
        if(Objects.isNull(modifiedStaff.getFirstname()) || modifiedStaff.getFirstname().isEmpty()) {
            MessageBox.show(Tcyh.stage, "「名」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        try {
            em.merge(modifiedStaff);
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            tx.rollback();
            em.clear();
            MessageBox.show(Tcyh.stage, "問題が発生しました。\n処理を中止します。", "警告", MessageBox.OK);
            return;
        }
        MessageBox.show(Tcyh.stage, "スタッフ情報を更新しました。", "完了", MessageBox.OK);
    }
}
