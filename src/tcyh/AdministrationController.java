/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import jfx.messagebox.MessageBox;
import static tcyh.Constants.*;
import tcyh.utils.DatePickerStringConverter;
/**
 *
 * @author masanori
 */
public class AdministrationController implements Initializable {
    @FXML private Accordion accordionFX;
    
    @FXML private DatePicker closedDayDatePickerFX;
    @FXML private RadioButton closedDayRadioFX;
    @FXML private RadioButton openDayRadioFX;
    @FXML private ListView<String> closedDayListFX;
    
    @FXML private TitledPane consistencyCheckTitledPaneFX;
    @FXML private DatePicker consistencyFromDateFX;
    @FXML private DatePicker consistencyToDateFX;
    @FXML private ListView<String> errorListFX;
    
    @FXML private Spinner<Integer> newSourceIdNoFX;
    @FXML private TextField newSourceNameFX;
    @FXML private TableView<ReservationSource> reservationSourceTableFX;

    @FXML private Spinner<Integer> newStaffIdNoFX;
    @FXML private TextField newStaffLastNameFX;
    @FXML private TextField newStaffFirstNameFX;
    @FXML private TextField newStaffPhoneNumberFX;
    @FXML private TextField newStaffEmailAddressFX;
    @FXML private TextField updateStaffIdNoFX;
    @FXML private TextField updateStaffLastNameFX;
    @FXML private TextField updateStaffFirstNameFX;
    @FXML private TextField updateStaffPhoneNumberFX;
    @FXML private TextField updateStaffEmailAddressFX;
    @FXML private TableView<Staff> staffTableFX;

    private static AdministrationController instance;
    static AdministrationController getInstance() {
        return instance;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
        
        //休館日の設定用
        closedDayDatePickerFX.setConverter(new DatePickerStringConverter());
        closedDayDatePickerFX.setValue(LocalDate.now());
        refreshClosedDayList();
        
        //予約の整合性チェック
        consistencyFromDateFX.setConverter(new DatePickerStringConverter());
        consistencyToDateFX.setConverter(new DatePickerStringConverter());
        
        //予約方法の管理
        refreshSourceTable();
        newSourceIdNoFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0));
        
        //スタッフ情報の管理
        refreshStaffTable();
        newStaffIdNoFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 200, 1));
        staffTableFX.getSelectionModel().selectedIndexProperty().addListener(ae -> {
            loadStaff();
        });
    }

    @FXML private void refreshClosedDayList() {
        closedDayListFX.getItems().setAll(BedOccupationModel.fetchAllClosedDayList());
    }
    @FXML private void setClosedOrOpen() {
        BedOccupation bo =
                BedOccupationModel.fetchBO(closedDayDatePickerFX.getValue());

        if( ! bo.getIsclosedday() && closedDayRadioFX.selectedProperty().getValue()) {
            Integer yesOrNo = MessageBox.show(Tcyh.stage,
                    closedDayDatePickerFX.getValue().format(DateTimeFormatter.ofPattern("YYYY年MM月dd日(E)")) +
                    "を休館日に設定します。\nよろしいですか？",
                    "確認", MessageBox.YES|MessageBox.NO);            

            if(yesOrNo.equals(MessageBox.YES)) {
                List<Reservations> list = ReservationsModel
                        .fetchReservationsByDate(closedDayDatePickerFX.getValue());
                Long rsvCount = list.stream().filter(rsv ->
                        rsv.getReservationstatus().equals(reservationStatusValidString))
                        .count();
                if(rsvCount > 0) {
                    MessageBox.show(Tcyh.stage,
                            "該当の日付には、既に予約が入っています。" +
                            "\n予約をキャンセルしてから再度、試して下さい。",
                            "警告", MessageBox.OK);
                    return;
                }
                bo.setIsclosedday(Boolean.TRUE);
                BedOccupationModel.commitBO(bo);
                refreshClosedDayList();
            }
        } else if( bo.getIsclosedday() && openDayRadioFX.selectedProperty().getValue()) {
            Integer yesOrNo = MessageBox.show(Tcyh.stage,
                    closedDayDatePickerFX.getValue().format(DateTimeFormatter.ofPattern("YYYY年MM月dd日(E)")) +
                    "を営業日に設定します。\nよろしいですか？", "確認", MessageBox.YES|MessageBox.NO);
            if(yesOrNo.equals(MessageBox.YES)) {
                bo.setIsclosedday(Boolean.FALSE);
                BedOccupationModel.commitBO(bo);
                refreshClosedDayList();
            }
        } else {
            MessageBox.show(Tcyh.stage,
                    closedDayDatePickerFX.getValue().format(DateTimeFormatter.ofPattern("YYYY年MM月dd日(E)")) +
                    "は元々" + (bo.getIsclosedday() ? "休館日" : "営業日") +
                    "に設定されています。",
                    "警告",MessageBox.OK);
        }
    }

    @FXML private void refreshSourceTable() {
        reservationSourceTableFX.getItems().setAll(ReservationSourceModel.fetchReservationSource());
    }
    @FXML private void addNewSource() {
        ReservationSourceModel.addNewSource(newSourceIdNoFX.getValue().shortValue(), newSourceNameFX.getText());
        refreshSourceTable();
    }
    @FXML private void deleteSource() {
        if(reservationSourceTableFX.getSelectionModel().isEmpty()) {
            MessageBox.show(Tcyh.stage, "何も選択されていません。", "警告", MessageBox.OK);
            return;
        }
        ReservationSourceModel.deleteSource(
                reservationSourceTableFX.getSelectionModel()
                        .getSelectedItem().getId());
        refreshSourceTable();
    }

    @FXML private void refreshStaffTable() {
        staffTableFX.getItems().setAll(StaffModel.fetchStaff());
    }
    @FXML private void addNewStaff() {
        Staff staff = new Staff();
        staff.setId(newStaffIdNoFX.getValue().shortValue());
        staff.setLastname(newStaffLastNameFX.getText());
        staff.setFirstname(newStaffFirstNameFX.getText());
        staff.setPhonenumber(newStaffPhoneNumberFX.getText());
        staff.setEmailaddress(newStaffEmailAddressFX.getText());
        StaffModel.addNewStaff(staff);
        refreshStaffTable();
    }
    @FXML private void deleteStaff() {
        if(staffTableFX.getSelectionModel().isEmpty()) {
            MessageBox.show(Tcyh.stage, "何も選択されていません。", "警告", MessageBox.OK);
            return;
        }
        StaffModel.deleteStaff(staffTableFX.getSelectionModel().getSelectedItem().getId());
        refreshStaffTable();
    }
    @FXML private void modifyStaff() {
        if(MessageBox.show(Tcyh.stage, "データを更新します。\nよろしいですか？", "確認",
                MessageBox.YES|MessageBox.NO) != MessageBox.YES)
            return;
        Staff modifiedStaff = new Staff();
        modifiedStaff.setId(Short.decode(updateStaffIdNoFX.getText()));
        modifiedStaff.setLastname(updateStaffLastNameFX.getText());
        modifiedStaff.setFirstname(updateStaffFirstNameFX.getText());
        modifiedStaff.setPhonenumber(updateStaffPhoneNumberFX.getText());
        modifiedStaff.setEmailaddress(updateStaffEmailAddressFX.getText());
        StaffModel.modifyStaff(modifiedStaff);
        refreshStaffTable();
    }
    private void loadStaff() {
        Staff staff = staffTableFX.getSelectionModel().getSelectedItem();
        if(Objects.isNull(staff))
            return;
        updateStaffIdNoFX.setText(staff.getId().toString());
        updateStaffLastNameFX.setText(staff.getLastname());
        updateStaffFirstNameFX.setText(staff.getFirstname());
        updateStaffPhoneNumberFX.setText(staff.getPhonenumber());
        updateStaffEmailAddressFX.setText(staff.getEmailaddress());
    }
    
    @FXML private void checkConsistencyButtonAction() {
        if(Objects.isNull(consistencyFromDateFX.getValue())) {
            MessageBox.show(Tcyh.stage, "開始日付が入力されていません。", "警告", MessageBox.OK);
            return;
        } else if(Objects.isNull(consistencyToDateFX.getValue())) {
            MessageBox.show(Tcyh.stage, "終了日付が入力されていません。", "警告", MessageBox.OK);
            return;
        }
        Date from = java.sql.Date.valueOf(consistencyFromDateFX.getValue());
        Date to = java.sql.Date.valueOf(consistencyToDateFX.getValue());
        errorListFX.getItems().clear();
        ObservableList<String> errors = ConsistencyCheck.examinAll(from, to);
        if( ! errors.isEmpty())
            errorListFX.setItems(errors);
        else
            MessageBox.show(Tcyh.stage, "エラーは検出されませんでした。", "情報", MessageBox.OK);
    }
    protected void showConsistencyCheckErrors(Date start, Date end, ObservableList<String> errors) {
        MainWindowController.getInstance().tabPaneFX.getSelectionModel().select(
                MainWindowController.getInstance().AdministrationTabFX);
        accordionFX.setExpandedPane(consistencyCheckTitledPaneFX);
        consistencyFromDateFX.setValue(MainWindowController.asLocalDate(start));
        consistencyToDateFX.setValue(MainWindowController.asLocalDate(end));
        errorListFX.setItems(errors);
    }
}