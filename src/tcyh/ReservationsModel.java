/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import com.opencsv.CSVParser;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import static tcyh.Constants.*;

/**
 *
 * @author masanori
 */
class ReservationsModel {
    private static final ReservationsModel instance = new ReservationsModel();
    private ReservationsModel() {
    }
    protected static ReservationsModel getInstance() {
        return instance;
    }

    private static ObservableList<Reservations> rsvListTodayStatic;
    protected static ObservableList<Reservations> getRsvListToday(){
        return ReservationsModel.rsvListTodayStatic;
    }
    protected static void setRsvListToday(LocalDate ld){
        ReservationsModel.rsvListTodayStatic = fetchReservationsByDate(ld);
    }
    private static ObservableList<Reservations> rsvListYesterdayStatic;
    protected static ObservableList<Reservations> getRsvListYesterday(){
        return ReservationsModel.rsvListYesterdayStatic;
    }
    protected static void setRsvListYesterday(LocalDate ld){
        ReservationsModel.rsvListYesterdayStatic = fetchReservationsByDate(ld.minusDays(1));
    }

    protected static ObservableList<Reservations> fetchReservationsByDate(LocalDate ld) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Reservations.findByDateofstay", Reservations.class);
        tquery.setParameter("dateofstay", java.sql.Date.valueOf(ld));
        long start = System.currentTimeMillis();
        List<Reservations> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static ObservableList<Reservations> fetchReservationsByRsvNum(Integer rsvNum) {
        long start = System.currentTimeMillis();
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Reservations.findByReservationnumber", Reservations.class);
        tquery.setParameter("reservationnumber", rsvNum);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        List<Reservations> list = (List<Reservations>) tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        return data;
    }
    protected static ObservableList<Reservations> fetchReservationsByGuestName(String guestName) {
        long start = System.currentTimeMillis();
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Reservations.findByGuestname", Reservations.class);
        tquery.setParameter("guestname", guestName);
        tquery.setParameter("from", new Date(0));
        tquery.setParameter("to", new Date(70000000000000L));
        tquery.setParameter("reservationstatus",
                Arrays.asList(reservationStatusValidString, reservationStatusCanceledString));
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        List<Reservations> list = (List<Reservations>) tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        return data;
    }
    protected static ObservableList<Reservations> fetchReservationsByMonth(LocalDate firstDateOfThisMonth, LocalDate lastDateOfThisMonth) {
        //前月最終日から当月最終日までのReservationsを取得する
        Date startDate = java.sql.Date.valueOf(firstDateOfThisMonth.minusDays(1));
        Date endDate = java.sql.Date.valueOf(lastDateOfThisMonth);
        
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        TypedQuery tquery = em.createNamedQuery("Reservations.findAllByMonth", Reservations.class);
        tquery.setParameter("start", startDate);
        tquery.setParameter("end", endDate);
        List<Reservations> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static ObservableList<Reservations> fetchReservationsBySpecificPeriod(Date startDate, Date endDate) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        TypedQuery tquery = em.createNamedQuery("Reservations.findBySpecificPeriod", Reservations.class);
        tquery.setParameter("start", startDate);
        tquery.setParameter("end", endDate);
        List<Reservations> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static ObservableList<Reservations> fetchNewestReservations(Integer rsvNum) {
        long start = System.currentTimeMillis();
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Reservations.findNewestByReservationnumber", Reservations.class);
        tquery.setParameter("reservationnumber", rsvNum);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        List<Reservations> list = (List<Reservations>) tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<Reservations> data = FXCollections.observableArrayList(list);
        return data;
    }
    protected static ReservationsPK fetchNewReservationsPK() {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("Reservations.findGreatestreservationnumber", Integer.class);
        long start = System.currentTimeMillis();
        Integer greatest = (Integer) tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getSingleResult();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        System.out.println("GreatestRNum : " + greatest);
        if(Objects.isNull(greatest))return new ReservationsPK(1, (short) (1), (short) (1));
        return new ReservationsPK(greatest + 1, (short) (1), (short) (1));
    }
    protected static Boolean commitRsv(Reservations rsv){
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        try {
            em.clear();
            tx.begin();
            em.merge(rsv);
            tx.commit();
            em.clear();
            long start = System.currentTimeMillis();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[2].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("コミットに失敗しました: " + rsv + "\n");
            tx.rollback();
            em.clear();
            return false;
        }
        return true;
    }
    protected static HashMap<String, Integer> calcRsvSummary(){
        List<Reservations> rsvListYesterday = getRsvListYesterday().stream()
                .filter(rsv -> rsv.getReservationstatus().equals(reservationStatusValidString))
                .collect(Collectors.toList());
        List<Reservations> rsvListToday = getRsvListToday().stream()
                .filter(rsv -> rsv.getReservationstatus().equals(reservationStatusValidString))
                .collect(Collectors.toList());

        int totalCheckIn = 0;
        int remainedCheckIn = 0;
        int totalCheckOut = 0;
        int numOfCheckOutPeople = 0;
        int totalMaleNum = 0;
        int totalFemaleNum = 0;
        int plannedBreakfastNum = 0;
        int plannedLunchNum = 0;
        int plannedDinnerNum = 0;
        int paidBreakfastNum = 0;
        int totalMaleResultNum = 0;
        int totalFemaleResultNum = 0;
        
        for(Reservations rsv : rsvListToday){
            totalCheckIn += rsv.getNightspassed()==1 ? 1 : 0;
            remainedCheckIn += rsv.getNightspassed()==1 && (Objects.isNull(rsv.getResultnum()) || rsv.getResultnum()==0) ? 1 : 0;
            totalMaleNum += rsv.getNumberofmale();
            totalFemaleNum += rsv.getNumberoffemale();
            plannedBreakfastNum += Objects.isNull(rsv.getBreakfast()) ? 0 : rsv.getBreakfast();
            plannedLunchNum += Objects.isNull(rsv.getBreakfast()) ? 0 : rsv.getLunch();
            plannedDinnerNum += Objects.isNull(rsv.getDinner()) ? 0 : rsv.getDinner();
            paidBreakfastNum += Objects.isNull(rsv.getPaidbreakfast()) ? 0 : rsv.getPaidbreakfast();
            totalMaleResultNum += Objects.isNull(rsv.getMaleresult()) ? 0 : rsv.getMaleresult();
            totalFemaleResultNum += Objects.isNull(rsv.getFemaleresult()) ? 0 : rsv.getFemaleresult();
        }
        for(Reservations rsv : rsvListYesterday){
            totalCheckOut += rsv.getNightspassed().equals(rsv.getTotalnights()) ? 1 : 0;
            numOfCheckOutPeople += rsv.getNightspassed().equals(rsv.getTotalnights()) ? rsv.getTotalnum() : 0;
        }
        
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        hm.put("totalCheckIn", totalCheckIn);
        hm.put("remainedCheckIn", remainedCheckIn);
        hm.put("totalCheckOut", totalCheckOut);
        hm.put("numOfCheckOutPeople", numOfCheckOutPeople);
        hm.put("totalMaleNum", totalMaleNum);
        hm.put("totalFemaleNum", totalFemaleNum);
        hm.put("plannedBreakfastNum", plannedBreakfastNum);
        hm.put("plannedLunchNum", plannedLunchNum);
        hm.put("plannedDinnerNum", plannedDinnerNum);
        hm.put("paidBreakfastNum", paidBreakfastNum);
        hm.put("totalMaleResultNum", totalMaleResultNum);
        hm.put("totalFemaleResultNum", totalFemaleResultNum);
        return hm;
    }
    protected static void parseTSVData(String[] strArray) {
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        ReservationsPK rpk = fetchNewReservationsPK();
        Reservations rsvRef = null;
        try {
            for(String str : strArray) {
                CSVParser parser = new CSVParser('\t');
                String[] array;
                try {
                    array = parser.parseLine(str);
                } catch (IOException ioe) {
                    MessageBox.show(Tcyh.stage, "IOExceptionをcatchしました。", "警告", MessageBox.OK);
                    return;
                }

                if(array.length < 2) {
                    System.out.println(">>空行をスキップします。");
                    continue;
                }
                int i = 0;
                Reservations rsv = new Reservations();
                try {
                    rsv.setReservationsPK(rpk.clone());
                } catch (Exception ex) {
                    Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
                }

                if(Objects.isNull(array[i]) || array[i].isEmpty()) {
                    System.out.println("Skipped : " + Arrays.toString(array));
                    continue;
                }
                rsv.setDateofstay(java.sql.Date.valueOf(array[i++].replaceAll("/", "-")));
                if(Objects.isNull(array[i]) || array[i].isEmpty()) {
                    System.out.println("Skipped : " + Arrays.toString(array));
                    continue;
                }
                rsv.setReservationdate(java.sql.Date.valueOf(array[i++].replaceAll("/", "-")));
                rsv.setReservationsource(array[i++]);
                rsv.setStaffname(array[i++]);
                if(Objects.isNull(array[i]) || array[i].isEmpty()) {
                    System.out.println("Skipped : " + Arrays.toString(array));
                    continue;
                }
                rsv.setGuestname(array[i++]);
                rsv.setComments(array[i++]);
                rsv.setCountry(array[i++]);
                rsv.setPhonenumber(array[i++]);
                rsv.setNumberofmale(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                rsv.setNumberoffemale(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                if(Objects.isNull(array[i]) || array[i].isEmpty() || array[i].equals("0"))
                    rsv.setReservationstatus(reservationStatusCanceledString);
                else
                    rsv.setReservationstatus(reservationStatusValidString);
                i++;
                rsv.setDinner(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                rsv.setBreakfast(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                rsv.setLunch(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                if(Objects.isNull(array[i]) || array[i].isEmpty()) {
                    rsv.setTotalnights((short)1);
                } else {
                    rsv.setTotalnights(Short.valueOf(array[i].replaceAll("－", "").replace("-", "")));
                }
                i++;
                if(Objects.isNull(array[i]) || array[i].isEmpty()) {
                    rsv.setNightspassed((short)1);
                    rsv.setSequencenumber((short)1);
                } else {
                    rsv.setNightspassed(Short.valueOf(array[i]));
                    rsv.setSequencenumber(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                }
                i++;
                rsv.setPlannedcheckintime(array[i++]);
                rsv.setMaleresult(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                rsv.setFemaleresult(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                i++;
                rsv.setPaidbreakfast(Objects.isNull(array[i]) || array[i].isEmpty() ? 0 : Short.valueOf(array[i]));
                i++;
                rsv.setCashier(array[i++]);
                rsv.setRoomnumresult(array[i++] + array[i++]);
                i++;
                rsv.setChangecomment(array[i++]);

                rsv.setMalebed((short)0);
                rsv.setFemalebed((short)0);
                rsv.setQuadroom((short)0);
                rsv.setBarrierfreeroom((short)0);
                rsv.setTatamiroom((short)0);
                rsv.setStaffroom((short)0);
                rsv.setLarge1808(Boolean.FALSE);
                rsv.setLarge1820(Boolean.FALSE);
                rsv.setLarge1908(Boolean.FALSE);
                rsv.setLarge1917(Boolean.FALSE);
                rsv.setNumberofbaby((short)0);
                rsv.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));

                rpk.setReservationnumber(rpk.getReservationnumber() + 1);
                //同じ予約者名の予約が前日にある場合、自動で同じ予約番号にする。
                ReservationsModel.fetchReservationsByGuestName(rsv.getGuestname())
                        .stream().forEach(item -> {
                    if(item.getGuestname().equals(rsv.getGuestname())
                            && MainWindowController.asLocalDate(item.getDateofstay())
                                    .plusDays(rsv.getNightspassed() - 1)
                                    .equals(MainWindowController.asLocalDate(rsv.getDateofstay()))
                            && item.getTotalnights().equals(rsv.getTotalnights())) {
                        rsv.setReservationnumber(
                                item.getReservationnumber());
                        rpk.setReservationnumber(rpk.getReservationnumber() - 1);
                    }
                });
                rsvRef = rsv;
                em.persist(rsv);
            }
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            System.out.println("コミットに失敗しました: " + rsvRef + "\n");
        }
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[2].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        MessageBox.show(Tcyh.stage, "データの入力が完了しました。", "情報", MessageBox.OK);
    }

    protected static HashMap<String, Integer> masterRnumCounter(List<Reservations> list) {
        HashMap<String, Integer> master = new HashMap<String, Integer>();
        master.put("mdorm", list.stream().mapToInt(rsv -> rsv.getMalebed()).sum());
        master.put("fdorm", list.stream().mapToInt(rsv -> rsv.getFemalebed()).sum());
        master.put("quadroom", list.stream().mapToInt(rsv -> rsv.getQuadroom()).sum());
        master.put("1820", Long.valueOf(list.stream().map(rsv -> rsv.getLarge1820()).count()).intValue());
        master.put("1917", Long.valueOf(list.stream().map(rsv -> rsv.getLarge1917()).count()).intValue());
        master.put("1808", Long.valueOf(list.stream().map(rsv -> rsv.getLarge1808()).count()).intValue());
        master.put("1908", Long.valueOf(list.stream().map(rsv -> rsv.getLarge1908()).count()).intValue());
        master.put("barrierfree", list.stream().mapToInt(rsv -> rsv.getBarrierfreeroom()).sum());
        master.put("tatami", list.stream().mapToInt(rsv -> rsv.getTatamiroom()).sum());
        master.put("staff", list.stream().mapToInt(rsv -> rsv.getStaffroom()).sum());
        return master;
    }

    protected static Boolean insertNewReservation(Reservations newReservation) {
        //DBにInsert
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        if( ! fetchNewReservationsPK().getReservationnumber()
                .equals(newReservation.getReservationnumber())) {
            MessageBox.show(Tcyh.stage, "データの更新中に、該当の予約番号"
                    + newReservation.getReservationnumber() + "で他の予約が作成されました。"
                    + "一旦処理を中止しますので、再度保存を試みて下さい。", "警告", MessageBox.OK);
            tx.rollback();
            em.clear();
            return Boolean.FALSE;
        }
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(newReservation.getDateofstay());

        for (int i = 1; i <= newReservation.getTotalnights(); i++) {
            try {
                if(i > 1){
                    newReservation = newReservation.cloneEntity(em);
                    cal.add(Calendar.DATE, 1);
                    newReservation.setDateofstay(cal.getTime());
                    newReservation.setNightspassed(Integer.valueOf(newReservation.getNightspassed()+1).shortValue());
                    newReservation.setSequencenumber(
                            Integer.valueOf(newReservation.getSequencenumber()+1).shortValue());
                }
                if( ! BedOccupationModel.isModifiable(newReservation,null)) {
                    tx.rollback();
                    em.clear();
                    return Boolean.FALSE;
                }
                System.out.println("Reservations:\n" + newReservation);
                em.merge(newReservation.cloneEntity(em));
                em.merge(BedOccupationModel.addRsv(newReservation));
                em.flush();
                em.detach(newReservation);
            } catch (Exception ex) {
                Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
                tx.rollback();
                em.clear();
            }
        }
        try {
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            return Boolean.FALSE;
        }
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return Boolean.TRUE;
    }
    protected static Boolean commitChanges(ObservableList<Reservations> newList, String comments) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        em.clear();
        long start = System.currentTimeMillis();
        List<Reservations> oldList = fetchNewestReservations(newList.get(0).getReservationnumber());
        if( ! newList.get(0).getRevisionnumber().equals(oldList.get(0).getRevisionnumber())) {
            MessageBox.show(Tcyh.stage,
                    "該当の予約は、データ取得後に誰かに変更されたようです。\n" +
                    "予約をロードし直してから再度お試し下さい。",
                    "変更を中止", MessageBox.OK);
            return Boolean.FALSE;
        }
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            for(int i = 0;i<newList.size();i++){
                    Reservations rsvNew = newList.get(i);
                    Reservations rsvOld = oldList.get(i);
                    if( ! BedOccupationModel.isModifiable(rsvNew,rsvOld)) {
                        tx.rollback();
                        em.clear();
                        return false;
                    }
                    String statusBefore = rsvOld.getReservationstatus();
                    rsvOld.setReservationstatus(reservationStatusChangedString);
                    rsvOld.setChangecomment(rsvNew.getChangecomment() + comments);
                    em.merge(rsvOld);
                    em.flush();
                    em.detach(rsvOld);
                    rsvNew.setRevisionnumber((short) (rsvNew.getRevisionnumber() + 1));
                    rsvNew.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
                    rsvNew.setChangecomment(rsvNew.getChangecomment() + comments);
                    rsvNew.setReservationstatus(statusBefore);
                    em.persist(rsvNew);
                    em.flush();
                    em.detach(rsvNew);
                    if(statusBefore.equals(reservationStatusValidString))
                        em.merge(BedOccupationModel.changeBedRoomCount(rsvOld, rsvNew));
            }
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            return false;
        }
        tx.commit();
        em.clear();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return true;
    }
    protected static Boolean addDays(ReservationsPK pk, String comment, final int daysBefore, final int daysAfter) {
        List<Reservations> oldList = fetchNewestReservations(pk.getReservationnumber());
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        if( ! pk.getRevisionnumber().equals(oldList.get(0).getRevisionnumber())) {
            MessageBox.show(Tcyh.stage,
                    "該当の予約は、データ取得後に誰かに変更されたようです。\n" +
                    "予約をロードし直してから再度お試し下さい。",
                    "変更を中止", MessageBox.OK);
            return Boolean.FALSE;
        }
        List<Reservations> newList = new ArrayList<>();
        long start = System.currentTimeMillis();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        for(Reservations oldRsv : oldList ) {
            try {
                newList.add(oldRsv.cloneEntity(em));
                oldRsv.setReservationstatus(reservationStatusChangedString);
                em.merge(oldRsv);
                em.flush();
                em.detach(oldRsv);
            } catch (Exception e) {
                Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, e);
                tx.rollback();
                em.clear();
                return Boolean.FALSE;
            }
        }

        int newRevNum = oldList.get(0).getRevisionnumber() + 1;
        Reservations rsvNew;
        for (int i = 1; i <= daysBefore; i++) {
            rsvNew = newList.get(0).cloneEntity(em);
            rsvNew.setDateofstay(java.sql.Date.from(
                    rsvNew.getDateofstay().toInstant().plus(Duration.ofDays(-1))));
            newList.add(0, rsvNew);
            if( ! BedOccupationModel.isModifiable(rsvNew,null)) {
                tx.rollback();
                em.clear();
                return Boolean.FALSE;
            }
        }

        for (int i = 1; i <= daysAfter; i++) {
            rsvNew = newList.get(newList.size() - 1).cloneEntity(em);
            rsvNew.setDateofstay(java.sql.Date.from(
                    rsvNew.getDateofstay().toInstant().plus(Duration.ofDays(1))));
            newList.add(rsvNew);
            if( ! BedOccupationModel.isModifiable(rsvNew,null)) {
                tx.rollback();
                em.clear();
                return Boolean.FALSE;
            }
        }

        Reservations rsv;
        Short newTotalNights = (short) (newList.get(0).getTotalnights() + daysBefore + daysAfter);
        for (int j = 1; j <= newList.size(); j++) {
            rsv = newList.get(j - 1);
            rsv.setTotalnights(newTotalNights);
            rsv.setNightspassed((short) j);
            rsv.setSequencenumber((short) j);
            rsv.setRevisionnumber((short) newRevNum);
            rsv.setChangecomment(rsv.getChangecomment() + comment);
            rsv.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
            if( ! rsv.getReservationstatus().equals(reservationStatusNoshowString))
                rsv.setReservationstatus(reservationStatusValidString);
            try {
                em.persist(rsv);
                em.flush();
                em.detach(rsv);
            } catch(Exception e) {
                Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, e);
                MessageBox.show(Tcyh.stage,
                        "何らかの理由で追加した予約が保存できませんでした。\n処理を中止します。",
                        "警告",
                        MessageBox.OK);
                tx.rollback();
                em.clear();
                return Boolean.FALSE;
            }
        }

        for (int k = daysBefore; k != 0; k--)
            em.merge(BedOccupationModel.addRsv(newList.get(k - 1)));
        for (int k = daysAfter; k != 0; k--)
            em.merge(BedOccupationModel.addRsv(newList.get(newList.size() - k)));
        
        try {
            tx.commit();
            em.clear();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
            return Boolean.TRUE;
        } catch(Exception e) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, e);
            tx.rollback();
            em.clear();
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, e);
            MessageBox.show(Tcyh.stage,
                    "何らかの理由で追加した予約が保存できませんでした。\n処理を中止します。",
                    "警告",
                    MessageBox.OK);
            tx.rollback();
            em.clear();
            return Boolean.FALSE;
        }
    }
    protected static Boolean cancelSomeDates(ObservableList<Reservations> oldRsvList, Boolean isNoShow, String comments) {
        //古い予約をCANCELED、CHANGEDのステータスに変更する
        //古い予約をコピーして新しい予約を作成する
        //新しい予約をスプリットして、予約番号、コメントなどを変更する。
        Reservations rsvRef = fetchNewestReservations(oldRsvList.get(0).getReservationnumber()).get(0);
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        if( ! oldRsvList.get(0).getRevisionnumber().equals(rsvRef.getRevisionnumber())) {
            MessageBox.show(Tcyh.stage,
                    "該当の予約は、データ取得後に誰かに変更されたようです。\n" +
                    "予約をロードし直してから再度お試し下さい。",
                    "変更を中止", MessageBox.OK);
            return Boolean.FALSE;
        }
        //NoShowの処理の場合は、先頭の予約から連続してNoShowとなっている必要がある。
        //(複数日の予約がある時に、例えば真ん中の日付だけNoShow処理、のようなことはできない)
        if(isNoShow) {
            Boolean isLastNoShow = true;
            Boolean isThisNoShow;
            for(Reservations rsv : oldRsvList) {
                isThisNoShow = rsv.getChecked() ||
                        rsv.getReservationstatus().equals(reservationStatusNoshowString);
                if ( ! isLastNoShow && isThisNoShow ) {
                    MessageBox.show(Tcyh.stage,
                            "NoShowに設定できるのは、先頭から連続した日だけです。"
                            + "\n(例えば「真ん中の日付だけNoShow処理」のようなことはできません)",
                            "警告",
                            MessageBox.OK);
                    return Boolean.FALSE;
                }
                isLastNoShow = isThisNoShow;
            }
        }
        
        EntityTransaction tx = em.getTransaction();
        long start = System.currentTimeMillis();
        em.clear();
        tx.begin();
        List<Reservations> newRsvList = new ArrayList<Reservations>();
        List<Reservations> canceledRsvList = new ArrayList<Reservations>();
        oldRsvList.forEach(rsv -> {
            String statusBefore = rsv.getReservationstatus();
            rsv.setReservationstatus(
                    rsv.getChecked() ?
                        (isNoShow ? reservationStatusChangedString : reservationStatusCanceledString) :
                        reservationStatusChangedString);
            rsv.setChangecomment(rsv.getChangecomment() + comments);
            em.merge(rsv);
            em.flush();
            em.detach(rsv);
            Reservations rsvNew = rsv.cloneEntity(em);
            //cloneEntityするとcancelカラムのデータが元の状態(非選択:false)のままになるので、setCheckedする。
            //TODO: 上記のような問題があるので、#cloneと#cloneEntityの使い分けを一度考える。
//            rsvNew.setChecked(rsv.getChecked());
            rsvNew.setReservationstatus(statusBefore);
            newRsvList.add(rsvNew);
        });

        //変更後の予約の仕分けを行う
        List<List<Reservations>> listOfSplittedList = new ArrayList<List<Reservations>>();
        List<Reservations> splittedList = new ArrayList<Reservations>();
        boolean isLastChecked = false;
        for(Reservations rsv : newRsvList) {
            Boolean isThisChecked = rsv.getChecked();
            //まず、チェックが付いている予約(=cancelかnoshow)は全てcanceledRsvListに入れて後で処理する
            if (isThisChecked.equals(true))
                canceledRsvList.add(rsv.cloneEntity(em));
            //Cancel以外のステータスの場合は以下も実行する
            if ((isThisChecked.equals(true) && isNoShow) || isThisChecked.equals(false)) {
                //ひとつ前のrsvがcheckedかつ!isNoShow(=cancel処理)の場合、splittedListを初期化する
                if (isLastChecked == true && ! isNoShow) {
                    listOfSplittedList.add(splittedList);
                    splittedList = new ArrayList<Reservations>();
                }
                splittedList.add(rsv.cloneEntity(em));
            }
            isLastChecked = isThisChecked;
        }
        //最後のsplittedList上記ループの中でaddされないのでここでする
        listOfSplittedList.add(splittedList);

        //変更後の予約の予約番号採番、コメントの追加、泊数の変更などを行う
        boolean first = true;
        ReservationsPK rpk;
        int num = 0;
        try {
            for (List<Reservations> list : listOfSplittedList) {
                //全ての予約がキャンセルされた場合
                if (list.isEmpty()) {
                    continue;
                }
                //最初のlistのみ、もともとの予約番号を引き継ぐ
                if (first) {
                    rpk = oldRsvList.get(0).getReservationsPK().clone();
                    rpk.setRevisionnumber((short) (rpk.getRevisionnumber()+1));
                } else {
                    rpk = fetchNewReservationsPK();
                    rpk.setReservationnumber(rpk.getReservationnumber() + num);
                    num++;
                }
                for (short i = 1; i <= list.size(); i++) {
                    Reservations rsv = list.get(i - 1);
                    if(isNoShow && rsv.getChecked())
                        rsv.setReservationstatus(reservationStatusNoshowString);
                    rsv.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
                    rpk.setSequencenumber(i);
                    rsv.setReservationsPK(rpk);
                    rsv.setNightspassed(i);
                    rsv.setTotalnights((short)list.size());
                    if (!first) {
                        rsv.setReferencenumber(oldRsvList.get(0).getReservationnumber());
                        //予約がsplitされて番号が変更される時、BedOccupationの予約番号も入れ替える
                        BedOccupation bo = BedOccupationModel.replaceRsvNum(
                                rsv.getDateofstay(),
                                oldRsvList.get(0).getReservationnumber().toString(),
                                rsv.getReservationnumber().toString()
                        );
                        em.merge(bo);
                    }
                    em.merge(rsv.cloneEntity(em));
                    em.flush();
                    em.detach(rsv);
                }
                first = false;
            }
            canceledRsvList.forEach(rsv -> em.merge(BedOccupationModel.removeRsv(rsv)));
            tx.commit();
            em.clear();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
            return Boolean.TRUE;
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
        }
        return Boolean.FALSE;
    }
    protected static Boolean uniteReservations(Reservations rsvA, Reservations rsvB, String comments) {
        Reservations rsvRef = fetchNewestReservations(rsvA.getReservationnumber()).get(0);
        if( ! rsvA.getRevisionnumber().equals(rsvRef.getRevisionnumber())) {
            MessageBox.show(Tcyh.stage,
                    "該当の予約は、データ取得後に誰かに変更されたようです。\n" +
                    "予約をロードし直してから再度お試し下さい。",
                    "変更を中止", MessageBox.OK);
            return Boolean.FALSE;
        }
        rsvRef = fetchNewestReservations(rsvB.getReservationnumber()).get(0);
        if( ! rsvB.getRevisionnumber().equals(rsvRef.getRevisionnumber())) {
            MessageBox.show(Tcyh.stage,
                    "該当の予約は、データ取得後に誰かに変更されたようです。\n" +
                    "予約をロードし直してから再度お試し下さい。",
                    "変更を中止", MessageBox.OK);
            return Boolean.FALSE;
        }

        Reservations rsv1;
        Reservations rsv2;
        if(rsvB.getDateofstay().equals(rsvA.getCheckOutDate())) {
            rsv1 = rsvA;
            rsv2 = rsvB;
        } else {
            rsv1 = rsvB;
            rsv2 = rsvA;
        }
        List<Reservations> list1Old = fetchNewestReservations(rsv1.getReservationnumber());
        List<Reservations> list2Old = fetchNewestReservations(rsv2.getReservationnumber());
        
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            Short totalNights = (short) (rsv1.getTotalnights() + rsv2.getTotalnights());
            ReservationsPK rpk = rsv1.getReservationsPK().clone();
            rpk.setRevisionnumber((short) (rpk.getRevisionnumber() + 1));
            short i = 1;
            Reservations rsvNew;
            for(Reservations rsvOld : list1Old) {
                rsvNew = rsvOld.cloneEntity(em);
                rsvOld.setReservationstatus(reservationStatusChangedString);
                em.merge(rsvOld);
                em.flush();
                em.detach(rsvOld);

                rpk.setSequencenumber(i);
                rsvNew.setReservationsPK(rpk.clone());
                rsvNew.setTotalnights(totalNights);
                rsvNew.setNightspassed(i);
                rsvNew.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
                rsvNew.setChangecomment(rsvNew.getChangecomment() + comments);
                if( ! rsvNew.getReservationstatus().equals(reservationStatusNoshowString))
                    rsvNew.setReservationstatus(reservationStatusValidString);
                em.persist(rsvNew);
                em.flush();
                em.detach(rsvNew);
                i++;
            }
            for(Reservations rsvOld : list2Old) {
                rsvNew = rsvOld.cloneEntity(em);
                rsvOld.setReservationstatus(reservationStatusChangedString);
                em.merge(rsvOld);
                em.flush();
                em.detach(rsvOld);

                //こちらの予約は予約番号が上書きされるので、ReferenceNumberとして古い予約番号を残す
                rsvNew.setReferencenumber(rsvNew.getReservationnumber());
                rpk.setSequencenumber(i);
                rsvNew.setReservationsPK(rpk.clone());
                rsvNew.setTotalnights(totalNights);
                rsvNew.setNightspassed(i);
                rsvNew.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
                rsvNew.setChangecomment(rsvNew.getChangecomment() + comments);
                if( ! rsvNew.getReservationstatus().equals(reservationStatusNoshowString))
                    rsvNew.setReservationstatus(reservationStatusValidString);
                em.persist(rsvNew);
                //予約番号が変わったので、BedOccupation上の予約番号も変更
                em.merge(BedOccupationModel.replaceRsvNum(
                        rsvNew.getDateofstay(),
                        rsvOld.getReservationnumber().toString(),
                        rsvNew.getReservationnumber().toString()));
                em.flush();
                em.detach(rsvNew);
                i++;
            }
            tx.commit();
            em.clear();
            return Boolean.TRUE;
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
        }
        return Boolean.FALSE;
    }
    protected static Boolean deleteData(Integer rsvNum) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            fetchReservationsByRsvNum(rsvNum).forEach(rsv -> em.remove(rsv));
            tx.commit();
            em.clear();
        } catch (Exception e) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, e);
            tx.rollback();
            em.clear();
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    protected static Boolean restoreCanceledRsv(Integer rsvNum, String comments) {
        ObservableList<Reservations> rsvList = fetchNewestReservations(rsvNum);
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        try {
            for(Reservations rsv : rsvList) {
                if(rsv.getReservationstatus().equals(reservationStatusValidString)) {
                    MessageBox.show(Tcyh.stage, "該当の予約番号で、有効な予約がまだ残っています。"
                            + "\n(全てCXL済みの予約しか復元できません。)", "警告", MessageBox.OK);
                    return Boolean.FALSE;
                }
                rsv.setReservationstatus(reservationStatusValidString);
                rsv.setRevisionnumber(Integer.valueOf(rsv.getRevisionnumber() + 1).shortValue());
                em.persist(rsv);
                em.merge(BedOccupationModel.addRsv(rsv));
                em.flush();
            }
            tx.commit();
            em.clear();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
            return Boolean.TRUE;
        } catch (Exception ex) {
            Logger.getLogger(ReservationsModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            return Boolean.FALSE;
        }
   }
}