/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import holiday.Holiday;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Popup;
import jfx.messagebox.MessageBox;
import static tcyh.Constants.*;

/**
 * FXML Controller class
 *
 * @author masanori
 */
public class StatisticsController implements Initializable {
    @FXML ChoiceBox<Integer> yearChoiceFX;
    @FXML ChoiceBox<Integer> monthChoiceFX;
    @FXML GridPane statisticsFX;
    @FXML Button csvButtonFX;
    
    private static StatisticsController instance;
    static StatisticsController getInstance() {
        return instance;
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        yearChoiceFX.setItems(FXCollections.observableArrayList(2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026));
        yearChoiceFX.setValue(LocalDate.now().getYear());
        monthChoiceFX.setItems(FXCollections.observableArrayList(1,2,3,4,5,6,7,8,9,10,11,12));
        monthChoiceFX.setValue(LocalDate.now().getMonthValue());
    }
    
    @FXML private void showStatistics() {
        final LocalDate firstDateOfThisMonth = LocalDate.of(yearChoiceFX.getValue(), monthChoiceFX.getValue(), 1);
        final LocalDate lastDateOfThisMonth = firstDateOfThisMonth.plusMonths(1).minusDays(1);
        final Integer lastDayNumberOfThisMonth = lastDateOfThisMonth.getDayOfMonth();
        final LocalDate lastDateOfLastMonth = firstDateOfThisMonth.minusDays(1);

        //#dayと#weekday以外はHashMapにidとそのsumを格納して、使える場所では使う方針。
        HashMap<String, Integer> idPrefixAndSum = new HashMap();
        idPrefixAndSum.put("totalBookedPeople", 0);
        idPrefixAndSum.put("checkinPeople", 0);
        idPrefixAndSum.put("checkinCount", 0);
        idPrefixAndSum.put("checkoutPeople", 0);
        idPrefixAndSum.put("checkoutCount", 0);
        idPrefixAndSum.put("breakfast", 0);
        idPrefixAndSum.put("lunch", 0);
        idPrefixAndSum.put("dinner", 0);
        idPrefixAndSum.put("resultMale", 0);
        idPrefixAndSum.put("resultFemale", 0);
        idPrefixAndSum.put("resultTotalPeople", 0);
        idPrefixAndSum.put("noShowPeople", 0);
        idPrefixAndSum.put("noShowCount", 0);
        
        Integer day = 1;
        Boolean isNextMondayHoliday = false;
        LocalDate ldNow = firstDateOfThisMonth;

        final List<Reservations> monthlyRsvList =
                ReservationsModel.fetchReservationsByMonth(firstDateOfThisMonth, lastDateOfThisMonth);
        final List<BedOccupation> monthlyBOList =
                BedOccupationModel.fetchBedOccupationByMonth(firstDateOfThisMonth, lastDateOfThisMonth);
        Label label;
        label = (Label)statisticsFX.lookup("#year");
        label.setText(yearChoiceFX.getValue() + "年");
        label = (Label)statisticsFX.lookup("#month");
        label.setText(monthChoiceFX.getValue() + "月");
        while(day <= lastDayNumberOfThisMonth) {
            Date dateToday = Date.valueOf(ldNow);
            label = (Label)statisticsFX.lookup("#day" + day);
            label.setVisible(true);
            label.getParent().setVisible(true);
            String weekDay = ldNow.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.JAPANESE);
            label = (Label)statisticsFX.lookup("#weekday" + day);
            label.setText(weekDay);
            label.setVisible(true);
            label.getParent().setVisible(true);

            List<Reservations> dailyRsvList = monthlyRsvList.stream()
                    .filter(rsv -> rsv.getDateofstay().equals(dateToday)
                            && ! rsv.getReservationstatus().equals(reservationStatusNoshowString))
                    .collect(Collectors.toList());
            
            Integer totalBookedPeople = dailyRsvList.stream()
                    .mapToInt(rsv -> rsv.getTotalnum())
                    .sum();
            idPrefixAndSum.replace("totalBookedPeople", idPrefixAndSum.get("totalBookedPeople") + totalBookedPeople);
            setUpLabelShown("#totalBookedPeople" + day, totalBookedPeople);

            Integer checkinPeople = dailyRsvList.stream()
                    .filter(rsv -> rsv.getNightspassed().equals((short)1))
                    .mapToInt(rsv -> rsv.getTotalnum())
                    .sum();
            idPrefixAndSum.replace("checkinPeople", idPrefixAndSum.get("checkinPeople") + checkinPeople);
            setUpLabelShown("#checkinPeople" + day, checkinPeople);
            
            Integer checkinCount = dailyRsvList.stream()
                    .filter(rsv -> rsv.getNightspassed().equals((short)1))
                    .mapToInt(rsv -> 1)
                    .sum();
            idPrefixAndSum.replace("checkinCount", idPrefixAndSum.get("checkinCount") + checkinCount);
            setUpLabelShown("#checkinCount" + day, checkinCount);

            //以下のif文内の項目は、一日先(day+1)の値を埋め込んで行くので、最終日(lastDayNumberOfThisMonth)は含まない。
            if(day < lastDayNumberOfThisMonth) {
                Integer checkoutPeople = dailyRsvList.stream()
                        .filter(rsv -> rsv.getNightspassed().equals(rsv.getTotalnights()))
                        .mapToInt(rsv -> rsv.getTotalnum())
                        .sum();
                idPrefixAndSum.replace("checkoutPeople", idPrefixAndSum.get("checkoutPeople") + checkoutPeople);
                setUpLabelShown("#checkoutPeople" + (day+1), checkoutPeople);

                Integer checkoutCount = dailyRsvList.stream()
                        .filter(rsv -> rsv.getNightspassed().equals(rsv.getTotalnights()))
                        .mapToInt(rsv -> 1)
                        .sum();
                idPrefixAndSum.replace("checkoutCount", idPrefixAndSum.get("checkoutCount") + checkoutCount);
                setUpLabelShown("#checkoutCount" + (day+1), checkoutCount);

                Integer breakfast = dailyRsvList.stream()
                        .mapToInt(rsv -> rsv.getBreakfast())
                        .sum();
                idPrefixAndSum.replace("breakfast", idPrefixAndSum.get("breakfast") + breakfast);
                setUpLabelShown("#breakfast" + (day+1), breakfast);

                Integer lunch = dailyRsvList.stream()
                        .mapToInt(rsv -> rsv.getLunch())
                        .sum();
                idPrefixAndSum.replace("lunch", idPrefixAndSum.get("lunch") + lunch);
                setUpLabelShown("#lunch" + (day+1), lunch);
            }

            Integer dinner = dailyRsvList.stream()
                    .mapToInt(rsv -> rsv.getDinner())
                    .sum();
            idPrefixAndSum.replace("dinner", idPrefixAndSum.get("dinner") + dinner);
            setUpLabelShown("#dinner" + day, dinner);

            Integer resultMale = dailyRsvList.stream()
                    .mapToInt(rsv -> Objects.isNull(rsv.getMaleresult()) ? 0 : rsv.getMaleresult())
                    .sum();
            idPrefixAndSum.replace("resultMale", idPrefixAndSum.get("resultMale") + resultMale);
            setUpLabelShown("#resultMale" + day, resultMale);

            Integer resultFemale = dailyRsvList.stream()
                    .mapToInt(rsv -> Objects.isNull(rsv.getFemaleresult()) ? 0 : rsv.getFemaleresult())
                    .sum();
            idPrefixAndSum.replace("resultFemale", idPrefixAndSum.get("resultFemale") + resultFemale);
            setUpLabelShown("#resultFemale" + day, resultFemale);

            Integer resultTotalPeople = dailyRsvList.stream()
                    .mapToInt(rsv -> Objects.isNull(rsv.getResultnum()) ? 0 : rsv.getResultnum())
                    .sum();
            idPrefixAndSum.replace("resultTotalPeople", idPrefixAndSum.get("resultTotalPeople") + resultTotalPeople);
            setUpLabelShown("#resultTotalPeople" + day, resultTotalPeople);
            
            //以下からはNoshowの予約のみに絞るので別のListを使う
            List<Reservations> noshowList = monthlyRsvList.stream()
                    .filter(rsv -> rsv.getDateofstay().equals(dateToday)
                            && rsv.getReservationstatus().equals(reservationStatusNoshowString))
                    .collect(Collectors.toList());
            Integer noShowPeople = noshowList.stream()
                    .mapToInt(rsv -> Objects.isNull(rsv.getTotalnum()) ? 0 : rsv.getTotalnum())
                    .sum();
            idPrefixAndSum.replace("noShowPeople", idPrefixAndSum.get("noShowPeople") + noShowPeople);
            setUpLabelShown("#noShowPeople" + day, noShowPeople);
            
            Integer noShowCount = noshowList.stream()
                    .mapToInt(rsv -> Objects.isNull(rsv.getTotalnum()) ? 0 : 1)
                    .sum();
            idPrefixAndSum.replace("noShowCount", idPrefixAndSum.get("noShowCount") + noShowCount);
            setUpLabelShown("#noShowCount" + day, noShowCount);
            
            day++;
            ldNow = ldNow.plusDays(1);
        }
        
        //上記で1日先の値を取得した項目について、以下で該当月1日の情報を前日の予約から抽出して表示
        List<Reservations> dailyRsvList = monthlyRsvList.stream()
                .filter(rsv -> rsv.getDateofstay().equals(java.sql.Date.valueOf(lastDateOfLastMonth)))
                .collect(Collectors.toList());
        Integer checkoutPeople = dailyRsvList.stream()
                .mapToInt(rsv -> rsv.getTotalnum())
                .sum();
        idPrefixAndSum.replace("checkoutPeople", idPrefixAndSum.get("checkoutPeople") + checkoutPeople);
        setUpLabelShown("#checkoutPeople1", checkoutPeople);

        Integer checkoutCount = dailyRsvList.stream()
                .filter(rsv -> rsv.getNightspassed().equals(rsv.getTotalnights()))
                .mapToInt(rsv -> 1)
                .sum();
        idPrefixAndSum.replace("checkoutCount", idPrefixAndSum.get("checkoutCount") + checkoutCount);
        setUpLabelShown("#checkoutCount1", checkoutCount);

        StackPane sp;
        Integer breakfast = dailyRsvList.stream()
                .mapToInt(rsv -> rsv.getBreakfast())
                .sum();
        idPrefixAndSum.replace("breakfast", idPrefixAndSum.get("breakfast") + breakfast);
        setUpLabelShown("#breakfast1", breakfast);

        Integer lunch = dailyRsvList.stream()
                .mapToInt(rsv -> rsv.getLunch())
                .sum();
        idPrefixAndSum.replace("lunch", idPrefixAndSum.get("lunch") + lunch);        
        setUpLabelShown("#lunch1", lunch);
        
        //合計値を表示
        for(String key : idPrefixAndSum.keySet()) {
            label = (Label)statisticsFX.lookup("#" + key + "Sum");
            label.setText(Objects.isNull(idPrefixAndSum.get(key))
                    || idPrefixAndSum.get(key).equals(0)
                    ? "0" : idPrefixAndSum.get(key).toString());
        }
        
        //最終日の日付より大きい日付を非表示にする
        int num = 31;
        while(lastDayNumberOfThisMonth < num) {
            label = (Label)statisticsFX.lookup("#day" + num);
            label.setVisible(false);
            label.getParent().setVisible(false);
            label = (Label)statisticsFX.lookup("#weekday" + num);
            label.setVisible(false);
            label.getParent().setVisible(false);

            for(String key : idPrefixAndSum.keySet()) {
                setUpLabelHidden("#" + key + num);
            }
            num--;
        }
        
        ldNow = firstDateOfThisMonth.withDayOfMonth(lastDayNumberOfThisMonth);
        num = lastDayNumberOfThisMonth;
        while(num >= 1) {
//            label = (Label)statisticsFX.lookup("#day" + num);

            label = (Label)statisticsFX.lookup("#weekday" + num);
            Date dateToday = Date.valueOf(ldNow);
            sp = (StackPane)label.getParent();
            switch(label.getText()) {
                case "土" :
                    if(Holiday.isHoliday(dateToday))
                        sp.setStyle("-fx-background-color: Lime");
                    else
                        sp.setStyle("-fx-background-color: lightblue");
                    break;
                case "日" :
                    sp.setStyle("-fx-background-color: Pink");
                    if(Holiday.isHoliday(dateToday))
                        isNextMondayHoliday = true;
                    break;
                default :
                    if(Holiday.isHoliday(dateToday) || isNextMondayHoliday)
                        sp.setStyle("-fx-background-color: Lime");
                    else
                        sp.setStyle("");
                    isNextMondayHoliday = false;
                    break;
            }
            ldNow = ldNow.minusDays(1);

            label = (Label)statisticsFX.lookup("#totalBookedPeople" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: yellow");
            else
                label.getParent().setStyle("-fx-background-color: orange");
                
            label = (Label)statisticsFX.lookup("#checkinPeople" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: yellow");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#checkinCount" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 10)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 30)
                label.getParent().setStyle("-fx-background-color: tomato");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#checkoutPeople" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: yellow");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#checkoutCount" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 10)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 30)
                label.getParent().setStyle("-fx-background-color: tomato");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#breakfast" + num);
            if( ! monthlyBOList.get(num - 1).getIsbreakfastservedtomorrow()) {
                if( ! label.getText().equals("-") && ! Integer.decode(label.getText()).equals(0)) {
                    LocalDate ldToday = MainWindowController
                            .asLocalDate(monthlyBOList.get(num - 1).getDateofstay());
                    MessageBox.show(Tcyh.stage, "朝食が休みの日("
                            + ldToday.plusDays(1).getMonthValue() + "月"
                            + ldToday.plusDays(1).getDayOfMonth() + "日"
                            + ")に朝食の予約が入っています。\n"
                            + ldToday.getMonthValue() + "月"
                            + ldToday.getDayOfMonth() + "日"
                            + "の予約を確認して下さい。",
                            "警告", MessageBox.OK);
                }
                label.setText("休み");
                label.getParent().setStyle("-fx-background-color: gray");
            } else if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 30)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: yellow");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: lime");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#lunch" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 30)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: yellow");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: lime");
            else
                label.getParent().setStyle("-fx-background-color: orange");

            label = (Label)statisticsFX.lookup("#dinner" + num);
            if(label.getText().equals("-") || Integer.decode(label.getText()).equals(0))
                label.getParent().setStyle("");
            else if( 0 < Integer.decode(label.getText()) && Integer.decode(label.getText()) < 30)
                label.getParent().setStyle("-fx-background-color: cyan");
            else if(Integer.decode(label.getText()) < 50)
                label.getParent().setStyle("-fx-background-color: yellow");
            else if(Integer.decode(label.getText()) < 100)
                label.getParent().setStyle("-fx-background-color: lime");
            else
                label.getParent().setStyle("-fx-background-color: orange");

//            label = (Label)statisticsFX.lookup("#resultMale" + num);
//            label = (Label)statisticsFX.lookup("#resultFemale" + num);
//            label = (Label)statisticsFX.lookup("#resultTotalPeople" + num);
//            label = (Label)statisticsFX.lookup("#noShowPeople" + num);
//            label = (Label)statisticsFX.lookup("#noShowCount" + num);
            num--;
        }
    }
    private void setUpLabelShown(String lookupId, Integer valueToBeShown) {
        Label label = (Label)statisticsFX.lookup(lookupId);
        label.setText(Objects.isNull(valueToBeShown) || valueToBeShown.equals(0) ? "-" : valueToBeShown.toString());
        label.setVisible(true);
        label.getParent().setVisible(true);
    }
    private void setUpLabelHidden(String lookupId) {
        Label label = (Label)statisticsFX.lookup(lookupId);
        label.setVisible(false);
        label.setText("-");
        label.getParent().setVisible(false);
    }
    @FXML private void popupCSVData(){
        Label label;
        HashMap<String, StringBuilder> prefixesOfIdAndCSV = new HashMap();
        prefixesOfIdAndCSV.put("#totalBookedPeople", new StringBuilder("宿泊人数 合計"));
        prefixesOfIdAndCSV.put("#checkinPeople", new StringBuilder("チェックイン人数"));
        prefixesOfIdAndCSV.put("#checkinCount", new StringBuilder("チェックイン件数"));
        prefixesOfIdAndCSV.put("#checkoutPeople", new StringBuilder("チェックアウト人数"));
        prefixesOfIdAndCSV.put("#checkoutCount", new StringBuilder("チェックアウト件数"));
        prefixesOfIdAndCSV.put("#breakfast", new StringBuilder("朝食"));
        prefixesOfIdAndCSV.put("#lunch", new StringBuilder("昼食"));
        prefixesOfIdAndCSV.put("#dinner", new StringBuilder("夕食"));
        prefixesOfIdAndCSV.put("#resultMale", new StringBuilder("実績人数 男性"));
        prefixesOfIdAndCSV.put("#resultFemale", new StringBuilder("実績人数 女性"));
        prefixesOfIdAndCSV.put("#resultTotalPeople", new StringBuilder("実績人数 合計"));
        prefixesOfIdAndCSV.put("#noShowPeople", new StringBuilder("NoShow 人数"));
        prefixesOfIdAndCSV.put("#noShowCount", new StringBuilder("NoShow 件数"));

        Integer day = 1;
        //各日付の値を入力していく
        while(day <= 31) {
            for (String prefix : prefixesOfIdAndCSV.keySet()) {
                label = (Label)statisticsFX.lookup(prefix + day);
                prefixesOfIdAndCSV.get(prefix).append(",").append(label.getText());
            }
            day++;
        }
        
        //各値のSumを入力していく
        for (String prefix : prefixesOfIdAndCSV.keySet()) {
            label = (Label)statisticsFX.lookup(prefix + "Sum");
            prefixesOfIdAndCSV.get(prefix).append(",").append(label.getText());
        }
        
        //集計した値をTextAreaに出力する
        StringBuilder finalSB = new StringBuilder();
        for (String prefix : prefixesOfIdAndCSV.keySet())
            finalSB.append(prefixesOfIdAndCSV.get(prefix).append("\n"));
        TextArea ta = new TextArea(finalSB.toString());
        ta.setPrefRowCount(14);
        AnchorPane ap = new AnchorPane();
        ap.getChildren().add(ta);
        Popup pup = new Popup();
        pup.getContent().add(ap);
        pup.setAutoHide(true);
        pup.show(statisticsFX, 400, 100);
    }
    @FXML private void forwardMonth() {
        Integer year = yearChoiceFX.getValue();
        Integer month = monthChoiceFX.getValue();
        if(month.equals(12)) {
            month = 1;
            year++;
        } else {
            month++;
        }
        yearChoiceFX.setValue(year);
        monthChoiceFX.setValue(month);
    }
    @FXML private void backwardMonth() {
        Integer year = yearChoiceFX.getValue();
        Integer month = monthChoiceFX.getValue();
        if(month.equals(1)) {
            month = 12;
            year--;
        } else {
            month--;
        }
        yearChoiceFX.setValue(year);
        monthChoiceFX.setValue(month);
    }
}