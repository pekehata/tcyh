/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author masanori
 */
@Embeddable
@EqualsAndHashCode
@ToString
public class ReservationsPK implements Serializable,Cloneable {
    @Basic(optional = false)
    @Column(name = "RESERVATIONNUMBER")
    private Integer reservationnumber;
    @Basic(optional = false)
    @Column(name = "REVISIONNUMBER")
    private short revisionnumber;
    @Basic(optional = false)
    @Column(name = "SEQUENCENUMBER")
    private short sequencenumber;

    public ReservationsPK() {
    }
    
    public ReservationsPK(Integer reservationnumber, Short revisionnumber, Short sequencenumber) {
        this.reservationnumber = reservationnumber;
        this.revisionnumber = revisionnumber;
        this.sequencenumber = sequencenumber;
    }

    public Integer getReservationnumber() {
        return reservationnumber;
    }

    public void setReservationnumber(Integer reservationnumber) {
        this.reservationnumber = reservationnumber;
    }

    public Short getRevisionnumber() {
        return revisionnumber;
    }

    public void setRevisionnumber(Short revisionnumber) {
        this.revisionnumber = revisionnumber;
    }

    public Short getSequencenumber() {
        return sequencenumber;
    }

    public void setSequencenumber(Short sequencenumber) {
        this.sequencenumber = sequencenumber;
    }
    
    @Override
    public ReservationsPK clone() throws CloneNotSupportedException{
        return (ReservationsPK)super.clone();
    }
}
