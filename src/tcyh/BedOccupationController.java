/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Popup;
import jfx.messagebox.MessageBox;
import org.controlsfx.control.PopOver;
import static tcyh.Constants.*;
import static tcyh.utils.ListUtils.*;
import tcyh.utils.USVMap;

/**
 *
 * @author masanori
 */
public class BedOccupationController implements Initializable {
    private List<VBox> childrenList;
    private final List<Line> spareLineList = new ArrayList<>();
    private final HashMap<Label, String> modifiedLabelHM = new HashMap<Label, String>();
    private final HashMap<Integer, Integer> correspondIndexHM = new HashMap<Integer, Integer>();
    private final HashSet<VBox> toFrontHS = new HashSet<VBox>();

    private final List<VBox> vBoxListMdorm = new ArrayList<VBox>();
    private final List<VBox> vBoxListFdorm = new ArrayList<VBox>();
    private List<VBox> vBoxList1808;
    private List<VBox> vBoxList1820;
    private List<VBox> vBoxList1908;
    private List<VBox> vBoxList1917;
    private List<VBox> vBoxList1801;
    private List<VBox> vBoxList1811;
    private List<VBox> vBoxList1807;
    private List<VBox> vBoxList1819;
    private List<VBox> vBoxListStaffL;
    private List<VBox> vBoxListStaffR;
    private List<VBox> vBoxListQuad;
    private Label label1801;
    private Label label1811;
    private Label label1807;
    private Label label1819;
    private Label label1808;
    private Label label1820;
    private Label label1908;
    private Label label1917;
    private Label labelStaffL;
    private Label labelStaffR;
    private List<Label> quadLabelList;

    @FXML GridPane baseGridPaneFX;
    @FXML HTMLEditor commentsFX;
    @FXML private Label closedDayLabel;
    @FXML private VBox turningPointVBox;
    private PopOver po;
    private final Popup pup = new Popup();
    private Text rsvSrc = new Text("");
    private SVGPath brkDwnSign = new SVGPath();
    private BorderPane rsvSrcBP = new BorderPane();
    private Label rsvName = new Label("");
    private Text numMAndF = new Text("");
    private Text days = new Text("");
    private BorderPane daysAndMarkBP = new BorderPane();
    private Text blnkTxt = new Text("");
    private Label brkDwn = new Label("");
    private HBox markHB = new HBox();
    //orgStrをlookupしてTextにキャストしている部分があるのでText以外にするときは要編集
    private Text orgStr = new Text("");
    private Text assign = new Text("");
    private Text opt6 = new Text("");
    private final String styleForMarks =
                "-fx-font: 8pt \"Meiryo \";" +
                "-fx-border-width: 1;" +
                "-fx-border-radius: 3;" +
                "-fx-border-color: ";

    //updateInfoPopoverのNode群
    private final CheckBox luggageCB = new CheckBox("荷預かり");
    private final CheckBox guestcardCB = new CheckBox("ゲスカ有り");
    private final CheckBox recheckinCB = new CheckBox("再イン予定有り");
    private final CheckBox lowerbedCB = new CheckBox("下段ベッド希望");
    private final CheckBox upperbedCB = new CheckBox("上段ベッド希望");
    private final CheckBox creditpaymentCB = new CheckBox("事後/事前カード払い");
    private final Button closeBtn = new Button("✕");
    private final Button marksBtn = new Button("変更");
    private final VBox marks1VB = new VBox(luggageCB, lowerbedCB, guestcardCB);
    private final VBox marks2VB = new VBox(creditpaymentCB, upperbedCB, recheckinCB);
    private final Label exBedLabel = new Label("ベッド数\n※ +の値はエキストラベッド、\n   -の値は未使用ベッドを表す");
    private final HBox marksHB = new HBox(marks1VB, marks2VB, marksBtn);
    private final Spinner<Integer> exBedSP = new Spinner<Integer>(-9,100,0);
    private final Button exBedBtn = new Button("変更");
    private final HBox exBedHB = new HBox(exBedLabel, exBedSP, exBedBtn);
    private final Label brkDwnLabel = new Label("各部屋の内訳\n ※ドミはこのポップアップ画面\n  でのみ表示");
    private final TextArea brkDwnTA = new TextArea();
    private final Button brkDwnBtn = new Button("変更");
    private final HBox brkDwnHB = new HBox(brkDwnLabel, brkDwnTA, brkDwnBtn);
    private final VBox popupVB = new VBox(closeBtn, marksHB, exBedHB, brkDwnHB);
    private final AnchorPane popupAP = new AnchorPane(popupVB);

    //mouseOverTipsのNode群
    private final Label guestNameLabel = new Label("氏名 : ");
    private final Text guestNameText = new Text();
    private final Label guestNumbersLabel = new Label("人数 : ");
    private final Text guestNumbersText = new Text();
    private final Label phoneNumberLabel = new Label("TEL : ");
    private final Text phoneNumberText = new Text();
    private final Label commentsLabel = new Label("備考 : ");
    private final TextArea commentsTextArea = new TextArea();
    private final Label roomAssignmentLabel = new Label("部屋割り : ");
    private final Text roomAssignmentText = new Text();
    private final GridPane mouseOverGP = new GridPane();

    private static BedOccupationController instance;
    static BedOccupationController getInstance() {
        return instance;
    }
    
    LoadBedOccupation loadBedOccupation;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
        loadBedOccupation = new LoadBedOccupation();
        loadBedOccupation.createTask();
        
        childrenList = new ArrayList(baseGridPaneFX.getChildren());
        vBoxList1808 = lookupVBoxList("#1808VBox");
        vBoxList1820 = lookupVBoxList("#1820VBox");
        vBoxList1908 = lookupVBoxList("#1908VBox");
        vBoxList1917 = lookupVBoxList("#1917VBox");
        vBoxList1801 = lookupVBoxList("#1801VBox");
        vBoxList1811 = lookupVBoxList("#1811VBox");
        vBoxList1807 = lookupVBoxList("#1807VBox");
        vBoxList1819 = lookupVBoxList("#1819VBox");
        vBoxListStaffR = lookupVBoxList("#staffrVBox");
        vBoxListStaffL = lookupVBoxList("#stafflVBox");
        vBoxListQuad = lookupVBoxList("#quadVBox");
        label1801 = (Label)baseGridPaneFX.lookup("#1801Label");
        label1811 = (Label)baseGridPaneFX.lookup("#1811Label");
        label1807 = (Label)baseGridPaneFX.lookup("#1807Label");
        label1819 = (Label)baseGridPaneFX.lookup("#1819Label");
        label1808 = (Label)baseGridPaneFX.lookup("#1808Label");
        label1820 = (Label)baseGridPaneFX.lookup("#1820Label");
        label1908 = (Label)baseGridPaneFX.lookup("#1908Label");
        label1917 = (Label)baseGridPaneFX.lookup("#1917Label");
        labelStaffL = (Label)baseGridPaneFX.lookup("#stafflLabel");
        labelStaffR = (Label)baseGridPaneFX.lookup("#staffrLabel");
        quadLabelList = baseGridPaneFX.lookupAll("#quadLabel").stream()
                .map(node -> (Label)node)
                .sorted((a,b) -> {
                    Integer indexA = baseGridPaneFX.getChildren().indexOf(a);
                    Integer indexB = baseGridPaneFX.getChildren().indexOf(b);
                    return indexA.compareTo(indexB);
                }).collect(Collectors.toList());
        
        //VBoxにMouseOverした時に表示されるポップアップの初期設定
        commentsTextArea.setEditable(false);
        commentsTextArea.setMaxWidth(200);
        commentsTextArea.setPrefRowCount(3);
        commentsTextArea.setWrapText(true);
        mouseOverGP.getChildren().clear();
        mouseOverGP.getChildren().addAll(guestNameLabel,guestNameText,
                guestNumbersLabel,guestNumbersText,
                phoneNumberLabel,phoneNumberText,
                commentsLabel,commentsTextArea,
                roomAssignmentLabel,roomAssignmentText);
        mouseOverGP.setStyle("-fx-background-color: LightCyan");
        mouseOverGP.setPadding(new Insets(10,10,10,10));
        GridPane.setConstraints(guestNameLabel, 0, 0);
        GridPane.setConstraints(guestNameText, 1, 0);
        GridPane.setConstraints(guestNumbersLabel, 0, 1);
        GridPane.setConstraints(guestNumbersText, 1, 1);
        GridPane.setConstraints(phoneNumberLabel, 0, 2);
        GridPane.setConstraints(phoneNumberText, 1, 2);
        GridPane.setConstraints(commentsLabel, 0, 3);
        GridPane.setConstraints(commentsTextArea, 1, 3);
        GridPane.setConstraints(roomAssignmentLabel, 0, 4);
        GridPane.setConstraints(roomAssignmentText, 1, 4);
        
        //VBoxクリック時のPopOverの初期設定
        //以下のListenerの処理で、lowerbedとupperbedをどちらかしか選択できないように制限する。
        lowerbedCB.selectedProperty().addListener(ae -> {
            if(upperbedCB.selectedProperty().getValue()
                    && lowerbedCB.selectedProperty().getValue())
                upperbedCB.selectedProperty().setValue(Boolean.FALSE);
        });
        upperbedCB.selectedProperty().addListener(ae -> {
            if(upperbedCB.selectedProperty().getValue()
                    && lowerbedCB.selectedProperty().getValue())
                lowerbedCB.selectedProperty().setValue(Boolean.FALSE);
        });
        closeBtn.setOnAction(ae -> {
            pup.hide();
            ae.consume();
        });
        closeBtn.setPadding(new Insets(0,2,0,2));
        VBox.setMargin(closeBtn, new Insets(10,0,0,10));
        marks1VB.setPadding(new Insets(5));
        marks2VB.setPadding(new Insets(5));
        marks1VB.setSpacing(2);
        marks2VB.setSpacing(2);
        marksHB.setPadding(new Insets(5));
        marksHB.setAlignment(Pos.BOTTOM_CENTER);
        exBedSP.setPrefSize(80, 25);
        exBedSP.setEditable(true);
        exBedHB.setSpacing(5.0);
        exBedHB.setPadding(new Insets(5, 5, 5, 5));
        exBedHB.setAlignment(Pos.CENTER_LEFT);
        exBedLabel.setPadding(new Insets(5, 0, 0, 0));
        brkDwnLabel.setPadding(new Insets(5, 0, 0, 0));
        brkDwnTA.setPrefSize(120, 25);
        brkDwnTA.setEditable(true);
        brkDwnHB.setSpacing(5.0);
        brkDwnHB.setPadding(new Insets(5, 5, 5, 5));
        popupVB.setSpacing(5);
        popupAP.setStyle("-fx-background-color: MistyRose;");
        pup.setAutoHide(true);
        
        if(false) {
            for(BedOccupation bo :
                    BedOccupationModel.fetchBedOccupationBySpecificPeriod(
                            Date.valueOf("2015-01-01"), Date.valueOf("2018-01-01"))) {
                List<String> roomTypeList = Arrays.asList(
                        "quad", "1801", "1811", "1807", "1819", "1808", "1820",
                        "1908", "1917", "staffl", "staffr", "mdorm", "fdorm");
                for(String roomType : roomTypeList) {
                    for(String str : bo.getList(roomType)) {
                        USVMap um = new USVMap(str);
                        um.putRoomType(roomType);
                        bo = replaceUM(bo, um);
                    }
                }
                BedOccupationModel.commitBO(bo);
            }
        }
    }
    private List<VBox> lookupVBoxList(String vBoxId) {
        return baseGridPaneFX.lookupAll(vBoxId).stream()
                .map(node -> (VBox)node)
                .sorted((a,b) -> {
                    Integer indexA = baseGridPaneFX.getChildren().indexOf(a);
                    Integer indexB = baseGridPaneFX.getChildren().indexOf(b);
                    return indexA.compareTo(indexB);
                }).collect(Collectors.toList());
    }

    void loadBedOccupation() {
        loadBedOccupation.restart();
    }
    private void refreshGridPane() {
        //toFrontで入れ変わったindexをリセットするためにsetAllする
        baseGridPaneFX.getChildren().setAll(childrenList);
        for (Node node : baseGridPaneFX.getChildren()) {
            if( ! (node instanceof VBox) )
                continue;
            VBox vb = (VBox) node;
            vb.setStyle("-fx-background-color: transparent;");
            vb.getStyleClass().removeAll(Arrays.asList(
                    "privateroom-left",
                    "privateroom-center",
                    "privateroom-right",
                    "more-than-two-in-dorm"
            ));
            vb.getChildren().clear();
            vb.setUserData(null);
            vb.setAlignment(Pos.TOP_LEFT);
            GridPane.setColumnSpan(vb, 1);
            GridPane.setRowSpan(vb, 1);
        }

        //上記で全てのVBoxがRowSpan=1になるが、畳部屋だけはRowSpan=4にする
        GridPane.setRowSpan((VBox) vBoxList1807.get(0), 4);
        GridPane.setRowSpan((VBox) vBoxList1819.get(0), 4);

        //変更を加えたLabelの表示をリセットするため、modifiedLabelHMから変更前の値を取り出し、再度設定する。
        modifiedLabelHM.forEach((modifiedLabel, txt) -> {
            modifiedLabel.setText(txt);
            modifiedLabel.setStyle("-fx-background-color: null");
        });
        modifiedLabelHM.clear();
    }
    private void removeSpareLines() {
        if (spareLineList.isEmpty()) {
            return;
        }
        spareLineList.stream().forEach((line) -> {
            line.setVisible(false);
        });
        spareLineList.clear();
    }

    private void drawAll() {
        toFrontHS.clear();
        BedOccupation bo = BedOccupationModel.getBOToday();
        drawLargeRoom(bo.getL1820List(), vBoxList1820, label1820);
        drawLargeRoom(bo.getL1917List(), vBoxList1917, label1917);
        drawLargeRoom(bo.getL1808List(), vBoxList1808, label1808);
        drawLargeRoom(bo.getL1908List(), vBoxList1908, label1908);

        drawQuadPrivateRoom(bo.getQuadroomList());

        drawBarrierFreeTatamiStaff(bo.getB1801List(), vBoxList1801, label1801, 2);
        drawBarrierFreeTatamiStaff(bo.getB1811List(), vBoxList1811, label1811, 2);
        drawBarrierFreeTatamiStaff(bo.getT1807List(), vBoxList1807, label1807, 4);
        drawBarrierFreeTatamiStaff(bo.getT1819List(), vBoxList1819, label1819, 4);
        drawBarrierFreeTatamiStaff(bo.getStaffLList(), vBoxListStaffL, labelStaffL, 2);
        drawBarrierFreeTatamiStaff(bo.getStaffRList(), vBoxListStaffR, labelStaffR, 2);

        drawDormBed(bo.getMDormList(), vBoxListMdorm);
        drawDormBed(bo.getFDormList(), vBoxListFdorm);

        //toFrontするとindexの順番が変わり、上のdrawのロジックに影響するので必ず最後に実行する
        // - drawメソッドはnodeとindex通り(FXML内に記載されている順番)に並んでいる前提となっているが、
        //   toFrontしたnodeはindexの最後に回され順番が狂う。
        // - indexの最後尾から順に前面に表示される(つまり、toFrontするとindexの最後尾に回される)
        //
        //また、描画後(toFront後)に元のindexと現在のindexを紐付けて後で参照できるようにするため、
        //remainedHSにそれぞれの値を格納する
        correspondIndexHM.clear();
        toFrontHS.forEach((vb) -> {
            int indexBefore = baseGridPaneFX.getChildren().indexOf(vb);
            vb.toFront();
            //toFrontしたnodeの元々のindexは削除され、後続のindexがデクリメントされるので、
            //toFrontしていないnodeのindexが変更されないよう、
            //以下でtoFrontしたnodeの元のindexの位置に空のTextを挿入する
            baseGridPaneFX.getChildren().add(indexBefore, new Text());
            correspondIndexHM.put(baseGridPaneFX.getChildren().indexOf(vb), indexBefore);
        });
        //VBoxの後でLineが最前面に出るようにする
        spareLineList.forEach((line) -> {
            line.toFront();
        });
        //休館日の場合にLabelを設定
        if(bo.getIsclosedday()) {
            closedDayLabel.setVisible(true);
            closedDayLabel.toFront();
        } else {
            closedDayLabel.setVisible(false);
        }
    }
    private void drawLargeRoom(List<String> largeRoomList, List<VBox> vBoxList, Label roomLabel) {
        if (isFirstEmpty(largeRoomList)) {
            return;
        }
        Reservations rsv = new Reservations();
        VBox vbNow;
        USVMap um = new USVMap(largeRoomList.get(0));
        switch (um.getRsvNum()) {
            case mdormString:
            case mdormFixedString:
                setUpDormLabel(vBoxList, roomLabel, um);
                break;
            case fdormString:
            case fdormFixedString:
                setUpDormLabel(vBoxList, roomLabel, um);
                break;
            case spareString:
                vbNow = vBoxList.get(vBoxList.size()/2);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                    vbNow.setStyle("-fx-background-color: MistyRose;");
                else
                    vbNow.setStyle("-fx-background-color: White;");
                GridPane.setColumnSpan(vbNow, 2);
                GridPane.setRowSpan(vbNow, (vBoxList.size()/2));
                toFrontHS.add(vbNow);
                Line line1 = new Line(0, 0, sideLength*2-3, sideLength*(vBoxList.size()/2)-3);
                Line line2 = new Line(sideLength*2-3, 0, 0, sideLength*(vBoxList.size()/2)-3);
                spareLineList.add(line1);
                spareLineList.add(line2);
                AnchorPane ap = new AnchorPane(line1, line2);
                ap.setMaxSize(vbNow.getWidth(), vbNow.getHeight());
                vbNow.getChildren().add(ap);
                break;
            default:
                rsv = findRsv(um);
                setUpTextsInCell(rsv, um);
                vbNow = vBoxList.get(vBoxList.size()/2);
                GridPane.setRowSpan(vbNow, vBoxList.size() / 2);
                GridPane.setColumnSpan(vbNow, 2);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() > 0)
                    vbNow.setStyle("-fx-background-color: MistyRose;");
                else
                    vbNow.setStyle("-fx-background-color: White;");
                vbNow.getChildren().addAll(rsvSrc, rsvName, numMAndF, daysAndMarkBP, blnkTxt, brkDwn, orgStr);
                int ri = GridPane.getRowIndex(vbNow);
                int ci = GridPane.getColumnIndex(vbNow);
                if (ri == 1 && (ci == 14 || ci == 15)) {
                    drawEmptyBedExBed(um, vBoxList.get(vBoxList.size() / 2), 4);
                } else if (ri == 1 && (ci == 16 || ci == 17)) {
                    drawEmptyBedExBed(um, vBoxList.get(vBoxList.size() / 2), 5);
                } else if (ri == 7 && (ci == 13 || ci == 14)) {
                    drawEmptyBedExBed(um, vBoxList.get(vBoxList.size() / 2), 4);
                } else if (ri == 7 && (ci == 15 || ci == 16)) {
                    drawEmptyBedExBed(um, vBoxList.get(vBoxList.size() / 2), 5);
                }
                //label.setTextする前のTextを先に保存しておく
                modifiedLabelHM.put(roomLabel, roomLabel.getText());
                roomLabel.setText("個室利用" + "\n" + roomLabel.getText());
                roomLabel.setTextAlignment(TextAlignment.CENTER);
                roomLabel.setStyle("-fx-background-color: GreenYellow");
                break;
        }
    }
    private void drawDormBed(List<String> dormList, List<VBox> dormVBoxList) {
        int continueCount = 1;
        VBox vBoxPre = new VBox();
        VBox vBoxNow;
        String numPre;
        Iterator<String> itStr = dormList.iterator();
        Iterator<VBox> dormVBoxIT = dormVBoxList.iterator();
        Line line1, line2;
        Reservations rsv = new Reservations();
        USVMap um = new USVMap("");
        while (itStr.hasNext() && dormVBoxIT.hasNext()) {
            numPre = um.getRsvNum();
            um = new USVMap(itStr.next());
            vBoxNow = dormVBoxIT.next();
            if (Objects.isNull(um.getRsvNum()) || um.getRsvNum().equals("")) {
                continue;
            } else if (um.getRsvNum().equals(spareString)) {
                setUpTextsInCell(null, um);
                toFrontHS.add(vBoxNow);
                line1 = new Line(0, 0, sideLength-3, sideLength-3);
                line2 = new Line(0, sideLength-3, sideLength-3, 0);
                spareLineList.add(line1);
                spareLineList.add(line2);
                AnchorPane ap = new AnchorPane(line1, line2);
                ap.setMaxSize(vBoxNow.getWidth(), vBoxNow.getHeight());
                vBoxNow.getChildren().addAll(ap, orgStr);
            } else if ( ! um.getRsvNum().equals(numPre)) {
                continueCount = 1;
                vBoxPre = vBoxNow;
                rsv = findRsv(um);
                setUpTextsInCell(rsv, um);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                    vBoxNow.setStyle("-fx-background-color: MistyRose;");
                else
                    vBoxNow.setStyle("-fx-background-color: White;");
                vBoxNow.getChildren().addAll(rsvSrcBP, rsvName, daysAndMarkBP, orgStr);
                toFrontHS.add(vBoxNow);
                //ベッド2台以上目で同じカラム内の場合
            } else if (um.getRsvNum().equals(numPre) &&
                    Objects.equals(GridPane.getColumnIndex(vBoxNow), GridPane.getColumnIndex(vBoxPre))) {
                continueCount++;
                vBoxPre.getChildren().clear();
                vBoxPre.getChildren().addAll(rsvSrcBP, rsvName, numMAndF, daysAndMarkBP, orgStr);
                vBoxPre.getStyleClass().add("more-than-two-in-dorm");
                GridPane.setRowSpan(vBoxPre, continueCount);
                GridPane.setValignment(vBoxPre, VPos.TOP);
                toFrontHS.add(vBoxPre);
                //ベッド2台以上目で別のカラムになる場合
            } else if (um.getRsvNum().equals(numPre) && ! Objects.equals(
                            GridPane.getColumnIndex(vBoxNow),
                            GridPane.getColumnIndex(vBoxPre))) {
                vBoxPre.getStyleClass().add("more-than-two-in-dorm");
                continueCount = 1;
                vBoxPre = vBoxNow;
                setUpTextsInCell(rsv, um);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                    vBoxNow.setStyle("-fx-background-color: MistyRose;");
                else
                    vBoxNow.setStyle("-fx-background-color: White;");
                vBoxNow.getChildren().addAll(rsvSrcBP, rsvName, daysAndMarkBP, orgStr);
                vBoxNow.getStyleClass().add("more-than-two-in-dorm");
            }
        }
    }
    private void drawQuadPrivateRoom(List<String> list) {
        Reservations rsv = new Reservations();
        int continueCount = 1;
        VBox privateRoomVBoxPre = new VBox();
        VBox privateRoomVBoxNow = vBoxListQuad.get(0);
        //下記の-5 = -1(リストの最後のVBoxのindex)-4(同一列先頭行のVBoxを取得)
        VBox quadDormVBox = vBoxListQuad.get(vBoxListQuad.size() - 5);
        Label dormLabel = quadLabelList.get(quadLabelList.size() - 1);
        String numPre;
        USVMap um = new USVMap("");
        for (String str : list) {
            numPre = um.getRsvNum();
            um = new USVMap(str);
            //以下2つのif分岐で、ドミの場合の表示設定をする
            if(um.getRsvNum().equals("")){
                //DO NOTHING
            } else if (um.getRsvNum().matches(
                    mdormString + "|" + mdormFixedString
                    + "|" + fdormString + "|" + fdormFixedString)) {
                setUpDormLabel(Arrays.asList(
                        vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox)),
                        vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox) + 1),
                        vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox) + 2),
                        vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox) + 3)),
                        dormLabel, um);
                quadDormVBox = vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox) - 5);
                dormLabel = quadLabelList.get(quadLabelList.indexOf(dormLabel) - 1);
            } else if (um.getRsvNum().equals(spareString)) {
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() > 0)
                    quadDormVBox.setStyle("-fx-background-color: MistyRose;");
                else
                    quadDormVBox.setStyle("-fx-background-color: White;");                
                GridPane.setRowSpan(quadDormVBox, 4);
                toFrontHS.add(quadDormVBox);
                Line line1 = new Line(0, 0, sideLength-3, sideLength*4-3);
                Line line2 = new Line(sideLength-3, 0, 0, sideLength*4-3);
                spareLineList.add(line1);
                spareLineList.add(line2);
                AnchorPane ap = new AnchorPane(line1, line2);
                ap.setMaxSize(quadDormVBox.getWidth(), quadDormVBox.getHeight());
                setUpTextsInCell(null, um);
                quadDormVBox.getChildren().addAll(ap, orgStr);
                quadDormVBox = vBoxListQuad.get(vBoxListQuad.indexOf(quadDormVBox) - 5);
                dormLabel = quadLabelList.get(quadLabelList.indexOf(dormLabel) - 1);
                //個室予約の1室目の表示設定
            } else if ( ! um.getRsvNum().equals(numPre)) {
                rsv = findRsv(um);
                setUpTextsInCell(rsv, um);
                drawEmptyBedExBed(um, privateRoomVBoxNow, 4);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() > 0)
                    privateRoomVBoxNow.setStyle("-fx-background-color: MistyRose;");
                else
                    privateRoomVBoxNow.setStyle("-fx-background-color: White;");
                privateRoomVBoxNow.getChildren().addAll(rsvSrc, rsvName, numMAndF, daysAndMarkBP, blnkTxt, brkDwn, orgStr);
                GridPane.setRowSpan(privateRoomVBoxNow, 4);
                privateRoomVBoxPre = privateRoomVBoxNow;
                if(vBoxListQuad.indexOf(privateRoomVBoxNow) >= 120)
                    return;
                privateRoomVBoxNow = vBoxListQuad.get(vBoxListQuad.indexOf(privateRoomVBoxNow) + 5);
                continueCount = 1;
                //2室以上確保している予約を連結して表示する設定（ただし折り返し地点(vbNow=65)を除く）
            } else if ( ! privateRoomVBoxNow.equals(turningPointVBox) ) {
                //今から描画処理する部屋(=privateRoomVBoxNow)と連結するため、1つ前の部屋(=privateRoomVBoxPre)の枠線を変更する
                if (continueCount == 1) {
                    privateRoomVBoxPre.getStyleClass().removeAll(Arrays.asList(
                            "privateroom-left",
                            "privateroom-center",
                            "privateroom-right"));
                    privateRoomVBoxPre.getStyleClass().add("privateroom-left");
                } else {
                    privateRoomVBoxPre.getStyleClass().removeAll(Arrays.asList(
                            "privateroom-left",
                            "privateroom-center",
                            "privateroom-right"));
                    privateRoomVBoxPre.getStyleClass().add("privateroom-center");
                }

                //vBoxNowの部屋の描画処理
                setUpTextsInCell(rsv, um);
                drawEmptyBedExBed(um, privateRoomVBoxNow, 4);
                privateRoomVBoxNow.getStyleClass().removeAll(Arrays.asList(
                        "privateroom-left",
                        "privateroom-center",
                        "privateroom-right"));
                privateRoomVBoxNow.getStyleClass().add("privateroom-right");
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                    privateRoomVBoxNow.setStyle("-fx-background-color: MistyRose;");
                else
                    privateRoomVBoxNow.setStyle("-fx-background-color: White;");
                privateRoomVBoxNow.getChildren().addAll(blnkTxt, brkDwn, orgStr);
                brkDwn.setText("\n\n\n" + brkDwn.getText());
                GridPane.setRowSpan(privateRoomVBoxNow, 4);
                privateRoomVBoxPre = privateRoomVBoxNow;
                if(vBoxListQuad.indexOf(privateRoomVBoxNow) >= 120)
                    return;
                privateRoomVBoxNow = vBoxListQuad.get(vBoxListQuad.indexOf(privateRoomVBoxNow) + 5);
                continueCount++;
                //最後に、2室以上の個室予約で折り返し地点の予約の表示設定
            } else if (privateRoomVBoxNow.equals(turningPointVBox)) {
                setUpTextsInCell(rsv, um);
                drawEmptyBedExBed(um, privateRoomVBoxNow, 4);
                if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() > 0)
                    privateRoomVBoxNow.setStyle("-fx-background-color: MistyRose;");
                else
                    privateRoomVBoxNow.setStyle("-fx-background-color: White;");
                privateRoomVBoxNow.getChildren().addAll(rsvSrc, rsvName, numMAndF, daysAndMarkBP, blnkTxt, brkDwn, orgStr);
                GridPane.setRowSpan(privateRoomVBoxNow, 4);
                privateRoomVBoxPre = privateRoomVBoxNow;
                if(vBoxListQuad.indexOf(privateRoomVBoxNow) >= 120)
                    return;
                privateRoomVBoxNow = vBoxListQuad.get(vBoxListQuad.indexOf(privateRoomVBoxNow) + 5);
                continueCount = 1;
            }
        }
    }
    private void drawBarrierFreeTatamiStaff(List<String> eachRoomList, List<VBox> vbList, Label roomLabel, Integer length) {
        Reservations rsv = new Reservations();
        VBox vb;
        if (!eachRoomList.isEmpty()) {
            USVMap um = new USVMap(eachRoomList.get(0));
            if(um.getRsvNum().equals(""))return;
            switch (um.getRsvNum()) {
                case mdormString:
                case mdormFixedString:
                    setUpDormLabel(vbList, roomLabel, um);
                    break;
                case fdormString:
                case fdormFixedString:
                    setUpDormLabel(vbList, roomLabel, um);
                    break;
                case spareString:
                    vb = (VBox) vbList.get(0);
                    if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                        vb.setStyle("-fx-background-color: MistyRose;");
                    else
                        vb.setStyle("-fx-background-color: White;");
                    GridPane.setRowSpan(vb, length);
                    toFrontHS.add(vb);
                    Line line1 = new Line(0, 0, sideLength-3, sideLength*length-3);
                    Line line2 = new Line(sideLength-3, 0, 0, sideLength*length-3);
                    spareLineList.add(line1);
                    spareLineList.add(line2);
                    AnchorPane ap = new AnchorPane(line1, line2);
                    ap.setMaxSize(vb.getWidth(), vb.getHeight());
                    vb.getChildren().add(ap);
                    break;
                default:
                    rsv = findRsv(um);
                    setUpTextsInCell(rsv, um);
                    drawEmptyBedExBed(um, vbList.get(0), length);
                    toFrontHS.add(vbList.get(0));
                    vb = (VBox) vbList.get(0);
                    if( ! Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() >0)
                        vb.setStyle("-fx-background-color: MistyRose;");
                    else
                        vb.setStyle("-fx-background-color: White;");
                    vb.getChildren().addAll(rsvSrc, rsvName, numMAndF, daysAndMarkBP, brkDwn, orgStr);
                    GridPane.setRowSpan(vb, length);
                    break;
            }
        }
    }
    private void drawEmptyBedExBed(USVMap um, VBox vb, Integer exBedPosition) {
        Integer e = Integer.parseInt(um.getExBed());
        AnchorPane ap = new AnchorPane();
        baseGridPaneFX.add(ap, GridPane.getColumnIndex(vb) + GridPane.getColumnSpan(vb) - 1, GridPane.getRowIndex(vb) + exBedPosition);
        for (int i = e; i > 0; i--) {
            ap.setStyle("-fx-background-color: MediumSpringGreen");
            ap.setPrefSize(vb.getWidth(), vb.getHeight());
            Line line = new Line(sideLength * i / (e + 1), 0, sideLength * i / (e + 1), sideLength / 2);
            Circle circle = new Circle(sideLength * i / (e + 1), sideLength / 2, circleRadius);
            ap.getChildren().addAll(line, circle);
        }
        for (int i = e; i < 0; i++) {
            VBox starVB = (VBox)baseGridPaneFX.getChildren().get(
                    baseGridPaneFX.getChildren().indexOf(vb) + exBedPosition + i);
            SVGPath star = new SVGPath();
            star.setContent("M 30,2 L 22.2,24 3,24 21,36 12,57 30,45 48,57 42,36 57,24 37.8,24 Z");
            starVB.getChildren().add(star);
            starVB.setAlignment(Pos.CENTER);
        }
        toFrontHS.add(vb);
    }

    private void setUpDormLabel(List<VBox> dormBedVBoxList, Label roomLabel, USVMap um) {
        //label.setTextする前に今のTextの内容をmodifiedLabelHMに格納しておく
        modifiedLabelHM.put(roomLabel, roomLabel.getText());
        switch(um.getRsvNum()){
            case mdormString :
                roomLabel.setText("Mドミ" + "\n" + roomLabel.getText());
                roomLabel.setStyle("-fx-background-color: lightskyblue");
                roomLabel.setUserData(um.getOrgStr());
                vBoxListMdorm.addAll(dormBedVBoxList);
                vBoxListMdorm.forEach((VBox vb) ->
                        //Marksを設定するため、全てのドミのVBoxにUserDataを設定する
                        vb.setUserData("mdormVBox"));
                break;
            case mdormFixedString :
                roomLabel.setText("Mドミ\n固定");
                roomLabel.setStyle("-fx-background-color: DeepSkyBlue");
                roomLabel.setUserData(um.getOrgStr());
                vBoxListMdorm.addAll(dormBedVBoxList);
                vBoxListMdorm.forEach((VBox vb) ->
                        //Marksを設定するため、全てのドミのVBoxにUserDataを設定する
                        vb.setUserData("mdormVBox"));
                break;
            case fdormString :
                roomLabel.setText("Fドミ" + "\n" + roomLabel.getText());
                roomLabel.setStyle("-fx-background-color: lightpink");
                roomLabel.setUserData(um.getOrgStr());
                vBoxListFdorm.addAll(dormBedVBoxList);
                vBoxListFdorm.forEach((VBox vb) ->
                        //Marksを設定するため、全てのドミのVBoxにUserDataを設定する
                        vb.setUserData("fdormVBox"));
                break;
            case fdormFixedString :
                roomLabel.setText("Fドミ\n固定");
                roomLabel.setStyle("-fx-background-color: HotPink");
                roomLabel.setUserData(um.getOrgStr());
                vBoxListFdorm.addAll(dormBedVBoxList);
                vBoxListFdorm.forEach((VBox vb) ->
                        //Marksを設定するため、全てのドミのVBoxにUserDataを設定する
                        vb.setUserData("fdormVBox"));
                break;
        }
        roomLabel.setTextAlignment(TextAlignment.CENTER);
    }
    private void setUpTextsInCell(Reservations rsv, USVMap um) {
        //必要なjavafx.scene.text.Textを、このメソッド終了後に別途VBoxへaddする必要あり
        orgStr = new Text(um.getOrgStr());
        orgStr.setId("orgStr");
        orgStr.setVisible(false);
        orgStr.setFont(Font.font(1));
        //ドミ、予備などの場合は以降の処理が不要
        if(um.getRsvNum().matches(mdormString + "|" + mdormFixedString
                            + "|" + fdormString + "|" + fdormFixedString
                            + "|" +spareString + "|" + ""))
            return;
        rsvSrc = new Text(rsv.getReservationsource());
        brkDwnSign = new SVGPath();
        brkDwnSign.setContent("M0,0 L12,0 L12,12 Z");
        rsvSrcBP = new BorderPane();
        rsvName = new Label(Objects.isNull(rsv.getGuestname()) ? "" :
                rsv.getGuestname().replaceFirst(" |　", "\n ") + "");
        numMAndF = new Text(Objects.isNull(rsv.getNumberofmale()) ? "" :
                rsv.getNumberofmale().equals(0) ? "" : "M" + rsv.getNumberofmale() +
                (Objects.isNull(rsv.getNumberoffemale()) ? " " :
                rsv.getNumberoffemale().equals(0) ? " " : " F" + rsv.getNumberoffemale()));
        days = new Text(rsv.getNightspassed() + "/" + rsv.getTotalnights());
        daysAndMarkBP = new BorderPane();
        markHB = new HBox();
        blnkTxt = new Text();
        brkDwn = um.getBrkDwnDecoded().equals("") ? new Label() : new Label(um.getBrkDwnDecoded());
        
        rsvSrc.setId("rsvSrc");
        rsvName.setId("rsvName");
        numMAndF.setId("numMAndF");
        days.setId("days");
        daysAndMarkBP.setId("daysAndMarkBP");
        markHB.setId("markHB");
        brkDwn.setId("brkDwn");

        rsvSrc.setUnderline(true);
        rsvSrc.setFill(Color.BLUE);
        rsvSrcBP.setLeft(rsvSrc);
        if( ! um.getBrkDwnDecoded().equals(""))
            rsvSrcBP.setRight(brkDwnSign);
        rsvName.setFont(Font.font("Meiryo"));
        rsvName.setTextOverrun(OverrunStyle.ELLIPSIS);
        rsvName.setEllipsisString("..");
        rsvName.setLineSpacing(-6);
        numMAndF.setFill(Color.TURQUOISE);
        brkDwn.setFont(Font.font(18));
        brkDwn.setMaxHeight(sideLength*4);
        brkDwn.setWrapText(true);
        brkDwn.setPadding(new Insets(-6,0,-10,0));
        daysAndMarkBP.setLeft(markHB);
        daysAndMarkBP.setRight(days);
        String marks = um.getMark();
        if(marks.contains(luggageMarkString)) {
            Label luggageMark = new Label("荷");
            luggageMark.setStyle(styleForMarks + "red");
            markHB.getChildren().add(luggageMark);
        }
        if(marks.contains(guestcardMarkString)) {
            Label guestcardMark = new Label("ゲ");
            guestcardMark.setStyle(styleForMarks + "gold");
            markHB.getChildren().add(guestcardMark);
        }
        if(marks.contains(recheckinMarkString)) {
            Label recheckinMark = new Label("再");
            recheckinMark.setStyle(styleForMarks + "springgreen");
            markHB.getChildren().add(recheckinMark);
        }
        if(marks.contains(lowerbedMarkString)) {
            Label lowerbedMark = new Label("下");
            lowerbedMark.setStyle(styleForMarks + "deeppink");
            markHB.getChildren().add(lowerbedMark);
        }
        if(marks.contains(upperbedMarkString)) {
            Label upperbedMark = new Label("上");
            upperbedMark.setStyle(styleForMarks + "blue");
            markHB.getChildren().add(upperbedMark);
        }
        if(marks.contains(creditpaymentMarkString)) {
            Label creditpaymentMark = new Label("事");
            creditpaymentMark.setStyle(styleForMarks + "darkorange");
            markHB.getChildren().add(creditpaymentMark);
        }
    }
    private Reservations findRsv(USVMap um) {
        for (Reservations rsv : ReservationsModel.getRsvListToday()) {
            if( ! rsv.getReservationstatus().equals(reservationStatusValidString))
                continue;
            if (String.valueOf(rsv.getReservationnumber()).equals(um.getRsvNum()))
                return rsv;
        }
        return new Reservations();
    }

    @FXML private void changeRoomState(MouseEvent me) {
        me.consume();
        if (!Objects.isNull(po)) po.hide();
        Label label = (Label) me.getSource();
        BedOccupation boOld = BedOccupationModel.getBOToday();
        Button prvBtn = new Button("空室");
        prvBtn.setOnAction(ae -> {
            BedOccupationModel.commitBO(BedOccupationModel.nonQuadDormToEmpty(boOld, label.getId().replace("Label", "")));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        Button mDormBtn = new Button("Mドミ");
        mDormBtn.setOnAction(ae -> {
            BedOccupationModel.commitBO(BedOccupationModel.emptyRoomToDorm(boOld, label.getId().replace("Label", ""), mdormString));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        Button mDormFixedBtn = new Button("Mドミ(固定)");
        mDormFixedBtn.setOnAction(ae -> {
            BedOccupationModel.commitBO(BedOccupationModel.emptyRoomToDorm(boOld, label.getId().replace("Label", ""), mdormFixedString));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        Button fDormBtn = new Button("Fドミ");
        fDormBtn.setOnAction(ae -> {
            BedOccupationModel.commitBO(BedOccupationModel.emptyRoomToDorm(boOld, label.getId().replace("Label", ""), fdormString));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        Button fDormFixedBtn = new Button("Fドミ(固定)");
        fDormFixedBtn.setOnAction(ae -> {
            BedOccupationModel.commitBO(BedOccupationModel.emptyRoomToDorm(boOld, label.getId().replace("Label", ""), fdormFixedString));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        Button closeBtn = new Button("✕");
        closeBtn.setOnAction(ae -> {
            po.hide();
            ae.consume();
        });
        closeBtn.setPadding(new Insets(0,2,0,2));

        HBox hb = new HBox();
        hb.getChildren().addAll(prvBtn, mDormBtn, mDormFixedBtn, fDormBtn, fDormFixedBtn);
        hb.setSpacing(10);
        hb.setPadding(new Insets(10, 10, 10, 10));

        Button swapBtn = new Button("入れ替える");
        swapBtn.setOnAction(ae -> {
            BedOccupationModel.swapContents(boOld, label.getId().replace("Label", ""));
            MainWindowController.getInstance().selectTab();
            po.hide();
            ae.consume();
        });
        String labelStr = null;
        switch(label.getId().replace("Label", "")){
            case "1801" :
            case "1811" :
                labelStr = "バリアフリールーム(1801と1811)";
                break;
            case "1807" :
            case "1819" :
                labelStr = "畳部屋(1807と1819)";
                hb.setDisable(true);
                break;
            case "staffl" :
            case "staffr" :
                labelStr = "宿直室(左<窓あり>と右<窓なし>)";
                hb.setDisable(true);
                break;
            case "1808" :
            case "1820" :
                labelStr = "10人部屋(1808と1820)";
                break;
            case "1908" :
            case "1917" :
                labelStr = "8人部屋(1908と1917)";
                break;
           default :
                swapBtn.setDisable(true);
        }

        VBox vb = new VBox();
        vb.getChildren().addAll(closeBtn,
                new Label("お部屋の用途を変更します。"),
                hb,
                new Label(Objects.isNull(labelStr)? "" : labelStr + "の利用状況を入れ替えます。"),
                swapBtn);
        vb.setSpacing(10);
        vb.setPadding(new Insets(10, 10, 10, 10));
        
        AnchorPane ap = new AnchorPane();
        ap.setStyle("-fx-background-color: #99FF99;");
        ap.getChildren().add(vb);

        po = new PopOver(ap);
        po.setAutoHide(true);
        po.setArrowLocation(PopOver.ArrowLocation.BOTTOM_LEFT);
        po.show(baseGridPaneFX, me.getSceneX() + 10, me.getSceneY() + 5);
    }
    @FXML private void updateInfoPopover(MouseEvent me) {
        me.consume();
        VBox vb = (VBox) me.getSource();
        if ( ! Objects.isNull(po)) po.hide();
        else if (Objects.isNull(pup)) {
            return;
        }
        if (Objects.isNull(vb.lookup("#orgStr"))) {
            pup.hide();
            return;
        }
        if ( pup.isShowing() && pup.getUserData().equals("updateInfoPopover")) {
            pup.hide();
            return;
        }
        USVMap um = new USVMap(((Text) vb.lookup("#orgStr")).getText());
        if(Objects.isNull(vb.getUserData()))
            um.putRoomType(vb.getId().replaceAll("VBox", ""));
        else
            um.putRoomType(((String)vb.getUserData()).replaceAll("VBox", ""));

        luggageCB.selectedProperty().set(um.getMark().contains(luggageMarkString));
        guestcardCB.selectedProperty().set(um.getMark().contains(guestcardMarkString));
        recheckinCB.selectedProperty().set(um.getMark().contains(recheckinMarkString));
        lowerbedCB.selectedProperty().set(um.getMark().contains(lowerbedMarkString));
        upperbedCB.selectedProperty().set(um.getMark().contains(upperbedMarkString));
        creditpaymentCB.selectedProperty().set(um.getMark().contains(creditpaymentMarkString));
        
        marksBtn.setOnAction((ActionEvent ae) -> {
            if (Objects.isNull(um.getRsvNum())) {
                ae.consume();
                return;
            }
            BedOccupationModel.updateMarks(um,
                    luggageCB.selectedProperty().getValue(),
                    guestcardCB.selectedProperty().getValue(),
                    recheckinCB.selectedProperty().getValue(),
                    lowerbedCB.selectedProperty().getValue(),
                    upperbedCB.selectedProperty().getValue(),
                    creditpaymentCB.selectedProperty().getValue()
            );
            MainWindowController.getInstance().selectTab();
            pup.hide();
            ae.consume();
        });
        exBedSP.getEditor().setText(Integer.decode(um.getExBed()).toString());
        exBedBtn.setOnAction((ActionEvent ae) -> {
            ae.consume();
            if (Objects.isNull(um.getRsvNum())) {
                return;
            } else if (Objects.isNull(exBedSP.getValue())) {
                return;
            }
            BedOccupationModel.updateExBedNumber(um, exBedSP.getValue().toString());
            MainWindowController.getInstance().selectTab();
            pup.hide();
        });
        brkDwnTA.setText(um.getBrkDwnDecoded());
        brkDwnBtn.setOnAction((ActionEvent ae) -> {
            ae.consume();
            if (Objects.isNull(um.getRsvNum())) {
                return;
            } else if (Objects.isNull(brkDwnTA.getText())) {
                return;
            }
            BedOccupationModel.updateBreakDownString(um, brkDwnTA.getText());
            MainWindowController.getInstance().selectTab();
            pup.hide();
        });
        if ( ! Objects.isNull(pup) && pup.isShowing()) {
            pup.hide();
        }
        pup.getContent().clear();
        pup.getContent().add(popupAP);
        pup.setAutoHide(true);
        pup.setUserData("updateInfoPopover");
        pup.show(baseGridPaneFX, me.getSceneX() + 30, me.getSceneY() + 30);
    }
    @FXML private void commitChangeButtonAction() {
        BedOccupationModel.updateComments(commentsFX.getHtmlText());
        MessageBox.show(Tcyh.stage, "変更した内容を保存しました。", "完了", MessageBox.OK);
        MainWindowController.getInstance().selectTab();
    }
    @FXML private void revertChangeButtonAction() {
        Integer val = MessageBox.show(Tcyh.stage, "本当に、元に戻しますか？", "確認", MessageBox.YES|MessageBox.NO);
        if(val.equals(MessageBox.YES))
            commentsFX.setHtmlText(BedOccupationModel.getBOToday().getComments());
    }
    @FXML private void mouseOverTips(MouseEvent me) {
        me.consume();
        if ( ! Objects.isNull(pup.getUserData()) &&
                pup.isShowing() &&
                ! pup.getUserData().equals("mouseOverTips")) {
            return;
        }
        else if( ! Objects.isNull(pup)) pup.hide();

        VBox vb = (VBox)me.getSource();
        if( ! (vb instanceof VBox)) {
            return;
        }

        Text text = (Text)vb.lookup("#orgStr");
        if(Objects.isNull(text)) {
            return;
        }
        
        USVMap um = new USVMap(text.getText());        
        List<Reservations> rsvList = ReservationsModel.getRsvListToday();
        if(um.getRsvNum().matches(spareString)) {
                guestNameText.setText("予備");
                guestNumbersText.setText("");
                phoneNumberText.setText("");
                commentsTextArea.setText("");
                roomAssignmentText.setText(
                        um.getAssign().isEmpty() ? "未割り当て" : um.getAssign());
        } else {
            for(Reservations rsvNow : rsvList) {
                Integer vbRsvNum = Integer.decode(um.getRsvNum());
                if(vbRsvNum.equals(rsvNow.getReservationnumber())) {
                    guestNameText.setText(rsvNow.getGuestname());
                    guestNumbersText.setText(
                            "M: " + rsvNow.getNumberofmale() +
                            " F: " + rsvNow.getNumberoffemale() +
                            " 内幼児: " + rsvNow.getNumberofbaby());
                    phoneNumberText.setText(rsvNow.getPhonenumber());
                    commentsTextArea.setText(rsvNow.getComments());
                    roomAssignmentText.setText(
                            um.getAssign().isEmpty() ? "未割り当て" : um.getAssign());
                    break;
                }
            }
        }
        pup.getContent().clear();
        pup.getContent().add(mouseOverGP);
        pup.setUserData("mouseOverTips");
        pup.show(baseGridPaneFX, me.getSceneX() + sideLength + 10, me.getSceneY() + sideLength + 30);
    }
    @FXML private void mouseExitedTips() {
        if( ! Objects.isNull(pup) &&
                ! Objects.isNull(pup.getUserData()) &&
                pup.getUserData().equals("mouseOverTips"))
            pup.hide();
    }
    
    @FXML private void startFullDragLabel(MouseEvent me) {
        me.consume();
        Label label = (Label)me.getSource();
        label.startFullDrag();
        label.startDragAndDrop(TransferMode.ANY);
    }
    @FXML private void startFullDragVBox(MouseEvent me) {
        VBox vb = (VBox)me.getSource();
        vb.startFullDrag();
        me.consume();
        vb.startDragAndDrop(TransferMode.ANY);
    }
    
    private class LoadBedOccupation extends Service {
        @Override
        protected Task createTask() {
            return new Task<Void>(){
                @Override
                protected Void call() {
                    Platform.runLater(() -> {
                        long start = System.currentTimeMillis();
                        vBoxListMdorm.clear();
                        vBoxListFdorm.clear();
                        removeSpareLines();
                        refreshGridPane();
                        drawAll();
                        commentsFX.setHtmlText("");
                        commentsFX.setHtmlText(BedOccupationModel.getBOToday().getComments());
                        long end = System.currentTimeMillis();
                        System.out.println("Time to drawAll "
                                + new SimpleDateFormat("YYYY月MM月dd日(E)").format(
                                        BedOccupationModel.getBOToday().getDateofstay())
                                + " : " + (end - start) + " msec");
                    });
                    return null;
                }
            };
        }    
    }
}