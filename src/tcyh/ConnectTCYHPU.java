/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.internal.jpa.EntityManagerFactoryImpl;

/**
 *
 * @author masanori
 */
public class ConnectTCYHPU {
    private static final ConnectTCYHPU instance = new ConnectTCYHPU();
    private EntityManagerFactory emf;
    private EntityManager em;
    private long lastConnectedTime = System.currentTimeMillis();

    private ConnectTCYHPU(){
        createNewConnection();
    }
    
    private void createNewConnection(){
        if( ! Objects.isNull(em) && em.isOpen()) em.close();
        if( ! Objects.isNull(emf) && emf.isOpen()) emf.close();
        emf = Persistence.createEntityManagerFactory("tcyhPU");
        String dbPath = emf.unwrap(EntityManagerFactoryImpl.class)
                .unwrap()
                .getSetupImpl()
                .getPersistenceUnitInfo()
                .getProperties()
                .get("javax.persistence.jdbc.url").toString()
                .replace("jdbc:ucanaccess://", "")
                .replaceAll(";.*", "");
        try {
            em = emf.createEntityManager();
        } catch (PersistenceException pe1) {
            Logger.getLogger(ConnectTCYHPU.class.getName()).log(Level.SEVERE, null, pe1);
            MessageBox.show(Tcyh.stage,
                    "AccessのDBファイル\"" + dbPath + "\"が存在しません。\n"
                    + "バックアップファイルへの接続を試みます。", "警告", MessageBox.OK);
            emf = Persistence.createEntityManagerFactory("tcyhPU_BKUP");
            dbPath = emf.unwrap(EntityManagerFactoryImpl.class)
                    .unwrap()
                    .getSetupImpl()
                    .getPersistenceUnitInfo()
                    .getProperties()
                    .get("javax.persistence.jdbc.url").toString()
                    .replace("jdbc:ucanaccess://", "")
                    .replaceAll(";.*", "");
            try {
                em = emf.createEntityManager();
            } catch (PersistenceException pe2) {
                Logger.getLogger(ConnectTCYHPU.class.getName()).log(Level.SEVERE, null, pe2);
                MessageBox.show(Tcyh.stage,
                        "バックアップのDBファイル\"" + dbPath + "\"も存在しません。\n"
                        + "アプリを終了します。", "警告", MessageBox.OK);
                return;
            }
            
            MessageBox.show(Tcyh.stage,
                    "バックアップファイルに接続しましたが、\n"
                    + "情報の更新はできません。", "警告", MessageBox.OK);
        }
    }
    
    public static ConnectTCYHPU getInstance(){
        return instance;
    }
    
    public EntityManagerFactory getEMF(){
        if( ! emf.isOpen()){
            createNewConnection();
        }
        return emf;
    }

    public EntityManager getEM(){
//毎回evictAll()しているとパフォーマンスがかなり落ちるので辞めた
//        em.getEntityManagerFactory().getCache().evictAll();
        if( ! em.isOpen() || ! emf.isOpen() ||
                System.currentTimeMillis() - lastConnectedTime > 300000){
            createNewConnection();
        }
        lastConnectedTime = System.currentTimeMillis();
        return em;
    }
}