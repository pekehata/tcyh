/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import static tcyh.Constants.*;
import tcyh.customcontrols.ColoredTableRow;
import tcyh.customcontrols.FormattedDateTableCell;
import tcyh.utils.DatePickerStringConverter;

/**
 * FXML Controller class
 *
 * @author masanori
 */
public class SearchWindow implements Initializable {

    @FXML DatePicker fromDatePickerFX;
    @FXML DatePicker toDatePickerFX;
    @FXML CheckBox dateNotConsideredFX;
    @FXML ChoiceBox<String> targetFX;
    @FXML TextField keywordFX;
    @FXML CheckBox showCanceledRsvFX;
    @FXML Button searchButtonFX;
    
    @FXML TableView<Reservations> searchResultTableFX;
    @FXML TableColumn<Reservations, Integer> rsvNumColFX;
    @FXML TableColumn<Reservations, String> nameColFX;
    @FXML TableColumn<Reservations, Date> checkInDateColFX;
    @FXML TableColumn<Reservations, Date> checkOutDateColFX;
    @FXML TableColumn<Reservations, Short> totalNightsColFX;
    @FXML TableColumn<Reservations, Short> maleNumColFX;
    @FXML TableColumn<Reservations, Short> femaleNumColFX;
    @FXML TableColumn<Reservations, Short> totalHeadCountColFX;
    @FXML TableColumn<Reservations, String> phoneNumberColFX;
    @FXML TableColumn<Reservations, String> mailAddressColFX;
    @FXML TableColumn<Reservations, String> commentColFX;
    
    private static SearchWindow instance;
    static SearchWindow getInstance() {
        return instance;
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        fromDatePickerFX.setConverter(new DatePickerStringConverter());
        toDatePickerFX.setConverter(new DatePickerStringConverter());
        
        searchResultTableFX.setRowFactory(tv -> new ColoredTableRow());
        checkInDateColFX.setCellFactory(column -> new FormattedDateTableCell());
        checkOutDateColFX.setCellFactory(column -> new FormattedDateTableCell());
        checkOutDateColFX.setCellValueFactory((TableColumn.CellDataFeatures<Reservations, Date> p) -> {
            LocalDate ld = MainWindowController.asLocalDate(p.getValue().getDateofstay())
                    .plusDays(p.getValue().getTotalnights());
            return new ReadOnlyObjectWrapper<Date>(java.sql.Date.valueOf(ld));
        });
        rsvNumColFX.setCellValueFactory((TableColumn.CellDataFeatures<Reservations, Integer> p)
            -> new ReadOnlyObjectWrapper<Integer>(p.getValue().getReservationnumber()));

        dateNotConsideredFX.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(observable.getValue()){
                fromDatePickerFX.setDisable(true);
                toDatePickerFX.setDisable(true);
            }else{
                fromDatePickerFX.setDisable(false);
                toDatePickerFX.setDisable(false);
            }
        });
        dateNotConsideredFX.selectedProperty().setValue(true);
        
        targetFX.setItems(FXCollections.observableArrayList("氏名","予約方法","電話番号","メールアドレス","備考"));
        targetFX.getSelectionModel().select(0);
        fromDatePickerFX.setValue(LocalDate.now());
        toDatePickerFX.setValue(LocalDate.now());
    }
    
    @FXML public void search(){
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery;
        if(keywordFX.getText().replaceAll(" ", "").replaceAll("　", "").length() == 0) {
            MessageBox.show(Tcyh.stage,"空白のみでの検索はできません。","",MessageBox.OK);
            return;
        }
        switch(targetFX.getSelectionModel().getSelectedItem()){
            case "氏名" :
                tquery = em.createNamedQuery("Reservations.findByGuestname", Reservations.class);
                tquery.setParameter("guestname", "%" + keywordFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
                break;
            case "予約方法" :
                tquery = em.createNamedQuery("Reservations.findByReservationsource", Reservations.class);
                tquery.setParameter("reservationsource", "%" + keywordFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
                break;
            case "電話番号" :
                StringBuilder sb = new StringBuilder();
                String numOnly = keywordFX.getText().replaceAll("-", "");
                for(char c : numOnly.toCharArray())
                    sb.append("%").append(c);                    
                tquery = em.createNamedQuery("Reservations.findByPhonenumber", Reservations.class);
                tquery.setParameter("phonenumber", sb.toString() + "%");
                break;
            case "メールアドレス" :
                tquery = em.createNamedQuery("Reservations.findByMailaddress", Reservations.class);
                tquery.setParameter("mailaddress", "%" + keywordFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
                break;
            case "備考" :
                tquery = em.createNamedQuery("Reservations.findByComments", Reservations.class);
                tquery.setParameter("comments", "%" + keywordFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
                break;
            default : return;
        }
        if(dateNotConsideredFX.selectedProperty().getValue()) {
            tquery.setParameter("from", new Date(0));
            //SQL ServerでDateの上限値に引っかかるので、適当な値(7.0E+13=約4188年)を設定
            tquery.setParameter("to", new Date(70000000000000L));
        }else{
            tquery.setParameter("from", java.sql.Date.valueOf(fromDatePickerFX.getValue()));
            tquery.setParameter("to", java.sql.Date.valueOf(toDatePickerFX.getValue()));
        }
        if(showCanceledRsvFX.selectedProperty().get())
            tquery.setParameter("reservationstatus", Arrays.asList(
                    reservationStatusValidString,
                    reservationStatusCanceledString,
                    reservationStatusNoshowString));
        else
            tquery.setParameter("reservationstatus", Arrays.asList(
                    reservationStatusValidString));
        long start = System.currentTimeMillis();
        List<Reservations> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        if(list.isEmpty()) {
            MessageBox.show(Tcyh.stage,"検索結果は0件でした。","",MessageBox.OK);
            return;
        }
        searchResultTableFX.getItems().clear();
        searchResultTableFX.setItems(FXCollections.observableList(list));
    }
    
    @FXML void openReservation(MouseEvent me) {
        if(me.getClickCount() < 2) {
            me.consume();
            return;
        }
        me.consume();
        Reservations rsv = searchResultTableFX.getSelectionModel().getSelectedItem();
        if( ! Objects.isNull(rsv))
            MainWindowController.getInstance().selectReservation(rsv);
    }
}
