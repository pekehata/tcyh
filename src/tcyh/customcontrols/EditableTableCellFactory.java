/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.ShortStringConverter;

/**
 *
 * @author masanori
 * @param <Reservations>
 * @param <T>
 */
public class EditableTableCellFactory<Reservations, T> implements Callback<TableColumn<Reservations, T>,TableCell<Reservations,T>> {
    private final StringProperty type = new SimpleStringProperty();
    public void setType(String type) {
        this.type.setValue(type);
    }
    public String getType() {
        return this.type.getValue();
    }
    @Override
    public TableCell<Reservations, T> call(TableColumn<Reservations, T> param) {
        if(this.type.getValue().equals("String"))
            return new EditableTableCell(new DefaultStringConverter());
        if(this.type.getValue().equals("Short"))
            return new EditableTableCell(new ShortStringConverter());
        return null;
    }
}
