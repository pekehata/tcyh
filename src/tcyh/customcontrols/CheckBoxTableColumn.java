/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class CheckBoxTableColumn extends TableColumn<Reservations, Boolean> {
    public CheckBoxTableColumn() {
        this.setCellFactory(CheckBoxTableCell.forTableColumn(this));
    }
}