/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;

/**
 *
 * @author masanori
 * @param <Reservations>
 * @param <T>
 */
public class EditableTableCell<Reservations, T> extends TableCell<Reservations, T> {
    private TextField textField;
    private boolean escapePressed = false;
    private TablePosition<Reservations, ?> tablePos = null;
    private T itemBefore;
    private tcyh.Reservations rsvBefore;

    public EditableTableCell(StringConverter<T> sc) {
        setConverter(sc);
        itemBefore = null;
        rsvBefore = null;
        //textFieldの設定
        this.textField = new TextField();
        // Use onAction here rather than onKeyReleased (with check for Enter),
        // as otherwise we encounter RT-34685
        this.textField.setOnAction((javafx.event.ActionEvent event) -> {
            if (converter == null) {
                throw new IllegalStateException("Attempting to convert text input into Object, but provided " + "StringConverter is null. Be sure to set a StringConverter " + "in your cell factory.");
            }
            this.commitEdit(getConverter().fromString(this.textField.getText()));
            event.consume();
        });
        this.textField.setOnKeyPressed((javafx.scene.input.KeyEvent t) -> {
            escapePressed = t.getCode() == KeyCode.ESCAPE;
        });
        this.textField.focusedProperty().addListener((ae) -> {
            if (this.textField.focusedProperty().get()) {
                this.textField.commitValue();
            }
        });
    } //textFieldの設定
    // Use onAction here rather than onKeyReleased (with check for Enter),
    // as otherwise we encounter RT-34685

    private static Boolean modified = Boolean.FALSE;
    public static Boolean getModified() {return modified;}
    public static void setModified(Boolean newValue) {modified = newValue;}
    @Override
    public void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        this.getStyleClass().removeAll("edited-cell");
        this.updateItem();
        if (empty) {
            modified = Boolean.FALSE;
            return;
        }
        tcyh.Reservations rsvNow = (tcyh.Reservations) this.getTableRow().getItem();
        if (Objects.isNull(rsvNow)) {
            modified = Boolean.FALSE;
            rsvBefore = rsvNow;
        } else if (Objects.isNull(rsvBefore)) {
            modified = Boolean.FALSE;
            rsvBefore = rsvNow;
            itemBefore = item;
        } else if ( ! rsvNow.equals(rsvBefore)) {
            modified = Boolean.FALSE;
            rsvBefore = rsvNow;
        } else if ( ! Objects.isNull(item) && ! item.equals(itemBefore)) {
            modified = Boolean.TRUE;
            this.getStyleClass().add("edited-cell");
        } else if ( ! Objects.isNull(item) && item.equals(itemBefore)) {
            //DO NOTHING
        } else if (Objects.isNull(item) && ! Objects.isNull(itemBefore)) {
            this.textProperty().setValue(itemBefore.toString());
        }
    }
    private ObjectProperty<StringConverter<T>> converter = new SimpleObjectProperty<StringConverter<T>>(this, "converter");

    public final ObjectProperty<StringConverter<T>> converterProperty() {
        return converter;
    }

    public final void setConverter(StringConverter<T> value) {
        converterProperty().set(value);
    }

    public final StringConverter<T> getConverter() {
        return converterProperty().get();
    }

    @Override
    public void startEdit() {
        if (!isEditable() || !getTableView().isEditable() || !getTableColumn().isEditable()) {
            return;
        }
        super.startEdit();
        if (isEditing()) {
            if (this.textField == null) {
                this.textField = getTextField();
            }
            escapePressed = false;
            startEdit(this.textField);
            final TableView<Reservations> table = getTableView();
            tablePos = table.getEditingCell();
        }
    }

    @Override
    public void commitEdit(T newValue) {
        if (!isEditing()) {
            return;
        }
        final TableView<Reservations> table = getTableView();
        if (table != null) {
            // Inform the TableView of the edit being ready to be committed.
            TableColumn.CellEditEvent editEvent = new TableColumn.CellEditEvent(table, tablePos, TableColumn.editCommitEvent(), newValue);
            Event.fireEvent(getTableColumn(), editEvent);
        }
        // we need to setEditing(false):
        super.cancelEdit(); // this fires an invalid EditCancelEvent.
        // update the item within this cell, so that it represents the new value
        updateItem(newValue, false);
        if (table != null) {
            // reset the editing cell on the TableView
            table.edit(-1, null);
            // request focus back onto the table, only if the current focus
            // owner has the table as a parent (otherwise the user might have
            // clicked out of the table entirely and given focus to something else.
            // It would be rude of us to request it back again.
            // requestFocusOnControlOnlyIfCurrentFocusOwnerIsChild(table);
        }
    }

    @Override
    public void cancelEdit() {
        if (escapePressed) {
            // this is a cancel event after escape key
            super.cancelEdit();
            setText(getItemText()); // restore the original text in the view
        } else {
            // this is not a cancel event after escape key
            // we interpret it as commit.
            String newText = textField.getText(); // get the new text from the view
            this.commitEdit(getConverter().fromString(newText)); // commit the new text to the model
        }
        setGraphic(null); // stop editing with TextField
    }

    private TextField getTextField() {
        final TextField tf = new TextField(getItemText());
        // Use onAction here rather than onKeyReleased (with check for Enter),
        // as otherwise we encounter RT-34685
        tf.setOnAction((javafx.event.ActionEvent event) -> {
            if (converter == null) {
                throw new IllegalStateException("Attempting to convert text input into Object, but provided " + "StringConverter is null. Be sure to set a StringConverter " + "in your cell factory.");
            }
            this.commitEdit(getConverter().fromString(tf.getText()));
            event.consume();
        });
        tf.setOnKeyPressed((javafx.scene.input.KeyEvent t) -> {
            escapePressed = t.getCode() == KeyCode.ESCAPE;
        });
        tf.focusedProperty().addListener((ae) -> {
            if (tf.focusedProperty().get()) {
                tf.commitValue();
            }
        });
        this.textField.addEventFilter(KeyEvent.ANY, (KeyEvent event) -> {
            System.out.println("event.getCode() : " + event.getCode());
            if (event.getCode() == KeyCode.TAB) {
                this.getTableView().getFocusModel().focusRightCell();
            }
        });
        return tf;
    }

    private String getItemText() {
        return getConverter() == null ? getItem() == null ? "" : getItem().toString() : getConverter().toString(getItem());
    }

    private void updateItem() {
        if (isEmpty()) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getItemText());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getItemText());
                setGraphic(null);
            }
        }
    }

    private void startEdit(final TextField textField) {
        if (textField != null) {
            textField.setText(getItemText());
        }
        setText(null);
        setGraphic(textField);
        textField.selectAll();
        // requesting focus so that key input can immediately go into the
        // TextField (see RT-28132)
        textField.requestFocus();
    }
    
}
