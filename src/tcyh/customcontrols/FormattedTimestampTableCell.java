/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javafx.scene.control.TableCell;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class FormattedTimestampTableCell extends TableCell<Reservations, Timestamp> {
    
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");

    @Override
    protected void updateItem(final Timestamp item, boolean empty) {
        setText(item == null ? null : sdf.format(item));
    }
    
}
