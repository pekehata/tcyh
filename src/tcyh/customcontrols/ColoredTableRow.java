/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.util.Objects;
import javafx.scene.control.TableRow;
import tcyh.Constants;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class ColoredTableRow extends TableRow<Reservations> {
    
    @Override
    protected void updateItem(Reservations rsv, boolean empty) {
        super.updateItem(rsv, empty);
        super.getStyleClass().removeAll("canceled-row", "noshow-row", "checkedin-row-FirstDay", "checkedin-row-AfterSecondDay");
        if (empty) {
            ;
        } else if (rsv.getReservationstatus().equals(Constants.reservationStatusCanceledString)) {
            super.getStyleClass().add("canceled-row");
        } else if (rsv.getReservationstatus().equals(Constants.reservationStatusNoshowString)) {
            super.getStyleClass().add("noshow-row");
        } else if (!Objects.isNull(rsv.getResultnum()) && rsv.getResultnum() != 0) {
            if (rsv.getNightspassed().equals((short) 1)) {
                super.getStyleClass().add("checkedin-row-FirstDay");
            } else {
                super.getStyleClass().add("checkedin-row-AfterSecondDay");
            }
        }
    }
    
}
