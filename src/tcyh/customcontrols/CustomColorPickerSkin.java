/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import com.sun.javafx.scene.control.skin.ColorPickerSkin;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;

/**
 *
 * @author masanori
 */
public class CustomColorPickerSkin extends ColorPickerSkin {
    
    public CustomColorPickerSkin(ColorPicker colorPicker) {
        super(colorPicker);
        Label label = (Label) super.getDisplayNode();
        label.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    }
    
}
