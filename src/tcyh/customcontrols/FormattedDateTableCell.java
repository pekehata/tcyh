/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.control.TableCell;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class FormattedDateTableCell extends TableCell<Reservations, Date> {
    
    SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");

    @Override
    protected void updateItem(final Date item, boolean empty) {
        setText(item == null ? null : sdf.format(item));
    }
    
}
