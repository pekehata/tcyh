/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.util.Objects;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class ColorPickerTableTableCell extends TableCell<Reservations, String> {
    
    private ColorPicker cp;

    public ColorPickerTableTableCell() {
        cp = new ColorPicker();
        cp.setOnAction((ae) -> {
            commitEdit(cp.getValue().toString());
            updateItem(cp.getValue().toString(), isEmpty());
        });
        cp.setSkin(new CustomColorPickerSkin(cp));
        this.setGraphic(cp);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);
    }

    @Override
    public final void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        cp.setVisible(!empty);
        Reservations rsv = (Reservations) this.getTableRow().getItem();
        if (empty || Objects.isNull(rsv)) {
            return;
        }
//        if (!Objects.isNull(item) && !item.equals("")) {
//            if (this.getTableColumn().equals(ReservationsController.commentbackgroundcolorCol2)) {
//                rsv.setCommentbackgroundcolor(item);
//                cp.setValue(Color.valueOf(item));
//            } else if (this.getTableColumn().equals(ReservationsController.commentforegroundcolorCol2)) {
//                rsv.setCommentforegroundcolor(item);
//                cp.setValue(Color.valueOf(item));
//            }
//        } else {
//            if (this.getTableColumn().equals(ReservationsController.commentbackgroundcolorCol2)) {
//                cp.setValue(Color.WHITE);
//            } else if (this.getTableColumn().equals(ReservationsController.commentforegroundcolorCol2)) {
//                cp.setValue(Color.BLACK);
//            }
//        }
    }
    
}
