/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.util.Objects;
import javafx.util.StringConverter;
import tcyh.BedOccupation;
import tcyh.MainWindowController;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class BreakfastNumTableCell extends EditableTableCell<Reservations, Short> {
    
    public BreakfastNumTableCell(StringConverter<Short> sc) {
        super(sc);
    }

    @Override
    public void updateItem(Short item, boolean empty) {
        super.updateItem(item, empty);
        this.setStyle("");
        BedOccupation bo;
        Reservations rsv = (Reservations) this.getTableRow().getItem();
        if (!Objects.isNull(rsv) && MainWindowController.getSelectedDate().equals(MainWindowController.asLocalDate(rsv.getDateofstay()))) {
            bo = tcyh.BedOccupationModel.getBOToday();
        } else if (!Objects.isNull(rsv)) {
            bo = tcyh.BedOccupationModel.fetchBO(rsv.getDateofstay());
        } else {
            return;
        }
        if (Objects.isNull(bo)) {
            return;
        }
        if (!empty && !bo.getIsbreakfastservedtomorrow()) {
            this.setStyle("-fx-background-color: Gray;");
        }
    }
    
}
