/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import javafx.scene.control.cell.TextFieldTableCell;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class TotalAndPassedNightsTableCell extends TextFieldTableCell<Reservations, Short> {
    
    public TotalAndPassedNightsTableCell() {
        super();
    }

    @Override
    public void updateItem(Short item, boolean empty) {
        super.updateItem(item, empty);
        this.getStyleClass().remove("total-and-passed-nights-cell");
        if (!empty) {
            this.getStyleClass().add("total-and-passed-nights-cell");
        }
    }
    
}
