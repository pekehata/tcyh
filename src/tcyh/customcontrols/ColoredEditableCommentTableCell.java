/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh.customcontrols;

import java.util.Objects;
import javafx.util.StringConverter;
import tcyh.Reservations;

/**
 *
 * @author masanori
 */
public class ColoredEditableCommentTableCell extends EditableTableCell<Reservations, String> {
    
    public ColoredEditableCommentTableCell(StringConverter<String> sc) {
        super(sc);
        this.setEditable(true);
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        this.setStyle("-fx-background-color: null;");
        this.setStyle(this.getStyle() + "-fx-text-fill : BLACK;");
        Reservations rsv = (Reservations) this.getTableRow().getItem();
        if (empty || Objects.isNull(rsv)) {
            return;
        }
//        rsv.commentbackgroundcolorProperty().addListener((ae) -> this.updateItem(item, empty));
//        rsv.commentforegroundcolorProperty().addListener((ae) -> this.updateItem(item, empty));
        String back = rsv.getCommentbackgroundcolor();
        String fore = rsv.getCommentbackgroundcolor();
        if (!Objects.isNull(back) && !back.equals("") && !back.equals("WHITE") && !back.equals("0xffffffff")) {
            this.setStyle("-fx-background-color: " + back.replace("0x", "#") + ";");
        }
        if (!Objects.isNull(fore) && !fore.equals("")) {
            this.setStyle(this.getStyle() + "-fx-text-fill : " + fore.replace("0x", "#") + ";");
        }
    }
    
}
