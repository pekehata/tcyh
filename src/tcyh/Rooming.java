/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author masanori
 */
@Data
@Entity
@Table(name = "ROOMING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rooming.findAll", query = "SELECT r FROM Rooming r"),
    @NamedQuery(name = "Rooming.findByB1801", query = "SELECT r FROM Rooming r WHERE r.b1801 = :b1801"),
    @NamedQuery(name = "Rooming.findByQ1802", query = "SELECT r FROM Rooming r WHERE r.q1802 = :q1802"),
    @NamedQuery(name = "Rooming.findByQ1803", query = "SELECT r FROM Rooming r WHERE r.q1803 = :q1803"),
    @NamedQuery(name = "Rooming.findByQ1804", query = "SELECT r FROM Rooming r WHERE r.q1804 = :q1804"),
    @NamedQuery(name = "Rooming.findByQ1805", query = "SELECT r FROM Rooming r WHERE r.q1805 = :q1805"),
    @NamedQuery(name = "Rooming.findByQ1806", query = "SELECT r FROM Rooming r WHERE r.q1806 = :q1806"),
    @NamedQuery(name = "Rooming.findByT1807", query = "SELECT r FROM Rooming r WHERE r.t1807 = :t1807"),
    @NamedQuery(name = "Rooming.findByL1808", query = "SELECT r FROM Rooming r WHERE r.l1808 = :l1808"),
    @NamedQuery(name = "Rooming.findByB1811", query = "SELECT r FROM Rooming r WHERE r.b1811 = :b1811"),
    @NamedQuery(name = "Rooming.findByQ1812", query = "SELECT r FROM Rooming r WHERE r.q1812 = :q1812"),
    @NamedQuery(name = "Rooming.findByQ1813", query = "SELECT r FROM Rooming r WHERE r.q1813 = :q1813"),
    @NamedQuery(name = "Rooming.findByQ1814", query = "SELECT r FROM Rooming r WHERE r.q1814 = :q1814"),
    @NamedQuery(name = "Rooming.findByQ1815", query = "SELECT r FROM Rooming r WHERE r.q1815 = :q1815"),
    @NamedQuery(name = "Rooming.findByQ1816", query = "SELECT r FROM Rooming r WHERE r.q1816 = :q1816"),
    @NamedQuery(name = "Rooming.findByQ1817", query = "SELECT r FROM Rooming r WHERE r.q1817 = :q1817"),
    @NamedQuery(name = "Rooming.findByQ1818", query = "SELECT r FROM Rooming r WHERE r.q1818 = :q1818"),
    @NamedQuery(name = "Rooming.findByT1819", query = "SELECT r FROM Rooming r WHERE r.t1819 = :t1819"),
    @NamedQuery(name = "Rooming.findByL1820", query = "SELECT r FROM Rooming r WHERE r.l1820 = :l1820"),
    @NamedQuery(name = "Rooming.findByQ1901", query = "SELECT r FROM Rooming r WHERE r.q1901 = :q1901"),
    @NamedQuery(name = "Rooming.findByQ1902", query = "SELECT r FROM Rooming r WHERE r.q1902 = :q1902"),
    @NamedQuery(name = "Rooming.findByQ1903", query = "SELECT r FROM Rooming r WHERE r.q1903 = :q1903"),
    @NamedQuery(name = "Rooming.findByQ1904", query = "SELECT r FROM Rooming r WHERE r.q1904 = :q1904"),
    @NamedQuery(name = "Rooming.findByQ1905", query = "SELECT r FROM Rooming r WHERE r.q1905 = :q1905"),
    @NamedQuery(name = "Rooming.findByQ1906", query = "SELECT r FROM Rooming r WHERE r.q1906 = :q1906"),
    @NamedQuery(name = "Rooming.findByQ1907", query = "SELECT r FROM Rooming r WHERE r.q1907 = :q1907"),
    @NamedQuery(name = "Rooming.findByL1908", query = "SELECT r FROM Rooming r WHERE r.l1908 = :l1908"),
    @NamedQuery(name = "Rooming.findByQ1911", query = "SELECT r FROM Rooming r WHERE r.q1911 = :q1911"),
    @NamedQuery(name = "Rooming.findByQ1912", query = "SELECT r FROM Rooming r WHERE r.q1912 = :q1912"),
    @NamedQuery(name = "Rooming.findByQ1913", query = "SELECT r FROM Rooming r WHERE r.q1913 = :q1913"),
    @NamedQuery(name = "Rooming.findByQ1914", query = "SELECT r FROM Rooming r WHERE r.q1914 = :q1914"),
    @NamedQuery(name = "Rooming.findByQ1915", query = "SELECT r FROM Rooming r WHERE r.q1915 = :q1915"),
    @NamedQuery(name = "Rooming.findByQ1916", query = "SELECT r FROM Rooming r WHERE r.q1916 = :q1916"),
    @NamedQuery(name = "Rooming.findByL1917", query = "SELECT r FROM Rooming r WHERE r.l1917 = :l1917"),
    @NamedQuery(name = "Rooming.findByStaffl", query = "SELECT r FROM Rooming r WHERE r.staffl = :staffl"),
    @NamedQuery(name = "Rooming.findByStaffr", query = "SELECT r FROM Rooming r WHERE r.staffr = :staffr"),
    @NamedQuery(name = "Rooming.findByComments", query = "SELECT r FROM Rooming r WHERE r.comments = :comments"),
    @NamedQuery(name = "Rooming.findByDateofstay", query = "SELECT r FROM Rooming r WHERE r.dateofstay = :dateofstay")})
public class Rooming implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "B1801")
    private String b1801;
    @Column(name = "Q1802")
    private String q1802;
    @Column(name = "Q1803")
    private String q1803;
    @Column(name = "Q1804")
    private String q1804;
    @Column(name = "Q1805")
    private String q1805;
    @Column(name = "Q1806")
    private String q1806;
    @Column(name = "T1807")
    private String t1807;
    @Column(name = "L1808")
    private String l1808;
    @Column(name = "B1811")
    private String b1811;
    @Column(name = "Q1812")
    private String q1812;
    @Column(name = "Q1813")
    private String q1813;
    @Column(name = "Q1814")
    private String q1814;
    @Column(name = "Q1815")
    private String q1815;
    @Column(name = "Q1816")
    private String q1816;
    @Column(name = "Q1817")
    private String q1817;
    @Column(name = "Q1818")
    private String q1818;
    @Column(name = "T1819")
    private String t1819;
    @Column(name = "L1820")
    private String l1820;
    @Column(name = "Q1901")
    private String q1901;
    @Column(name = "Q1902")
    private String q1902;
    @Column(name = "Q1903")
    private String q1903;
    @Column(name = "Q1904")
    private String q1904;
    @Column(name = "Q1905")
    private String q1905;
    @Column(name = "Q1906")
    private String q1906;
    @Column(name = "Q1907")
    private String q1907;
    @Column(name = "L1908")
    private String l1908;
    @Column(name = "Q1911")
    private String q1911;
    @Column(name = "Q1912")
    private String q1912;
    @Column(name = "Q1913")
    private String q1913;
    @Column(name = "Q1914")
    private String q1914;
    @Column(name = "Q1915")
    private String q1915;
    @Column(name = "Q1916")
    private String q1916;
    @Column(name = "L1917")
    private String l1917;
    @Column(name = "STAFFL")
    private String staffl;
    @Column(name = "STAFFR")
    private String staffr;
    @Column(name = "COMMENTS")
    private String comments;
    @Id
    @Basic(optional = false)
    @Column(name = "DATEOFSTAY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateofstay;

    public Rooming() {
    }
    public Rooming(Date dateofstay) {
        this.dateofstay = dateofstay;
    }

    public List<String> getB1801List() {
        return CSVToList(b1801);
    }
    public void setB1801List(List<String> b1801List) {
        this.b1801 = listToCSV(b1801List);
    }

    public List<String> getQ1802List() {
        return CSVToList(q1802);
    }
    public void setQ1802List(List<String> q1802List) {
        this.q1802 = listToCSV(q1802List);
    }

    public List<String> getQ1803List() {
        return CSVToList(q1803);
    }
    public void setQ1803List(List<String> q1803List) {
        this.q1803 = listToCSV(q1803List);
    }

    public List<String> getQ1804List() {
        return CSVToList(q1804);
    }
    public void setQ1804List(List<String> q1804List) {
        this.q1804 = listToCSV(q1804List);
    }

    public List<String> getQ1805List() {
        return CSVToList(q1805);
    }
    public void setQ1805List(List<String> q1805List) {
        this.q1805 = listToCSV(q1805List);
    }

    public List<String> getQ1806List() {
        return CSVToList(q1806);
    }
    public void setQ1806List(List<String> q1806List) {
        this.q1806 = listToCSV(q1806List);
    }

    public List<String> getT1807List() {
        return CSVToList(t1807);
    }
    public void setT1807List(List<String> t1807List) {
        this.t1807 = listToCSV(t1807List);
    }

    public List<String> getL1808List() {
        return CSVToList(l1808);
    }
    public void setL1808List(List<String> l1808List) {
        this.l1808 = listToCSV(l1808List);
    }

    public List<String> getB1811List() {
        return CSVToList(b1811);
    }
    public void setB1811List(List<String> b1811List) {
        this.b1811 = listToCSV(b1811List);
    }

    public List<String> getQ1812List() {
        return CSVToList(q1812);
    }
    public void setQ1812List(List<String> q1812List) {
        this.q1812 = listToCSV(q1812List);
    }

    public List<String> getQ1813List() {
        return CSVToList(q1813);
    }
    public void setQ1813List(List<String> q1813List) {
        this.q1813 = listToCSV(q1813List);
    }

    public List<String> getQ1814List() {
        return CSVToList(q1814);
    }
    public void setQ1814List(List<String> q1814List) {
        this.q1814 = listToCSV(q1814List);
    }

    public List<String> getQ1815List() {
        return CSVToList(q1815);
    }
    public void setQ1815List(List<String> q1815List) {
        this.q1815 = listToCSV(q1815List);
    }

    public List<String> getQ1816List() {
        return CSVToList(q1816);
    }
    public void setQ1816List(List<String> q1816List) {
        this.q1816 = listToCSV(q1816List);
    }

    public List<String> getQ1817List() {
        return CSVToList(q1817);
    }
    public void setQ1817List(List<String> q1817List) {
        this.q1817 = listToCSV(q1817List);
    }

    public List<String> getQ1818List() {
        return CSVToList(q1818);
    }
    public void setQ1818List(List<String> q1818List) {
        this.q1818 = listToCSV(q1818List);
    }

    public List<String> getT1819List() {
        return CSVToList(t1819);
    }
    public void setT1819List(List<String> t1819List) {
        this.t1819 = listToCSV(t1819List);
    }

    public List<String> getL1820List() {
        return CSVToList(l1820);
    }
    public void setL1820List(List<String> l1820List) {
        this.l1820 = listToCSV(l1820List);
    }

    public List<String> getQ1901List() {
        return CSVToList(q1901);
    }
    public void setQ1901List(List<String> q1901List) {
        this.q1901 = listToCSV(q1901List);
    }

    public List<String> getQ1902List() {
        return CSVToList(q1902);
    }
    public void setQ1902List(List<String> q1902List) {
        this.q1902 = listToCSV(q1902List);
    }

    public List<String> getQ1903List() {
        return CSVToList(q1903);
    }
    public void setQ1903List(List<String> q1903List) {
        this.q1903 = listToCSV(q1903List);
    }

    public List<String> getQ1904List() {
        return CSVToList(q1904);
    }
    public void setQ1904List(List<String> q1904List) {
        this.q1904 = listToCSV(q1904List);
    }

    public List<String> getQ1905List() {
        return CSVToList(q1905);
    }
    public void setQ1905List(List<String> q1905List) {
        this.q1905 = listToCSV(q1905List);
    }

    public List<String> getQ1906List() {
        return CSVToList(q1906);
    }
    public void setQ1906List(List<String> q1906List) {
        this.q1906 = listToCSV(q1906List);
    }

    public List<String> getQ1907List() {
        return CSVToList(q1907);
    }
    public void setQ1907List(List<String> q1907List) {
        this.q1907 = listToCSV(q1907List);
    }

    public List<String> getL1908List() {
        return CSVToList(l1908);
    }
    public void setL1908List(List<String> l1908List) {
        this.l1908 = listToCSV(l1908List);
    }

    public List<String> getQ1911List() {
        return CSVToList(q1911);
    }
    public void setQ1911List(List<String> q1911List) {
        this.q1911 = listToCSV(q1911List);
    }

    public List<String> getQ1912List() {
        return CSVToList(q1912);
    }
    public void setQ1912List(List<String> q1912List) {
        this.q1912 = listToCSV(q1912List);
    }

    public List<String> getQ1913List() {
        return CSVToList(q1913);
    }
    public void setQ1913List(List<String> q1913List) {
        this.q1913 = listToCSV(q1913List);
    }

    public List<String> getQ1914List() {
        return CSVToList(q1914);
    }
    public void setQ1914List(List<String> q1914List) {
        this.q1914 = listToCSV(q1914List);
    }

    public List<String> getQ1915List() {
        return CSVToList(q1915);
    }
    public void setQ1915List(List<String> q1915List) {
        this.q1915 = listToCSV(q1915List);
    }

    public List<String> getQ1916List() {
        return CSVToList(q1916);
    }
    public void setQ1916List(List<String> q1916List) {
        this.q1916 = listToCSV(q1916List);
    }

    public List<String> getL1917List() {
        return CSVToList(l1917);
    }
    public void setL1917List(List<String> l1917List) {
        this.l1917 = listToCSV(l1917List);
    }

    public List<String> getStafflList() {
        return CSVToList(staffl);
    }
    public void setStafflList(List<String> stafflList) {
        this.staffl = listToCSV(stafflList);
    }

    public List<String> getStaffrList() {
        return CSVToList(staffr);
    }
    public void setStaffrList(List<String> staffrList) {
        this.staffr = listToCSV(staffrList);
    }

    public List<String> getCommentsList() {
        return CSVToList(comments);
    }
    public void setCommentsList(List<String> commentsList) {
        this.comments = listToCSV(commentsList);
    }
    
    public List<String> getList(String roomNum) {
        switch(roomNum) {
            case "1801" : return getB1801List();
            case "1811" : return getB1811List();
            case "1802" : return getQ1802List();
            case "1803" : return getQ1803List();
            case "1804" : return getQ1804List();
            case "1805" : return getQ1805List();
            case "1806" : return getQ1806List();
            case "1812" : return getQ1812List();
            case "1813" : return getQ1813List();
            case "1814" : return getQ1814List();
            case "1815" : return getQ1815List();
            case "1816" : return getQ1816List();
            case "1817" : return getQ1817List();
            case "1818" : return getQ1818List();
            case "1901" : return getQ1901List();
            case "1902" : return getQ1902List();
            case "1903" : return getQ1903List();
            case "1904" : return getQ1904List();
            case "1905" : return getQ1905List();
            case "1906" : return getQ1906List();
            case "1907" : return getQ1907List();
            case "1911" : return getQ1911List();
            case "1912" : return getQ1912List();
            case "1913" : return getQ1913List();
            case "1914" : return getQ1914List();
            case "1915" : return getQ1915List();
            case "1916" : return getQ1916List();
            case "1807" : return getT1807List();
            case "1819" : return getT1819List();
            case "1808" : return getL1808List();
            case "1820" : return getL1820List();
            case "1908" : return getL1908List();
            case "1917" : return getL1917List();
            case "staffl" : return getStafflList();
            case "staffr" : return getStaffrList();
            default : return null;
        }
    }
    
    public HashMap<String, List<String>> getQuads() {
        HashMap<String, List<String>> hm = new HashMap();
        hm.put("1802", getQ1802List());
        hm.put("1803", getQ1803List());
        hm.put("1804", getQ1804List());
        hm.put("1805", getQ1805List());
        hm.put("1806", getQ1806List());
        hm.put("1812", getQ1812List());
        hm.put("1813", getQ1813List());
        hm.put("1814", getQ1814List());
        hm.put("1815", getQ1815List());
        hm.put("1816", getQ1816List());
        hm.put("1817", getQ1817List());
        hm.put("1818", getQ1818List());
        hm.put("1901", getQ1901List());
        hm.put("1902", getQ1902List());
        hm.put("1903", getQ1903List());
        hm.put("1904", getQ1904List());
        hm.put("1905", getQ1905List());
        hm.put("1906", getQ1906List());
        hm.put("1907", getQ1907List());
        hm.put("1911", getQ1911List());
        hm.put("1912", getQ1912List());
        hm.put("1913", getQ1913List());
        hm.put("1914", getQ1914List());
        hm.put("1915", getQ1915List());
        hm.put("1916", getQ1916List());
        return hm;
    }
    public HashMap<String, List<String>> getLarges() {
        HashMap<String, List<String>> hm = new HashMap();
        hm.put("1808", getL1808List());
        hm.put("1820", getL1820List());
        hm.put("1908", getL1908List());
        hm.put("1917", getL1917List());
        return hm;
    }

    private List<String> CSVToList(String str) {
        if(Objects.isNull(str) || str.isEmpty()) return new ArrayList<String>();
        //Arrays.asList()をListのインスタンス化に使うと、「配列をListのように見せる」ような動きになり、
        //要素の追加削除ができない「配列」に対してlist.add()などのメソッドを使うと
        //java.lang.UnsupportedOperationExceptionが発生するので、new ArrayList<>()してからaddAllする。
        List<String> list = new ArrayList<String>();
        list.addAll(Arrays.asList(str.split(",", -1)));
        return list;
    }
    private String listToCSV(List list) {
        if(list.isEmpty()) return new String();
        StringBuilder strb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            strb.append(list.get(i));
            if (i < list.size() - 1) {
                strb.append(",");
            }
        }
        return strb.toString();
    }
}
