/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

/**
 *
 * @author masanori
 */
public final class Constants {
    //予約がなかった時の表の文字列
    public static final String rsvByDateOfStayTableNormalString = "まだ予約がありません";
    public static final String rsvByDateOfStayTableClosedString = "休館日";
    public static final String rsvByRsvNumTableNormalString = "";

    //予約のステータス各種
    public static final String reservationStatusValidString = "BOOKED";
    public static final String reservationStatusCanceledString = "CANCELED";
    public static final String reservationStatusChangedString = "CHANGED";
    public static final String reservationStatusNoshowString = "NOSHOW";

    //RoomAssignmentテーブル上の表記
    //※新しい定数を作成する際、3桁以上にしないこと！
    public static final String spareString = "-100";
    public static final String mdormString = "-101";
    public static final String fdormString = "-102";
    public static final String mdormFixedString = "-103";
    public static final String fdormFixedString = "-104";
    
    public static final double sideLength = 70;//BedOccupation,Roomingの四角の1辺の長さ
    public static final double sideLength2Times = sideLength * 2; //Rooming.fxmlからの参照
    public static final double sideLength4Times = sideLength * 4; //Rooming.fxmlからの参照
    public static final double circleRadius = 8;//BedOccupation,Roomingのextra bedを表す円の半径
    
    public static final String luggageMarkString = "荷";
    public static final String guestcardMarkString = "ゲ";
    public static final String recheckinMarkString = "再";
    public static final String lowerbedMarkString = "下";
    public static final String upperbedMarkString = "上";
    public static final String creditpaymentMarkString = "事";
    
    //DatePickerなどの標準フォーマット
    public static final String datePattern = "yyyy/MM/dd(E)";
}