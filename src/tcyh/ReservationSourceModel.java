/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 *
 * @author masanori
 */
public class ReservationSourceModel {
    private static final ReservationSourceModel instance = new ReservationSourceModel();
    protected static ReservationSourceModel getInstance() {
        return instance;
    }
    private ReservationSourceModel() {}
    protected static ObservableList<ReservationSource> fetchReservationSource() {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("ReservationSource.findAll", ReservationSource.class);
        long start = System.currentTimeMillis();
        List<ReservationSource> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        ObservableList<ReservationSource> data = FXCollections.observableArrayList(list);
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static void addNewSource(Short idStr, String sourceName) {
        if(Objects.isNull(idStr)) {
            MessageBox.show(Tcyh.stage, "「No.」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }
        Boolean isNewIdDuplicated = Boolean.FALSE;
        for(ReservationSource rsvSource : fetchReservationSource()) {
            if(rsvSource.getId().equals(idStr))
                isNewIdDuplicated = Boolean.TRUE;
        }
        if (isNewIdDuplicated) {
            MessageBox.show(Tcyh.stage, "入力したNo.は既に使用されています。", "警告", MessageBox.OK);
            return;
        }
        if(Objects.isNull(sourceName) || sourceName.isEmpty()) {
            MessageBox.show(Tcyh.stage, "「予約方法」は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }

        ReservationSource newRsvSource = new ReservationSource();
        newRsvSource.setId(idStr);
        newRsvSource.setSourcename(sourceName);
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        em.clear();
        tx.begin();
        try {
            em.persist(newRsvSource);
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            tx.rollback();
            em.clear();
            MessageBox.show(Tcyh.stage, "問題が発生しました。\n処理を中止します。", "警告", MessageBox.OK);
        }
    }
    protected static void deleteSource(Short newSourceId) {
        List<ReservationSource> list = fetchReservationSource().stream()
                .filter(source -> source.getId().equals(newSourceId))
                .collect(Collectors.toList());
        if(Objects.isNull(list) || list.isEmpty()) {
            MessageBox.show(Tcyh.stage, "対応するNo.の予約方法が、データベースに存在しません。", "警告", MessageBox.OK);
            return;
        }
        if(MessageBox.show(Tcyh.stage, "選択した予約方法を削除します。\nよろしいですか？",
                "確認", MessageBox.YES|MessageBox.NO) == MessageBox.NO)
            return;

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            em.persist(list.get(0));
            em.remove(list.get(0));
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(ReservationSourceModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            MessageBox.show(Tcyh.stage, "問題が発生しました。\n処理を中止します。", "警告", MessageBox.OK);
            return;
        }
        MessageBox.show(Tcyh.stage, "該当の予約方法を削除しました。", "完了", MessageBox.OK);
    }
}