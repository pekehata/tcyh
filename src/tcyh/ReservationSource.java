/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author masanori
 */
@Data
@Entity
@Table(name = "RESERVATIONSOURCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservationSource.findAll", query = "SELECT r FROM ReservationSource r order by r.id asc"),
    @NamedQuery(name = "ReservationSource.findById", query = "SELECT r FROM ReservationSource r WHERE r.id = :id"),
    @NamedQuery(name = "ReservationSource.findBySourcename", query = "SELECT r FROM ReservationSource r WHERE r.sourcename = :sourcename"),
    @NamedQuery(name = "ReservationSource.findAllSourceName", query = "SELECT r.sourcename FROM ReservationSource r order by r.id asc")})
public class ReservationSource implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Short id;
    @Basic(optional = false)
    @Column(name = "SOURCENAME")
    private String sourcename;

    public ReservationSource() {
    }

    public ReservationSource(Short id) {
        this.id = id;
    }

    public ReservationSource(Short id, String sourcename) {
        this.id = id;
        this.sourcename = sourcename;
    }
}
