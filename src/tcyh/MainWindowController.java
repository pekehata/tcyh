/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.converter.IntegerStringConverter;
import javax.persistence.NoResultException;
import jfx.messagebox.MessageBox;
import lombok.Getter;
import org.controlsfx.control.PopOver;
import static tcyh.Constants.*;
import tcyh.utils.CustomDatePickerSkin;
import tcyh.utils.DatePickerStringConverter;
import static tcyh.utils.ListUtils.contains;
import tcyh.utils.ReplaceFullWithHalf;
import tcyh.utils.USVMap;

/**
 *
 * @author masanori
 */
public class MainWindowController implements Initializable {

    @FXML private AnchorPane baseNodeFX;
    @FXML private DatePicker selectedDateFX;
    @FXML private CheckMenuItem showFirstNightFX;
    @FXML private CheckMenuItem showSecondOrLaterNightsFX;
    @FXML private CheckMenuItem showCanceledFX;
    @FXML private TextField guestNameFX;
    @FXML private DatePicker reservationDateFX;
    @FXML private DatePicker checkInDateFX;
    @FXML private DatePicker checkOutDateFX;
    @FXML private TextField phoneNumberFX;
    @FXML private TextField mailAddressFX;
    @FXML private Spinner<Integer> totalNightsFX;
    @FXML private ComboBox<String>  countryFX;
    @FXML private ComboBox<String>  prefectureFX;
    @FXML private TextField plannedcheckintimeFX;
    @FXML private TextArea commentsFX;
    @FXML private ColorPicker commentbackgroundcolorFX;
    @FXML private ColorPicker commentforegroundcolorFX;
    @FXML private Spinner<Integer> numberOfMaleFX;
    @FXML private Spinner<Integer> numberOfFemaleFX;
    @FXML private TextField totalNumFX;
    @FXML private Spinner<Integer> numberOfBabyFX;
    @FXML private Spinner<Integer> maleBedFX;
    @FXML private Spinner<Integer> femaleBedFX;
    @FXML private Spinner<Integer> quadRoomFX;
    @FXML private Spinner<Integer> tatamiRoomFX;
    @FXML private Spinner<Integer> barrierFreeRoomFX;
    @FXML private Spinner<Integer> staffRoomFX;
    @FXML private CheckBox large1820FX;
    @FXML private CheckBox large1917FX;
    @FXML private CheckBox large1808FX;
    @FXML private CheckBox large1908FX;
    @FXML private Spinner<Integer> lunchFX;
    @FXML private Spinner<Integer> dinnerFX;
    @FXML private Spinner<Integer> breakfastFX;
    @FXML private ComboBox<String> reservationSourceFX;
    @FXML private ComboBox<String> staffNameFX;
    @FXML private TextField availableMaleBedFX;
    @FXML private TextField maleBedSpareFX;
    @FXML private TextField availableFemaleBedFX;
    @FXML private TextField femaleBedSpareFX;
    @FXML private TextField availableQuadRoomFX;
    @FXML private TextField availableTatamiRoomFX;
    @FXML private TextField availableBarrierFreeRoomFX;
    @FXML private TextField availableStaffRoomFX;
    @FXML private TextField totalCheckInFX;
    @FXML private TextField remainedCheckInFX;
    @FXML private TextField totalCheckOutFX;
    @FXML private TextField numOfCheckOutPeopleFX;
    @FXML private TextField totalMaleNumFX;
    @FXML private TextField totalFemaleNumFX;
    @FXML private TextField plannedBreakfastNumFX;
    @FXML private TextField plannedLunchNumFX;
    @FXML private TextField plannedDinnerNumFX;
    @FXML private TextField paidBreakfastNumFX;
    @FXML private TextField resultBreakfastNumFX;
    @FXML private TextField totalMaleResulsNumFX;
    @FXML private TextField totalFemaleResulsNumFX;

    @FXML private SplitPane reservationsSplitPaneFX;
    @FXML private Label dividerControlLabelFX;

    @FXML protected TabPane tabPaneFX;
    @FXML protected Tab reservationsTabFX;
    @FXML protected Tab bedOccupationAndRoomingTabFX;
    @FXML protected Tab changeHistoryTabFX;
    @FXML protected Tab AdministrationTabFX;
    
    @FXML private TextArea tempTextAreaFX;

    @Getter static LocalDate selectedDate = LocalDate.now();

    private static MainWindowController instance;
    static MainWindowController getInstance() {
        return instance;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(System.getProperty("os.name").toLowerCase().startsWith("mac")) {
            baseNodeFX.setLayoutX(-90.0);
            baseNodeFX.setLayoutY(-70.0);
            baseNodeFX.setScaleX(0.89);
            baseNodeFX.setScaleY(0.85);
        }
        
        instance = this;
        
        selectedDateFX.setValue(LocalDate.now());
        reservationDateFX.setValue(LocalDate.now());
        checkInDateFX.setValue(LocalDate.now());
        checkOutDateFX.setValue(LocalDate.now().plusDays(1));
        
        commentbackgroundcolorFX.setValue(Color.WHITE);
        commentforegroundcolorFX.setValue(Color.BLACK);
        
        selectedDateFX.setEditable(false);
        //DatePickerの日付表示フォーマットを指定
        selectedDateFX.setConverter(new DatePickerStringConverter());
        reservationDateFX.setConverter(new DatePickerStringConverter());
        checkInDateFX.setConverter(new DatePickerStringConverter());
        checkOutDateFX.setConverter(new DatePickerStringConverter());
        
        //DatePickerから年の前後ボタンを取り除いたSkinを設定(予約の入力ミスを防ぐため)
        selectedDateFX.setSkin(new CustomDatePickerSkin(selectedDateFX));
        reservationDateFX.setSkin(new CustomDatePickerSkin(reservationDateFX));
        checkInDateFX.setSkin(new CustomDatePickerSkin(checkInDateFX));
        checkOutDateFX.setSkin(new CustomDatePickerSkin(checkOutDateFX));
        
        //ReservationDateの初期値を当日日付に設定
        reservationDateFX.setValue(LocalDate.now());

        //ComboBoxにDBからロードしたListを割り当て
        reservationSourceFX.setItems(FXCollections.observableArrayList(
                ReservationSourceModel.fetchReservationSource()
                .stream()
                .map(rsvSrc -> rsvSrc.getSourcename())
                .collect(Collectors.toList())
        ));
        reservationSourceFX.setVisibleRowCount(20);
        staffNameFX.setItems(FXCollections.observableArrayList(
                StaffModel.fetchStaff().stream()
                .map(staffname -> staffname.getLastname())
                .collect(Collectors.toList())
        ));
        staffNameFX.setVisibleRowCount(20);
        //ComboBoxに定数値を割り当て
        ObservableList<String> countries = FXCollections.observableArrayList();
        countries.addAll(Arrays.asList("日本","アイスランド","アイルランド","アゼルバイジャン","アフガニスタン","アメリカ","アラブ首長国連邦","アルジェリア","アルゼンチン","アルバニア","アルメニア","アンゴラ","アンティグア・バーブーダ","アンドラ","イエメン","イギリス","イスラエル","イタリア","イラク","イラン","インド","インドネシア","ウガンダ","ウクライナ","ウズベキスタン","ウルグアイ","エクアドル","エジプト","エストニア","エチオピア","エリトリア","エルサルバドル","オマーン","オランダ","オーストラリア","オーストリア","カザフスタン","カタール","カナダ","カメルーン","韓国","カンボジア","カーボヴェルデ","ガイアナ","ガボン","ガンビア","ガーナ","北朝鮮","キプロス","キューバ","キリバス","キルギス","ギニア","ギニアビサウ","ギリシャ","クウェート","クック諸島","クロアチア","グアテマラ","グルジア","グレナダ","ケニア","コスタリカ","コソボ","コモロ","コロンビア","コンゴ(旧ザイール)","コンゴ共和国","コートジボワール","サウジアラビア","サモア","サントメ・プリンシペ","サンマリノ","ザンビア","シエラレオネ","シリア","シンガポール","ジブチ","ジャマイカ","ジンバブエ","スイス","スウェーデン","スペイン","スリナム","スリランカ","スロバキア","スロベニア","スワジランド","スーダン","赤道ギニア","セネガル","セルビア","セントクリストファー・ネーヴィス","セントビンセント・グレナディーン","セントルシア","セーシェル","ソマリア","ソロモン諸島","タイ","台湾","タジキスタン","タンザニア","チェコ","チャド","中央アフリカ","中国","チュニジア","チリ","ツバル","デンマーク","トリニダード・トバゴ","トルクメニスタン","トルコ","トンガ","トーゴ","ドイツ","ドミニカ共和国","ドミニカ国","ナイジェリア","ナウル","ナミビア","ニウエ","ニカラグア","ニジェール","ニュージーランド","ネパール","ノルウェー","ハイチ","ハンガリー","バチカン","バヌアツ","バハマ","バルバドス","バングラデシュ","バーレーン","パキスタン","パナマ","パプアニューギニア","パラオ","パラグアイ","パレスチナ","東ティモール","フィジー","フィリピン","フィンランド","フランス","ブラジル","ブルガリア","ブルキナファソ","ブルネイ","ブルンジ","ブータン","ベトナム","ベナン","ベネズエラ","ベラルーシ","ベリーズ","ベルギー","ペルー","香港","ホンジュラス","ボスニア・ヘルツェゴビナ","ボツワナ","ボリビア","ポルトガル","ポーランド","マカオ","マケドニア","マダガスカル","マラウイ","マリ","マルタ","マレーシア","マーシャル","ミクロネシア","南アフリカ","南スーダン","ミャンマー","メキシコ","モザンビーク","モナコ","モルディブ","モルドバ","モロッコ","モンゴル","モンテネグロ","モーリシャス","モーリタニア","ヨルダン","ラオス","ラトビア","リトアニア","リヒテンシュタイン","リビア","リベリア","ルクセンブルク","ルワンダ","ルーマニア","レソト","レバノン","ロシア"));
        countryFX.setItems(countries);
        ObservableList<String> prefectures = FXCollections.observableArrayList();
        prefectures.addAll(Arrays.asList("北海道","青森","岩手","宮城","秋田","山形","福島","茨城","栃木","群馬","埼玉","千葉","東京","神奈川","新潟","富山","石川","福井","山梨","長野","岐阜","静岡","愛知","三重","滋賀","京都","大阪","兵庫","奈良","和歌山","鳥取","島根","岡山","広島","山口","徳島","香川","愛媛","高知","福岡","佐賀","長崎","熊本","大分","宮崎","鹿児島","沖縄"));
        prefectureFX.setItems(prefectures);

        selectedDateFX.getEditor().setOnKeyPressed(showComboBoxList());
        checkInDateFX.getEditor().setOnKeyPressed(showComboBoxList());
        checkOutDateFX.getEditor().setOnKeyPressed(showComboBoxList());
        countryFX.getEditor().setOnKeyPressed(showComboBoxList());
        prefectureFX.getEditor().setOnKeyPressed(showComboBoxList());
        reservationSourceFX.getEditor().setOnKeyPressed(showComboBoxList());
        staffNameFX.getEditor().setOnKeyPressed(showComboBoxList());

        //Spinnerの初期化
        totalNightsFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 200, 0));
        numberOfMaleFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0));
        numberOfFemaleFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0));
        numberOfBabyFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0));
        lunchFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 300, 0));
        dinnerFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 300, 0));
        breakfastFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 300, 0));
        maleBedFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 250, 0));
        femaleBedFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 250, 0));
        quadRoomFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 25, 0));
        tatamiRoomFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 2, 0));
        barrierFreeRoomFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 2, 0));
        staffRoomFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 2, 0));
        
        totalNightsFX.focusedProperty().addListener(new ReplaceFullWithHalf(totalNightsFX));
        numberOfMaleFX.focusedProperty().addListener(new ReplaceFullWithHalf(numberOfMaleFX));
        numberOfFemaleFX.focusedProperty().addListener(new ReplaceFullWithHalf(numberOfFemaleFX));
        numberOfBabyFX.focusedProperty().addListener(new ReplaceFullWithHalf(numberOfBabyFX));
        maleBedFX.focusedProperty().addListener(new ReplaceFullWithHalf(maleBedFX));
        femaleBedFX.focusedProperty().addListener(new ReplaceFullWithHalf(femaleBedFX));
        quadRoomFX.focusedProperty().addListener(new ReplaceFullWithHalf(quadRoomFX));
        barrierFreeRoomFX.focusedProperty().addListener(new ReplaceFullWithHalf(barrierFreeRoomFX));
        tatamiRoomFX.focusedProperty().addListener(new ReplaceFullWithHalf(tatamiRoomFX));
        staffRoomFX.focusedProperty().addListener(new ReplaceFullWithHalf(staffRoomFX));
        dinnerFX.focusedProperty().addListener(new ReplaceFullWithHalf(dinnerFX));
        breakfastFX.focusedProperty().addListener(new ReplaceFullWithHalf(breakfastFX));
        lunchFX.focusedProperty().addListener(new ReplaceFullWithHalf(lunchFX));
        checkInDateFX.getEditor().textProperty().addListener(listener -> {
            if ( ! Objects.isNull(checkInDateFX.getValue())
                        && ! Objects.isNull(totalNightsFX.getEditor().getText())
                        && ! Objects.isNull(checkOutDateFX.getValue()))
                calcCheckOutDate();
            else
                calcDatesAndNights();
        });
        checkOutDateFX.getEditor().textProperty().addListener(listener -> {
            if ( ! Objects.isNull(checkInDateFX.getValue())
                        && ! Objects.isNull(totalNightsFX.getEditor().getText())
                        && ! Objects.isNull(checkOutDateFX.getValue()))
                calcCheckInDate();
            else
                calcDatesAndNights();
        });
        //totalNightsFXのSpinnerは、値を変更する度にチェックアウト日時を計算する。
        totalNightsFX.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
            if ( ! Objects.isNull(checkInDateFX.getValue())
                        && ! Objects.isNull(totalNightsFX.getEditor().getText())
                        && ! Objects.isNull(checkOutDateFX.getValue()))
                calcCheckOutDate();
            else
                calcDatesAndNights();
            event.consume();
        });
        totalNightsFX.getEditor().textProperty().addListener(listener -> {
            if ( ! Objects.isNull(checkInDateFX.getValue())
                        && ! Objects.isNull(totalNightsFX.getEditor().getText())
                        && ! Objects.isNull(checkOutDateFX.getValue()))
                calcCheckOutDate();
            else
                calcDatesAndNights();
        });
        
        numberOfMaleFX.getEditor().textProperty().addListener(listener -> {
            Integer maleNum =
                    Objects.isNull(numberOfMaleFX.getEditor().getText())
                    || numberOfMaleFX.getEditor().getText().isEmpty()
                            ? 0 : Integer.decode(numberOfMaleFX.getEditor().getText());
            Integer femaleNum =
                    Objects.isNull(numberOfFemaleFX.getEditor().getText())
                    || numberOfFemaleFX.getEditor().getText().isEmpty()
                            ? 0 : Integer.decode(numberOfFemaleFX.getEditor().getText());
            totalNumFX.setText(Integer.toString(maleNum+femaleNum));
        });
        numberOfFemaleFX.getEditor().textProperty().addListener(listener -> {
            Integer maleNum =
                    Objects.isNull(numberOfMaleFX.getEditor().getText())
                    || numberOfMaleFX.getEditor().getText().isEmpty()
                            ? 0 : Integer.decode(numberOfMaleFX.getEditor().getText());
            Integer femaleNum =
                    Objects.isNull(numberOfFemaleFX.getEditor().getText())
                    || numberOfFemaleFX.getEditor().getText().isEmpty()
                            ? 0 : Integer.decode(numberOfFemaleFX.getEditor().getText());
            totalNumFX.setText(Integer.toString(maleNum+femaleNum));
        });

        //予約操作ペインを隠す
        contractSplitPaneSize();

        selectTab();
    }

    void setReservationsValuesToFields() {
        removeDataFromFields();
        TableViewSelectionModel<Reservations> tvsm = ReservationsController.getInstance().rsvByDateOfStayTableFX.getSelectionModel();
        Reservations r = (Reservations)tvsm.getSelectedItem();
        if(Objects.isNull(r)) return;
        ReservationsPK rpk = r.getReservationsPK();

        //チェックイン日のデータを取得するため、rpkを元に検索し、get(0)する
        ObservableList<Reservations> list = ReservationsModel.fetchNewestReservations(rpk.getReservationnumber());
        Reservations rsv = list.get(0);

        guestNameFX.setText(rsv.getGuestname());
        reservationDateFX.setValue(asLocalDate(rsv.getReservationdate()));
        checkInDateFX.setValue(asLocalDate(rsv.getCheckInDate()));
        checkOutDateFX.setValue(asLocalDate(rsv.getCheckOutDate()));
        phoneNumberFX.setText(rsv.getPhonenumber());
        mailAddressFX.setText(rsv.getMailaddress());
        totalNightsFX.getEditor().setText(Short.toString(rsv.getTotalnights()));
        plannedcheckintimeFX.setText(rsv.getPlannedcheckintime());
        countryFX.setValue(rsv.getCountry());
        prefectureFX.setValue(rsv.getPrefecture());
        commentsFX.setText(rsv.getComments());
        commentbackgroundcolorFX.setValue(
                Objects.isNull(rsv.getCommentbackgroundcolor()) || rsv.getCommentbackgroundcolor().equals("") ?
                        Color.WHITE :
                        Color.valueOf(rsv.getCommentbackgroundcolor()));
        commentforegroundcolorFX.setValue(
                Objects.isNull(rsv.getCommentforegroundcolor()) || rsv.getCommentforegroundcolor().equals("") ?
                        Color.BLACK :
                        Color.valueOf(rsv.getCommentforegroundcolor()));
        numberOfMaleFX.getEditor().setText(Short.toString(rsv.getNumberofmale()));
        numberOfFemaleFX.getEditor().setText(Short.toString(rsv.getNumberoffemale()));
        numberOfBabyFX.getEditor().setText(Short.toString(rsv.getNumberofbaby()));
        maleBedFX.getEditor().setText(Short.toString(rsv.getMalebed()));
        femaleBedFX.getEditor().setText(Short.toString(rsv.getFemalebed()));
        quadRoomFX.getEditor().setText(Short.toString(rsv.getQuadroom()));
        tatamiRoomFX.getEditor().setText(Short.toString(rsv.getTatamiroom()));
        barrierFreeRoomFX.getEditor().setText(Short.toString(rsv.getBarrierfreeroom()));
        staffRoomFX.getEditor().setText(Short.toString(rsv.getStaffroom()));
        large1820FX.setSelected(rsv.getLarge1820());
        large1917FX.setSelected(rsv.getLarge1917());
        large1808FX.setSelected(rsv.getLarge1808());
        large1908FX.setSelected(rsv.getLarge1908());
        reservationSourceFX.setValue(rsv.getReservationsource());
        staffNameFX.setValue(rsv.getStaffname());

        expandSplitPaneSize();
    }
    @FXML private void removeDataFromFields() {
        guestNameFX.setText("");
        reservationDateFX.setValue(LocalDate.now());
        checkInDateFX.setValue(null);
        totalNightsFX.getEditor().setText("1");
        checkOutDateFX.setValue(null);
        phoneNumberFX.setText(null);
        mailAddressFX.setText(null);
        countryFX.setValue(null);
        prefectureFX.setValue(null);
        plannedcheckintimeFX.setText(null);
        commentsFX.setText(null);
        commentbackgroundcolorFX.setValue(Color.WHITE);
        commentforegroundcolorFX.setValue(Color.BLACK);
        numberOfMaleFX.getEditor().setText("0");
        numberOfFemaleFX.getEditor().setText("0");
        numberOfBabyFX.getEditor().setText("0");
        maleBedFX.getEditor().setText("0");
        femaleBedFX.getEditor().setText("0");
        quadRoomFX.getEditor().setText("0");
        tatamiRoomFX.getEditor().setText("0");
        barrierFreeRoomFX.getEditor().setText("0");
        staffRoomFX.getEditor().setText("0");
        dinnerFX.getEditor().setText("0");
        breakfastFX.getEditor().setText("0");
        lunchFX.getEditor().setText("0");
        large1820FX.setSelected(false);
        large1917FX.setSelected(false);
        large1808FX.setSelected(false);
        large1908FX.setSelected(false);
        reservationSourceFX.setValue(null);
        staffNameFX.setValue(null);
    }
    @FXML private void adjustSplitPaneSize(){
        double position = reservationsSplitPaneFX.getDividerPositions()[0];
        if(position < 0.98){
            contractSplitPaneSize();
        }else{
            expandSplitPaneSize();
        }
    }
    private void expandSplitPaneSize(){
        reservationsSplitPaneFX.setDividerPosition(0, 0.72);
        dividerControlLabelFX.setRotate(0);
    }
    private void contractSplitPaneSize(){
        reservationsSplitPaneFX.setDividerPosition(0, 0.985);
        dividerControlLabelFX.setRotate(180);
    }

    public static LocalDate asLocalDate(Date date) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date)
            return ((java.sql.Date) date).toLocalDate();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    void showSummary(LocalDate ld){
        HashMap<String, Integer> boMap = BedOccupationModel.calcBOSummary();
        availableMaleBedFX.setText(String.valueOf(boMap.get("availableMaleBed")));
        maleBedSpareFX.setText(String.valueOf(boMap.get("maleBedSpare")));
        availableFemaleBedFX.setText(String.valueOf(boMap.get("availableFemaleBed")));
        femaleBedSpareFX.setText(String.valueOf(boMap.get("femaleBedSpare")));
        availableQuadRoomFX.setText(String.valueOf(boMap.get("availableQuadRoom")));
        availableTatamiRoomFX.setText(String.valueOf(boMap.get("availableTatamiRoom")));
        availableBarrierFreeRoomFX.setText(String.valueOf(boMap.get("availableBarrierFreeRoom")));
        availableStaffRoomFX.setText(String.valueOf(boMap.get("availableStaffRoom")));
        resultBreakfastNumFX.setText(boMap.get("resultBreakfastNum").toString());

        maleBedSpareFX.getStyleClass().removeAll("warning", "closedday");

        if(Integer.parseInt(maleBedSpareFX.getText())<2)
            maleBedSpareFX.getStyleClass().add("warning");

        femaleBedSpareFX.getStyleClass().removeAll("warning", "closedday");
        if(Integer.parseInt(femaleBedSpareFX.getText())<2)
            femaleBedSpareFX.getStyleClass().add("warning");
        
        //休館日は下記のテキストボックスの背景色を変更する
        if(BedOccupationModel.getBOToday().getIsclosedday()) {
            availableMaleBedFX.getStyleClass().add("closedday");
            maleBedSpareFX.getStyleClass().add("closedday");
            availableFemaleBedFX.getStyleClass().add("closedday");
            femaleBedSpareFX.getStyleClass().add("closedday");
            availableQuadRoomFX.getStyleClass().add("closedday");
            availableTatamiRoomFX.getStyleClass().add("closedday");
            availableBarrierFreeRoomFX.getStyleClass().add("closedday");
            availableStaffRoomFX.getStyleClass().add("closedday");
        } else {
            availableMaleBedFX.getStyleClass().removeAll("closedday");
            maleBedSpareFX.getStyleClass().removeAll("closedday");
            availableFemaleBedFX.getStyleClass().removeAll("closedday");
            femaleBedSpareFX.getStyleClass().removeAll("closedday");
            availableQuadRoomFX.getStyleClass().removeAll("closedday");
            availableTatamiRoomFX.getStyleClass().removeAll("closedday");
            availableBarrierFreeRoomFX.getStyleClass().removeAll("closedday");
            availableStaffRoomFX.getStyleClass().removeAll("closedday");
        }

        
        HashMap<String, Integer> rsvMap = ReservationsModel.calcRsvSummary();
        totalCheckInFX.setText(rsvMap.get("totalCheckIn").toString());
        remainedCheckInFX.setText(rsvMap.get("remainedCheckIn").toString());
        totalCheckOutFX.setText(rsvMap.get("totalCheckOut").toString());
        numOfCheckOutPeopleFX.setText(rsvMap.get("numOfCheckOutPeople").toString());
        totalMaleNumFX.setText(rsvMap.get("totalMaleNum").toString());
        totalFemaleNumFX.setText(rsvMap.get("totalFemaleNum").toString());
        totalMaleResulsNumFX.setText(rsvMap.get("totalMaleResultNum").toString());
        totalFemaleResulsNumFX.setText(rsvMap.get("totalFemaleResultNum").toString());
        plannedBreakfastNumFX.setText(rsvMap.get("plannedBreakfastNum").toString());
        plannedLunchNumFX.setText(rsvMap.get("plannedLunchNum").toString());
        plannedDinnerNumFX.setText(rsvMap.get("plannedDinnerNum").toString());
        paidBreakfastNumFX.setText(rsvMap.get("paidBreakfastNum").toString());
    }
    private Short checkShort(String str) {
        if ("".equals(str) || str == null) {
            return null;
        } else {
            return Short.parseShort(str);
        }
    }
    private Reservations newReservationFactory() {
        Reservations newReservation
                = new Reservations(ReservationsModel.fetchNewReservationsPK());
        newReservation.setReferencenumber(0);
        //滞在日時はチェックイン日付のDatePickerから取得
        newReservation.setDateofstay(java.sql.Date.valueOf(checkInDateFX.getValue()));
        newReservation.setReservationdate(java.sql.Date.valueOf(reservationDateFX.getValue()));
        newReservation.setTotalnights(checkShort(totalNightsFX.getEditor().getText()));
        newReservation.setNightspassed((short)1);
        newReservation.setGuestname(guestNameFX.getText());
        newReservation.setPhonenumber(phoneNumberFX.getText());
        newReservation.setMailaddress(mailAddressFX.getText());
        newReservation.setCountry(countryFX.getValue());
        newReservation.setPrefecture(prefectureFX.getValue());
        newReservation.setPlannedcheckintime(plannedcheckintimeFX.getText());
        newReservation.setComments(commentsFX.getText());
        newReservation.setCommentbackgroundcolor(
                commentbackgroundcolorFX.getValue().equals(Color.WHITE) ? ""
                : commentbackgroundcolorFX.getValue().toString());
        newReservation.setCommentforegroundcolor(
                commentforegroundcolorFX.getValue().equals(Color.BLACK) ? ""
                : commentforegroundcolorFX.getValue().toString());
        newReservation.setNumberofmale(checkShort(numberOfMaleFX.getEditor().getText()));
        newReservation.setNumberoffemale(checkShort(numberOfFemaleFX.getEditor().getText()));
        newReservation.setNumberofbaby(checkShort(numberOfBabyFX.getEditor().getText()));
        newReservation.setMalebed(checkShort(maleBedFX.getEditor().getText()));
        newReservation.setFemalebed(checkShort(femaleBedFX.getEditor().getText()));
        newReservation.setQuadroom(checkShort(quadRoomFX.getEditor().getText()));
        newReservation.setTatamiroom(checkShort(tatamiRoomFX.getEditor().getText()));
        newReservation.setBarrierfreeroom(checkShort(barrierFreeRoomFX.getEditor().getText()));
        newReservation.setStaffroom(checkShort(staffRoomFX.getEditor().getText()));
        newReservation.setLarge1820(large1820FX.selectedProperty().getValue());
        newReservation.setLarge1917(large1917FX.selectedProperty().getValue());
        newReservation.setLarge1808(large1808FX.selectedProperty().getValue());
        newReservation.setLarge1908(large1908FX.selectedProperty().getValue());
        newReservation.setLunch(checkShort(lunchFX.getEditor().getText()));
        newReservation.setDinner(checkShort(dinnerFX.getEditor().getText()));
        newReservation.setBreakfast(checkShort(breakfastFX.getEditor().getText()));
        newReservation.setStaffname(staffNameFX.getValue());
        newReservation.setReservationsource(reservationSourceFX.getValue());
        newReservation.setReservationstatus(reservationStatusValidString);
        newReservation.setUpdatetimestamp(Timestamp.valueOf(LocalDateTime.now()));
        newReservation.setChangecomment("");
        return newReservation;
    }
    @FXML private void saveNewReservation(){
        String str = new String();
        Boolean isInsertable = true;
        if(Objects.isNull(guestNameFX.getText()) || guestNameFX.getText().length()==0) {
            str = "予約者の氏名";
            isInsertable = false;
        } else if(Objects.isNull(checkInDateFX.getValue())) {
            str = "チェックイン日";
            isInsertable = false;
        } else if(Objects.isNull(totalNightsFX.getEditor().getText())
                || totalNightsFX.getEditor().getText().length()==0) {
            str = "泊数";
            isInsertable = false;
        } else if(Objects.isNull(numberOfMaleFX.getEditor().getText())
                || numberOfMaleFX.getEditor().getText().length()==0) {
            str = "男性の人数";
            isInsertable = false;
        } else if(Objects.isNull(numberOfFemaleFX.getEditor().getText())
                || numberOfFemaleFX.getEditor().getText().length()==0) {
            str = "女性の人数";
            isInsertable = false;
        } else if(Objects.isNull(numberOfBabyFX.getEditor().getText())
                || numberOfBabyFX.getEditor().getText().length()==0) {
            str = "幼児の人数";
            isInsertable = false;
        } else if(Objects.isNull(maleBedFX.getEditor().getText())
                || maleBedFX.getEditor().getText().length()==0) {
            str = "必要なMドミのベッド数";
            isInsertable = false;
        } else if(Objects.isNull(femaleBedFX.getEditor().getText())
                || femaleBedFX.getEditor().getText().length()==0) {
            str = "必要なFドミのベッド数";
            isInsertable = false;
        } else if(Objects.isNull(quadRoomFX.getEditor().getText())
                || quadRoomFX.getEditor().getText().length()==0) {
            str = "必要な4人部屋の数";
            isInsertable = false;
        } else if(Objects.isNull(barrierFreeRoomFX.getEditor().getText())
                || barrierFreeRoomFX.getEditor().getText().length()==0) {
            str = "必要なバリアフリー部屋の数";
            isInsertable = false;
        } else if(Objects.isNull(tatamiRoomFX.getEditor().getText())
                || tatamiRoomFX.getEditor().getText().length()==0) {
            str = "必要な和室の数";
            isInsertable = false;
        } else if(Objects.isNull(staffRoomFX.getEditor().getText())
                || staffRoomFX.getEditor().getText().length()==0) {
            str = "必要なスタッフ部屋の数";
            isInsertable = false;
        } else if(Objects.isNull(dinnerFX.getEditor().getText())
                || dinnerFX.getEditor().getText().length()==0) {
            str = "夕食の食数";
            isInsertable = false;
        } else if(Objects.isNull(breakfastFX.getEditor().getText())
                || breakfastFX.getEditor().getText().length()==0) {
            str = "朝食の食数";
            isInsertable = false;
        } else if(Objects.isNull(lunchFX.getEditor().getText())
                || lunchFX.getEditor().getText().length()==0) {
            str = "昼食の食数";
            isInsertable = false;
        } else if( ! Objects.isNull(reservationSourceFX.getEditor().getText()) &&
                reservationSourceFX.getEditor().getText().length()==0) {
            str = "予約方法";
            isInsertable = false;
        } else if( ! Objects.isNull(staffNameFX.getEditor().getText()) &&
                staffNameFX.getEditor().getText().length()==0) {
            str = "担当者名";
            isInsertable = false;
        }

        if( ! isInsertable) {
            MessageBox.show(Tcyh.stage, str + "は入力必須項目です。", "警告", MessageBox.OK);
            return;
        }
        if(Integer.decode(totalNightsFX.getEditor().getText()) <= 0) {
            MessageBox.show(Tcyh.stage, "泊数は1未満にはできません。", "警告", MessageBox.OK);
            return;
        }
        if( ! Objects.isNull(reservationDateFX.getValue())
                && reservationDateFX.getValue().isAfter(LocalDate.now())) {
            MessageBox.show(Tcyh.stage, "予約日に今日より先の日付は入力できません。\n"
                    + " ※どうしても必要な場合は一旦今日の日付で保存して、\n"
                    + "  予約内容の変更を行って下さい。", "警告", MessageBox.OK);
            return;
        }

        if(Objects.isNull(countryFX.getValue()))
            countryFX.setValue("");
        if(Objects.isNull(prefectureFX.getValue()))
            prefectureFX.setValue("");
        if(Objects.isNull(reservationSourceFX.getValue()))
            reservationSourceFX.setValue("");
        if(Objects.isNull(staffNameFX.getValue()))
            staffNameFX.setValue("");
        Reservations newReservation = newReservationFactory();
        int pressed = MessageBox.show(Tcyh.stage,"以下の内容で予約します。\nよろしいですか？\n"
                + "\n  予約番号: " + newReservation.getReservationnumber()
                + "\n  宿泊者氏名: " + newReservation.getGuestname()
                + "\n  チェックイン: " + new SimpleDateFormat(datePattern).format(newReservation.getDateofstay())
                + "\n  泊数: " + newReservation.getTotalnights()
                + "\n  人数:  (男性)" + newReservation.getNumberofmale() + "  (女性)" + newReservation.getNumberoffemale() + "  (幼児)" + newReservation.getNumberofbaby()
                ,"予約の確認"
                ,MessageBox.CANCEL|MessageBox.OK);
        if(pressed == MessageBox.OK){
            if(ReservationsModel.insertNewReservation(newReservation)) {
                MessageBox.show(Tcyh.stage, "新規の予約を保存しました。", "情報", MessageBox.OK);
                selectedDateFX.setValue(checkInDateFX.getValue());
                selectTab();
            } else
                MessageBox.show(Tcyh.stage, "予約の保存に失敗しました。\n処理を中止します。", "警告", MessageBox.OK);
        }
    }
    @FXML void selectTab() {
        long start = System.currentTimeMillis();
        //初回呼び出し時にDatePickerに値が設定されていないので設定し、
        //尚且つ無駄に2回呼び出されるのを回避するためすぐにreturnする。
        if(Objects.isNull(selectedDateFX.getValue())){
            selectedDateFX.setValue(LocalDate.now());
            return;
        }
        
        selectedDate = selectedDateFX.getValue();
        //BedOccupationは結果が0件だと困るのでtry-catchする
        try {
            BedOccupationModel.setBOToday(selectedDate);
        } catch (NoResultException nre) {
            MessageBox.show(Tcyh.stage,
                    "該当の日付は、システムが対応していない日付です。\n"
                    + "処理を中断します。",
                    "警告", MessageBox.OK);
            return;
        }
        ReservationsModel.setRsvListToday(selectedDate);
        ReservationsModel.setRsvListYesterday(selectedDate);
        showSummary(selectedDate);
        
        //朝食の提供ステータスによって、実績などの表示用TextAreaを灰色にする
        if( ! BedOccupationModel.getBOToday().getIsbreakfastservedtomorrow()) {
            plannedBreakfastNumFX.setStyle("-fx-background-color: DarkGray");
            paidBreakfastNumFX.setStyle("-fx-background-color: DarkGray");
            resultBreakfastNumFX.setStyle("-fx-background-color: DarkGray");
        } else {
            plannedBreakfastNumFX.setStyle("");
            paidBreakfastNumFX.setStyle("");
            resultBreakfastNumFX.setStyle("");
        }
        
        if(reservationsTabFX.isSelected()) {
            ReservationsController.getInstance()
                    .showAllReservationsOnThisDate(
                            showFirstNightFX.selectedProperty().get(),
                            showSecondOrLaterNightsFX.selectedProperty().get(),
                            showCanceledFX.selectedProperty().get()
                    );
        } else if(bedOccupationAndRoomingTabFX.isSelected()) {
            BedOccupationController.getInstance().loadBedOccupation();
            RoomingController.getInstance().loadRooming();
        }
        long end = System.currentTimeMillis();
        System.out.println("Time to selectTab : " + (end - start) + " msec");
    }
    void selectReservation(Reservations rsv){
        selectedDateFX.setValue(asLocalDate(rsv.getDateofstay()));
        reservationsTabFX.getTabPane().getSelectionModel().select(reservationsTabFX);
        ReservationsController.getInstance()
                .showAllDatesForThisReservation(Boolean.FALSE, rsv);
    }
    @FXML private void showReservationsToday() {
        selectedDateFX.setValue(LocalDate.now());
    }
    @FXML private void showReservationsNextDay() {
        selectedDateFX.setValue(selectedDate.plusDays(1));
    }
    @FXML private void showReservationsLastDay() {
        selectedDateFX.setValue(selectedDate.minusDays(1));
    }
    @FXML private void showReservationsNextWeek() {
        selectedDateFX.setValue(selectedDate.plusWeeks(1));
    }
    @FXML private void showReservationsLastWeek() {
        selectedDateFX.setValue(selectedDate.minusWeeks(1));
    }
    @FXML private void showReservationsNextMonth() {
        selectedDateFX.setValue(selectedDate.plusMonths(1));
    }
    @FXML private void showReservationsLastMonth() {
        selectedDateFX.setValue(selectedDate.minusMonths(1));
    }

    private void calcDatesAndNights() {
        LocalDate checkInDate = checkInDateFX.getValue();
        LocalDate checkOutDate = checkOutDateFX.getValue();
        String totalNights = totalNightsFX.getEditor().getText();
        if( ! (Objects.isNull(checkInDate) || checkInDate.toString().equals(""))
        && ! (Objects.isNull(totalNights) || totalNights.equals(""))
        && (Objects.isNull(checkOutDate) || checkOutDate.toString().equals(""))){
            calcCheckOutDate();
        } else if( ! (Objects.isNull(checkInDate) || checkInDate.toString().equals(""))
        && (Objects.isNull(totalNights) || totalNights.equals(""))
        && ! (Objects.isNull(checkOutDate) || checkOutDate.toString().equals(""))){
            calcNights();
        } else if((Objects.isNull(checkInDate) || checkInDate.toString().equals(""))
        && ! (Objects.isNull(totalNights) || totalNights.equals(""))
        && ! (Objects.isNull(checkOutDate) || checkOutDate.toString().equals(""))){
            calcCheckInDate();
        }
    }
    private void calcCheckInDate() {
        if( totalNightsFX.getEditor().getText().length() > 0)
            checkInDateFX.setValue(checkOutDateFX.getValue().minusDays(
                    Integer.decode(totalNightsFX.getEditor().getText())));
    }
    private void calcCheckOutDate() {
        if( totalNightsFX.getEditor().getText().length() > 0)
            checkOutDateFX.setValue(checkInDateFX.getValue().plusDays(
                    Integer.decode(totalNightsFX.getEditor().getText())));
    }
    private void calcNights() {
        Long days = ChronoUnit.DAYS.between(checkInDateFX.getValue(), checkOutDateFX.getValue());
        totalNightsFX.getEditor().setText(days.toString());
    }

    @FXML private EventHandler<KeyEvent> showComboBoxList() {
        return (KeyEvent ke) -> {
            TextField tf = (TextField)ke.getSource();
            if(ke.getCode().equals(KeyCode.DOWN)){
                if(tf.getParent() instanceof ComboBox) {
                    ComboBox cb = (ComboBox)tf.getParent();
                    cb.show();
                } else if(tf.getParent() instanceof DatePicker) {
                    DatePicker dp = (DatePicker)tf.getParent();
                    dp.show();
                }
            }
            ke.consume();
        };
    }
    @FXML private void changeSpareNum(MouseEvent me){
        BedOccupation bo = BedOccupationModel.getBOToday();

        Label maleLabel = new Label("Mドミ");
        maleLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> maleCB = new ComboBox<Integer>();
        maleCB.setEditable(true);
        maleCB.getEditor().focusedProperty().addListener(new ReplaceFullWithHalf(maleCB.getEditor()));
        maleCB.setConverter(new IntegerStringConverter());
        maleCB.setPrefSize(80, 25);
        maleCB.setItems(FXCollections.observableArrayList(0,1,2,3,4,5,6,7,8,9,10));
        maleCB.setValue(Long.valueOf(bo.getMDormList().stream().filter(str -> new USVMap(str).getRsvNum().equals(spareString)).count()).intValue());
        maleCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox maleHB = new HBox();
        maleHB.setSpacing(5.0);
        maleHB.setPadding(new Insets(5, 5, 5, 5));
        maleHB.getChildren().addAll(maleLabel, maleCB);

        Label femaleLabel = new Label("Fドミ");
        femaleLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> femaleCB = new ComboBox<Integer>();
        femaleCB.setEditable(true);
        femaleCB.setConverter(new IntegerStringConverter());
        femaleCB.focusedProperty().addListener(new ReplaceFullWithHalf(femaleCB.getEditor()));
        femaleCB.setPrefSize(80, 25);
        femaleCB.setItems(FXCollections.observableArrayList(0,1,2,3,4,5,6,7,8,9,10));
        femaleCB.setValue(Long.valueOf(bo.getFDormList().stream().filter(str -> new USVMap(str).getRsvNum().equals(spareString)).count()).intValue());
        femaleCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox femaleHB = new HBox();
        femaleHB.setSpacing(5.0);
        femaleHB.setPadding(new Insets(5, 5, 5, 5));
        femaleHB.getChildren().addAll(femaleLabel, femaleCB);

        Label quadLabel = new Label("4人部屋");
        quadLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> quadCB = new ComboBox<Integer>();
        quadCB.setEditable(true);
        quadCB.setConverter(new IntegerStringConverter());
        quadCB.focusedProperty().addListener(new ReplaceFullWithHalf(quadCB.getEditor()));
        quadCB.setPrefSize(80, 25);
        quadCB.setItems(FXCollections.observableArrayList(0,1,2,3,4,5,6,7,8,9,10));
        quadCB.setValue(Long.valueOf(bo.getQuadroomList().stream().filter(str -> new USVMap(str).getRsvNum().equals(spareString)).count()).intValue());
        quadCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox quadHB = new HBox();
        quadHB.setSpacing(5.0);
        quadHB.setPadding(new Insets(5, 5, 5, 5));
        quadHB.getChildren().addAll(quadLabel, quadCB);

        Label barrierFreeLabel = new Label("バリアフリー部屋");
        barrierFreeLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> barrierFreeCB = new ComboBox<Integer>();
        barrierFreeCB.setEditable(true);
        barrierFreeCB.setConverter(new IntegerStringConverter());
        barrierFreeCB.focusedProperty().addListener(new ReplaceFullWithHalf(barrierFreeCB.getEditor()));
        barrierFreeCB.setPrefSize(80, 25);
        barrierFreeCB.setItems(FXCollections.observableArrayList(0,1,2));
        barrierFreeCB.setValue(
            (contains(bo.getB1801List(), spareString) ? 1 : 0) +
            (contains(bo.getB1811List(), spareString) ? 1 : 0)
        );
        barrierFreeCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox barrierFreeHB = new HBox();
        barrierFreeHB.setSpacing(5.0);
        barrierFreeHB.setPadding(new Insets(5, 5, 5, 5));
        barrierFreeHB.getChildren().addAll(barrierFreeLabel, barrierFreeCB);

        Label tatamiLabel = new Label("和室");
        tatamiLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> tatamiCB = new ComboBox<Integer>();
        tatamiCB.setEditable(true);
        tatamiCB.setConverter(new IntegerStringConverter());
        tatamiCB.focusedProperty().addListener(new ReplaceFullWithHalf(tatamiCB.getEditor()));
        tatamiCB.setPrefSize(80, 25);
        tatamiCB.setItems(FXCollections.observableArrayList(0,1,2));
        tatamiCB.setValue(
            (contains(bo.getT1807List(), spareString) ? 1 : 0) +
            (contains(bo.getT1819List(), spareString) ? 1 : 0)
        );
        tatamiCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox tatamiHB = new HBox();
        tatamiHB.setSpacing(5.0);
        tatamiHB.setPadding(new Insets(5, 5, 5, 5));
        tatamiHB.getChildren().addAll(tatamiLabel, tatamiCB);

        Label staffLabel = new Label("スタッフ室");
        staffLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> staffCB = new ComboBox<Integer>();
        staffCB.setEditable(true);
        staffCB.setConverter(new IntegerStringConverter());
        staffCB.focusedProperty().addListener(new ReplaceFullWithHalf(staffCB.getEditor()));
        staffCB.setPrefSize(80, 25);
        staffCB.setItems(FXCollections.observableArrayList(0,1,2));
        staffCB.setValue(
            (contains(bo.getStaffLList(), spareString) ? 1 : 0) +
            (contains(bo.getStaffRList(), spareString) ? 1 : 0)
        );
        staffCB.getEditor().setOnKeyPressed(showComboBoxList());
        HBox staffHB = new HBox();
        staffHB.setSpacing(5.0);
        staffHB.setPadding(new Insets(5, 5, 5, 5));
        staffHB.getChildren().addAll(staffLabel, staffCB);

        Label l1808Label = new Label("1808");
        l1808Label.setPadding(new Insets(5, 0, 0, 0));
        CheckBox l1808CB = new CheckBox();
        l1808CB.setPrefSize(80, 25);
        l1808CB.setSelected(bo.getL1808List().isEmpty()? false : bo.getL1808List().get(0).startsWith(spareString));
        HBox l1808HB = new HBox();
        l1808HB.setSpacing(5.0);
        l1808HB.setPadding(new Insets(5, 5, 5, 5));
        l1808HB.getChildren().addAll(l1808Label, l1808CB);

        Label l1820Label = new Label("1820");
        l1820Label.setPadding(new Insets(5, 0, 0, 0));
        CheckBox l1820CB = new CheckBox();
        l1820CB.setPrefSize(80, 25);
        l1820CB.setSelected(bo.getL1820List().isEmpty()? false : bo.getL1820List().get(0).startsWith(spareString));
        HBox l1820HB = new HBox();
        l1820HB.setSpacing(5.0);
        l1820HB.setPadding(new Insets(5, 5, 5, 5));
        l1820HB.getChildren().addAll(l1820Label, l1820CB);

        Label l1908Label = new Label("1908");
        l1908Label.setPadding(new Insets(5, 0, 0, 0));
        CheckBox l1908CB = new CheckBox();
        l1908CB.setPrefSize(80, 25);
        l1908CB.setSelected(bo.getL1908List().isEmpty()? false : bo.getL1908List().get(0).startsWith(spareString));
        HBox l1908HB = new HBox();
        l1908HB.setSpacing(5.0);
        l1908HB.setPadding(new Insets(5, 5, 5, 5));
        l1908HB.getChildren().addAll(l1908Label, l1908CB);

        Label l1917Label = new Label("1917");
        l1917Label.setPadding(new Insets(5, 0, 0, 0));
        CheckBox l1917CB = new CheckBox();
        l1917CB.setPrefSize(80, 25);
        l1917CB.setSelected(bo.getL1917List().isEmpty()? false : bo.getL1917List().get(0).startsWith(spareString));
        HBox l1917HB = new HBox();
        l1917HB.setSpacing(5.0);
        l1917HB.setPadding(new Insets(5, 5, 5, 5));
        l1917HB.getChildren().addAll(l1917Label, l1917CB);

        Button applyBtn = new Button("変更");

        VBox VB = new VBox(5);
        VB.getChildren().addAll(maleHB, femaleHB, quadHB, barrierFreeHB, tatamiHB, staffHB, l1808HB, l1820HB, l1908HB, l1917HB, applyBtn);
        VBox.setMargin(applyBtn, new Insets(5, 5, 5, 5));
        PopOver popover = new PopOver(VB);
        popover.setAutoHide(true);
        popover.setArrowLocation(PopOver.ArrowLocation.TOP_LEFT);
        popover.show(maleBedSpareFX,me.getScreenX(),me.getScreenY()+20);

        applyBtn.setOnAction((ActionEvent e) -> {
            BedOccupationModel.updateSpareNumber(
                    maleCB.getValue(),
                    femaleCB.getValue(),
                    quadCB.getValue(),
                    barrierFreeCB.getValue(),
                    tatamiCB.getValue(),
                    staffCB.getValue(),
                    l1808CB.selectedProperty().getValue(),
                    l1820CB.selectedProperty().getValue(),
                    l1908CB.selectedProperty().getValue(),
                    l1917CB.selectedProperty().getValue()
                    );
            selectTab();
            popover.hide();
            e.consume();
        });
        me.consume();
    }
    @FXML private void changeQuadDormFixed(MouseEvent me){
        Label maleLabel = new Label("固定するMドミ数");
        Label femaleLabel = new Label("固定するFドミ数");
        maleLabel.setPadding(new Insets(5, 0, 0, 0));
        femaleLabel.setPadding(new Insets(5, 0, 0, 0));
        ComboBox<Integer> maleCB = new ComboBox<Integer>();
        ComboBox<Integer> femaleCB = new ComboBox<Integer>();
        maleCB.setEditable(true);
        maleCB.setConverter(new IntegerStringConverter());
        maleCB.focusedProperty().addListener(new ReplaceFullWithHalf(maleCB.getEditor()));
        maleCB.setPrefSize(80, 25);
        maleCB.setItems(FXCollections.observableArrayList(0,1,2,3,4,5,6,7,8,9,10));
        maleCB.setValue(Long.valueOf(BedOccupationModel.getBOToday().getQuadroomList()
                .stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(mdormFixedString))
                .count())
                .intValue());
        femaleCB.setEditable(true);
        femaleCB.setConverter(new IntegerStringConverter());
        femaleCB.focusedProperty().addListener(new ReplaceFullWithHalf(femaleCB.getEditor()));
        femaleCB.setPrefSize(80, 25);
        femaleCB.setItems(FXCollections.observableArrayList(0,1,2,3,4,5,6,7,8,9,10));
        femaleCB.setValue(Long.valueOf(BedOccupationModel.getBOToday().getQuadroomList()
                .stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(fdormFixedString))
                .count())
                .intValue());

        HBox maleHB = new HBox();
        maleHB.setSpacing(5.0);
        maleHB.setPadding(new Insets(5, 5, 5, 5));
        maleHB.getChildren().addAll(maleLabel, maleCB);

        HBox femaleHB = new HBox();
        femaleHB.setSpacing(5.0);
        femaleHB.setPadding(new Insets(5, 5, 5, 5));
        femaleHB.getChildren().addAll(femaleLabel, femaleCB);

        Button applyBtn = new Button("変更");

        VBox VB = new VBox(5);
        VB.getChildren().addAll(maleHB, femaleHB, applyBtn);
        VBox.setMargin(applyBtn, new Insets(5, 5, 5, 5));
        PopOver popover = new PopOver(VB);
        popover.setAutoHide(true);
        popover.setArrowLocation(PopOver.ArrowLocation.TOP_LEFT);
        popover.show(maleBedSpareFX,me.getScreenX(),me.getScreenY()+20);

        applyBtn.setOnAction((ActionEvent e) -> {
            BedOccupationModel.updateQuadDormFixed(maleCB.getValue(),femaleCB.getValue());
            selectTab();
            popover.hide();
            e.consume();
        });
        me.consume();
    }
    @FXML private void manageBreakfastButtonAction(MouseEvent me) {
        LocalDate dateTomorrow = selectedDateFX.getValue().plusDays(1);
        Label resultLabel = new Label(
                dateTomorrow.getMonthValue() + "月" +
                dateTomorrow.getDayOfMonth() + "日" +
                "の朝食の実績数:");
        Spinner<Integer> resultSpinner = new Spinner(0,200,Integer.decode(resultBreakfastNumFX.getText()));
        resultSpinner.focusedProperty().addListener(new ReplaceFullWithHalf(resultSpinner));
        resultSpinner.setPrefWidth(80);
        Button resultBtn = new Button("更新");
        HBox resultHB = new HBox(resultLabel, resultSpinner, resultBtn);
        resultHB.setAlignment(Pos.CENTER_LEFT);
        resultHB.setSpacing(5);
        
        Label changeServingLabel = new Label("朝食提供の可/不可を変更する:");
        Button changeServingBtn = new Button("変更");
        HBox changeServingHB = new HBox(changeServingLabel, changeServingBtn);
        changeServingHB.setAlignment(Pos.CENTER_LEFT);
        changeServingHB.setSpacing(5);

        VBox vb = new VBox(resultHB, changeServingHB);
        vb.setSpacing(10);
        vb.setPadding(new Insets(10,10,10,10));
        AnchorPane ap = new AnchorPane(vb);
        
        PopOver popover = new PopOver(ap);
        popover.setAutoHide(true);
        popover.setArrowLocation(PopOver.ArrowLocation.TOP_LEFT);
        popover.show(maleBedSpareFX,me.getScreenX(),me.getScreenY()+20);

        resultBtn.setOnAction(ae -> {
            if(Objects.isNull(resultSpinner.getEditor().getText())) {
                MessageBox.show(Tcyh.stage, "実績数が入力されていません。", "エラー", MessageBox.OK);
                return;
            } else if(resultSpinner.getEditor().getText().replaceAll("[0-9]", "").length() > 0) {
                MessageBox.show(Tcyh.stage, "実績数が入力されていないか、数値以外が含まれています。", "エラー", MessageBox.OK);
                return;
            }
            BedOccupationModel.getInstance().updateBreakfastResult(Short.decode(resultSpinner.getEditor().getText()));
            MessageBox.show(Tcyh.stage, "実績数を"+resultSpinner.getEditor().getText()+"に変更しました。", "変更完了", MessageBox.OK);
            selectTab();
            popover.hide();
            ae.consume();
        });
        
        changeServingBtn.setOnAction(ae -> {
            final Boolean isbreakfastserved =
                    BedOccupationModel.getBOToday().getIsbreakfastservedtomorrow();
            final Integer val = MessageBox.show(
                    Tcyh.stage,
                    "現在、翌日("
                    + dateTomorrow.getMonthValue() + "月"
                    + dateTomorrow.getDayOfMonth() + "日"
                    +")の朝食提供「"
                    + (isbreakfastserved ? "可" : "不可")
                    + "」になっています。\n朝食提供「"
                    + (isbreakfastserved ? "不可" : "可")
                    + "」に変更しますか？",
                    "朝食提供の可否",
                    MessageBox.YES|MessageBox.NO);
            if (val.equals(MessageBox.YES)) {
                BedOccupationModel.changeBreakfastStatus( ! isbreakfastserved);
                MessageBox.show(Tcyh.stage,
                        "朝食提供「"
                        + ( ! isbreakfastserved ? "可" : "不可")
                        + "」に変更しました。",
                        "変更", MessageBox.OK);
                selectTab();
            }
            popover.hide();
            ae.consume();
        });
        me.consume();
    }

    @FXML private void changeCommentColors() {
        if( commentsFX.isNeedsLayout())
            return;
        if( ! Objects.isNull(commentbackgroundcolorFX.getValue())) {
            Region region = (Region)commentsFX.lookup( ".content" );
            region.setStyle(
                    "-fx-background-color: "
                    + commentbackgroundcolorFX.getValue()
                            .toString().replace("0x", "#"));
        } 
        if( ! Objects.isNull(commentforegroundcolorFX.getValue()))
            commentsFX.setStyle("-fx-text-fill: "
                    + commentforegroundcolorFX.getValue()
                            .toString().replace("0x", "#"));
    }
    @FXML private void loadTSVData() {
        ReservationsModel.parseTSVData(tempTextAreaFX.getText().split("\n"));
    }
    @FXML private void horizontalScroll(ScrollEvent event) {
        if( ! (event.getSource() instanceof ScrollPane))
            return;
        ScrollPane sp = (ScrollPane)event.getSource();
        System.out.println("sp.getHvalue(): " + sp.getHvalue());
        if (event.getDeltaY() > 0) {
            sp.setHvalue(sp.getHvalue() == sp.getHmin() ? sp.getHmin() : sp.getHvalue()-1);
        } else {
            sp.setHvalue(sp.getHvalue() == sp.getHmax() ? sp.getHmax() : sp.getHvalue()+1);
        }
    }
}
