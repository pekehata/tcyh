/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import tcyh.utils.USVMap;
import static tcyh.Constants.*;
import static tcyh.utils.ListUtils.*;

/**
 *
 * @author masanori
 */
public class BedOccupationModel {
    private static final BedOccupationModel instance = new BedOccupationModel();
    protected static BedOccupationModel getInstance() {
        return instance;
    }

    private static BedOccupation boToday;
    public static BedOccupation getBOToday(){
        return BedOccupationModel.boToday;
    };
    public static void setBOToday(LocalDate ld){
        BedOccupationModel.boToday = fetchBO(ld);
    }

    protected static HashMap<String, Integer> calcBOSummary() {
        return calcBOSummary(getBOToday());
    }
    protected static HashMap<String, Integer> calcBOSummary(BedOccupation bo) {
        Integer mdormUsed = bo.getMDormList().size() - getBlankNum(bo.getMDormList());
        Integer fdormUsed = bo.getFDormList().size() - getBlankNum(bo.getFDormList());
        Integer quadRoomUsed = bo.getQuadroomList().size() - getBlankNum(bo.getQuadroomList());
        Integer mdormSpare = Long.valueOf(bo.getMDormList().stream()
                .filter((str) -> new USVMap(str).getRsvNum().equals(spareString))
                .count()).intValue();
        Integer fdormSpare = Long.valueOf(bo.getFDormList().stream()
                .filter((str) -> new USVMap(str).getRsvNum().equals(spareString))
                .count()).intValue();
        Integer barrierFreeRoomUsed =
                (isFirstEmpty(bo.getB1801List()) ? 0 : 1) +
                (isFirstEmpty(bo.getB1811List()) ? 0 : 1);
        Integer tatamiRoomUsed =
                (isFirstEmpty(bo.getT1807List()) ? 0 : 1) +
                (isFirstEmpty(bo.getT1819List()) ? 0 : 1);
        Integer staffRoomUsed =
                (isFirstEmpty(bo.getStaffLList()) ? 0 : 1) +
                (isFirstEmpty(bo.getStaffRList()) ? 0 : 1);
        Integer l1808Available = isFirstEmpty(bo.getL1808List()) ? 1 : 0;
        Integer l1820Available = isFirstEmpty(bo.getL1820List()) ? 1 : 0;
        Integer l1908Available = isFirstEmpty(bo.getL1908List()) ? 1 : 0;
        Integer l1917Available = isFirstEmpty(bo.getL1917List()) ? 1 : 0;
        Integer l1808Type = isFirstEmpty(bo.getL1808List()) ? 0 :
                Integer.decode(new USVMap(bo.getL1808List().get(0)).getRsvNum());
        Integer l1820Type = isFirstEmpty(bo.getL1820List()) ? 0 :
                Integer.decode(new USVMap(bo.getL1820List().get(0)).getRsvNum());
        Integer l1908Type = isFirstEmpty(bo.getL1908List()) ? 0 :
                Integer.decode(new USVMap(bo.getL1908List().get(0)).getRsvNum());
        Integer l1917Type = isFirstEmpty(bo.getL1917List()) ? 0 :
                Integer.decode(new USVMap(bo.getL1917List().get(0)).getRsvNum());
        Integer resultBreakfastNum =
                Objects.isNull(bo.getBreakfastresult()) ? 0 :
                bo.getBreakfastresult().intValue();

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("availableMaleBed", bo.getMDormList().size() - mdormUsed);
        map.put("maleBedSpare", mdormSpare);
        map.put("availableFemaleBed", bo.getFDormList().size() - fdormUsed);
        map.put("femaleBedSpare", fdormSpare);
        map.put("availableQuadRoom", bo.getQuadroomList().size() - quadRoomUsed);
        map.put("availableTatamiRoom", 2 - tatamiRoomUsed);
        map.put("availableBarrierFreeRoom", 2 - barrierFreeRoomUsed);
        map.put("availableStaffRoom", 2 - staffRoomUsed);
        map.put("l1808Available", l1808Available);
        map.put("l1820Available", l1820Available);
        map.put("l1908Available", l1908Available);
        map.put("l1917Available", l1917Available);
        map.put("l1808Type", l1808Type);
        map.put("l1820Type", l1820Type);
        map.put("l1908Type", l1908Type);
        map.put("l1917Type", l1917Type);
        map.put("resultBreakfastNum", resultBreakfastNum);
        return map;
    }
    public static BedOccupation fetchBO(Date date) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("BedOccupation.findByDateofstay", BedOccupation.class);
        tquery.setParameter("dateofstay", date);
        long start = System.currentTimeMillis();
        BedOccupation bo = (BedOccupation) tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getSingleResult();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return bo;
    }
    public static BedOccupation fetchBO(LocalDate ld) {
        return fetchBO(java.sql.Date.valueOf(ld));
    }
    protected static ObservableList<String> fetchAllClosedDayList() {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        TypedQuery tquery = em.createNamedQuery("BedOccupation.findByIsclosedday", BedOccupation.class);
        tquery.setParameter("isclosedday", true);
        long start = System.currentTimeMillis();
        List<BedOccupation> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return FXCollections.observableArrayList(list.stream()
                .map(bo -> new SimpleDateFormat("YYYY月MM月dd日(E)").format(bo.getDateofstay()))
                .collect(Collectors.toList()));
    }
    protected static ObservableList<BedOccupation> fetchBedOccupationByMonth(LocalDate firstDateOfThisMonth, LocalDate lastDateOfThisMonth) {
        //当月初日から当月最終日までのReservationsを取得する
        Date startDate = java.sql.Date.valueOf(firstDateOfThisMonth.minusDays(1));
        Date endDate = java.sql.Date.valueOf(lastDateOfThisMonth.minusDays(1));

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        TypedQuery tquery = em.createNamedQuery("BedOccupation.findByYearMonth", BedOccupation.class);
        tquery.setParameter("start", startDate);
        tquery.setParameter("end", endDate);
        List<BedOccupation> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<BedOccupation> data = FXCollections.observableArrayList(list);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static ObservableList<BedOccupation> fetchBedOccupationBySpecificPeriod(Date startDate, Date endDate) {
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        long start = System.currentTimeMillis();
        TypedQuery tquery = em.createNamedQuery("BedOccupation.findBySpecificPeriod", BedOccupation.class);
        tquery.setParameter("start", startDate);
        tquery.setParameter("end", endDate);
        List<BedOccupation> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        ObservableList<BedOccupation> data = FXCollections.observableArrayList(list);
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        return data;
    }
    protected static Boolean commitBO(BedOccupation bo){
        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        try {
            em.clear();
            tx.begin();
            em.merge(bo);
            tx.commit();
            em.clear();
            long start = System.currentTimeMillis();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[2].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        } catch (Exception ex) {
            Logger.getLogger(BedOccupationModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            return false;
        }
        return true;
    }
    protected static BedOccupation addRsv(Reservations rsv) {
        USVMap um = new USVMap(rsv.getReservationnumber());
        Date dateOfStay = rsv.getDateofstay();
        Integer maleBedNeeded = rsv.getMalebed().intValue();
        Integer femaleBedNeeded = rsv.getFemalebed().intValue();
        Integer quadRoomNeeded = rsv.getQuadroom().intValue();
        Integer tatamiRoomNeeded = rsv.getTatamiroom().intValue();
        Integer barrierFreeRoomNeeded = rsv.getBarrierfreeroom().intValue();
        Integer staffRoomNeeded = rsv.getStaffroom().intValue();

        BedOccupation bo = fetchBO(dateOfStay);

        if (rsv.getLarge1820()) {
            um.putRoomType("1820");
            bo = nonQuadRoomToPrivate(bo, "1820", um.toString());
        }
        if (rsv.getLarge1917()) {
            um.putRoomType("1917");
            bo = nonQuadRoomToPrivate(bo, "1917", um.toString());
        }
        if (rsv.getLarge1808()) {
            um.putRoomType("1808");
            bo = nonQuadRoomToPrivate(bo, "1808", um.toString());
        }
        if (rsv.getLarge1908()) {
            um.putRoomType("1908");
            bo = nonQuadRoomToPrivate(bo, "1908", um.toString());
        }

        //必要なベッド数がなかったら4人部屋をドミに割り当て
        bo =  quadEmptyToDorm(bo, maleBedNeeded, mdormString);
        bo =  quadEmptyToDorm(bo, femaleBedNeeded, fdormString);

        um.putRoomType("mdorm");
        bo.setMDormList(replaceMultipleBlank(bo.getMDormList(), um, maleBedNeeded));
        um.putRoomType("fdorm");
        bo.setFDormList(replaceMultipleBlank(bo.getFDormList(), um, femaleBedNeeded));
        um.putRoomType("quad");
        bo.setQuadroomList(replaceMultipleBlank(bo.getQuadroomList(), um, quadRoomNeeded));

        if(0 < barrierFreeRoomNeeded && isFirstEmpty(bo.getB1801List())) {
            um.putRoomType("1801");
            bo.setB1801List(replaceSingleBlank(bo.getB1801List(), um));
            barrierFreeRoomNeeded--;
        }
        if(0 < barrierFreeRoomNeeded && isFirstEmpty(bo.getB1811List())) {
            um.putRoomType("1811");
            bo.setB1811List(replaceSingleBlank(bo.getB1811List(), um));
            barrierFreeRoomNeeded--;
        }
        if(0 < tatamiRoomNeeded && isFirstEmpty(bo.getT1807List())) {
            um.putRoomType("1807");
            bo.setT1807List(replaceSingleBlank(bo.getT1807List(), um));
            tatamiRoomNeeded--;
        }
        if(0 < tatamiRoomNeeded && isFirstEmpty(bo.getT1819List())) {
            um.putRoomType("1819");
            bo.setT1819List(replaceSingleBlank(bo.getT1819List(), um));
            tatamiRoomNeeded--;
        }
        if(0 < staffRoomNeeded && isFirstEmpty(bo.getStaffLList())) {
            um.putRoomType("staffl");
            bo.setStaffLList(replaceSingleBlank(bo.getStaffLList(), um));
            staffRoomNeeded--;
        }
        if(0 < staffRoomNeeded && isFirstEmpty(bo.getStaffRList())) {
            um.putRoomType("staffr");
            bo.setStaffRList(replaceSingleBlank(bo.getStaffRList(), um));
            staffRoomNeeded--;
        }
        return bo;
    }
    protected static BedOccupation removeRsv(Reservations rsv) {
        final Date dateOfStay = rsv.getDateofstay();
        BedOccupation bo = fetchBO(dateOfStay);
        Integer rsvNum = rsv.getReservationnumber();

        bo.setMDormList(removeAll(bo.getMDormList(), rsvNum.toString()));
        bo.setFDormList(removeAll(bo.getFDormList(), rsvNum.toString()));
        bo.setQuadroomList(removeAll(bo.getQuadroomList(), rsvNum.toString()));
        bo.setB1801List(removeAll(bo.getB1801List(), rsvNum.toString()));
        bo.setB1811List(removeAll(bo.getB1811List(), rsvNum.toString()));
        bo.setT1807List(removeAll(bo.getT1807List(), rsvNum.toString()));
        bo.setT1819List(removeAll(bo.getT1819List(), rsvNum.toString()));
        bo.setStaffLList(removeAll(bo.getStaffLList(), rsvNum.toString()));
        bo.setStaffRList(removeAll(bo.getStaffRList(), rsvNum.toString()));
        bo.setL1808List(removeAll(bo.getL1808List(), rsvNum.toString()));
        bo.setL1820List(removeAll(bo.getL1820List(), rsvNum.toString()));
        bo.setL1908List(removeAll(bo.getL1908List(), rsvNum.toString()));
        bo.setL1917List(removeAll(bo.getL1917List(), rsvNum.toString()));

        //4人部屋のドミを空室に戻す処理
        bo = deleteUnusedQuadDorm(bo);
        return bo;
    }
    protected static BedOccupation changeBedRoomCount(Reservations rsvOld, Reservations rsvNew) {
        String rsvNumStr = rsvNew.getReservationnumber().toString();
        Integer mdormDiff = rsvNew.getMalebed() - rsvOld.getMalebed();
        Integer fdormDiff = rsvNew.getFemalebed() - rsvOld.getFemalebed();
        Integer quadroomDiff = rsvNew.getQuadroom() - rsvOld.getQuadroom();
        Integer barrierfreeroomDiff = rsvNew.getBarrierfreeroom() - rsvOld.getBarrierfreeroom();
        Integer tatamiroomDiff = rsvNew.getTatamiroom() - rsvOld.getTatamiroom();
        Integer staffroomDiff = rsvNew.getStaffroom() - rsvOld.getStaffroom();
        Integer l1808Diff = (rsvNew.getLarge1808() ? 1 : 0) - (rsvOld.getLarge1808() ? 1 : 0);
        Integer l1820Diff = (rsvNew.getLarge1820() ? 1 : 0) - (rsvOld.getLarge1820() ? 1 : 0);
        Integer l1908Diff = (rsvNew.getLarge1908() ? 1 : 0) - (rsvOld.getLarge1908() ? 1 : 0);
        Integer l1917Diff = (rsvNew.getLarge1917() ? 1 : 0) - (rsvOld.getLarge1917() ? 1 : 0);

        BedOccupation bo = fetchBO(rsvNew.getDateofstay());

        if(mdormDiff > 0) {
            USVMap um = new USVMap(rsvNumStr);
            um.putRoomType("mdorm");
            bo = quadEmptyToDorm(bo, mdormDiff, mdormString);
            bo.setMDormList(replaceMultipleBlank(
                    bo.getMDormList(), um, mdormDiff));
        }
        for (;mdormDiff < 0; mdormDiff++)
            bo.setMDormList(remove(bo.getMDormList(), rsvNumStr));

        if(fdormDiff > 0) {
            USVMap um = new USVMap(rsvNumStr);
            um.putRoomType("fdorm");
            bo = quadEmptyToDorm(bo, fdormDiff, fdormString);
            bo.setFDormList(replaceMultipleBlank(
                    bo.getFDormList(), um, fdormDiff));
        }
        for (;fdormDiff < 0; fdormDiff++)
            bo.setFDormList(remove(bo.getFDormList(), rsvNumStr));

        bo = deleteUnusedQuadDorm(bo);

        if(quadroomDiff > 0) {
            USVMap um = new USVMap(rsvNumStr);
            um.putRoomType("quad");
            bo.setQuadroomList(replaceMultipleBlank(bo.getQuadroomList(), um, quadroomDiff));
        }
        for (;quadroomDiff < 0; quadroomDiff++)
            bo.setQuadroomList(remove(bo.getQuadroomList(), rsvNumStr));

        counter = barrierfreeroomDiff;
        bo.setB1801List(addOrRemoveRsv(bo.getB1801List(), rsvNumStr, "1801"));
        bo.setB1811List(addOrRemoveRsv(bo.getB1811List(), rsvNumStr, "1811"));

        counter = tatamiroomDiff;
        bo.setT1807List(addOrRemoveRsv(bo.getT1807List(), rsvNumStr, "1807"));
        bo.setT1819List(addOrRemoveRsv(bo.getT1819List(), rsvNumStr, "1819"));

        counter = staffroomDiff;
        bo.setStaffLList(addOrRemoveRsv(bo.getStaffLList(), rsvNumStr, "staffl"));
        bo.setStaffRList(addOrRemoveRsv(bo.getStaffRList(), rsvNumStr, "staffr"));

        counter = l1808Diff;
        if(counter > 0 )
            bo = nonQuadDormToEmpty(bo,"1808");
        bo.setL1808List(addOrRemoveRsv(bo.getL1808List(), rsvNumStr, "1808"));

        counter = l1820Diff;
        if(counter > 0 )
            bo = nonQuadDormToEmpty(bo,"1820");
        bo.setL1820List(addOrRemoveRsv(bo.getL1820List(), rsvNumStr, "1820"));

        counter = l1908Diff;
        if(counter > 0 )
            bo = nonQuadDormToEmpty(bo,"1908");
        bo.setL1908List(addOrRemoveRsv(bo.getL1908List(), rsvNumStr, "1908"));

        counter = l1917Diff;
        if(counter > 0 )
            bo = nonQuadDormToEmpty(bo,"1917");
        bo.setL1917List(addOrRemoveRsv(bo.getL1917List(), rsvNumStr, "1917"));
        return bo;
    }
    private static List<String> addOrRemoveRsv(List<String> list, String rsvNumStr, String roomType) {
        if(counter > 0 && ( list.isEmpty() || isFirstEmpty(list) )) {
            USVMap um = new USVMap(rsvNumStr);
            um.putRoomType(roomType);
            list = replaceFirstBlank(list, um.toString());
            counter--;
        }
        if (counter < 0 && ! list.isEmpty()
                && list.get(0).startsWith(rsvNumStr)) {
            list = remove(list, rsvNumStr);
            counter++;
        }
        return list;
    }

    protected static void swapContents(BedOccupation bo, String roomNumber) {
        ObservableList<Reservations> rsvList = ReservationsModel.getRsvListToday();
        List<String> tempList;
        Reservations rsvTemp;
        Boolean changed = Boolean.FALSE;
        switch(roomNumber){
            case "1801" :
            case "1811" :
                tempList = bo.getB1801List();
                bo.setB1801List(bo.getB1811List());
                bo.setB1811List(tempList);
                bo.setB1801List(bo.getB1801List().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("1811");
                                return um.toString();
                        }).collect(Collectors.toList()));
                bo.setB1811List(bo.getB1811List().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("1801");
                                return um.toString();
                        }).collect(Collectors.toList()));
                break;
            case "1807" :
            case "1819" :
                tempList = bo.getT1807List();
                bo.setT1807List(bo.getT1819List());
                bo.setT1819List(tempList);
                bo.setT1807List(bo.getT1807List().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("1819");
                                return um.toString();
                        }).collect(Collectors.toList()));
                bo.setT1819List(bo.getT1819List().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("1807");
                                return um.toString();
                        }).collect(Collectors.toList()));
                break;
            case "staffl" :
            case "staffr" :
                tempList = bo.getStaffLList();
                bo.setStaffLList(bo.getStaffRList());
                bo.setStaffRList(tempList);
                bo.setStaffLList(bo.getStaffLList().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("staffr");
                                return um.toString();
                        }).collect(Collectors.toList()));
                bo.setStaffRList(bo.getStaffRList().stream()
                        .map(str -> {
                                USVMap um = new USVMap(str);
                                um.putRoomType("staffl");
                                return um.toString();
                        }).collect(Collectors.toList()));
                break;
            case "1808" :
            case "1820" :
                USVMap um1808 = new USVMap(
                        Objects.isNull(bo.getL1808List()) || bo.getL1808List().isEmpty()
                                ? "" : bo.getL1808List().get(0));
                USVMap um1820 = new USVMap(
                        Objects.isNull(bo.getL1820List()) || bo.getL1820List().isEmpty()
                                ? "" : bo.getL1820List().get(0));
                List<Reservations> list1808 = rsvList.filtered(rsv -> um1808.getRsvNum().equals(
                        rsv.getReservationnumber().toString()));
                List<Reservations> list1820 = rsvList.filtered(rsv -> um1820.getRsvNum().equals(
                        rsv.getReservationnumber().toString()));
                if(list1808.size() > 1 || list1820.size() > 1) {
                    MessageBox.show(Tcyh.stage,
                            "交換対象の予約の予約番号が重複しています。\n処理を中止します。",
                            "警告",
                            MessageBox.OK);
                    return;
                }
                if( ! list1808.isEmpty()) {
                    rsvTemp = list1808.get(0);
                    Boolean temp1808 = rsvTemp.getLarge1808();
                    rsvTemp.setLarge1808(rsvTemp.getLarge1820());
                    rsvTemp.setLarge1820(temp1808);
                    ReservationsModel.commitRsv(rsvTemp);
                    if(rsvTemp.getTotalnights() > 1)
                        changed = Boolean.TRUE;
                }
                if( ! list1820.isEmpty()) {
                    rsvTemp = list1820.get(0);
                    Boolean temp1820 = rsvTemp.getLarge1820();
                    rsvTemp.setLarge1820(rsvTemp.getLarge1808());
                    rsvTemp.setLarge1808(temp1820);
                    ReservationsModel.commitRsv(rsvTemp);
                    if(rsvTemp.getTotalnights() > 1)
                        changed = Boolean.TRUE;
                }
                tempList = bo.getL1808List();
                bo.setL1808List(bo.getL1820List());
                bo.setL1820List(tempList);
                break;
            case "1908" :
            case "1917" :
                USVMap um1908 = new USVMap(
                        Objects.isNull(bo.getL1908List()) || bo.getL1908List().isEmpty()
                                ? "" : bo.getL1908List().get(0));
                USVMap um1917 = new USVMap(
                        Objects.isNull(bo.getL1917List()) || bo.getL1917List().isEmpty()
                                ? "" : bo.getL1917List().get(0));
                List<Reservations> list1908 = rsvList.filtered(rsv -> um1908.getRsvNum().equals(
                        rsv.getReservationnumber().toString()));
                List<Reservations> list1917 = rsvList.filtered(rsv -> um1917.getRsvNum().equals(
                        rsv.getReservationnumber().toString()));
                if(list1908.size() > 1 || list1917.size() > 1) {
                    MessageBox.show(Tcyh.stage,
                            "交換対象の予約の予約番号が重複しています。\n処理を中止します。",
                            "警告",
                            MessageBox.OK);
                    return;
                }
                if( ! list1908.isEmpty()) {
                    rsvTemp = list1908.get(0);
                    Boolean temp1908 = rsvTemp.getLarge1908();
                    rsvTemp.setLarge1908(rsvTemp.getLarge1917());
                    rsvTemp.setLarge1917(temp1908);
                    ReservationsModel.commitRsv(rsvTemp);
                    if(rsvTemp.getTotalnights() > 1)
                        changed = Boolean.TRUE;
                }
                if( ! list1917.isEmpty()) {
                    rsvTemp = list1917.get(0);
                    Boolean temp1917 = rsvTemp.getLarge1917();
                    rsvTemp.setLarge1917(rsvTemp.getLarge1908());
                    rsvTemp.setLarge1908(temp1917);
                    ReservationsModel.commitRsv(rsvTemp);
                    if(rsvTemp.getTotalnights() > 1)
                        changed = Boolean.TRUE;
                }
                tempList = bo.getL1908List();
                bo.setL1908List(bo.getL1917List());
                bo.setL1917List(tempList);
                break;
        }
        commitBO(bo);
        if(changed) MessageBox.show(Tcyh.stage
                    ,"他の日付は変更されていないので、\n"
                    + "必要であれば他の日付でも部屋の入れ替えを行って下さい。"
                    ,"警告"
                    ,MessageBox.OK);
    }
    protected static void changeBreakfastStatus(Boolean willServe) {
        BedOccupation bo = getBOToday();
        bo.setIsbreakfastservedtomorrow(willServe);
        commitBO(bo);
    }
    protected static void updateBreakfastResult(Short breakfastResult) {
        BedOccupation bo = getBOToday();
        bo.setBreakfastresult(breakfastResult);
        commitBO(bo);
    }
    protected static void updateComments(String comments) {
        BedOccupation bo = getBOToday();
        bo.setComments(comments);
        commitBO(bo);
    }
    protected static void updateRoomState(LocalDate ld, String roomNumber, String roomType) {
        BedOccupation bo = fetchBO(ld);
        bo = emptyRoomToDorm(bo, roomNumber, roomType);
        commitBO(bo);
    }
    protected static void updateSpareNumber(
            Integer newMDormSpare,
            Integer newFDormSpare,
            Integer newQuadSpare,
            Integer newBarrierfreeSpare,
            Integer newTatamiSpare,
            Integer newStaffSpare,
            Boolean newL1808Spare,
            Boolean newL1820Spare,
            Boolean newL1908Spare,
            Boolean newL1917Spare
        ){
        //最初にできる簡単な入力値の検査
        if( newBarrierfreeSpare > 2 ||
            newTatamiSpare > 2 ||
            newStaffSpare > 2
        ){
            MessageBox.show(Tcyh.stage,"バリアフリー部屋、畳部屋、スタッフ室の予備数は、\n2以下である必要があります。","",MessageBox.OK);
            return;
        } else if ( newQuadSpare > 25) {
            MessageBox.show(Tcyh.stage,"4人部屋の予備数は、\n25以下である必要があります。","",MessageBox.OK);
            return;
        }

        BedOccupation bo = getBOToday();

        Integer nowMDormSpare = Long.valueOf(bo.getMDormList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(spareString))
                .count()).intValue();
        while(newMDormSpare - nowMDormSpare > 0) {
            if(getBlankNum(bo.getMDormList()).equals(0))
                if(getBlankNum(bo.getQuadroomList()) > 0)
                    bo = quadEmptyToDorm(bo, newMDormSpare - nowMDormSpare, mdormString);
                else {
                    MessageBox.show(
                            Tcyh.stage,
                            "入力したMドミの予備ベッドを確保するだけの部屋がありません。\n処理を中止します。",
                            "警告",
                            MessageBox.OK);
                    return;
                }
            USVMap um = new USVMap(spareString);
            um.putRoomType("mdorm");
            bo.setMDormList(replaceFirstBlank(bo.getMDormList(), um.toString()));
            nowMDormSpare++;
        }
        while(newMDormSpare - nowMDormSpare < 0) {
            bo.setMDormList(remove(bo.getMDormList(), spareString));
            nowMDormSpare--;
        }
        //後で#quadDormToEmptyする

        Integer nowFDormSpare = Long.valueOf(bo.getFDormList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(spareString))
                .count()).intValue();
        while(newFDormSpare - nowFDormSpare > 0) {
            if(getBlankNum(bo.getFDormList()).equals(0))
                if(getBlankNum(bo.getQuadroomList()) > 0)
                    bo = quadEmptyToDorm(bo, newFDormSpare - nowFDormSpare, fdormString);
                else {
                    MessageBox.show(
                            Tcyh.stage,
                            "入力したFドミの予備ベッドを確保するだけの部屋がありません。\n処理を中止します。",
                            "警告",
                            MessageBox.OK);
                    return;
                }
            USVMap um = new USVMap(spareString);
            um.putRoomType("fdorm");
            bo.setFDormList(replaceFirstBlank(bo.getFDormList(), um.toString()));
            nowFDormSpare++;
        }
        while(newFDormSpare - nowFDormSpare < 0) {
            bo.setFDormList(remove(bo.getFDormList(), spareString));
            nowFDormSpare--;
        }
        //後で#quadDormToEmptyする

        Integer nowQuadSpare = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(spareString))
                .count()).intValue();
        Integer nowQuadEmpty = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> Objects.isNull(str) || str.equals(""))
                .count()).intValue();
        if(newQuadSpare - nowQuadSpare < nowQuadEmpty) {
            USVMap um = new USVMap(spareString);
            um.putRoomType("quad");
            bo.setQuadroomList(replaceMultipleBlank(
                    bo.getQuadroomList(),um,newQuadSpare - nowQuadSpare));
        } else if(newQuadSpare - nowQuadSpare > 0) {
            MessageBox.show(Tcyh.stage, "追加した4人部屋の予備数が、空室数を超えています。\n処理を中止します。", "警告", MessageBox.OK);
            return;
        }
        while(newQuadSpare - nowQuadSpare < 0) {
            bo.setQuadroomList(remove(bo.getQuadroomList(), spareString));
            nowQuadSpare--;
        }

        List<List<String>> roomTypeList = new ArrayList<List<String>>();
        roomTypeList.add(Arrays.asList("1801","1811"));
        roomTypeList.add(Arrays.asList("1807","1819"));
        roomTypeList.add(Arrays.asList("staffl","staffr"));
        List<Integer> newSpareNumList = Arrays.<Integer>asList(newBarrierfreeSpare, newTatamiSpare, newStaffSpare);
        Iterator<Integer> itNSNL = newSpareNumList.<Integer>iterator();
        for (List<String> roomType : roomTypeList){
            USVMap um = new USVMap(spareString);
            Integer newSpareNum = itNSNL.next();
            Integer nowSpareNum =
                    (isFirstEmpty(bo.getList(roomType.get(0))) || ! bo.getList(roomType.get(0)).get(0).equals(spareString) ? 0 : 1)+
                    (isFirstEmpty(bo.getList(roomType.get(1))) || ! bo.getList(roomType.get(1)).get(0).equals(spareString) ? 0 : 1);
            if(newSpareNum - nowSpareNum > 0 && isFirstEmpty(bo.getList(roomType.get(0)))) {
                um.putRoomType(roomType.get(0));
                bo.setList(roomType.get(0), replaceFirstBlank(bo.getList(roomType.get(0)),um.toString()));
                nowSpareNum++;
            }
            if(newSpareNum - nowSpareNum > 0 && isFirstEmpty(bo.getList(roomType.get(1)))) {
                um.putRoomType(roomType.get(1));
                bo.setList(roomType.get(1), replaceFirstBlank(bo.getList(roomType.get(1)),um.toString()));
                nowSpareNum++;
            }
            if(newSpareNum - nowSpareNum < 0 && bo.getList(roomType.get(0)).get(0).startsWith(spareString)) {
                bo.setList(roomType.get(0), remove(bo.getList(roomType.get(0)), spareString));
                nowSpareNum--;
            }
            if(newSpareNum - nowSpareNum < 0 && bo.getList(roomType.get(1)).get(0).startsWith(spareString)) {
                bo.setList(roomType.get(1), remove(bo.getList(roomType.get(1)), spareString));
                nowSpareNum--;
            }
        }

        List<String> largeRoomTypeList = Arrays.asList("1808","1820","1908","1917");
        List<Boolean> newSpareBoolList = Arrays.asList(newL1808Spare,newL1820Spare,newL1908Spare,newL1917Spare);
        Iterator<Boolean> itNSBL = newSpareBoolList.iterator();
        for (String roomType : largeRoomTypeList){
            Boolean newSpareBool = itNSBL.next();
            Boolean nowSpareBool = contains(bo.getList(roomType), spareString);
            if(newSpareBool.equals(nowSpareBool)) continue;
            if(newSpareBool) {
                //空室の場合
                if (Objects.isNull(bo.getList(roomType)) || bo.getList(roomType).isEmpty()) {
                    bo = nonQuadRoomToPrivate(bo, roomType, spareString);
                    continue;
                }
                USVMap um = new USVMap(bo.getList(roomType).get(0));
                um.putRoomType(roomType);
                //固定ドミになっている場合
                if( ! isFirstEmpty(bo.getList(roomType))) {
                    if(um.getRsvNum().matches(mdormString + "|" + mdormFixedString + "|"
                                    + fdormString + "|" + fdormFixedString)) {
                    MessageBox.show(Tcyh.stage,
                            roomType + "はドミトリーに固定されています。\n処理を中止します。",
                            "警告",
                            MessageBox.OK);
                    return;
                    //既に個室利用されている場合
                    } else {
                        MessageBox.show(Tcyh.stage,
                                roomType + "は既に個室利用されています。\n処理を中止します。",
                                "警告",
                                MessageBox.OK);
                        return;
                    }
                }
                //ドミの場合
                if ( um.getRsvNum().matches(mdormString + "|" + fdormString)) {
                        Integer availableDormBedNum = um.getRsvNum().matches(mdormString)
                                ? getBlankNum(bo.getMDormList())
                                : um.getRsvNum().matches(fdormString)
                                ? getBlankNum(bo.getFDormList())
                                : 0;
                        Integer unassignedBedInQuad = nowQuadEmpty * 4;
                        if(availableDormBedNum + unassignedBedInQuad < getBedCount(roomType)) {
                            MessageBox.show(Tcyh.stage,
                                    roomType + "を予備にするために必要な4人部屋の数が足りません。\n処理を中止します。",
                                    "警告",
                                    MessageBox.OK);
                            return;
                        } else {
                            bo = nonQuadRoomToPrivate(bo, roomType, spareString);
                            nowQuadEmpty = Long.valueOf(bo.getQuadroomList().stream()
                                    .filter(str -> Objects.isNull(str) || str.equals(""))
                                    .count()).intValue();
                        }
                }
            } else if( nowSpareBool && ! newSpareBool)
                bo = nonQuadDormToEmpty(bo, roomType);
        }
        
        bo = deleteUnusedQuadDorm(bo);

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        try {
            em.clear();
            tx.begin();
            em.merge(bo);
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(BedOccupationModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
        }
    }
    protected static void updateQuadDormFixed(Integer newFixedMDorm, Integer newFixedFDorm) {
        BedOccupation bo = getBOToday();
        Integer nowFixedMDorm = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(mdormFixedString)).count()).intValue();
        Integer nowMDorm = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(mdormString)).count()).intValue();
        //まず既存のMドミをFixedに置換する
        while(nowMDorm > 0 && newFixedMDorm > nowFixedMDorm) {
            bo.setQuadroomList(replace(bo.getQuadroomList(), mdormString, mdormFixedString));
            nowMDorm--;
            nowFixedMDorm++;
        }
        //それでも希望するFixedの部屋に満たない場合は、新たなFixedの部屋を追加する
        if(newFixedMDorm > nowFixedMDorm)
            bo = quadEmptyToDorm(bo, (newFixedMDorm - nowFixedMDorm)*4, mdormFixedString);
        //Fixedの部屋数が減る場合
        while(newFixedMDorm < nowFixedMDorm) {
            bo.setQuadroomList(replace(bo.getQuadroomList(), mdormFixedString, mdormString));
            nowMDorm++;
            nowFixedMDorm--;
        }

        Integer nowFixedFDorm = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(fdormFixedString)).count()).intValue();
        Integer nowFDorm = Long.valueOf(bo.getQuadroomList().stream()
                .filter(str -> new USVMap(str).getRsvNum().equals(fdormString)).count()).intValue();
        //まず既存のFドミをFixedに置換する
        while(nowFDorm > 0 && newFixedFDorm > nowFixedFDorm) {
            bo.setQuadroomList(replace(bo.getQuadroomList(), fdormString, fdormFixedString));
            nowFDorm--;
            nowFixedFDorm++;
        }
        //それでも希望するFixedの部屋に満たない場合は、新たなFixedの部屋を追加する
        if(newFixedFDorm > nowFixedFDorm)
            bo = quadEmptyToDorm(bo, (newFixedFDorm - nowFixedFDorm)*4, fdormFixedString);
        //Fixedの部屋数が減る場合
        while(newFixedFDorm < nowFixedFDorm) {
            bo.setQuadroomList(replace(bo.getQuadroomList(), fdormFixedString, fdormString));
            nowFDorm++;
            nowFixedFDorm--;
        }

        bo = deleteUnusedQuadDorm(bo);

        EntityManager em = ConnectTCYHPU.getInstance().getEM();
        EntityTransaction tx = em.getTransaction();
        try {
            em.clear();
            tx.begin();
            em.merge(bo);
            tx.commit();
            em.clear();
        } catch (Exception ex) {
            Logger.getLogger(BedOccupationModel.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
        }
    }
    protected static void updateExBedNumber(USVMap um, String exBedNum) {
        um.putExBed(exBedNum);
        replaceUSVMap(um);
    }
    protected static void updateMarks(
            USVMap um,
            Boolean luggage,
            Boolean guestcard,
            Boolean recheckin,
            Boolean lowerbed,
            Boolean upperbed,
            Boolean creditpayment) {
        StringBuilder marks = new StringBuilder();
        marks.append(luggage ? luggageMarkString : "");
        marks.append(guestcard ? guestcardMarkString : "");
        marks.append(recheckin ? recheckinMarkString : "");
        marks.append(lowerbed ? lowerbedMarkString : "");
        marks.append(upperbed ? upperbedMarkString : "");
        marks.append(creditpayment ? creditpaymentMarkString : "");
        if(um.getMark().equals(marks.toString()))
            return;
        um.putMark(marks.toString());
        replaceUSVMap(um);
    }
    protected static void updateBreakDownString(USVMap um, String brkDwn) {
        um.putBrkDwn(brkDwn);
        replaceUSVMap(um);
    }
    private static void replaceUSVMap(USVMap um) {
        LocalDate ld = MainWindowController.getSelectedDate();
        BedOccupation bo = fetchBO(ld);

        List<String> list;
        String roomType = um.getRoomType();
        try {
            list = bo.getList(roomType);
        } catch (Exception e) {
            MessageBox.show(Tcyh.stage, "選択した場所には情報を追加できません。", "警告", 0);
            return;
        }
        if((roomType.equals("1807VBox") || roomType.equals("1819VBox")) && Integer.decode(um.getExBed()) > 0) {
            MessageBox.show(Tcyh.stage, "選択した場所にはエキストラベッドを追加できません。", "警告", 0);
            return;
        }
        
        //オリジナルの文字列を新しい文字列に置き換える
        if(list.contains(um.getOrgStr())) {
            list.set(list.indexOf(um.getOrgStr()), um.toString());
            list.sort(BedOccupation.comparatorAsc());
        } else {
            MessageBox.show(Tcyh.stage, "データを置換できませんでした。\n処理を中断します。", "警告", 0);
            
            System.out.println("um.getOrgStr() : " + um.getOrgStr());
            System.out.println("list : " + list);
            return;
        }
        try {
            bo.setList(roomType, list);
        } catch (Exception e) {
                MessageBox.show(Tcyh.stage, "不明なインデックスを検出しました。\n処理を中断します。", "警告", MessageBox.OK);
                return;
        }
        commitBO(bo);
    }
    private static Integer counter;
    protected static BedOccupation replaceRsvNum(Date date, String oldNum, String newNum) {
        BedOccupation bo = fetchBO(date);
        bo.setMDormList(replaceAll(bo.getMDormList(), oldNum, newNum));
        bo.setFDormList(replaceAll(bo.getFDormList(), oldNum, newNum));
        bo.setQuadroomList(replaceAll(bo.getQuadroomList(),oldNum, newNum));
        bo.setL1808List(replaceAll(bo.getL1808List(), oldNum, newNum));
        bo.setL1820List(replaceAll(bo.getL1820List(), oldNum, newNum));
        bo.setL1908List(replaceAll(bo.getL1908List(), oldNum, newNum));
        bo.setL1917List(replaceAll(bo.getL1917List(), oldNum, newNum));
        bo.setB1801List(replaceAll(bo.getB1801List(), oldNum, newNum));
        bo.setB1811List(replaceAll(bo.getB1811List(), oldNum, newNum));
        bo.setT1807List(replaceAll(bo.getT1807List(), oldNum, newNum));
        bo.setT1819List(replaceAll(bo.getT1819List(), oldNum, newNum));
        bo.setStaffLList(replaceAll(bo.getStaffLList(), oldNum, newNum));
        bo.setStaffRList(replaceAll(bo.getStaffRList(), oldNum, newNum));
        return bo;
    }

    private static String dormTypeToMakeRoomEmpty;
    protected static BedOccupation nonQuadDormToEmpty(BedOccupation boBefore, String roomNum) {
        dormTypeToMakeRoomEmpty = null;
        BedOccupation boAfter;
        try {
            boAfter = boBefore.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(BedOccupationModel.class.getName()).log(Level.SEVERE, null, ex);
            return boBefore;
        }

        Integer eracableBedNum = getBedCount(roomNum);
        if (isFirstEmpty(boAfter.getList(roomNum))) {
            System.out.println("Room " + roomNum + " is not Dorm Room.");
            return boBefore;
        } else {
            dormTypeToMakeRoomEmpty = new USVMap(boAfter.getList(roomNum).get(0)).getRsvNum();
            boAfter.setList(roomNum, remove(boAfter.getList(roomNum), dormTypeToMakeRoomEmpty));
        }

        switch (dormTypeToMakeRoomEmpty) {
            case mdormString:
            case mdormFixedString:
                boAfter = quadEmptyToDorm(boAfter, eracableBedNum, dormTypeToMakeRoomEmpty);
                boAfter.setMDormList(removeBlank(boAfter.getMDormList(), eracableBedNum));
                break;
            case fdormString:
            case fdormFixedString:
                boAfter = quadEmptyToDorm(boAfter, eracableBedNum, dormTypeToMakeRoomEmpty);
                boAfter.setFDormList(removeBlank(boAfter.getFDormList(), eracableBedNum));
                break;
            case spareString:
                boAfter.setList(roomNum,remove(boAfter.getList(roomNum),spareString));
                break;
            default:
                MessageBox.show(
                        Tcyh.stage,
                        "既に予約が入っているため、" + roomNum + "を空室にできません。"
                        , "警告", MessageBox.OK);
                return boBefore;
        }
        return boAfter;
    }
    protected static BedOccupation nonQuadRoomToPrivate(BedOccupation bo, String roomNum, String rsvStr) {
        BedOccupation newBO;
        try {
            newBO = bo.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(BedOccupationModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        Integer size = getBedCount(roomNum);
        String dormType = "";
        if( ! newBO.getList(roomNum).isEmpty())
            dormType = new USVMap(newBO.getList(roomNum).get(0)).getRsvNum();
        newBO.setList(roomNum, Arrays.asList(rsvStr));

        List<String> dormList;
        if(Objects.isNull(dormType) || dormType.equals(""))
            return newBO;
        else if (dormType.matches(mdormString + "|" + mdormFixedString)) {
            newBO = quadEmptyToDorm(newBO, size, dormType);
            dormList = newBO.getMDormList();
            while (size > 0 && ! Objects.isNull(dormList)) {
                dormList.remove("");
                size--;
            }
            newBO.setMDormList(dormList);
        } else if (dormType.matches(fdormString + "|" + fdormFixedString)) {
            newBO = quadEmptyToDorm(newBO, size, dormType);
            dormList = newBO.getFDormList();
            while (size > 0 && ! Objects.isNull(dormList)) {
                dormList.remove("");
                size--;
            }
            newBO.setFDormList(dormList);
        }
        return newBO;
    }
    protected static BedOccupation emptyRoomToDorm(BedOccupation bo, String roomNum, String dormType) {
        USVMap um = new USVMap(isFirstEmpty(bo.getList(roomNum)) ? "" : bo.getList(roomNum).get(0));
        //変更前と後で部屋タイプが全く変わらない場合
        if(dormType.matches(um.getRsvNum())) {
            MessageBox.show(
                    Tcyh.stage,
                    "選択した部屋タイプは、元々の部屋タイプと同じため、\n変更処理は行われませんでした。",
                    "情報",
                    MessageBox.OK);
            return bo;
        }
        Boolean isListMatchedMDorm = um.getRsvNum().matches(mdormString + "|" + mdormFixedString);
        Boolean isListMatchedFDorm = um.getRsvNum().matches(fdormString + "|" + fdormFixedString);
        //既に個室利用されている場合(空室ではなく、MドミFドミどちらにもマッチしない)はエラー表示して終了
        if( ! isFirstEmpty(bo.getList(roomNum)) && ! isListMatchedMDorm && ! isListMatchedFDorm) {
            MessageBox.show(Tcyh.stage, roomNum + "は既に個室利用されています。", "警告", MessageBox.OK);
            return bo;
        }
        Boolean isDormTypeMatchedMDorm = dormType.matches(mdormString + "|" + mdormFixedString);
        Boolean isDormTypeMatchedFDorm = dormType.matches(fdormString + "|" + fdormFixedString);
        //変更元のドミタイプ(MかFか)が変更後と同じであれば、後処理なく新しい値に置換して終了する。
        if((isListMatchedMDorm && isDormTypeMatchedMDorm) ||
                (isListMatchedFDorm && isDormTypeMatchedFDorm)) {
            bo.setList(roomNum, Arrays.asList(dormType));
            return bo;
        }

        //以下、後処理が必要な場合。
        Integer bedNum = getBedCount(roomNum);
        switch (dormType) {
            case mdormString:
            case mdormFixedString:
                //元々がFドミだった場合
                if(um.getRsvNum().matches(fdormString + "|" + fdormFixedString)) {
                    Integer availableDormBedNum = contains(bo.getList(roomNum), fdormString)
                            ? getBlankNum(bo.getFDormList()) : 0;
                    Integer unassignedBedInQuad = Long.valueOf(bo.getQuadroomList().stream()
                            .filter(str -> Objects.isNull(str) || str.equals(""))
                            .count()).intValue() * 4;
                    //Fドミの空ベッド数が、bedNumより多い場合はそのままFドミからbedNum分のベッドを削除
                    if(getBlankNum(bo.getFDormList()) >= bedNum)
                        bo.setFDormList(removeBlank(bo.getFDormList(), bedNum));
                    //そうでない場合で、四人部屋にドミを作れる場合
                    else if(availableDormBedNum + unassignedBedInQuad < bedNum) {
                        quadEmptyToDorm(bo, bedNum, fdormString);
                        bo.setFDormList(removeBlank(bo.getFDormList(), bedNum));
                    //四人部屋にドミを作っても尚、Fドミの空ベッド数がbedNum以上に満たない場合
                    } else {
                        MessageBox.show(
                                Tcyh.stage,
                                "FドミをMドミに変えるための十分な空きがありません。\n処理を中断します。",
                                "警告",
                                MessageBox.OK);
                        return bo;
                    }
                }
                bo.setMDormList(addBlank(bo.getMDormList(), bedNum));
                break;
            case fdormString:
            case fdormFixedString:
                //元々がMドミだった場合
                if(um.getRsvNum().matches(mdormString + "|" + mdormFixedString)) {
                    Integer availableDormBedNum = contains(bo.getList(roomNum), mdormString)
                            ? getBlankNum(bo.getMDormList()) : 0;
                    Integer unassignedBedInQuad = Long.valueOf(bo.getQuadroomList().stream()
                            .filter(str -> Objects.isNull(str) || str.equals(""))
                            .count()).intValue() * 4;
                    //Mドミの空ベッド数が、bedNumより多い場合はそのままFドミからbedNum分のベッドを削除
                    if(getBlankNum(bo.getMDormList()) >= bedNum)
                        bo.setMDormList(removeBlank(bo.getMDormList(), bedNum));
                    //そうでない場合で、四人部屋にドミを作れる場合
                    else if(availableDormBedNum + unassignedBedInQuad < bedNum) {
                        quadEmptyToDorm(bo, bedNum, mdormString);
                        bo.setMDormList(removeBlank(bo.getMDormList(), bedNum));
                    //四人部屋にドミを作っても尚、Fドミの空ベッド数がbedNum以上に満たない場合
                    } else {
                        MessageBox.show(
                                Tcyh.stage,
                                "MドミをFドミに変えるための十分な空きがありません。\n処理を中断します。",
                                "警告",
                                MessageBox.OK);
                        return bo;
                    }
                }
                bo.setFDormList(addBlank(bo.getFDormList(), bedNum));
                break;
        }
        USVMap dormUM = new USVMap(dormType);
        dormUM.putRoomType(roomNum);
        bo.setList(roomNum, Arrays.asList(dormUM.toString()));
        bo = deleteUnusedQuadDorm(bo);
        return bo;
    }
    protected static BedOccupation quadEmptyToDorm(BedOccupation bo, Integer requiredBed, String dormType) {
        List<String> dormList;
        List<String> quadroomList = bo.getQuadroomList();
        if (requiredBed <= 0) return bo;
        else if (dormType.matches(mdormString + "|" + mdormFixedString)) {
            dormList = bo.getMDormList();
            while (getBlankNum(dormList) < requiredBed) {
                if(getBlankNum(quadroomList) <= 0) {
                    MessageBox.show(Tcyh.stage
                            ,"4人部屋の空室がなくなり、必要な4人部屋のMドミを確保できませんでした。\n"
                                    + "処理はそのまま継続されますので、注意して下さい。"
                            , "警告", MessageBox.OK);
                    return bo;
                }
                USVMap um = new USVMap(dormType);
                um.putRoomType("quad");
                quadroomList = replaceFirstBlank(quadroomList, um.toString());
                dormList.addAll(Arrays.asList("", "", "", ""));
            }
            bo.setMDormList(dormList);
        } else if (dormType.matches(fdormString + "|" + fdormFixedString)) {
            dormList = bo.getFDormList();
            while (getBlankNum(dormList) < requiredBed) {
                if(getBlankNum(quadroomList) <= 0) {
                    MessageBox.show(Tcyh.stage
                            ,"4人部屋の空室がなくなり、必要な4人部屋のFドミを確保できませんでした。\n"
                                    + "処理はそのまま継続されますので、注意して下さい。"
                            , "警告", MessageBox.OK);
                    return bo;
                }
                USVMap um = new USVMap(dormType);
                um.putRoomType("quad");
                quadroomList = replaceFirstBlank(quadroomList, um.toString());
                dormList.addAll(Arrays.asList("", "", "", ""));
            }
            bo.setFDormList(dormList);
        } else MessageBox.show(Tcyh.stage, "何の処理も行われませんでした。", "警告", MessageBox.OK);
        bo.setQuadroomList(quadroomList);
        return bo;
    }
    protected static BedOccupation deleteUnusedQuadDorm(BedOccupation bo) {
        List<String> quadroomList = bo.getQuadroomList();
        List<String> mdormList = bo.getMDormList();
        for(int i = 0; quadroomList.size() > i && getBlankNum(mdormList) >= 4; i++) {
            USVMap um = new USVMap(quadroomList.get(i));
            if(um.getRsvNum().equals(mdormString)) {
                quadroomList = remove(quadroomList, um.getRsvNum());
                mdormList = removeBlank(mdormList, 4);
            }
        }
        bo.setMDormList(mdormList);

        List<String> fdormList = bo.getFDormList();
        for(int i = 0; quadroomList.size() > i && getBlankNum(fdormList) >= 4; i++) {
            USVMap um = new USVMap(quadroomList.get(i));
            if(um.getRsvNum().equals(fdormString)) {
                quadroomList = remove(quadroomList, um.getRsvNum());
                fdormList = removeBlank(fdormList, 4);
            }
        }
        bo.setFDormList(fdormList);

        bo.setQuadroomList(quadroomList);
        return bo;
    }
    private static Integer getBedCount(String roomNum) {
        switch (roomNum) {
            case "1801":
            case "1811":
                return 2;
            case "1808":
            case "1820":
                return 10;
            case "1908":
            case "1917":
                return 8;
            default:
                throw new IllegalArgumentException("Wrong Room Number passed to the method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ".");
        }
    }

    protected static HashMap<String, HashMap<String, Integer>> masterRnumCounter() {
        return masterRnumCounter(getBOToday());
    }
    protected static HashMap<String, HashMap<String, Integer>> masterRnumCounter(BedOccupation bo) {
        HashMap<String, HashMap<String, Integer>> master = new HashMap<String, HashMap<String, Integer>>();
        master.put("mdorm", rnumCounterHM(bo.getMDormList()));
        master.put("fdorm", rnumCounterHM(bo.getFDormList()));
        master.put("quadroom", rnumCounterHM(bo.getQuadroomList()));
        master.put("1820", rnumCounterHM(bo.getL1820List()));
        master.put("1917", rnumCounterHM(bo.getL1917List()));
        master.put("1808", rnumCounterHM(bo.getL1808List()));
        master.put("1908", rnumCounterHM(bo.getL1908List()));
        master.put("1801", rnumCounterHM(bo.getB1801List()));
        master.put("1811", rnumCounterHM(bo.getB1811List()));
        master.put("1807", rnumCounterHM(bo.getT1807List()));
        master.put("1819", rnumCounterHM(bo.getT1819List()));
        master.put("staffl", rnumCounterHM(bo.getStaffLList()));
        master.put("staffr", rnumCounterHM(bo.getStaffRList()));

        if(! master.get("quadroom").containsKey(mdormString))
            master.get("quadroom").put(mdormString, 0);
        if(! master.get("quadroom").containsKey(fdormString))
            master.get("quadroom").put(fdormString, 0);
        return master;
    }
    private static HashMap<String, Integer> rnumCounterHM(List<String> list) {
        //ReservationNumber毎の合計数を集計
        Map<String, Long> mapLong = list.stream()
                .map(str -> new USVMap(str).getRsvNum())
                .sorted()
                .collect(Collectors.groupingBy(o -> o, Collectors.counting()));
        //Collectors.groupingByがMapを返し、Collectors.countingがLongを返す(-> Map<String, Long>)ので、
        //HashMap<String, Integer>に変換
        HashMap<String, Integer> mapInteger = new HashMap<String, Integer>();
        mapLong.forEach((String key, Long val) -> mapInteger.put(key, val.intValue()));
        return mapInteger;
    }
    protected static Boolean isModifiable(Reservations rsvNew, Reservations rsvOld) {
        BedOccupation bo = fetchBO(rsvNew.getDateofstay());
        if(bo.getIsclosedday()) {
            MessageBox.show(Tcyh.stage,
                    "該当の日付は休館日に設定されているため、予約を取ることができません。",
                    "警告", MessageBox.OK);
            return false;
        }
        HashMap<String, Integer> boSummaryHM = calcBOSummary(bo);
        HashMap<String, HashMap<String, Integer>> master = masterRnumCounter();
        
        if(Objects.isNull(rsvNew.getQuadroom())) {
            MessageBox.show(Tcyh.stage, "変更後の「四人部屋数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        if(Objects.isNull(rsvNew.getMalebed())) {
            MessageBox.show(Tcyh.stage, "変更後の「男性ドミベッド数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        if(Objects.isNull(rsvNew.getFemalebed())) {
            MessageBox.show(Tcyh.stage, "変更後の「女性ドミベッド数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        if(Objects.isNull(rsvNew.getBarrierfreeroom())) {
            MessageBox.show(Tcyh.stage, "変更後の「2人部屋数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        if(Objects.isNull(rsvNew.getTatamiroom())) {
            MessageBox.show(Tcyh.stage, "変更後の「和室数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        if(Objects.isNull(rsvNew.getStaffroom())) {
            MessageBox.show(Tcyh.stage, "変更後の「スタッフ室数」の値が空になっています。\n数字を入力してから再度お試し下さい。", "警告", MessageBox.OK);
            return Boolean.FALSE;
        }
        
        Boolean isOldNull = Objects.isNull(rsvOld);
        Integer requiredQuadRoom = isOldNull ? rsvNew.getQuadroom(): rsvNew.getQuadroom()- rsvOld.getQuadroom();
        Integer requiredMBed = isOldNull ? rsvNew.getMalebed() : rsvNew.getMalebed() - rsvOld.getMalebed();
        Integer requiredFBed = isOldNull ? rsvNew.getFemalebed() : rsvNew.getFemalebed() - rsvOld.getFemalebed();
        Integer requiredBFR = isOldNull ? rsvNew.getBarrierfreeroom() : rsvNew.getBarrierfreeroom() - rsvOld.getBarrierfreeroom();
        Integer requiredTatamiRoom = isOldNull ? rsvNew.getTatamiroom() : rsvNew.getTatamiroom()- rsvOld.getTatamiroom();
        Integer requiredStaffRoom = isOldNull ? rsvNew.getStaffroom() : rsvNew.getStaffroom()- rsvOld.getStaffroom();
        Integer requiredL1808 = isOldNull ?
                rsvNew.getLarge1808() ? 1 : 0 :
                (rsvNew.getLarge1808() ? 1 : 0) - (rsvOld.getLarge1808() ? 1 : 0);
        Integer requiredL1820 = isOldNull ?
                rsvNew.getLarge1820() ? 1 : 0 :
                (rsvNew.getLarge1820() ? 1 : 0) - (rsvOld.getLarge1820() ? 1 : 0);
        Integer requiredL1908 = isOldNull ?
                rsvNew.getLarge1908() ? 1 : 0 :
                (rsvNew.getLarge1908() ? 1 : 0) - (rsvOld.getLarge1908() ? 1 : 0);
        Integer requiredL1917 = isOldNull ?
                rsvNew.getLarge1917() ? 1 : 0 :
                (rsvNew.getLarge1917() ? 1 : 0) - (rsvOld.getLarge1917() ? 1 : 0);

        Integer availableQuadRoom = boSummaryHM.get("availableQuadRoom");
        Integer availableMaleBed = boSummaryHM.get("availableMaleBed");
        Integer availableFemaleBed = boSummaryHM.get("availableFemaleBed");
        Integer availableBarrierFreeRoom = boSummaryHM.get("availableBarrierFreeRoom");
        Integer availableTatamiRoom = boSummaryHM.get("availableTatamiRoom");
        Integer availableStaffRoom = boSummaryHM.get("availableStaffRoom");
        Integer l1808Available = boSummaryHM.get("l1808Available");
        Integer l1820Available = boSummaryHM.get("l1820Available");
        Integer l1908Available = boSummaryHM.get("l1908Available");
        Integer l1917Available = boSummaryHM.get("l1917Available");
        String l1808Type = boSummaryHM.get("l1808Type").toString();
        String l1820Type = boSummaryHM.get("l1820Type").toString();
        String l1908Type = boSummaryHM.get("l1908Type").toString();
        String l1917Type = boSummaryHM.get("l1917Type").toString();

        Boolean modifiable = true;
        Calendar cal = Calendar.getInstance();
        cal.setTime(rsvNew.getDateofstay());
        String dateStr = cal.get(Calendar.YEAR) + "年" +
                (cal.get(Calendar.MONTH)+1) + "月" +
                cal.get(Calendar.DAY_OF_MONTH) + "日";

        availableQuadRoom-=requiredQuadRoom;

        //ドミのベッド数が減る場合の、４人部屋を開放する処理
        if (requiredMBed < 0) {
            availableMaleBed-=requiredMBed;
            while(availableMaleBed < 4 && master.get("quadroom").get(mdormString) > 0) {
                master.get("quadroom").put(mdormString, master.get("quadroom").get(mdormString) - 1);
                availableQuadRoom+=1;
                availableMaleBed-=4;
            }
        }
        //ドミのベッド数が増える場合
        if (requiredMBed > 0) {
            while(availableMaleBed < requiredMBed) {
                availableQuadRoom-=1;
                availableMaleBed+=4;
            }
            if(availableQuadRoom < 0) {
                modifiable = false;
                MessageBox.show(Tcyh.stage, "4人部屋を使用しても、必要なMドミのベッド数を確保できませんでした。\n空室を増やしてから、再度試して下さい。", "警告 (" + dateStr + ")", 0);
            }
        }

        //ドミのベッド数が減る場合の、４人部屋を開放する処理
        if (requiredFBed < 0) {
            availableFemaleBed-=requiredFBed;
            while(availableFemaleBed < 4 && master.get("quadroom").get(fdormString) > 0) {
                master.get("quadroom").put(fdormString, master.get("quadroom").get(fdormString) - 1);
                availableQuadRoom+=1;
                availableFemaleBed-=4;
            }
        }
        //ドミのベッド数が増える場合
        if (requiredFBed > 0) {
            while(availableFemaleBed < requiredFBed) {
                availableQuadRoom-=1;
                availableFemaleBed+=4;
            }
            if(availableQuadRoom < 0) {
                modifiable = false;
                MessageBox.show(Tcyh.stage, "4人部屋を使用しても、必要なFドミのベッド数を確保できませんでした。\n空室を増やしてから、再度試して下さい。", "警告 (" + dateStr + ")", 0);
            }
        }

        //必要なドミの数を計算した結果、四人部屋の数が足りなくなる場合
        if( availableQuadRoom < 0 ) {
            modifiable = false;
            MessageBox.show(Tcyh.stage, "ドミトリーとして使用する4人部屋の数が足りません。\n空室を増やしてから、再度試して下さい。", "警告 (" + dateStr + ")", 0);
        }

        if(availableBarrierFreeRoom - requiredBFR < 0) {
            modifiable = false;
            MessageBox.show(Tcyh.stage, "バリアフリー部屋の空室が足りません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
        }
        if(availableTatamiRoom - requiredTatamiRoom < 0) {
            System.out.println("availableTatamiRoom : " + availableTatamiRoom + " requiredTatamiRoom : " + requiredTatamiRoom);
            modifiable = false;
            MessageBox.show(Tcyh.stage, "和室の空室が足りません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
        }
        if(availableStaffRoom - requiredStaffRoom < 0) {
            modifiable = false;
            MessageBox.show(Tcyh.stage, "スタッフ室の空室が足りません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
        }

        if(l1808Available - requiredL1808 < 0) {
            switch (l1808Type) {
                case mdormString:
                    availableMaleBed-=10;
                    while(availableMaleBed < 0) {
                        availableQuadRoom-=1;
                        availableMaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1808を個室にすることはできません。\nMドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case fdormString:
                    availableMaleBed-=10;
                    while(availableFemaleBed < 0) {
                        availableQuadRoom-=1;
                        availableFemaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1808を個室にすることはできません。\nFドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case "0" :
                    break;
                default :
                    modifiable = false;
                    MessageBox.show(Tcyh.stage, "1808を個室にすることはできません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    break;
            }
        }

        if(l1820Available - requiredL1820 < 0) {
            switch (l1820Type) {
                case mdormString:
                    availableMaleBed-=10;
                    while(availableMaleBed < 0) {
                        availableQuadRoom-=1;
                        availableMaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1820を個室にすることはできません。\nMドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case fdormString:
                    availableMaleBed-=10;
                    while(availableFemaleBed < 0) {
                        availableQuadRoom-=1;
                        availableFemaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1820を個室にすることはできません。\nFドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case "0" :
                    break;
                default :
                    modifiable = false;
                    MessageBox.show(Tcyh.stage, "1820を個室にすることはできません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    break;
            }
        }

        if(l1908Available - requiredL1908 < 0) {
            switch (l1908Type) {
                case mdormString:
                    availableMaleBed-=8;
                    while(availableMaleBed < 0) {
                        availableQuadRoom-=1;
                        availableMaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1908を個室にすることはできません。\nMドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case fdormString:
                    availableMaleBed-=8;
                    while(availableFemaleBed < 0) {
                        availableQuadRoom-=1;
                        availableFemaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1908を個室にすることはできません。\nFドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case "0" :
                    break;
                default :
                    modifiable = false;
                    MessageBox.show(Tcyh.stage, "1908を個室にすることはできません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    break;
            }
        }

        if(l1917Available - requiredL1917 < 0) {
            switch (l1917Type) {
                case mdormString:
                    System.out.println("availableQuadRoom Before : " + availableQuadRoom);
                    System.out.println("availableMaleBed Before : " + availableMaleBed);
                    availableMaleBed-=8;
                    while(availableMaleBed < 0) {
                        availableQuadRoom-=1;
                        availableMaleBed+=4;
                    }
                    System.out.println("availableQuadRoom After : " + availableQuadRoom);
                    System.out.println("availableMaleBed After : " + availableMaleBed);
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1917を個室にすることはできません。\nMドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case fdormString:
                    availableMaleBed-=8;
                    while(availableFemaleBed < 0) {
                        availableQuadRoom-=1;
                        availableFemaleBed+=4;
                    }
                    if( availableQuadRoom < 0 ) {
                        modifiable = false;
                        MessageBox.show(Tcyh.stage, "1917を個室にすることはできません。\nFドミを増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    }
                    break;
                case "0" :
                    break;
                default :
                    modifiable = false;
                    MessageBox.show(Tcyh.stage, "1917を個室にすることはできません。\n空室を増やしてから再度試して下さい。", "警告 (" + dateStr + ")", 0);
                    break;
            }
        }

        if( availableQuadRoom < 0 ) {
            modifiable = false;
            MessageBox.show(Tcyh.stage, "4人部屋の数が足りません。\n空室を増やしてから、再度試して下さい。", "警告 (" + dateStr + ")", 0);
        }

        if( ! modifiable )
            MessageBox.show(Tcyh.stage, "予約の変更処理は完了しませんでした。", "警告", 0);
        return modifiable;
    }
}