/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import tcyh.customcontrols.FormattedTimestampTableCell;
import tcyh.customcontrols.FormattedDateTableCell;
import tcyh.customcontrols.ColoredTableRow;
import tcyh.customcontrols.BreakfastNumTableCell;
import tcyh.customcontrols.TotalAndResultNumTableCell;
import tcyh.customcontrols.ColoredEditableCommentTableCell;
import tcyh.customcontrols.ColorPickerTableTableCell;
import tcyh.customcontrols.TotalAndPassedNightsTableCell;
import tcyh.customcontrols.EditableTableCell;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.ShortStringConverter;
import jfx.messagebox.MessageBox;
import org.controlsfx.control.PopOver;
import static tcyh.Constants.*;
import tcyh.utils.ReplaceFullWithHalf;

/**
 *
 * @author masanori
 */
public class ReservationsController implements Initializable {

    @FXML AnchorPane baseAnchorPane;

    @FXML TableView<Reservations> rsvByDateOfStayTableFX;
    @FXML TableColumn<Reservations, Integer> reservationNumberCol;
    @FXML TableColumn<Reservations, Date> reservationDateCol;
    @FXML TableColumn<Reservations, Short> nightsPassedCol;
    @FXML TableColumn<Reservations, Short> totalNightsCol;
    @FXML TableColumn<Reservations, String> commentsCol;
    @FXML TableColumn<Reservations, Short> totalNumCol;
    @FXML TableColumn<Reservations, Short> paidBreakfastCol;
    @FXML TableColumn<Reservations, Short> breakfastCol;
    @FXML TableColumn<Reservations, Short> revisionNumberCol;
    @FXML TableColumn<Reservations, Short> sequenceNumberCol;
    @FXML TableColumn<Reservations, Short> resultNumCol;
    @FXML TableColumn<Reservations, Timestamp> updateTimestampCol;

    @FXML TableView<Reservations> rsvByRsvNumTableFX;
    @FXML TableColumn<Reservations, Boolean> checkedCheckBoxCol2;
    @FXML TableColumn<Reservations, Integer> reservationNumberCol2;
    @FXML TableColumn<Reservations, Date> dateOfStayCol2;
    @FXML TableColumn<Reservations, Date> reservationDateCol2;
    @FXML TableColumn<Reservations, String> staffNameCol2;
    @FXML TableColumn<Reservations, String> guestNameCol2;
    @FXML TableColumn<Reservations, Short> nightsPassedCol2;
    @FXML TableColumn<Reservations, Short> totalNightsCol2;
    @FXML TableColumn<Reservations, String> phoneNumberCol2;
    @FXML TableColumn<Reservations, String> mailAddressCol2;
    @FXML TableColumn<Reservations, String> countryCol2;
    @FXML TableColumn<Reservations, String> prefectureCol2;
    @FXML TableColumn<Reservations, String> plannedcheckintimeCol2;
    @FXML TableColumn<Reservations, String> commentsCol2;
    @FXML TableColumn<Reservations, String> commentbackgroundcolorCol2;
    @FXML TableColumn<Reservations, String> commentforegroundcolorCol2;
    @FXML TableColumn<Reservations, Short> numberOfMaleCol2;
    @FXML TableColumn<Reservations, Short> numberOfFemaleCol2;
    @FXML TableColumn<Reservations, Short> numberOfBabyCol2;
    @FXML TableColumn<Reservations, Short> totalNumCol2;
    @FXML TableColumn<Reservations, Short> maleBedCol2;
    @FXML TableColumn<Reservations, Short> femaleBedCol2;
    @FXML TableColumn<Reservations, Short> quadRoomCol2;
    @FXML TableColumn<Reservations, Short> tatamiRoomCol2;
    @FXML TableColumn<Reservations, Short> barrierFreeRoomCol2;
    @FXML TableColumn<Reservations, Short> staffRoomCol2;
    @FXML TableColumn<Reservations, Short> lunchCol2;
    @FXML TableColumn<Reservations, Short> dinnerCol2;
    @FXML TableColumn<Reservations, Short> breakfastCol2;
    @FXML TableColumn<Reservations, String> reservationSourceCol2;
    @FXML TableColumn<Reservations, Short> revisionNumberCol2;
    @FXML TableColumn<Reservations, Short> sequenceNumberCol2;
    @FXML TableColumn<Reservations, Short> maleResultCol2;
    @FXML TableColumn<Reservations, Short> femaleResultCol2;
    @FXML TableColumn<Reservations, Short> resultNumCol2;
    @FXML TableColumn<Reservations, Short> paidBreakfastCol2;
    @FXML TableColumn<Reservations, String> roomNumResultCol2;
    @FXML TableColumn<Reservations, String> checkInTimeCol2;
    @FXML TableColumn<Reservations, String> updateCommentCol2;
    @FXML TableColumn<Reservations, String> cashierCol2;
    @FXML TableColumn<Reservations, Timestamp> updateTimestampCol2;

    @FXML Button commitChangesButtonFX;
    @FXML Button uniteReservationsButtonFX;
    private final String datePattern = "yyyy/MM/dd(E)";
    private final Text rsvByDateOfStayTableNormalPlaceHolder = new Text(rsvByDateOfStayTableNormalString);
    private final Text rsvByDateOfStayTableClosedDayPlaceHolder = new Text(rsvByDateOfStayTableClosedString);
    private final Text rsvByRsvNumTableNormalPlaceHolder = new Text(rsvByRsvNumTableNormalString);

    //既存の予約のチェックイン処理を纏めてするためのポップアップとそのコントロール
    private final Spinner<Integer> maleNum = new Spinner(0, 200, 0);
    private final Label maleNumLabel = new Label("男性");
    private final Spinner<Integer> femaleNum = new Spinner(0, 200, 0);
    private final Label femaleNumLabel = new Label("女性");
    private final TextField totalNum = new TextField("0");
    private final Label totalNumLabel = new Label("合計");
    private final Spinner<Integer> breakFastNum = new Spinner(0, 200, 0);
    private final Label breakFastNumLabel = new Label("朝食");
    private final TextField roomNum = new TextField();
    private final Label roomNumLabel = new Label("部屋番号");
    private final ComboBox<String> staffName = new ComboBox<String>();
    private final Label staffNameLabel = new Label("スタッフ名");
    private final Button applyBtn = new Button("適用");
    private final TextField checkinTime = new TextField();
    private final Label checkinTimeLabel = new Label("C/In時間");
    private final GridPane checkinGP = new GridPane();
    private final AnchorPane baseAP = new AnchorPane(checkinGP);
    private final Popup pu = new Popup();
    
    private static ReservationsController instance;
    static ReservationsController getInstance() {
        return instance;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;

        //テーブルのPlaceHolder(データが無い時に表示するText)の設定
        rsvByDateOfStayTableFX.setPlaceholder(rsvByDateOfStayTableNormalPlaceHolder);
        rsvByRsvNumTableFX.setPlaceholder(rsvByRsvNumTableNormalPlaceHolder);
        rsvByDateOfStayTableClosedDayPlaceHolder.setFont(Font.font("Meiryo", 100));
        //rsvByDateOfStayTableFX上で選択された行が変わった時に、rsvByRsvNumTableFXにデータをロードする
        rsvByDateOfStayTableFX.getSelectionModel()
                .selectedItemProperty().addListener(ae -> onSelectionChanged());
        //各テーブルで1行しか選択できないようにする
        rsvByDateOfStayTableFX.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        rsvByRsvNumTableFX.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        //予約の背景色をReservationStatusによって変更
        rsvByDateOfStayTableFX.setRowFactory(tv -> new ColoredTableRow());
        rsvByRsvNumTableFX.setRowFactory(tv -> new ColoredTableRow());

        //date型カラムの日付表示フォーマットを指定
        reservationDateCol.setCellFactory(column -> new FormattedDateTableCell());
        reservationDateCol2.setCellFactory(column -> new FormattedDateTableCell());
        dateOfStayCol2.setCellFactory(column -> new FormattedDateTableCell());
        //Timestamp型カラムの日付表示フォーマットを指定
        updateTimestampCol.setCellFactory(column -> new FormattedTimestampTableCell());
        updateTimestampCol2.setCellFactory(column -> new FormattedTimestampTableCell());

        //TableColumnとDBのデータの紐付け
        {
            totalNightsCol.setCellFactory(p -> new TotalAndPassedNightsTableCell());
            nightsPassedCol.setCellFactory(p -> new TotalAndPassedNightsTableCell());
            totalNumCol.setCellFactory(p -> new TotalAndResultNumTableCell());
            resultNumCol.setCellFactory(p -> new TotalAndResultNumTableCell());
            breakfastCol.setCellFactory(p -> new BreakfastNumTableCell(new ShortStringConverter()));
            paidBreakfastCol.setCellFactory(p -> new BreakfastNumTableCell(new ShortStringConverter()));
            commentsCol.setCellFactory(p -> new ColoredEditableCommentTableCell(new DefaultStringConverter()){
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setEditable(false);
                }
            });
        }
        {
            numberOfMaleCol2.setOnEditCommit((TableColumn.CellEditEvent<Reservations, Short> t) -> {
                t.getRowValue().setNumberofmale(t.getNewValue());
                t.consume();
            });
            numberOfFemaleCol2.setOnEditCommit((TableColumn.CellEditEvent<Reservations, Short> t) -> {
                t.getRowValue().setNumberoffemale(t.getNewValue());
                t.consume();
            });
            maleResultCol2.setOnEditCommit((TableColumn.CellEditEvent<Reservations, Short> t) -> {
                t.getRowValue().setMaleresult(t.getNewValue());
                t.consume();
            });
            femaleResultCol2.setOnEditCommit((TableColumn.CellEditEvent<Reservations, Short> t) -> {
                t.getRowValue().setFemaleresult(t.getNewValue());
                t.consume();
            });
            totalNightsCol2.setCellFactory(p -> new TotalAndPassedNightsTableCell());
            nightsPassedCol2.setCellFactory(p -> new TotalAndPassedNightsTableCell());
            totalNumCol2.setCellFactory(p -> new TotalAndResultNumTableCell());
            resultNumCol2.setCellFactory(p -> new TotalAndResultNumTableCell());
            breakfastCol2.setCellFactory(p -> new BreakfastNumTableCell(new ShortStringConverter()));
            paidBreakfastCol2.setCellFactory(p -> new BreakfastNumTableCell(new ShortStringConverter()));
            //備考欄のみColoredEditableCommentCellを利用する
            commentsCol2.setCellFactory(p -> new ColoredEditableCommentTableCell(new DefaultStringConverter()));
            commentbackgroundcolorCol2.setCellFactory(p -> new ColorPickerTableTableCell());
            commentforegroundcolorCol2.setCellFactory(p -> new ColorPickerTableTableCell());
        }
        
        //fillInCheckInDataのPopup用Controlの初期化
        maleNum.setPrefWidth(70);
        maleNum.setEditable(true);
        maleNum.focusedProperty().addListener(new ReplaceFullWithHalf(maleNum));
        maleNumLabel.setContentDisplay(ContentDisplay.BOTTOM);

        femaleNum.setPrefWidth(70);
        femaleNum.setEditable(true);
        femaleNum.focusedProperty().addListener(new ReplaceFullWithHalf(femaleNum));
        femaleNumLabel.setContentDisplay(ContentDisplay.BOTTOM);

        totalNum.setPrefWidth(40);
        totalNum.setEditable(false);
        totalNum.setDisable(true);
        totalNumLabel.setContentDisplay(ContentDisplay.BOTTOM);
        maleNum.valueProperty().addListener((Observable obs) -> {
            Integer male = Objects.isNull(maleNum.getValue()) ? 0 : maleNum.getValue();
            Integer female = Objects.isNull(femaleNum.getValue()) ? 0 : femaleNum.getValue();
            totalNum.setText(String.valueOf(male + female));
        });
        femaleNum.valueProperty().addListener((Observable obs) -> {
            Integer male = Objects.isNull(maleNum.getValue()) ? 0 : maleNum.getValue();
            Integer female = Objects.isNull(femaleNum.getValue()) ? 0 : femaleNum.getValue();
            totalNum.setText(String.valueOf(male + female));
        });

        breakFastNum.setPrefWidth(70);
        breakFastNum.setEditable(true);
        breakFastNum.focusedProperty().addListener(new ReplaceFullWithHalf(breakFastNum));
        breakFastNumLabel.setContentDisplay(ContentDisplay.BOTTOM);

        roomNum.setPrefWidth(100);
        roomNumLabel.setContentDisplay(ContentDisplay.BOTTOM);

        staffName.setEditable(true);
        staffName.focusedProperty().addListener(ae -> {
            staffName.setValue(staffName.editorProperty().getValue().getText());
        });
        staffName.setPrefWidth(150);
        staffName.setItems(
                FXCollections.observableArrayList(StaffModel.fetchStaff()
                        .stream()
                        .map(staff -> staff.getLastname())
                        .collect(Collectors.toList())
        ));
        staffNameLabel.setContentDisplay(ContentDisplay.BOTTOM);
        
        checkinTime.setPrefWidth(60);

        applyBtn.setAlignment(Pos.CENTER_LEFT);

        checkinGP.setPadding(new Insets(10,10,10,10));
        checkinGP.setHgap(5);
        checkinGP.setVgap(5);
        checkinGP.add(maleNumLabel,     0, 0, 1, 1);
        checkinGP.add(maleNum,          0, 1, 1, 1);
        checkinGP.add(femaleNumLabel,   1, 0, 1, 1);
        checkinGP.add(femaleNum,        1, 1, 1, 1);
        checkinGP.add(totalNumLabel,    2, 0, 1, 1);
        checkinGP.add(totalNum,         2, 1, 1, 1);
        checkinGP.add(breakFastNumLabel,3, 0, 1, 1);
        checkinGP.add(breakFastNum,     3, 1, 1, 1);
        checkinGP.add(staffNameLabel,   4, 0, 1, 1);
        checkinGP.add(staffName,        4, 1, 1, 1);
        checkinGP.add(roomNumLabel,     5, 0, 1, 1);
        checkinGP.add(roomNum,          5, 1, 1, 1);
        checkinGP.add(checkinTimeLabel, 6, 0, 1, 1);
        checkinGP.add(checkinTime,      6, 1, 1, 1);
        checkinGP.add(applyBtn,         6, 2, 1, 1);
        baseAP.setStyle("-fx-background-color: White;-fx-border-color: Black");

        pu.setAutoHide(true);
        pu.getContent().add(baseAP);
    }

    private Boolean showFirstNightBefore = Boolean.TRUE;
    private Boolean showSecondOrLaterNightBefore = Boolean.TRUE;
    private Boolean showCanceledBefore = Boolean.TRUE;
    void showAllReservationsOnThisDate(Boolean showFirstNight, Boolean showSecondOrLaterNight, Boolean showCanceled) {
        if( ! Objects.isNull(showFirstNight)) showFirstNightBefore = showFirstNight;
        if( ! Objects.isNull(showSecondOrLaterNight)) showSecondOrLaterNightBefore = showSecondOrLaterNight;
        if( ! Objects.isNull(showCanceled)) showCanceledBefore = showCanceled;

        long start = System.currentTimeMillis();
        if (rsvByDateOfStayTableFX.getSortOrder().size() > 0) {
            TableColumn sortColumn = (TableColumn) rsvByDateOfStayTableFX.getSortOrder().get(0);
            rsvByDateOfStayTableFX.getSortOrder().clear();
            rsvByDateOfStayTableFX.getSortOrder().add(sortColumn);
        }
        ObservableList<Reservations> reservations = ReservationsModel.getRsvListToday();
        if( ! showFirstNight || (Objects.isNull(showFirstNight) && ! showFirstNightBefore))
            reservations = FXCollections.observableArrayList(reservations.filtered(
                    rsv -> rsv.getReservationstatus().equals(reservationStatusCanceledString) ||
                            ! rsv.getNightspassed().equals((short)1)));
        if( ! showSecondOrLaterNight || (Objects.isNull(showSecondOrLaterNight) && ! showSecondOrLaterNightBefore))
            reservations = FXCollections.observableArrayList(reservations.filtered(
                    rsv -> rsv.getReservationstatus().equals(reservationStatusCanceledString) ||
                            ! (rsv.getNightspassed() > 1)));
        if( ! showCanceled || (Objects.isNull(showCanceled) && ! showCanceledBefore))
            reservations = FXCollections.observableArrayList(reservations.filtered(
                    rsv -> ! rsv.getReservationstatus().equals(reservationStatusCanceledString)));

        reservations = FXCollections.observableArrayList(reservations.sorted((Reservations a, Reservations b)->{
            //Reservationsテーブルの内容を見やすい順番にソートするアルゴリズム
            if( ! a.getReservationstatus().equals(b.getReservationstatus()))
                if(a.getReservationstatus().equals(reservationStatusNoshowString) &&
                        b.getReservationstatus().equals(reservationStatusValidString))
                    return 1;
                else if(a.getReservationstatus().equals(reservationStatusNoshowString) &&
                        b.getReservationstatus().equals(reservationStatusCanceledString))
                    return -1;
                else if(a.getReservationstatus().equals(reservationStatusCanceledString) &&
                        b.getReservationstatus().equals(reservationStatusNoshowString))
                    return 1;
                else
                    return a.getReservationstatus().compareTo(b.getReservationstatus());
            else if( ! a.getNightspassed().equals(b.getNightspassed()))
                return a.getNightspassed().compareTo(b.getNightspassed());
            else if( ! Objects.isNull(a.getResultnum())
                    && ! Objects.isNull(b.getResultnum())
                    && ! a.getResultnum().equals(b.getResultnum()))
                return a.getResultnum().compareTo(b.getResultnum());
            else if (Objects.isNull(a.getResultnum()) && ! Objects.isNull(a.getResultnum()))
                return -1;
            else if ( ! Objects.isNull(a.getResultnum()) && Objects.isNull(a.getResultnum()))
                return 1;
            else if( ! Objects.isNull(a.getGuestname()) && ! Objects.isNull(b.getGuestname()))
                return a.getGuestname().compareTo(b.getGuestname());
            else if(Objects.isNull(a.getGuestname()) && ! Objects.isNull(b.getGuestname()))
                return -1;
            else if( ! Objects.isNull(a.getGuestname()) && Objects.isNull(b.getGuestname()))
                return 1;
            return 0;
        }));
        rsvByDateOfStayTableFX.setItems(reservations);

        if(BedOccupationModel.getBOToday().getIsclosedday())
            rsvByDateOfStayTableFX.setPlaceholder(rsvByDateOfStayTableClosedDayPlaceHolder);
        else
            rsvByDateOfStayTableFX.setPlaceholder(rsvByDateOfStayTableNormalPlaceHolder);
        long end = System.currentTimeMillis();
        System.out.println("Time to showAllReservations : " + (end - start) + " msec");
    }
    protected void showAllDatesForThisReservation(Boolean ignoreChanges, Reservations rsv) {
        if (Objects.isNull(rsv)) {
            rsvByRsvNumTableFX.getItems().clear();
            return;
        }
        if( ! ignoreChanges && EditableTableCell.getModified().equals(Boolean.TRUE)
                && MessageBox.show(Tcyh.stage,
                        "修正中の予約があります。\n処理を続けると変更が破棄されますが、よろしいですか？",
                        "警告", MessageBox.YES|MessageBox.NO) != MessageBox.YES) {
            return;
        }
        Integer rsvNum = rsv.getReservationnumber();
        rsvByRsvNumTableFX.getItems().clear();
        rsvByRsvNumTableFX.setItems(ReservationsModel.fetchNewestReservations(rsvNum));
        EditableTableCell.setModified(Boolean.FALSE);
    }

    private final Label nameCBLabel = new Label("変更者名");
    private final ComboBox<String> nameCB = new ComboBox<String>();
    private final HBox nameCBHB = new HBox();
    private final TextArea changeCommentTA = new TextArea();
    private final Label changeCommentTALabel = new Label("変更理由/\n変更内容");
    private final HBox changeCommentTAHB = new HBox();
    private final VBox changeCommentAndStaffNameVB = new VBox();
    private VBox initChangeCommentAndStaffName() {
        if(Objects.isNull(nameCB.getItems()) || nameCB.getItems().isEmpty())
            nameCB.setItems(FXCollections.observableArrayList(
                    StaffModel.fetchStaff().stream()
                    .map(staff -> staff.getLastname())
                    .collect(Collectors.toList())
            ));
        nameCB.setEditable(true);
        nameCB.setDisable(false);
        nameCB.setValue("");
        nameCBHB.getChildren().clear();
        nameCBHB.getChildren().addAll(nameCBLabel, nameCB);
        nameCBHB.setPadding(new Insets(5, 5, 5, 5));
        nameCBHB.setAlignment(Pos.CENTER_LEFT);
        nameCBHB.setSpacing(10);
        changeCommentTA.setPrefSize(250, 80);
        changeCommentTA.setEditable(true);
        changeCommentTA.setDisable(false);
        changeCommentTA.setWrapText(true);
        changeCommentTA.setText("");
        changeCommentTAHB.getChildren().clear();
        changeCommentTAHB.getChildren().addAll(changeCommentTALabel, changeCommentTA);
        changeCommentTAHB.setPadding(new Insets(5, 5, 5, 5));
        changeCommentTAHB.setSpacing(5);
        changeCommentAndStaffNameVB.getChildren().clear();
        changeCommentAndStaffNameVB.getChildren().addAll(nameCBHB, changeCommentTAHB);
        return changeCommentAndStaffNameVB;
    }
    @FXML private void addDaysBeforeAndAfter() {
        if (rsvByRsvNumTableFX.getItems().isEmpty()) {
            return;
        }
        Spinner<Integer> beforeSpinner = new Spinner<Integer>(0, 100, 0);
        beforeSpinner.setMinWidth(60);
        beforeSpinner.setMaxWidth(60);
        Label beforeLabel1 = new Label("前に");
        Label beforeLabel2 = new Label("日 追加  ※チェックイン日の予約をコピー");
        HBox beforeHB = new HBox(beforeLabel1, beforeSpinner, beforeLabel2);
        beforeHB.setPadding(new Insets(5, 5, 5, 5));
        beforeHB.setSpacing(10);
        beforeHB.setAlignment(Pos.CENTER_LEFT);

        Spinner<Integer> afterSpinner = new Spinner<Integer>(0, 100, 0);
        afterSpinner.setMinWidth(60);
        afterSpinner.setMaxWidth(60);
        Label afterLabel1 = new Label("後に");
        Label afterLabel2 = new Label("日 追加  ※チェックアウト日の予約をコピー");
        HBox afterHB = new HBox(afterLabel1, afterSpinner, afterLabel2);
        afterHB.setPadding(new Insets(5, 5, 5, 5));
        afterHB.setSpacing(10);
        afterHB.setAlignment(Pos.CENTER_LEFT);

        Button cancelBtn = new Button("キャンセル");
        Button changeBtn = new Button("追加");
        HBox btnhb = new HBox(changeBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        VBox vb = new VBox(beforeHB, afterHB, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("変更履歴の入力");
        po.setHideOnEscape(true);
        po.setAutoHide(true);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 160
        );
        cancelBtn.setOnAction(a -> {
            po.hide();
            a.consume();
        });
        changeBtn.setOnAction(a -> {
            a.consume();
            if (Objects.isNull(nameCB.getValue()) || nameCB.getValue().isEmpty()) {
                MessageBox.show(
                        Tcyh.stage,
                        "担当者は入力が必須の項目です。\n入力してからやり直して下さい。",
                        "警告",
                        MessageBox.OK);
                return;
            }
            ReservationsPK pk = ((Reservations) rsvByRsvNumTableFX.getItems().get(0)).getReservationsPK();
            if((beforeSpinner.getValue() > 0 || afterSpinner.getValue() > 0) && ReservationsModel.addDays(
                        pk, changeCommentFormatter(true), beforeSpinner.getValue(), afterSpinner.getValue())) {
                MessageBox.show(Tcyh.stage, "予約を前後に追加しました。", "情報", MessageBox.OK);
                MainWindowController.getInstance().selectTab();
            } else
                MessageBox.show(Tcyh.stage, "追加する日数を、前後どちらかは必ず0以上にして下さい。", "警告", MessageBox.OK);
            po.hide();
        });
    }
    @FXML private void clickTableAction(MouseEvent me) {
        if( ! selectingRsvFlag && me.getClickCount() == 2) {
            MainWindowController.getInstance().setReservationsValuesToFields();
            me.consume();
            return;
        } else if( ! (selectingRsvFlag && me.getClickCount() == 2)) {
            me.consume();
            return;
        }
        Reservations rsv2 = rsvByDateOfStayTableFX.getSelectionModel().getSelectedItem();
        LocalDate checkinLDRsv1 = MainWindowController.asLocalDate(
                rsv1.getDateofstay()).minusDays(rsv1.getNightspassed() - 1);
        LocalDate checkinLDRsv2 = MainWindowController.asLocalDate(
                rsv2.getDateofstay()).minusDays(rsv2.getNightspassed() - 1);
        if( ! rsv1.getCheckOutDate().equals(java.sql.Date.valueOf(checkinLDRsv2)) &&
            ! rsv2.getCheckOutDate().equals(java.sql.Date.valueOf(checkinLDRsv1))) {
            MessageBox.show(Tcyh.stage, "一方の予約のチェックアウト日と、\n"
                    + "他方の予約のチェックイン日が同じ場合のみ\n"
                    + "連結することが可能です。"
                    , "エラー", MessageBox.OK);
            me.consume();
            return;
        }
        Text warning = new Text(
                "以下の予約を連結します。\nよろしいですか？\n"
                + "\n<予約1>"
                + "\n  予約番号: " + rsv1.getReservationnumber()
                + "\n  宿泊者氏名: " + rsv1.getGuestname()
                + "\n  チェックイン: " + new SimpleDateFormat(datePattern).format(rsv1.getDateofstay())
                + "\n  泊数: " + rsv1.getTotalnights()
                + "\n  人数:  (男性)" + rsv1.getNumberofmale()
                        + "  (女性)" + rsv1.getNumberoffemale()
                        + "  (幼児)" + rsv1.getNumberofbaby()
                + "\n\n<予約2>"
                + "\n  予約番号: " + rsv2.getReservationnumber()
                + "\n  宿泊者氏名: " + rsv2.getGuestname()
                + "\n  チェックイン: " + new SimpleDateFormat(datePattern).format(rsv2.getDateofstay())
                + "\n  泊数: " + rsv2.getTotalnights()
                + "\n  人数:  (男性)" + rsv2.getNumberofmale()
                        + "  (女性)" + rsv2.getNumberoffemale()
                        + "  (幼児)" + rsv2.getNumberofbaby()
        );
        Button cancelBtn = new Button("キャンセル");
        Button uniteBtn = new Button("連結");
        HBox btnhb = new HBox(uniteBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        VBox vb = new VBox(warning, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("予約の連結");
        po.setHideOnEscape(true);
        po.setAutoHide(false);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 80
        );
        cancelBtn.setOnAction(a -> {
            a.consume();
            po.hide();
        });
        uniteBtn.setOnAction(a -> {
            a.consume();
            if(Objects.isNull(nameCB.getValue()) || nameCB.getValue().isEmpty()) {
                MessageBox.show(Tcyh.stage, "担当者名は入力必須項目です。", "警告", MessageBox.OK);
                return;
            }
            selectingRsvFlag = false;
            uniteReservationsButtonFX.setText("予約を連結");
            MainWindowController.getInstance().tabPaneFX.getTabs()
                    .forEach((Tab tab) -> tab.setDisable(false));
            rsv1.setComments(rsv1.getComments());
            if( ! ReservationsModel.uniteReservations(
                    rsv1, rsv2,changeCommentFormatter(true))) {
                MessageBox.show(Tcyh.stage,
                        "連結処理に「失敗」しました。\n処理を中断します。", "警告", MessageBox.OK);
            } else {
                Date checkin = null, checkout = null;
                if (rsv1.getCheckOutDate().equals(java.sql.Date.valueOf(checkinLDRsv2))) {
                    checkin = rsv1.getCheckInDate();
                    checkout = rsv2.getCheckOutDate();
                } else if (rsv2.getCheckOutDate().equals(java.sql.Date.valueOf(checkinLDRsv1))) {
                    checkin = rsv2.getCheckInDate();
                    checkout = rsv1.getCheckOutDate();
                }
                ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
                if(errors.isEmpty()) {
                    MessageBox.show(Tcyh.stage, "予約の連結が完了しました。", "情報", MessageBox.OK);
                    MainWindowController.getInstance().selectTab();
                } else {
                    MessageBox.show(Tcyh.stage,
                            "予約の連結は完了しましたが、\n"
                                    + "連結した予約のチェックイン日(" + checkin + ")と"
                                    + "チェックアウト日(" + checkout + ")の間で、\n"
                                    + "エラーを検知しました。" , "警告", MessageBox.OK);
                    AdministrationController.getInstance()
                            .showConsistencyCheckErrors(checkin, checkout, errors);
                }
            }
            po.hide();
        });
        me.consume();
    }
    private void onSelectionChanged() {
        if( ! rsvByDateOfStayTableFX.getSelectionModel().isEmpty()) {
            showAllDatesForThisReservation(Boolean.FALSE, rsvByDateOfStayTableFX.getSelectionModel().getSelectedItem());
        }
    }
    @FXML private void cancelSomeDatesButtonAction() {
        List<Reservations> list = rsvByRsvNumTableFX.getItems();
        if (list.stream().filter(rsv -> checkedCheckBoxCol2.getCellData(rsv)).count() == 0) {
            MessageBox.show(Tcyh.stage, "どの予約もチェックがついていません。", "警告", MessageBox.OK);
            return;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(rsv -> {
            if ( ! rsv.getChecked()) {
                return;
            }
            sb.append("                  ");
            sb.append(new SimpleDateFormat(datePattern).format(rsv.getDateofstay()));
            sb.append("\n");
        });

        if (rsvByRsvNumTableFX.getItems().isEmpty()) {
            return;
        }
        Button cancelBtn = new Button("キャンセル");
        Button changeBtn = new Button("変更");
        HBox btnhb = new HBox(changeBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        Text warning = new Text("以下の日にちの予約をキャンセルします。\n\n" + sb.toString());
        VBox vb = new VBox(warning, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("予約のキャンセル");
        po.setHideOnEscape(true);
        po.setAutoHide(true);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 80
        );
        cancelBtn.setOnAction(a -> {
            a.consume();
            po.hide();
        });
        changeBtn.setOnAction(a -> {
            a.consume();
            if(Objects.isNull(nameCB.getValue()) || nameCB.getValue().isEmpty()) {
                MessageBox.show(Tcyh.stage, "担当者名は入力必須項目です。", "警告", MessageBox.OK);
                return;
            }
            if( ! ReservationsModel.cancelSomeDates(
                    rsvByRsvNumTableFX.getItems(), false, changeCommentFormatter(true))) {
                MessageBox.show(Tcyh.stage,
                        "予約のキャンセルに「失敗」しました。\n処理を中断します。", "警告", MessageBox.OK);
            } else {
                Date checkin = rsvByRsvNumTableFX.getItems().get(0).getDateofstay();
                Date checkout = rsvByRsvNumTableFX.getItems()
                        .get(rsvByRsvNumTableFX.getItems().size() - 1).getDateofstay();
                ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
                if(errors.isEmpty()) {
                    MessageBox.show(Tcyh.stage, "キャンセル処理が完了しました。", "情報", MessageBox.OK);
                    MainWindowController.getInstance().selectTab();
                    showAllDatesForThisReservation(Boolean.TRUE, rsvByRsvNumTableFX.getItems().get(0));
                } else {
                    MessageBox.show(Tcyh.stage,
                            "キャンセル処理は完了しましたが、\n"
                                    + "キャンセルした予約のチェックイン日(" + checkin + ")とチェックアウト日(" + checkout + ")の間で、\n"
                                    + "エラーを検知しました。" , "警告", MessageBox.OK);
                    AdministrationController.getInstance()
                            .showConsistencyCheckErrors(checkin, checkout, errors);
                }
            }
            po.hide();
        });
    }
    @FXML private void commitChangesButtonAction() {
        if (rsvByRsvNumTableFX.getItems().isEmpty()) {
            return;
        }
        Button cancelBtn = new Button("キャンセル");
        Button changeBtn = new Button("変更");
        HBox btnhb = new HBox(changeBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        CheckBox reviseChangeBefore = new CheckBox("直前の変更の訂正(※コメント等不要)");
        reviseChangeBefore.selectedProperty().addListener(ae -> {
            nameCB.setDisable(reviseChangeBefore.selectedProperty().get());
            changeCommentTA.setDisable(reviseChangeBefore.selectedProperty().get());
        });
        VBox vb = new VBox(reviseChangeBefore, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("変更履歴の入力");
        po.setHideOnEscape(true);
        po.setAutoHide(true);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 80
        );
        cancelBtn.setOnAction(a -> {
            a.consume();
            po.hide();
        });
        changeBtn.setOnAction(a -> {
            a.consume();
            ObservableList<Reservations> rsvList = rsvByRsvNumTableFX.getItems();
            if( ! ReservationsModel.commitChanges(
                    rsvList, changeCommentFormatter( ! reviseChangeBefore.selectedProperty().get()))) {
                MessageBox.show(Tcyh.stage,
                        "予約の変更に「失敗」しました。\n処理を中断します。", "警告", MessageBox.OK);
            } else {
                Date checkin = rsvByRsvNumTableFX.getItems().get(0).getDateofstay();
                Date checkout = rsvByRsvNumTableFX.getItems()
                        .get(rsvByRsvNumTableFX.getItems().size() - 1).getDateofstay();
                ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
                if(errors.isEmpty()) {
                    MessageBox.show(Tcyh.stage, "予約の変更が完了しました。", "情報", MessageBox.OK);
                    showAllDatesForThisReservation(Boolean.TRUE, rsvList.get(0));
                    MainWindowController.getInstance().selectTab();
                } else {
                    MessageBox.show(Tcyh.stage,
                            "予約の変更は完了しましたが、\n"
                                    + "変更した予約のチェックイン日(" + checkin + ")とチェックアウト日(" + checkout + ")の間で、\n"
                                    + "エラーを検知しました。" , "警告", MessageBox.OK);
                    AdministrationController.getInstance()
                            .showConsistencyCheckErrors(checkin, checkout, errors);
                }
            }
            po.hide();
        });
    }
    private static Boolean selectingRsvFlag = false;
    private static Reservations rsv1;
    @FXML private void uniteReservationsButtonAction() {
        if(Objects.isNull(rsvByDateOfStayTableFX.getSelectionModel().getSelectedItem())) {
            MessageBox.show(Tcyh.stage, "予約が選択されていません。\n連結元の予約を選択してから、再度お試し下さい。", "エラー", MessageBox.OK);
        } else if( ! selectingRsvFlag) {
            MessageBox.show(Tcyh.stage, "この予約に連結する、別の予約を選択して下さい。\n(ダブルクリックで決定)", "予約の連結", MessageBox.OK);
            rsv1 = rsvByDateOfStayTableFX.getSelectionModel().getSelectedItem();
            selectingRsvFlag = true;
            uniteReservationsButtonFX.setText("予約の連結の中止");
            MainWindowController.getInstance().tabPaneFX.getTabs()
                    .filtered(tab -> ! tab.equals(MainWindowController.getInstance().reservationsTabFX))
                    .forEach((Tab tab) -> tab.setDisable(true));
        } else {
            Integer val = MessageBox.show(Tcyh.stage, "予約の連結を中止しますか？", "連結の中止", MessageBox.YES|MessageBox.NO);
            if(val.equals(MessageBox.YES)) {
                selectingRsvFlag = false;
                uniteReservationsButtonFX.setText("予約を連結");
                MainWindowController.getInstance().tabPaneFX.getTabs()
                        .forEach((Tab tab) -> tab.setDisable(false));
            }
        }
    }
    @FXML private void noShowButtonAction() {
        List<Reservations> list = rsvByRsvNumTableFX.getItems();
        if (list.stream().filter(rsv -> rsv.getChecked()).count() == 0) {
            MessageBox.show(Tcyh.stage, "NoShow対象の予約が選択されていません。", "警告", MessageBox.OK);
            return;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(rsv -> {
            if (!rsv.getChecked()) {
                return;
            }
            sb.append("                  ");
            sb.append(new SimpleDateFormat(datePattern).format(rsv.getDateofstay()));
            sb.append("\n");
        });
        Button cancelBtn = new Button("キャンセル");
        Button changeBtn = new Button("変更");
        HBox btnhb = new HBox(changeBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        Label label = new Label("以下の日にちの予約をNoShow処理します。\n"
                + "(予約済みの部屋・ベッドも開放します)\n\n" + sb.toString());
        VBox vb = new VBox(label, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("変更履歴の入力");
        po.setHideOnEscape(true);
        po.setAutoHide(true);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 80
        );
        cancelBtn.setOnAction(a -> {
            a.consume();
            po.hide();
        });
        changeBtn.setOnAction(a -> {
            a.consume();
            if(Objects.isNull(nameCB.getValue()) || nameCB.getValue().isEmpty()) {
                MessageBox.show(Tcyh.stage, "担当者名は入力必須項目です。", "警告", MessageBox.OK);
                return;
            }
            if( ! ReservationsModel.cancelSomeDates(
                    rsvByRsvNumTableFX.getItems(), true, changeCommentFormatter(true)))
                MessageBox.show(Tcyh.stage,
                        "NoShow処理に「失敗」しました。\n処理を中断します。", "警告", MessageBox.OK);
            else {
                Date checkin = rsvByRsvNumTableFX.getItems().get(0).getDateofstay();
                Date checkout = rsvByRsvNumTableFX.getItems()
                        .get(rsvByRsvNumTableFX.getItems().size() - 1).getDateofstay();
                ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
                if(errors.isEmpty()) {
                    MessageBox.show(Tcyh.stage, "NoShow処理が完了しました。", "警告", MessageBox.OK);
                    rsvByRsvNumTableFX.getItems().clear();
                    MainWindowController.getInstance().selectTab();
                } else {
                    MessageBox.show(Tcyh.stage,
                            "NoShow処理は完了しましたが、\n"
                            + "処理した予約のチェックイン日(" + checkin + ")とチェックアウト日(" + checkout + ")の間で、\n"
                            + "エラーを検知しました。" , "情報", MessageBox.OK);
                    AdministrationController.getInstance()
                            .showConsistencyCheckErrors(checkin, checkout, errors);
                }
            }
            po.hide();
        });
    }
    @FXML private void deleteData() {
        if(Objects.isNull(rsvByRsvNumTableFX.getItems()) || rsvByRsvNumTableFX.getItems().isEmpty()) {
            MessageBox.show(Tcyh.stage, "削除対象の予約が選択されていません。", "警告", MessageBox.OK);
            return;
        }
        Integer rsvNum = rsvByRsvNumTableFX.getItems().get(0).getReservationnumber();
        for(Reservations rsv : ReservationsModel.fetchReservationsByRsvNum(rsvNum)) {
            if(rsv.getReservationstatus().equals(reservationStatusValidString)) {
                MessageBox.show(Tcyh.stage, "該当の予約番号で、有効な予約がまだ残っています。"
                        + "\nそれらをCXL/NoShow処理してから、再度お試し下さい。", "警告", MessageBox.OK);
                return;
            }
        }
        if(MessageBox.show(Tcyh.stage, "該当の予約を、過去の履歴を含めて完全に削除します。\n"
                + "よろしいですか？", "警告", MessageBox.YES|MessageBox.NO)
                != MessageBox.YES) return;
        Date checkin = rsvByRsvNumTableFX.getItems().get(0).getDateofstay();
        Date checkout = rsvByRsvNumTableFX.getItems()
                .get(rsvByRsvNumTableFX.getItems().size() - 1).getDateofstay();
        if( ! ReservationsModel.deleteData(rsvNum))
            MessageBox.show(Tcyh.stage,
                    "予約の削除に「失敗」しました。\n処理を中止します。","警告", MessageBox.OK);
        else {
            ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
            if(errors.isEmpty()) {
                MessageBox.show(Tcyh.stage, "予約の削除が完了しました。", "警告", MessageBox.OK);
                rsvByRsvNumTableFX.getItems().clear();
                MainWindowController.getInstance().selectTab();
            } else {
                MessageBox.show(Tcyh.stage,
                        "予約の削除は完了しましたが、\n"
                        + "削除した予約のチェックイン日(" + checkin + ")とチェックアウト日(" + checkout + ")の間で、\n"
                        + "エラーを検知しました。" , "情報", MessageBox.OK);
                AdministrationController.getInstance()
                        .showConsistencyCheckErrors(checkin, checkout, errors);
            }
        }
    }
    @FXML private void restoreCanceledRsv() {
        if(Objects.isNull(rsvByRsvNumTableFX.getItems()) || rsvByRsvNumTableFX.getItems().isEmpty()) {
            MessageBox.show(Tcyh.stage, "復元対象の予約が選択されていません。", "警告", MessageBox.OK);
            return;
        }
        Integer rsvNum = rsvByRsvNumTableFX.getItems().get(0).getReservationnumber();
        for(Reservations rsv : ReservationsModel.fetchReservationsByRsvNum(rsvNum)) {
            if(rsv.getReservationstatus().equals(reservationStatusValidString)) {
                MessageBox.show(Tcyh.stage, "該当の予約番号で、有効な予約がまだ残っています。"
                        + "\n(全てCXL済みの予約しか復元できません。)", "警告", MessageBox.OK);
                return;
            }
        }
        Button cancelBtn = new Button("キャンセル");
        Button changeBtn = new Button("変更");
        HBox btnhb = new HBox(changeBtn, cancelBtn);
        btnhb.setPadding(new Insets(5, 5, 5, 5));
        btnhb.setSpacing(5);
        CheckBox reviseChangeBefore = new CheckBox("直前の変更の訂正(※コメント等不要)");
        reviseChangeBefore.selectedProperty().addListener(ae -> {
            nameCB.setDisable(reviseChangeBefore.selectedProperty().get());
            changeCommentTA.setDisable(reviseChangeBefore.selectedProperty().get());
        });
        VBox vb = new VBox(reviseChangeBefore, initChangeCommentAndStaffName(), btnhb);
        vb.setPadding(new Insets(10, 10, 10, 10));
        AnchorPane ap = new AnchorPane(vb);
        ap.setStyle("-fx-background-color: lightgray;");
        PopOver po = new PopOver(ap);
        po.setDetached(true);
        po.setDetachedTitle("変更履歴の入力");
        po.setHideOnEscape(true);
        po.setAutoHide(true);
        po.show(baseAnchorPane,
                baseAnchorPane.getScene().getWindow().getWidth() / 2 - 180,
                baseAnchorPane.getScene().getWindow().getHeight() / 2 - 80
        );
        cancelBtn.setOnAction(a -> {
            a.consume();
            po.hide();
        });
        changeBtn.setOnAction(a -> {
            a.consume();
            if( ! reviseChangeBefore.selectedProperty().getValue()
                    && (Objects.isNull(nameCB.getValue()) || nameCB.getValue().isEmpty())) {
                MessageBox.show(Tcyh.stage, "担当者名は入力必須項目です。", "警告", MessageBox.OK);
                return;
            }
            if( ! ReservationsModel.restoreCanceledRsv(rsvNum, changeCommentFormatter(true))) {
                MessageBox.show(Tcyh.stage, "予約の復元に失敗しました。"
                        + "\n処理を中断します。", "警告", MessageBox.OK);
            } else {
                Date checkin = rsvByRsvNumTableFX.getItems().get(0).getDateofstay();
                Date checkout = rsvByRsvNumTableFX.getItems()
                        .get(rsvByRsvNumTableFX.getItems().size() - 1).getDateofstay();
                ObservableList<String> errors = ConsistencyCheck.examinAll(checkin, checkout);
                if(errors.isEmpty()) {
                    MessageBox.show(Tcyh.stage, "予約の復元が完了しました。", "警告", MessageBox.OK);
                    MainWindowController.getInstance().selectTab();
                    showAllReservationsOnThisDate(null, null, null);
                    showAllDatesForThisReservation(Boolean.TRUE, rsvByRsvNumTableFX.getItems().get(0));
                } else {
                    MessageBox.show(Tcyh.stage,
                            "予約の削除は完了しましたが、\n"
                            + "削除した予約のチェックイン日(" + checkin + ")と"
                            + "チェックアウト日(" + checkout + ")の間で、\n"
                            + "エラーを検知しました。" , "情報", MessageBox.OK);
                    AdministrationController.getInstance()
                            .showConsistencyCheckErrors(checkin, checkout, errors);
                }
            }
        });
    }
    
    @FXML private void fillInCheckInData(MouseEvent me) {
        List<Reservations> listOriginal = rsvByRsvNumTableFX.getItems();
        if(Objects.isNull(listOriginal) || listOriginal.isEmpty()) {
            MessageBox.show(Tcyh.stage, "予約が表示されていません。"
                    + "\n上段の表で予約を選択して下さい。", "警告", MessageBox.OK);
            return;
        }
        List<Reservations> listFilterd = listOriginal.stream()
                .filter(rsv -> rsv.getChecked()).collect(Collectors.toList());
        if(Objects.isNull(listFilterd) || listFilterd.isEmpty()) {
            MessageBox.show(Tcyh.stage, "対象となる予約が選択されていません。"
                    + "\n入力したい予約にチェックを入れて下さい。", "警告", MessageBox.OK);
            return;
        }
        checkinTime.setText(LocalTime.now().plusMinutes(-5)
                .format(DateTimeFormatter.ofPattern("HH:mm")));
        pu.show(rsvByRsvNumTableFX, me.getScreenX(), me.getScreenY() - 100);
        me.consume();
        applyBtn.setOnMouseClicked((MouseEvent applyBtnME) -> {
            applyBtnME.consume();
            listFilterd.forEach(rsv -> {
                rsv.setMaleresult(Objects.isNull(maleNum.getValue()) ?
                        null : Short.decode(maleNum.getValue().toString()));
                rsv.setFemaleresult(Objects.isNull(femaleNum.getValue()) ?
                        null : Short.decode(femaleNum.getValue().toString()));
                rsv.setPaidbreakfast(Objects.isNull(breakFastNum.getValue()) ?
                        null : Short.decode(breakFastNum.getValue().toString()));
                rsv.setCashier(staffName.getValue());
                rsv.setRoomnumresult(roomNum.getText());
                rsv.setCheckintime(checkinTime.getText());
            });
        });
    }
    @FXML private void showAllRevisions() {
        if(Objects.isNull(rsvByRsvNumTableFX.getItems())
                || rsvByRsvNumTableFX.getItems().isEmpty()) {
            MessageBox.show(Tcyh.stage, "予約が表示されていません。", "警告", MessageBox.OK);
            return;
        }
        MainWindowController.getInstance().tabPaneFX.getSelectionModel().select(
                MainWindowController.getInstance().changeHistoryTabFX
        );
        ChangeHistory.getInstance().showReservations(
                rsvByRsvNumTableFX.getItems().get(0).getReservationnumber());
    }

    private String changeCommentFormatter(Boolean nameAndCommentRequired) {
        String person = nameCB.getValue();
        String comment = changeCommentTA.getText();
        return nameAndCommentRequired ? "<" + person + "> " + comment + "\n" : "";
    }
}
