/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import static tcyh.Constants.*;

/**
 *
 * @author masanori
 */
public class ConsistencyCheck {
    private static final ConsistencyCheck instance = new ConsistencyCheck();
    protected static ConsistencyCheck getInstance() {
        return instance;
    }
    private ConsistencyCheck() {}

    private static final SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
    private static ObservableList<Reservations> rsvList;
    private static ObservableList<BedOccupation> boList;
    public static ObservableList<String> examinAll(Date from, Date to) {
        //検査前の初期化
        errors.clear();
        rsvList = ReservationsModel.fetchReservationsBySpecificPeriod(from, to);
        boList = BedOccupationModel.fetchBedOccupationBySpecificPeriod(from, to);

        compareRsvAndBO(from, to);
        rsvList.forEach(rsv -> {
            //NoShowの場合は以降の検査を行う必要なし
            if(rsv.getReservationstatus().equals(reservationStatusNoshowString))
                return;
            
            String header = "予約番号: " + rsv.getReservationnumber()
                        + " 宿泊日:" + sdf.format(rsv.getDateofstay());
            List<BedOccupation> boOnSameDate
                    = boList.filtered(bo -> bo.getDateofstay().equals(rsv.getDateofstay()));
            if(rsv.getTotalnum().equals(0))
                errors.add(header
                        + " :予約リストの該当の予約は、予約人数が0人になっています。"
                        + "(意図的にそうしている場合、このエラーは無視できます)");
            if(rsv.getMalebed().equals((short)0) && rsv.getFemalebed().equals((short)0)
                    && rsv.getQuadroom().equals((short)0) && rsv.getBarrierfreeroom().equals((short)0)
                    && rsv.getTatamiroom().equals((short)0) && rsv.getStaffroom().equals((short)0)
                    && rsv.getLarge1808().equals(Boolean.FALSE) && rsv.getLarge1820().equals(Boolean.FALSE)
                    && rsv.getLarge1908().equals(Boolean.FALSE) && rsv.getLarge1917().equals(Boolean.FALSE))
                errors.add(header
                        + " :予約リストの該当の予約は、部屋数が0(どの部屋もおさえられていない)の状態です。"
                        + "(意図的にそうしている場合、このエラーは無視できます)");
            if( ! boOnSameDate.get(0).getIsbreakfastservedtomorrow()
                    && ! (Objects.isNull(rsv.getBreakfast()) || rsv.getBreakfast().equals((short)0))
                    || ! (Objects.isNull(rsv.getPaidbreakfast()) || rsv.getPaidbreakfast().equals((short)0)))
                errors.add(header
                        + " :予約リストの該当の予約は、朝食提供なしの日に朝食数に0以上の値が入力されています。");
            rsvList.filtered((Reservations rsvFiltered) ->
                    rsvFiltered.getReservationnumber().equals(
                            rsv.getReservationnumber()))
                    .sorted((a,b) -> a.getSequencenumber().compareTo(b.getSequencenumber()));
        });
        Map<Integer, List<Reservations>> map = rsvList.stream()
                .collect(Collectors.groupingBy(rsv -> rsv.getReservationnumber(),
                        Collectors.mapping(rsv -> rsv, Collectors.toList())));
        map.forEach((rsvNum, rsvListByRsvNum) -> {
            rsvListByRsvNum.sort((a,b) ->
                    a.getSequencenumber().compareTo(b.getSequencenumber()));
            //リストの中に全ての宿泊日が含まれていない可能性があるので、
            //その場合は予約番号を元に全宿泊日を引っ張ってくる。
            if(rsvListByRsvNum.get(0).getCheckInDate().before(from)
                    || rsvListByRsvNum.get(0).getCheckInDate().after(to))
                    rsvListByRsvNum = ReservationsModel.fetchNewestReservations(rsvNum);
            String header = "予約番号: " + rsvListByRsvNum.get(0).getReservationnumber()
                        + " 宿泊日:" + sdf.format(rsvListByRsvNum.get(0).getDateofstay());
            LocalDate ldBefore = MainWindowController.asLocalDate(
                    rsvListByRsvNum.get(0).getDateofstay())
                    .minusDays(1);
            Short i = 1;
            for(Reservations rsvByRsvNum : rsvListByRsvNum) {
                if( ! rsvByRsvNum.getSequencenumber().equals(rsvByRsvNum.getNightspassed()))
                    errors.add(header
                            + " 履歴番号:" + rsvByRsvNum.getRevisionnumber()
                            + " 項番:" + rsvByRsvNum.getSequencenumber()
                            + " :該当の予約は、「項番」と「何泊目か」が一致していません。");
                else if( ! rsvByRsvNum.getSequencenumber().equals(i))
                    errors.add(header
                            + " 履歴番号:" + rsvByRsvNum.getRevisionnumber()
                            + " 項番:" + rsvByRsvNum.getSequencenumber()
                            + " :該当の予約は、「項番」が連続した値になっていません。");
                else if( ! rsvByRsvNum.getNightspassed().equals(i))
                    errors.add(header
                            + " 履歴番号:" + rsvByRsvNum.getRevisionnumber()
                            + " 項番:" + rsvByRsvNum.getSequencenumber()
                            + " :該当の予約は、「何泊目か」が連続した値になっていません。");
                if( ! MainWindowController.asLocalDate(rsvByRsvNum.getDateofstay())
                        .minusDays(1).equals(ldBefore))
                    errors.add(header
                            + " :項番" + (i -1) + "と項番" + i + "の間で、"
                            + "宿泊日が連続していません。");
                ldBefore = MainWindowController.asLocalDate(rsvByRsvNum.getDateofstay());
                i++;
            }
        });
        
        boList.forEach(bo -> {
            //TODO
        });
        return FXCollections.observableArrayList(errors);
    }
    private static final List<String> errors = new ArrayList<String>();
    private static BedOccupation boForCompareRoomCount;
    private static HashMap<String, HashMap<String, Integer>> boHM;
    private static String rsvNumNow;
    private static void compareRsvAndBO(Date from, Date to) {
        for(BedOccupation boNow : boList) {
            HashMap<String, HashMap<String, Integer>> boHMNow = BedOccupationModel.masterRnumCounter(boNow);

            //BarrierFree,Tatami,StaffのHashMapを1つにまとめる
            boHMNow.put("barrierfreeroom", new HashMap<String, Integer>());
            boHMNow.get("1801").keySet().forEach(rsvNum -> {
                if(boHMNow.get("1811").containsKey(rsvNum)) {
                    boHMNow.get("barrierfreeroom").put(rsvNum,
                            boHMNow.get("1801").get(rsvNum)
                            + boHMNow.get("1811").get(rsvNum));
                    boHMNow.get("1811").remove(rsvNum);
                }
                else {
                    boHMNow.get("barrierfreeroom").put(rsvNum, boHMNow.get("1801").get(rsvNum));
                }
            });
            boHMNow.get("1811").keySet().forEach(rsvNum -> {
                boHMNow.get("barrierfreeroom").put(rsvNum, boHMNow.get("1811").get(rsvNum));
            });
            boHMNow.put("tatamiroom", new HashMap<String, Integer>());
            boHMNow.get("1807").keySet().forEach(rsvNum -> {
                if(boHMNow.get("1819").containsKey(rsvNum)) {
                    boHMNow.get("tatamiroom").put(rsvNum,
                            boHMNow.get("1807").get(rsvNum)
                            + boHMNow.get("1819").get(rsvNum));
                    boHMNow.get("1819").remove(rsvNum);
                }
                else {
                    boHMNow.get("tatamiroom").put(rsvNum, boHMNow.get("1807").get(rsvNum));
                }
            });
            boHMNow.get("1819").keySet().forEach(rsvNum -> {
                boHMNow.get("tatamiroom").put(rsvNum, boHMNow.get("1819").get(rsvNum));
            });
            boHMNow.put("staffroom", new HashMap<String, Integer>());
            boHMNow.get("staffl").keySet().forEach(rsvNum -> {
                if(boHMNow.get("staffr").containsKey(rsvNum)) {
                    boHMNow.get("staffroom").put(rsvNum,
                            boHMNow.get("staffl").get(rsvNum)
                            + boHMNow.get("staffr").get(rsvNum));
                    boHMNow.get("staffr").remove(rsvNum);
                }
                else {
                    boHMNow.get("staffroom").put(rsvNum, boHMNow.get("staffl").get(rsvNum));
                }
            });
            boHMNow.get("staffr").keySet().forEach(rsvNum -> {
                boHMNow.get("staffroom").put(rsvNum, boHMNow.get("staffr").get(rsvNum));
            });

            //ReservationsとBedOccupationの比較を行う。
            boForCompareRoomCount = boNow;
            boHM = boHMNow;
            ObservableList<Reservations> rsvListNow = rsvList.filtered(rsv ->
                    rsv.getDateofstay().equals(boNow.getDateofstay())
                    && ! rsv.getReservationstatus().equals(reservationStatusNoshowString)
            );
            rsvListNow.forEach(rsvNow -> {
                String rsvNum = rsvNow.getReservationnumber().toString();
                rsvNumNow = rsvNum;
                //Reservations上の各部屋数が、BedOccupation上の数と一致していることを確認
                compareRoomCounts("mdorm", rsvNow.getMalebed(), "Mドミ");
                compareRoomCounts("fdorm", rsvNow.getFemalebed(), "Fドミ");
                compareRoomCounts("quadroom", rsvNow.getQuadroom(), "4人部屋");
                compareRoomCounts("1820", rsvNow.getLarge1820() ? (short)1 : (short)0, "1820");
                compareRoomCounts("1917", rsvNow.getLarge1917() ? (short)1 : (short)0, "1917");
                compareRoomCounts("1808", rsvNow.getLarge1808() ? (short)1 : (short)0, "1808");
                compareRoomCounts("1908", rsvNow.getLarge1908() ? (short)1 : (short)0, "1908");
                compareRoomCounts("barrierfreeroom", rsvNow.getBarrierfreeroom(), "2人部屋");
                compareRoomCounts("tatamiroom", rsvNow.getTatamiroom(), "和室");
                compareRoomCounts("staffroom", rsvNow.getStaffroom(), "スタッフ室");
            });
            
            //BedOccupation上にキャンセル/変更済みで本当は不要なのに、まだ残っている部屋がないか確認
            boHMNow.forEach((roomType, hm) -> {
                String roomTypeStr;
                switch(roomType) {
                    case "mdorm" : roomTypeStr = "Mドミ";break;
                    case "fdorm" : roomTypeStr = "Fドミ";break;
                    case "quadroom" : roomTypeStr = "4人部屋";break;
                    case "1820" : roomTypeStr = "1820";break;
                    case "1917" : roomTypeStr = "1917";break;
                    case "1808" : roomTypeStr = "1808";break;
                    case "1908" : roomTypeStr = "1908";break;
                    case "barrierfreeroom" : roomTypeStr = "2人部屋";break;
                    case "tatamiroom" : roomTypeStr = "和室";break;
                    case "staffroom" : roomTypeStr = "スタッフ室";break;
                    default : return;
                }
                hm.forEach((rsvNum, count) -> {
                    if( ! rsvNum.matches(mdormString+"|"+mdormFixedString
                            +"|"+fdormString+"|"+fdormFixedString
                            +"|"+spareString+"|"+"")) {
                        errors.add("予約番号:" + rsvNum
                                + " 宿泊日:" + sdf.format(boNow.getDateofstay())
                                + ":空室台帳上に、予約リストにない予約(変更/キャンセル済みの可能性あり)"
                                + "でおさえられた部屋・ベッド(" + roomTypeStr + ")が" + count + "つあります。"
                        );
                    }
                });
            });
        }
    }
    private static void compareRoomCounts(String roomType, Short requiredRoom, String roomName) {
        String date = sdf.format(boForCompareRoomCount.getDateofstay());
        if( ! Objects.isNull(boHM.get(roomType).get(rsvNumNow))) {
            if( ! boHM.get(roomType).get(rsvNumNow).equals(requiredRoom.intValue())) {
                errors.add("予約番号:" + rsvNumNow + " 宿泊日:" + date
                        + " :予約リストと空室台帳で、" + roomName + "の数が一致していません。"
                        + " (予約リスト: " + requiredRoom
                        + " 空室台帳: " + boHM.get(roomType).get(rsvNumNow) + " )");
            }
        } else if (requiredRoom > 0) {
            errors.add("予約番号:" + rsvNumNow + " 宿泊日:" + date
                    + " :予約リスト上の" + roomName + "の数が" + requiredRoom + "(0以上)なのに、"
                    + "空室台帳上で" + roomName + "がおさえられていません。"
                    + "一度予約リスト上で0に変更してから、再度" + requiredRoom + "に変更してみて下さい。"
                    + "(大部屋の場合は一度チェックを外して保存し、再度チェックを入れて保存してみる)");
        }
        boHM.get(roomType).remove(rsvNumNow);
    }
}
