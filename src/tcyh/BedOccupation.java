/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author masanori
 */
@Data
@Entity
@Table(name = "BEDOCCUPATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BedOccupation.findAll", query = "SELECT r FROM BedOccupation r"),
    @NamedQuery(name = "BedOccupation.findByDateofstay", query = "SELECT r FROM BedOccupation r WHERE r.dateofstay = :dateofstay"),
    @NamedQuery(name = "BedOccupation.findByMultipleDateofstay", query = "SELECT r FROM BedOccupation r WHERE r.dateofstay IN :dateofstay order by r.dateofstay"),
    @NamedQuery(name = "BedOccupation.findByMDorm", query = "SELECT r FROM BedOccupation r WHERE r.mdorm = :mdorm"),
    @NamedQuery(name = "BedOccupation.findByFDorm", query = "SELECT r FROM BedOccupation r WHERE r.fdorm = :fdorm"),
    @NamedQuery(name = "BedOccupation.findByL1820", query = "SELECT r FROM BedOccupation r WHERE r.l1820 = :l1820"),
    @NamedQuery(name = "BedOccupation.findByL1917", query = "SELECT r FROM BedOccupation r WHERE r.l1917 = :l1917"),
    @NamedQuery(name = "BedOccupation.findByL1808", query = "SELECT r FROM BedOccupation r WHERE r.l1808 = :l1808"),
    @NamedQuery(name = "BedOccupation.findByL1908", query = "SELECT r FROM BedOccupation r WHERE r.l1908 = :l1908"),
    @NamedQuery(name = "BedOccupation.findByB1801", query = "SELECT r FROM BedOccupation r WHERE r.b1801 = :b1801"),
    @NamedQuery(name = "BedOccupation.findByB1811", query = "SELECT r FROM BedOccupation r WHERE r.b1811 = :b1811"),
    @NamedQuery(name = "BedOccupation.findByT1807", query = "SELECT r FROM BedOccupation r WHERE r.t1807 = :t1807"),
    @NamedQuery(name = "BedOccupation.findByT1819", query = "SELECT r FROM BedOccupation r WHERE r.t1819 = :t1819"),
    @NamedQuery(name = "BedOccupation.findByStaffL", query = "SELECT r FROM BedOccupation r WHERE r.staffl = :staffl"),
    @NamedQuery(name = "BedOccupation.findByStaffR", query = "SELECT r FROM BedOccupation r WHERE r.staffr = :staffr"),
    @NamedQuery(name = "BedOccupation.findByQuadroom", query = "SELECT r FROM BedOccupation r WHERE r.quadroom = :quadroom"),
    @NamedQuery(name = "BedOccupation.findByIsclosedday", query = "SELECT r FROM BedOccupation r WHERE r.isclosedday = :isclosedday"),
    @NamedQuery(name = "BedOccupation.findByIsbreakfastservedtomorrow", query = "SELECT r FROM BedOccupation r WHERE r.isbreakfastservedtomorrow = :isbreakfastservedtomorrow"),
    @NamedQuery(name = "BedOccupation.findByComments", query = "SELECT r FROM BedOccupation r WHERE r.comments = :comments"),
    @NamedQuery(name = "BedOccupation.findBySpecificPeriod", query = "SELECT r FROM BedOccupation r WHERE r.dateofstay BETWEEN :start AND :end order by r.dateofstay"),
    @NamedQuery(name = "BedOccupation.findByYearMonth", query = "SELECT r FROM BedOccupation r WHERE r.dateofstay BETWEEN :start AND :end ORDER BY r.dateofstay")})

public class BedOccupation implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DATEOFSTAY")
    @Temporal(TemporalType.DATE)
    private Date dateofstay;
    @Basic(optional = false)
    @Column(name = "MDORM")
    private String mdorm;
    @Basic(optional = false)
    @Column(name = "FDORM")
    private String fdorm;
    @Basic(optional = false)
    @Column(name = "L1820")
    private String l1820;
    @Basic(optional = false)
    @Column(name = "L1917")
    private String l1917;
    @Basic(optional = false)
    @Column(name = "L1808")
    private String l1808;
    @Basic(optional = false)
    @Column(name = "L1908")
    private String l1908;
    @Basic(optional = false)
    @Column(name = "B1801")
    private String b1801;
    @Basic(optional = false)
    @Column(name = "B1811")
    private String b1811;
    @Basic(optional = false)
    @Column(name = "T1807")
    private String t1807;
    @Basic(optional = false)
    @Column(name = "T1819")
    private String t1819;
    @Basic(optional = false)
    @Column(name = "STAFFL")
    private String staffl;
    @Basic(optional = false)
    @Column(name = "STAFFR")
    private String staffr;
    @Basic(optional = false)
    @Column(name = "QUADROOM")
    private String quadroom;
    @Column(name = "ISCLOSEDDAY")
    private Boolean isclosedday;
    @Column(name = "ISBREAKFASTSERVEDTOMORROW")
    private Boolean isbreakfastservedtomorrow;
    @Column(name = "BREAKFASTRESULT")
    private Short breakfastresult;
    @Column(name = "COMMENTS")
    private String comments;

    public BedOccupation() {
    }

    public BedOccupation(Date dateofstay) {
        this.dateofstay = dateofstay;
    }

    public BedOccupation(Date dateofstay, String mdorm, String fdorm, String l1820, String l1917, String l1808, String l1908, String b1801, String b1811, String t1807, String t1819, String staffl, String staffr, String quadroom, Boolean isclosedday, Boolean isbreakfastservedtomorrow, Short breakfastresult, String comment) {
        this.dateofstay = dateofstay;
        this.mdorm = mdorm;
        this.fdorm = fdorm;
        this.l1820 = l1820;
        this.l1917 = l1917;
        this.l1808 = l1808;
        this.l1908 = l1908;
        this.b1801 = b1801;
        this.b1811 = b1811;
        this.t1807 = t1807;
        this.t1819 = t1819;
        this.staffl = staffl;
        this.staffr = staffr;
        this.quadroom = quadroom;
        this.isclosedday = isclosedday;
        this.isbreakfastservedtomorrow = isbreakfastservedtomorrow;
        this.breakfastresult = breakfastresult;
        this.comments = comment;
    }

    public List<String> getMDormList() {
        return CSVToList(mdorm);
    }
    public void setMDormList(List<String> mdormList) {
        this.mdorm = listToCSV(mdormList);
    }

    public List<String> getFDormList() {
        return CSVToList(fdorm);
    }
    public void setFDormList(List<String> fdormList) {
        this.fdorm = listToCSV(fdormList);
    }

    public List<String> getL1820List() {
        return CSVToList(l1820);
    }
    public void setL1820List(List<String> l1820List) {
        this.l1820 = listToCSV(l1820List);
    }

    public List<String> getL1917List() {
        return CSVToList(l1917);
    }
    public void setL1917List(List<String> l1917List) {
        this.l1917 = listToCSV(l1917List);
    }

    public List<String> getL1808List() {
        return CSVToList(l1808);
    }
    public void setL1808List(List<String> l1808List) {
        this.l1808 = listToCSV(l1808List);
    }

    public List<String> getL1908List() {
        return CSVToList(l1908);
    }
    public void setL1908List(List<String> l1908List) {
        this.l1908 = listToCSV(l1908List);
    }

    public List<String> getB1801List() {
        return CSVToList(b1801);
    }
    public void setB1801List(List<String> b1801List) {
        this.b1801 = listToCSV(b1801List);
    }

    public List<String> getB1811List() {
        return CSVToList(b1811);
    }
    public void setB1811List(List<String> b1811List) {
        this.b1811 = listToCSV(b1811List);
    }

    public List<String> getT1807List() {
        return CSVToList(t1807);
    }
    public void setT1807List(List<String> t1807List) {
        this.t1807 = listToCSV(t1807List);
    }

    public List<String> getT1819List() {
        return CSVToList(t1819);
    }
    public void setT1819List(List<String> t1819List) {
        this.t1819 = listToCSV(t1819List);
    }

    public List<String> getStaffLList() {
        return CSVToList(staffl);
    }
    public void setStaffLList(List<String> stafflList) {
        this.staffl = listToCSV(stafflList);
    }

    public List<String> getStaffRList() {
        return CSVToList(staffr);
    }
    public void setStaffRList(List<String> staffrList) {
        this.staffr = listToCSV(staffrList);
    }

    public List<String> getQuadroomList() {
        return CSVToList(quadroom);
    }
    public void setQuadroomList(List<String> quadroomList) {
        this.quadroom = listToCSV(quadroomList);
    }

    public List<String> getList(String roomNum){
        switch(roomNum){
            case "mdorm" :
                return this.getMDormList();
            case "fdorm" :
                return this.getFDormList();
            case "quad" :
                return this.getQuadroomList();
            case "1801" :
                return this.getB1801List();
            case "1811" :
                return this.getB1811List();
            case "1807" :
                return this.getT1807List();
            case "1819" :
                return this.getT1819List();
            case "1808" :
                return this.getL1808List();
            case "1820" :
                return this.getL1820List();
            case "1908" :
                return this.getL1908List();
            case "1917" :
                return this.getL1917List();
            case "staffl" :
                return this.getStaffLList();
            case "staffr" :
                return this.getStaffRList();
            default :
                throw new IllegalArgumentException("リストにない部屋番号を指定しています");
        }
    }
    public void setList(String roomNum, List<String> list){
        switch(roomNum){
            case "mdorm" :
                this.setMDormList(list);
                break;
            case "fdorm" :
                this.setFDormList(list);
                break;
            case "quad" :
                this.setQuadroomList(list);
                break;
            case "1801" :
                this.setB1801List(list);
                break;
            case "1811" :
                this.setB1811List(list);
                break;
            case "1807" :
                this.setT1807List(list);
                break;
            case "1819" :
                this.setT1819List(list);
                break;
            case "1808" :
                this.setL1808List(list);
                break;
            case "1820" :
                this.setL1820List(list);
                break;
            case "1908" :
                this.setL1908List(list);
                break;
            case "1917" :
                this.setL1917List(list);
                break;
            case "staffl" :
                this.setStaffLList(list);
                break;
            case "staffr" :
                this.setStaffRList(list);
                break;
            default :
                throw new IllegalArgumentException("リストにない部屋番号を指定しています");
        }
    }
    
    @Override
    public BedOccupation clone() throws CloneNotSupportedException{
        return (BedOccupation)super.clone();
    }
    
    private List<String> CSVToList(String str) {
        if(Objects.isNull(str) || str.isEmpty()) return new ArrayList<String>();
        //Arrays.asList()をListのインスタンス化に使うと、「配列をListのように見せる」ような動きになり、
        //要素の追加削除ができない「配列」に対してlist.add()などのメソッドを使うと
        //java.lang.UnsupportedOperationExceptionが発生するので、new ArrayList<>()してからaddAllする。
        List<String> list = new ArrayList<String>();
        list.addAll(Arrays.asList(str.split(",", -1)));
        return list;
    }

    private String listToCSV(List list) {
        if(list.isEmpty()) return new String();
        StringBuilder strb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            strb.append(list.get(i));
            if (i < list.size() - 1) {
                strb.append(",");
            }
        }
        return strb.toString();
    }
    
    public void sort(){
        List<String> list;

        list = this.getMDormList();
        list.sort(BedOccupation.comparatorAsc());
        this.setMDormList(list);
        list = this.getFDormList();
        list.sort(BedOccupation.comparatorAsc());
        this.setFDormList(list);
        list = this.getQuadroomList();
        list.sort(BedOccupation.comparatorAsc());
        this.setQuadroomList(list);
        list = this.getB1801List();
        list.sort(BedOccupation.comparatorAsc());
        this.setB1801List(list);
        list = this.getB1811List();
        list.sort(BedOccupation.comparatorAsc());
        this.setB1811List(list);
        list = this.getT1807List();
        list.sort(BedOccupation.comparatorAsc());
        this.setT1807List(list);
        list = this.getT1819List();
        list.sort(BedOccupation.comparatorAsc());
        this.setT1819List(list);
        list = this.getL1808List();
        list.sort(BedOccupation.comparatorAsc());
        this.setL1808List(list);
        list = this.getL1820List();
        list.sort(BedOccupation.comparatorAsc());
        this.setL1820List(list);
        list = this.getL1908List();
        list.sort(BedOccupation.comparatorAsc());
        this.setL1908List(list);
        list = this.getL1917List();
        list.sort(BedOccupation.comparatorAsc());
        this.setL1917List(list);
        list = this.getStaffLList();
        list.sort(BedOccupation.comparatorAsc());
        this.setStaffLList(list);
        list = this.getStaffRList();
        list.sort(BedOccupation.comparatorAsc());
        this.setStaffRList(list);
    }

    public static Comparator<String> comparatorAsc(){
        //USVMapの最初の値(予約番号)が、整数の昇順になっていないと、
        //BedOccupationControllerのdraw系メソッドでエラーになるので注意
        //(昇順でソートされた前提で処理を行っている)
        return (String aStr,String bStr) -> {
            if(aStr.equals("")) return 1;
            else if (bStr.equals("")) return -1;
            String[] a = aStr.split("_");
            String[] b = bStr.split("_");
            if (a[0].equals(b[0]) && a.length >= 2 && b.length >= 2){
                return Integer.decode(a[1])
                        .compareTo(Integer.decode(b[1]));
            }
            return Integer.decode(a[0])
                    .compareTo(Integer.decode(b[0]));
        };
    }
    
    public static Comparator<String> comparatorDesc(){
        return (String aStr,String bStr) -> {
            if(aStr.equals("")) return 1;
            else if (bStr.equals("")) return -1;
            String[] a = aStr.split("_");
            String[] b = bStr.split("_");
            if (a[0].equals(b[0]) && a.length >= 2 && b.length >= 2){
                return - Integer.decode(a[1])
                        .compareTo(Integer.decode(b[1]));
            }
            return - Integer.decode(a[0])
                    .compareTo(Integer.decode(b[0]));
        };
    }
}