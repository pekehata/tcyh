/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcyh;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Calendar;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 *
 * @author masanori
 */
@Entity
@Table(name = "RESERVATIONS")
@Access(AccessType.PROPERTY)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservations.findAll", query = "SELECT r FROM Reservations r"),
    @NamedQuery(name = "Reservations.findByReservationnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber = :reservationnumber order by r.reservationsPK.revisionnumber, r.reservationsPK.sequencenumber"),
    @NamedQuery(name = "Reservations.findByRevisionnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.revisionnumber = :revisionnumber order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findBySequencenumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.sequencenumber = :sequencenumber"),
    @NamedQuery(name = "Reservations.findByReferencenumber", query = "SELECT r FROM Reservations r WHERE r.referencenumber = :referencenumber"),
    @NamedQuery(name = "Reservations.findByReservationdate", query = "SELECT r FROM Reservations r WHERE r.reservationdate = :reservationdate"),
    @NamedQuery(name = "Reservations.findByDateofstay", query = "SELECT r FROM Reservations r WHERE r.dateofstay = :dateofstay AND r.reservationstatus IN ('BOOKED','CANCELED', 'NOSHOW') AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) ORDER BY r.reservationsPK.reservationnumber"),
    @NamedQuery(name = "Reservations.findByTotalnights", query = "SELECT r FROM Reservations r WHERE r.totalnights = :totalnights"),
    @NamedQuery(name = "Reservations.findByNightspassed", query = "SELECT r FROM Reservations r WHERE r.nightspassed = :nightspassed"),
    @NamedQuery(name = "Reservations.findByGuestname", query = "SELECT r FROM Reservations r WHERE r.guestname LIKE :guestname AND r.reservationsPK.sequencenumber = 1 AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) AND r.dateofstay BETWEEN :from AND :to AND r.reservationstatus IN :reservationstatus order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findByPhonenumber", query = "SELECT r FROM Reservations r WHERE r.phonenumber LIKE :phonenumber AND r.reservationsPK.sequencenumber = 1 AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) AND r.dateofstay BETWEEN  :from AND :to AND r.reservationstatus IN :reservationstatus order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findByMailaddress", query = "SELECT r FROM Reservations r WHERE r.mailaddress LIKE :mailaddress AND r.reservationsPK.sequencenumber = 1 AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) AND r.dateofstay BETWEEN  :from AND :to AND r.reservationstatus IN :reservationstatus order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findByCountry", query = "SELECT r FROM Reservations r WHERE r.country = :country"),
    @NamedQuery(name = "Reservations.findByPrefecture", query = "SELECT r FROM Reservations r WHERE r.prefecture = :prefecture"),
    @NamedQuery(name = "Reservations.findByPlannedcheckintime", query = "SELECT r FROM Reservations r WHERE r.plannedcheckintime = :plannedcheckintime"),
    @NamedQuery(name = "Reservations.findByComments", query = "SELECT r FROM Reservations r WHERE r.comments LIKE :comments AND r.reservationsPK.sequencenumber = 1 AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) AND r.dateofstay BETWEEN  :from AND :to AND r.reservationstatus IN :reservationstatus order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findByNumberofmale", query = "SELECT r FROM Reservations r WHERE r.numberofmale = :numberofmale"),
    @NamedQuery(name = "Reservations.findByNumberoffemale", query = "SELECT r FROM Reservations r WHERE r.numberoffemale = :numberoffemale"),
    @NamedQuery(name = "Reservations.findByNumberofbaby", query = "SELECT r FROM Reservations r WHERE r.numberofbaby = :numberofbaby"),
    @NamedQuery(name = "Reservations.findByMalebed", query = "SELECT r FROM Reservations r WHERE r.malebed = :malebed"),
    @NamedQuery(name = "Reservations.findByFemalebed", query = "SELECT r FROM Reservations r WHERE r.femalebed = :femalebed"),
    @NamedQuery(name = "Reservations.findByQuadroom", query = "SELECT r FROM Reservations r WHERE r.quadroom = :quadroom"),
    @NamedQuery(name = "Reservations.findByTatamiroom", query = "SELECT r FROM Reservations r WHERE r.tatamiroom = :tatamiroom"),
    @NamedQuery(name = "Reservations.findByBarrierfreeroom", query = "SELECT r FROM Reservations r WHERE r.barrierfreeroom = :barrierfreeroom"),
    @NamedQuery(name = "Reservations.findByStaffroom", query = "SELECT r FROM Reservations r WHERE r.staffroom = :staffroom"),
    @NamedQuery(name = "Reservations.findByStaffname", query = "SELECT r FROM Reservations r WHERE r.staffname = :staffname"),
    @NamedQuery(name = "Reservations.findByReservationsource", query = "SELECT r FROM Reservations r WHERE r.reservationsource LIKE :reservationsource AND r.reservationsPK.sequencenumber = 1 AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = r.reservationsPK.reservationnumber) AND r.dateofstay BETWEEN  :from AND :to AND r.reservationstatus IN :reservationstatus order by r.dateofstay"),
    @NamedQuery(name = "Reservations.findByReservationstatus", query = "SELECT r FROM Reservations r WHERE r.reservationstatus = :reservationstatus"),
    @NamedQuery(name = "Reservations.findByReservationnumberAndRevisionnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber = :reservationnumber AND r.reservationsPK.revisionnumber = :revisionnumber order by r.reservationsPK.sequencenumber asc"),
    @NamedQuery(name = "Reservations.findNewestByReservationnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber = :reservationnumber AND r.reservationsPK.revisionnumber = (SELECT MAX(a.reservationsPK.revisionnumber) FROM Reservations a WHERE a.reservationsPK.reservationnumber = :reservationnumber) ORDER BY r.dateofstay"),
    @NamedQuery(name = "Reservations.findNoShowByReservationnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber = :reservationnumber AND r.reservationstatus = 'NOSHOW' ORDER BY r.dateofstay"),
    @NamedQuery(name = "Reservations.findByMultipleReservationnumber", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber IN :reservationnumber AND r.dateofstay = :dateofstay order by r.reservationsPK.reservationnumber asc, r.reservationsPK.revisionnumber desc"),
    @NamedQuery(name = "Reservations.findByReservationnumberAndNightspassed", query = "SELECT r FROM Reservations r WHERE r.reservationsPK.reservationnumber = :reservationnumber AND r.nightspassed = :nightspassed order by r.reservationsPK.revisionnumber desc"),
    @NamedQuery(name = "Reservations.findAllByMonth", query = "SELECT r FROM Reservations r WHERE ( r.dateofstay BETWEEN :start AND :end ) AND r.reservationstatus IN ('BOOKED', 'NOSHOW') ORDER BY r.dateofstay"),
    @NamedQuery(name = "Reservations.findBySpecificPeriod", query = "SELECT r FROM Reservations r WHERE r.dateofstay BETWEEN :start AND :end AND r.reservationstatus IN ('BOOKED', 'NOSHOW') ORDER BY r.reservationsPK.reservationnumber"),
    @NamedQuery(name = "Reservations.findGreatestreservationnumber", query = "SELECT MAX(r.reservationsPK.reservationnumber) FROM Reservations r")
})

@EqualsAndHashCode
@ToString
public class Reservations implements Serializable,Cloneable {
    private final static long serialVersionUID = 1L;
    private ReservationsPK reservationsPK = new ReservationsPK();
    private final ObjectProperty<Short> maleresult = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> femaleresult = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> paidbreakfast = new SimpleObjectProperty<Short>();
    private final ObjectProperty<String> roomnumresult = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> checkintime = new SimpleObjectProperty<String>();
    private final ObjectProperty<Short> lunch = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> dinner = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> breakfast = new SimpleObjectProperty<Short>();
    private final BooleanProperty large1820 = new SimpleBooleanProperty();
    private final BooleanProperty large1917 = new SimpleBooleanProperty();
    private final BooleanProperty large1808 = new SimpleBooleanProperty();
    private final BooleanProperty large1908 = new SimpleBooleanProperty();
    private final ObjectProperty<Date> reservationdate = new SimpleObjectProperty<Date>();
    private final ObjectProperty<Short> nightspassed = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> malebed = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> femalebed = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> quadroom = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> tatamiroom = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> barrierfreeroom = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> staffroom = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Integer> referencenumber = new SimpleObjectProperty<Integer>();
    private final ObjectProperty<Date> dateofstay = new SimpleObjectProperty<Date>();
    private final ObjectProperty<Short> totalnights = new SimpleObjectProperty<Short>();
    private final ObjectProperty<String> guestname = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> phonenumber = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> mailaddress = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> country = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> prefecture = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> plannedcheckintime = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> comments = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> commentbackgroundcolor = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> commentforegroundcolor = new SimpleObjectProperty<String>();
    private final ObjectProperty<Short> numberofmale = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> numberoffemale = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> numberofbaby = new SimpleObjectProperty<Short>();
    private final ObjectProperty<String> staffname = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> reservationsource = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> reservationstatus = new SimpleObjectProperty<String>();
    private final ObjectProperty<Timestamp> updatetimestamp = new SimpleObjectProperty<Timestamp>();
    private final ObjectProperty<String> cashier = new SimpleObjectProperty<String>();
    private final ObjectProperty<String> changecomment = new SimpleObjectProperty<String>();
    private final BooleanProperty checked = new SimpleBooleanProperty(false);
    private final ObjectProperty<Short> totalnum = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Short> resultnum = new SimpleObjectProperty<Short>();
    private final ObjectProperty<Date> checkindate = new SimpleObjectProperty<Date>();
    private final ObjectProperty<Date> checkoutdate = new SimpleObjectProperty<Date>();

    public Reservations() {
    }

    public Reservations(ReservationsPK reservationsPK) {
        this.reservationsPK = reservationsPK;
    }
        
    @Override
    public Reservations clone() throws CloneNotSupportedException{
        Reservations rsv = (Reservations)super.clone();
        rsv.setReservationsPK(rsv.reservationsPK.clone());
        return rsv;
    }

    public Reservations cloneEntity( EntityManager em ) {
        CopyGroup group = new CopyGroup();
        group.setShouldResetPrimaryKey( true );
        Reservations copy = (Reservations)em.unwrap( JpaEntityManager.class ).copy( this, group );
        copy.setChecked(this.getChecked());
        return copy;
    }
        
    @EmbeddedId
    public ReservationsPK getReservationsPK() {
        return reservationsPK;
    }
    public void setReservationsPK(ReservationsPK reservationsPK) {
        this.reservationsPK = reservationsPK;
    }

    @Column(name = "REFERENCENUMBER")
    public Integer getReferencenumber() {
        return this.referencenumber.getValue();
    }
    public void setReferencenumber(Integer referencenumber) {
        this.referencenumber.set(referencenumber);
    }

    @Basic(optional = false)
    @Column(name = "DATEOFSTAY")
    @Temporal(TemporalType.DATE)
    public Date getDateofstay() {
        return this.dateofstay.getValue();
    }
    public void setDateofstay(Date dateofstay) {
        this.dateofstay.set(dateofstay);
    }
    public ObjectProperty<Date> dateofstayProperty() {
        return this.dateofstay;
    }

    @Basic(optional = false)
    @Column(name = "TOTALNIGHTS")
    public Short getTotalnights() {
        return this.totalnights.getValue();
    }
    public void setTotalnights(Short totalnights) {
        this.totalnights.set(totalnights);
    }
    public ObjectProperty<Short> totalnightsProperty() {
        return this.totalnights;
    }

    @Basic(optional = false)
    @Column(name = "GUESTNAME")
    public String getGuestname() {
        return this.guestname.getValue();
    }
    public void setGuestname(String guestname) {
        this.guestname.set(guestname);
    }
    public ObjectProperty<String> guestnameProperty() {
        return this.guestname;
    }
    
    @Column(name = "PHONENUMBER")
    public String getPhonenumber() {
        return this.phonenumber.getValue();
    }
    public void setPhonenumber(String phonenumber) {
        this.phonenumber.set(phonenumber);
    }
    public ObjectProperty<String> phonenumberProperty() {
        return this.phonenumber;
    }

    @Column(name = "MAILADDRESS")
    public String getMailaddress() {
        return this.mailaddress.getValue();
    }
    public void setMailaddress(String mailaddress) {
        this.mailaddress.set(mailaddress);
    }
    public ObjectProperty<String> mailaddressProperty() {
        return this.mailaddress;
    }

    @Column(name = "COUNTRY")
    public String getCountry() {
        return this.country.getValue();
    }
    public void setCountry(String country) {
        this.country.set(country);
    }
    public ObjectProperty<String> countryProperty() {
        return this.country;
    }

    @Column(name = "PREFECTURE")
    public String getPrefecture() {
        return this.prefecture.getValue();
    }
    public void setPrefecture(String prefecture) {
        this.prefecture.set(prefecture);
    }
    public ObjectProperty<String> prefectureProperty() {
        return this.prefecture;
    }

    @Column(name = "PLANNEDCHECKINTIME")
    public String getPlannedcheckintime() {
        return this.plannedcheckintime.getValue();
    }
    public void setPlannedcheckintime(String plannedcheckintime) {
        this.plannedcheckintime.set(plannedcheckintime);
    }
    public ObjectProperty<String> plannedcheckintimeProperty() {
        return this.plannedcheckintime;
    }

    @Column(name = "COMMENTS")
    public String getComments() {
        return this.comments.getValue();
    }
    public void setComments(String comments) {
        this.comments.set(comments);
    }
    public ObjectProperty<String> commentsProperty() {
        return this.comments;
    }

    @Column(name = "COMMENTBACKGROUNDCOLOR")
    public String getCommentbackgroundcolor() {
        return this.commentbackgroundcolor.getValue();
    }
    public void setCommentbackgroundcolor(String commentbackgroundcolor) {
        this.commentbackgroundcolor.set(commentbackgroundcolor);
    }
    public ObjectProperty<String> commentbackgroundcolorProperty() {
        return this.commentbackgroundcolor;
    }

    @Column(name = "COMMENTFOREGROUNDCOLOR")
    public String getCommentforegroundcolor() {
        return this.commentforegroundcolor.getValue();
    }
    public void setCommentforegroundcolor(String commentforegroundcolor) {
        this.commentforegroundcolor.set(commentforegroundcolor);
    }
    public ObjectProperty<String> commentforegroundcolorProperty() {
        return this.commentforegroundcolor;
    }

    @Basic(optional = false)
    @Column(name = "NUMBEROFMALE")
    public Short getNumberofmale() {
        return this.numberofmale.getValue();
    }
    public void setNumberofmale(Short numberofmale) {
        this.numberofmale.set(numberofmale);
        this.totalnum.set(this.getTotalnum());
    }

    @Basic(optional = false)
    @Column(name = "NUMBEROFFEMALE")
    public Short getNumberoffemale() {
        return this.numberoffemale.getValue();
    }
    public void setNumberoffemale(Short numberoffemale) {
        this.numberoffemale.set(numberoffemale);
        this.totalnum.set(this.getTotalnum());
    }

    @Basic(optional = false)
    @Column(name = "NUMBEROFBABY")
    public Short getNumberofbaby() {
        return this.numberofbaby.getValue();
    }
    public void setNumberofbaby(Short numberofbaby) {
        this.numberofbaby.set(numberofbaby);
    }
    public ObjectProperty<Short> numberofbabyProperty() {
        return this.numberofbaby;
    }


    @Column(name = "STAFFNAME")
    public String getStaffname() {
        return this.staffname.getValue();
    }
    public void setStaffname(String staffname) {
        this.staffname.set(staffname);
    }
    public ObjectProperty<String> staffnameProperty() {
        return this.staffname;
    }

    @Column(name = "RESERVATIONSOURCE")
    public String getReservationsource() {
        return this.reservationsource.getValue();
    }
    public void setReservationsource(String reservationsource) {
        this.reservationsource.set(reservationsource);
    }
    public ObjectProperty<String> reservationsourceProperty() {
        return this.reservationsource;
    }

    @Basic(optional = false)
    @Column(name = "RESERVATIONSTATUS")
    public String getReservationstatus() {
        return this.reservationstatus.getValue();
    }
    public void setReservationstatus(String reservationstatus) {
        this.reservationstatus.set(reservationstatus);
    }
    public ObjectProperty<String> reservationstatusProperty() {
        return this.reservationstatus;
    }

    @Basic(optional = false)
    @Column(name = "NIGHTSPASSED")
    public Short getNightspassed() {
        return this.nightspassed.getValue();
    }
    public void setNightspassed(Short nightspassed) {
        this.nightspassed.set(nightspassed);
    }
    public ObjectProperty<Short> nightspassedProperty() {
        return this.nightspassed;
    }

    @Basic(optional = false)
    @Column(name = "MALEBED")
    public Short getMalebed() {
        return this.malebed.getValue();
    }
    public void setMalebed(Short malebed) {
        this.malebed.set(malebed);
    }
    public ObjectProperty<Short> malebedProperty() {
        return this.malebed;
    }

    @Basic(optional = false)
    @Column(name = "FEMALEBED")
    public Short getFemalebed() {
        return this.femalebed.getValue();
    }
    public void setFemalebed(Short femalebed) {
        this.femalebed.set(femalebed);
    }
    public ObjectProperty<Short> femalebedProperty() {
        return this.femalebed;
    }

    @Basic(optional = false)
    @Column(name = "QuadRoom")
    public Short getQuadroom() {
        return this.quadroom.getValue();
    }
    public void setQuadroom(Short quadroom) {
        this.quadroom.set(quadroom);
    }
    public ObjectProperty<Short> quadroomProperty() {
        return this.quadroom;
    }

    @Basic(optional = false)
    @Column(name = "TATAMIROOM")
    public Short getTatamiroom() {
        return this.tatamiroom.getValue();
    }
    public void setTatamiroom(Short tatamiroom) {
        this.tatamiroom.set(tatamiroom);
    }
    public ObjectProperty<Short> tatamiroomProperty() {
        return this.tatamiroom;
    }

    @Basic(optional = false)
    @Column(name = "BARRIERFREEROOM")
    public Short getBarrierfreeroom() {
        return this.barrierfreeroom.getValue();
    }
    public void setBarrierfreeroom(Short barrierfreeroom) {
        this.barrierfreeroom.set(barrierfreeroom);
    }
    public ObjectProperty<Short> barrierfreeroomProperty() {
        return this.barrierfreeroom;
    }

    @Basic(optional = false)
    @Column(name = "STAFFROOM")
    public Short getStaffroom() {
        return this.staffroom.getValue();
    }
    public void setStaffroom(Short staffroom) {
        this.staffroom.set(staffroom);
    }
    public ObjectProperty<Short> staffroomProperty() {
        return this.staffroom;
    }

    @Column(name = "RESERVATIONDATE")
    @Temporal(TemporalType.DATE)
    public Date getReservationdate() {
        return this.reservationdate.getValue();
    }
    public void setReservationdate(Date reservationdate) {
        this.reservationdate.set(reservationdate);
    }
    public ObjectProperty<Date> reservationdateProperty() {
        return this.reservationdate;
    }

    @Basic(optional = false)
    @Column(name = "LARGE1820")
    public Boolean getLarge1820() {
        return large1820.getValue();
    }
    public void setLarge1820(Boolean large1820) {
        this.large1820.set(large1820);
    }
    public BooleanProperty large1820Property() {
        return this.large1820;
    }

    @Basic(optional = false)
    @Column(name = "LARGE1917")
    public Boolean getLarge1917() {
        return large1917.getValue();
    }
    public void setLarge1917(Boolean large1917) {
        this.large1917.set(large1917);
    }
    public BooleanProperty large1917Property() {
        return this.large1917;
    }

    @Basic(optional = false)
    @Column(name = "LARGE1808")
    public Boolean getLarge1808() {
        return large1808.getValue();
    }
    public void setLarge1808(Boolean large1808) {
        this.large1808.set(large1808);
    }
    public BooleanProperty large1808Property() {
        return this.large1808;
    }

    @Basic(optional = false)
    @Column(name = "LARGE1908")
    public Boolean getLarge1908() {
        return large1908.getValue();
    }
    public void setLarge1908(Boolean large1908) {
        this.large1908.set(large1908);
    }
    public BooleanProperty large1908Property() {
        return this.large1908;
    }

    @Basic(optional = false)
    @Column(name = "LUNCH")
    public Short getLunch() {
        return this.lunch.getValue();
    }
    public void setLunch(Short lunch) {
        this.lunch.set(lunch);
    }
    public ObjectProperty<Short> lunchProperty() {
        return this.lunch;
    }

    @Basic(optional = false)
    @Column(name = "DINNER")
    public Short getDinner() {
        return this.dinner.getValue();
    }
    public void setDinner(Short dinner) {
        this.dinner.set(dinner);
    }
    public ObjectProperty<Short> dinnerProperty() {
        return this.dinner;
    }

    @Basic(optional = false)
    @Column(name = "BREAKFAST")
    public Short getBreakfast() {
        return this.breakfast.getValue();
    }
    public void setBreakfast(Short breakfast) {
        this.breakfast.set(breakfast);
    }
    public ObjectProperty<Short> breakfastProperty() {
        return this.breakfast;
    }

    @Basic(optional = false)
    @Column(name = "UPDATETIMESTAMP")
    public Timestamp getUpdatetimestamp() {
        return this.updatetimestamp.getValue();
    }
    public void setUpdatetimestamp(Timestamp updatetimestamp) {
        this.updatetimestamp.set(updatetimestamp);
    }
    public ObjectProperty<Timestamp> updatetimestampProperty() {
        return this.updatetimestamp;
    }

    @Column(name = "CHANGECOMMENT")
    public String getChangecomment() {
        return this.changecomment.getValue();
    }
    public void setChangecomment(String changecomment) {
        this.changecomment.set(changecomment);
    }
    public ObjectProperty<String> changecommentProperty() {
        return this.changecomment;
    }
    
    @Column(name = "MALERESULT")
    public Short getMaleresult() {
        return this.maleresult.getValue();
    }
    public void setMaleresult(Short maleresult) {
        this.maleresult.set(maleresult);
        this.resultnum.set(this.getResultnum());
    }

    @Column(name = "FEMALERESULT")
    public Short getFemaleresult() {
        return this.femaleresult.getValue();
    }
    public void setFemaleresult(Short femaleresult) {
        this.femaleresult.set(femaleresult);
        this.resultnum.set(this.getResultnum());
    }

    @Column(name = "PAIDBREAKFAST")
    public Short getPaidbreakfast() {
        return this.paidbreakfast.getValue();
    }
    public void setPaidbreakfast(Short paidbreakfast) {
        this.paidbreakfast.set(paidbreakfast);
    }
    public ObjectProperty<Short> paidbreakfastProperty() {
        return this.paidbreakfast;
    }

    @Column(name = "ROOMNUMRESULT")
    public String getRoomnumresult() {
        return this.roomnumresult.getValue();
    }
    public void setRoomnumresult(String roomnumresult) {
        this.roomnumresult.set(roomnumresult);
    }
    public ObjectProperty<String> roomnumresultProperty() {
        return this.roomnumresult;
    }

    @Column(name = "CHECKINTIME")
    public String getCheckintime() {
        return this.checkintime.getValue();
    }
    public void setCheckintime(String checkintime) {
        this.checkintime.set(checkintime);
    }
    public ObjectProperty<String> checkintimeProperty() {
        return this.checkintime;
    }
    
    @Column(name = "CASHIER")
    public String getCashier() {
        return this.cashier.getValue();
    }
    public void setCashier(String cashier) {
        this.cashier.set(cashier);
    }
    public ObjectProperty<String> cashierProperty() {
        return this.cashier;
    }

    @Transient
    public Integer getReservationnumber() {
        return this.reservationsPK.getReservationnumber();
    }
    public void setReservationnumber(Integer reservationnumber) {
        this.reservationsPK.setReservationnumber(reservationnumber);
    }
    
    @Transient
    public Short getRevisionnumber() {
        return this.reservationsPK.getRevisionnumber();
    }
    public void setRevisionnumber(Short revisionnumber) {
        this.reservationsPK.setRevisionnumber(revisionnumber);
    }
    
    @Transient
    public Short getSequencenumber() {
        return this.reservationsPK.getSequencenumber();
    }
    public void setSequencenumber(Short sequencenumber) {
        this.reservationsPK.setSequencenumber(sequencenumber);
    }

    @Transient
    public Short getTotalnum() {
        Short numberOfMale = Objects.isNull(getNumberofmale()) ? 0 : getNumberofmale();
        Short numberOfFemale = Objects.isNull(getNumberoffemale()) ? 0 : getNumberoffemale();
        return (short)(numberOfMale + numberOfFemale);
    }
    public void setTotalnum(Short totalnum) {
        this.totalnum.set(totalnum);
    }
    public ObjectProperty<Short> totalnumProperty() {
        return this.totalnum;
    }

    @Transient
    public Short getResultnum() {
        Short num1 = Objects.isNull(getMaleresult()) ? 0 : getMaleresult();
        Short num2 = Objects.isNull(getFemaleresult()) ? 0 : getFemaleresult();
        return (short)(num1 + num2);
    }
    public void setResultnum(Short resultnum) {
        this.resultnum.set(resultnum);
    }
    public ObjectProperty<Short> resultnumProperty() {
        return this.resultnum;
    }    

    @Transient
    public Date getCheckInDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.dateofstay.getValue());
        cal.add(Calendar.DATE, - getNightspassed() + 1);
        return cal.getTime();
    }
    public void setCheckInDate(Date checkindate) {
        this.checkindate.set(checkindate);
    }
    public ObjectProperty<Date> checkindateProperty() {
        return this.checkindate;
    }

    @Transient
    public Date getCheckOutDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.dateofstay.getValue());
        cal.add(Calendar.DATE, getTotalnights() - getNightspassed() + 1);
        return cal.getTime();
    }
    public void setCheckOutDate(Date checkoutdate) {
        this.checkoutdate.set(checkoutdate);
    }
    public ObjectProperty<Date> checkoutdateProperty() {
        return this.checkoutdate;
    }

    @Transient
    public Boolean getChecked() {
        return this.checked.getValue();
    }
    public void setChecked(Boolean checked) {
        this.checked.set(checked);
    }
    public BooleanProperty checkedProperty() {
        return this.checked;
    }
}